package com.dojob.adapter;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.dojob.backend.RegisterApi;
import com.dojob.backend.SetupApi;
import com.dojob.backend.model.ProfileResponse;
import com.dojob.core.App;
import com.dojob.core.Logger;
import com.dojob.fragment.FeedFragment;
import com.dojob.fragment.InboxFragment;
import com.dojob.fragment.PortfolioFragment;
import com.dojob.fragment.ProfileSettingFragment;
import com.dojob.fragment.SettingFragment;
import com.dojob.fragment.SocialFeedFragment;

import java.util.ArrayList;
import java.util.List;

public class MainAdapter extends FragmentStatePagerAdapter {

    public final static int FEED_PAGE = 0;
    public final static int CHAT_PAGE = 1;
    public final static int SOCIAL_PAGE = 2;
    public final static int EDIT_PROFILE_PAGE = 3;
    public final static int SETTINGS_PAGE = 4;
    private List<Fragment> mItems;
    private Context context;
    public MainAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.context = context;
        mItems = new ArrayList<>();
        initFragment();
    }

    public void updateData(){
        mItems.clear();
        initFragment();
    }

    private void initFragment() {
        mItems.add(FeedFragment.newInstance(new Bundle()));
        Log.d("MainAdapter", "initFragment:"+RegisterApi.isLogin(context));
        if(RegisterApi.isLogin(context)){
            mItems.add(InboxFragment.newInstance());
            mItems.add(SocialFeedFragment.newInstance());
//            mItems.add(ProfileSettingFragment.newInstance(new Bundle()));
//            if(SetupApi.getUserData(context) != null){
                Bundle bundle = new Bundle();
                bundle.putBoolean(PortfolioFragment.NO_TOOLBAR, true);
                bundle.putInt(PortfolioFragment.ARGS_PORTFOLIO_TYPE, PortfolioAdapter.DEFAULT_TYPE);
//                bundle.putString(PortfolioFragment.ARGS_PORTFOLIO_DATA, App.gson().toJson(SetupApi.getUserData(context)));
                mItems.add(PortfolioFragment.newInstance(bundle));
//            }
//            else{
//                mItems.add(ProfileSettingFragment.newInstance(new Bundle()));
//            }
        }else{
            mItems.add(SocialFeedFragment.newInstance());
        }
        mItems.add(SettingFragment.newInstance());
    }

    @Override
    public Fragment getItem(int position) {
        if (mItems.size() == 0) {
            return null;
        }
        if (position > mItems.size()) {
            return null;
        }
        if (mItems.get(position) instanceof FeedFragment){
            ((FeedFragment) mItems.get(position)).reload();
        }
        return mItems.get(position);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {}

    @Override
    public int getCount() {
        return mItems.size();
    }

}
