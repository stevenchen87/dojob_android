package com.dojob.adapter.model;

import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.ProgressBar;

import com.dojob.R;

public class LoadingFooter {
    public static final int LAYOUT_ID = R.layout.layout_loading_footer;

    public static final String TYPE = "type_footer";
    public static final int TYPE_LOADING = 0;
    public static final int TYPE_MORE = 1;
    private int mType;

    public LoadingFooter(int type) {
        mType = type;
    }

    public void bindViewHolder(ViewHolder holder, int position){

        if(holder.viewMore != null){
            holder.viewMore.setText("View More");
            holder.viewMore.setTag(position);
            holder.viewMore.setVisibility((mType == TYPE_MORE)?View.VISIBLE:View.GONE);
        }

        if(holder.container != null){
            holder.container.setVisibility((mType == TYPE_LOADING)?View.VISIBLE:View.GONE);
        }

    }

    public static class ViewHolder{
        public ProgressBar container;
        public AppCompatButton viewMore;
        public ViewHolder(View view){
            container = view.findViewById(R.id.item_progress_bar);
            viewMore = view.findViewById(R.id.item_view_more);
        }
    }
}
