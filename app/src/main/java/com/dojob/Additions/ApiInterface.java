package com.dojob.Additions;

import com.dojob.Additions.local.AnnoucncementResponse;
import com.dojob.Additions.local.ForgotPassResult;
import com.dojob.Additions.posts.RatingPost;
import com.dojob.Additions.responses.WebResponseComment;
import com.dojob.Additions.responses.WebResponseOk;
import com.dojob.Additions.responses.WebResponseSocialFeed;
import com.dojob.Additions.responses.WebResponseTutorial;
import com.dojob.backend.model.ChatStatuResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {


    @GET("getSearchPage")
    Call<categoryModel> getcategories();

    @GET("getSearchPage")
    Call<RecommendedTalentSuperClass> getRecommendedTalent();

    @POST("resetPassword")
    Call<ForgotPassResult> forgotPassword(@Query("email") String email);

    @POST("changePassword")
    Call<ChangePasswordResponse> changePassword(@Header("Authorization") String Token, @Body ChangePassword changePassword);


    //    message/chatroom/get
    @GET("talent/message/chatroom/get")
    Call<ChatStatuResponse> getChatRoomStatu(@Header("Authorization") String Token,@Query("chatroom_id") String chatID);

    @POST("talent/review/save")
    Call<WebResponseOk> submitRating(@Header("Authorization") String Token, @Body RatingPost ratingPost);

   // https://<mount_point>api/profile/album/file/get
    @GET("profile/album/file/get")
    Call<WebResponseComment> getCommentData(@Header("Authorization") String Token, @Query("fileID") String fileId);

    @POST("doneTutorial")
    Call<WebResponseTutorial> doneTutorial(@Header("Authorization") String Token);

    @GET("socialFeed/get")
    Call<WebResponseSocialFeed> getSocialFeedData(@Query("filterType") String type, @Query("page") int pageNo);

    @GET("announcement/get")
    Call<AnnoucncementResponse> getAnnounceMents(@Header("Authorization") String Token,@Query("biggest_announcement_id") String type);

}
