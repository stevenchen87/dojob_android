package com.dojob.backend;

import android.content.Context;

import com.dojob.backend.model.SearchResponse;
import com.dojob.core.App;
import com.dojob.core.Http;
import com.dojob.core.Result;
import com.dojob.util.Utils;
import com.google.gson.JsonSyntaxException;
import com.dojob.core.Error;

import java.util.HashMap;
import java.util.Map;

public class SearchApi {
    private final static String TAG = "search_api_tag";
    private final Context mContext;
    private int page;
    private String url = "http://step4work.com/api/search?";

    public SearchApi(Context context) {
        mContext = context;
    }

    public Result request(String query) {
        SearchResponse response = null;
        if (!Utils.isOnline(mContext)) {
            return new Result(Error.OFFLINE);
        }
        Map<String, String> params = new HashMap<>();
        Http.Request request = Http.request(mContext)
                .url(url + "searchType=all&searchKey="+query/*+"&categoryId=0"*/)
                .tag(TAG);

        Result result = request.send();
        if (result.isSuccess()) {
            String responseText = (String) result.getResult();

            try {
                response = App.gson().fromJson(responseText, SearchResponse.class);
            } catch (JsonSyntaxException ex) {
                ex.printStackTrace();
            }
            if (response != null) {
                return new Result(null, response);
            }
            return new Result(Error.DATA);
        }

        return new Result(Error.NETWORK);
    }
    public Result SearchRequest(int id) {
        SearchResponse response = null;
        if (!Utils.isOnline(mContext)) {
            return new Result(Error.OFFLINE);
        }
        Map<String, String> params = new HashMap<>();
        Http.Request request = Http.request(mContext)

                .url(url + "searchType=category&categoryId="+id)
                .tag(TAG);

        Result result = request.send();
        if (result.isSuccess()) {
            String responseText = (String) result.getResult();

            try {
                response = App.gson().fromJson(responseText, SearchResponse.class);
            } catch (JsonSyntaxException ex) {
                ex.printStackTrace();
            }
            if (response != null) {
                return new Result(null, response);
            }
            return new Result(Error.DATA);
        }

        return new Result(Error.NETWORK);
    }
}
