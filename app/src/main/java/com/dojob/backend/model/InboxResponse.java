package com.dojob.backend.model;

public class InboxResponse {
    public int target_id;
    public String name;
    public boolean new_message;
    public String last_message;
    public long last_seen;
    public String avatar;
    public int chatroom_id;
    public String user_url;
    public InboxMessageResponse[] messages;
    public int chatIsRead;
}
