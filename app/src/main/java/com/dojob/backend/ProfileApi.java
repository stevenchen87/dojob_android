package com.dojob.backend;

import android.content.Context;
import android.content.SharedPreferences;

import com.dojob.backend.model.GalleryAlbumsItemResponse;
import com.dojob.backend.model.ProfileResponse;
import com.dojob.backend.model.Response;
import com.dojob.backend.model.ReviewResponse;
import com.dojob.backend.model.UserResponse;
import com.dojob.core.App;
import com.dojob.core.Error;
import com.dojob.core.Http;
import com.dojob.core.Logger;
import com.dojob.core.Result;
import com.dojob.util.Utils;
import com.google.gson.JsonSyntaxException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class ProfileApi {
    public final static String TAG = "profile_api_tag";
    public final static String RATE_TAG = "rate_tag";
    public final static String LIKE_TAG = "like_tag";
    public final static String REVIEW_TAG = "review_tag";

    private final Context mContext;
    public final static String GET_PROFILE_URL = "http://www.step4work.com/api/profile/get?";
    public final static String GET_RECRUITER_PROFILE_URL = "http://www.step4work.com/api/recruiter/profile/get?";
    public final static String GET_RECOMMENDED_URL = "http://www.step4work.com/api/getSearchPage?";
    public final static String GET_RATE_URL = "http://www.step4work.com/api/talent/review/save?";
    public final static String REQUST_LIKE_URL = "http://www.step4work.com/api/talent/like?";

    public ProfileApi(Context context) {
        mContext = context;
    }

    public static String getUserProfile(String userId){
        return GET_PROFILE_URL + "uid="+userId;
    }

    public Result request(){
        return SetupApi.isUserRecruiter(mContext)?request(GET_RECRUITER_PROFILE_URL):request(GET_PROFILE_URL);
    }

    public Result requestRecommended(){
        return request(GET_RECOMMENDED_URL);
    }

    public Result request(String url) {
        if(url == null) return null;
        UserResponse response = null;
        if (!Utils.isOnline(mContext)) {
            return new Result(Error.OFFLINE);
        }
        Http.Request request = Http.request(mContext)
                .url(url)
                .tag(TAG);

        Result result = request.send();
        if (result.isSuccess()) {
            String responseText = (String) result.getResult();
            try {
                response = App.gson().fromJson(responseText, UserResponse.class);
                if(LoginApi.isUser(mContext, response.profile.id)){
                    SetupApi.saveUserData(mContext, response.profile);
                }
            } catch (JsonSyntaxException ex) {
                ex.printStackTrace();
            }
            if (response != null) {
                return new Result(null, response);
            }
            return new Result(Error.DATA);
        }

        return new Result(Error.NETWORK);
    }

    public Result rate(String userId, String msg, String rate){
        Response response = null;
        if (!Utils.isOnline(mContext)) {
            return new Result(Error.OFFLINE);
        }
        Map<String, String> params = new HashMap<>();
        params.put("target_id", userId);
        params.put("review", msg);
        params.put("rating", rate);
        Http.Request request = Http.request(mContext)
                .url(GET_RATE_URL)
                .post(params)
                .tag(RATE_TAG);
        Result result = request.send();
        if (result.isSuccess()) {
            String responseText = (String) result.getResult();
            try {
                response = App.gson().fromJson(responseText, Response.class);
            } catch (JsonSyntaxException ex) {
                ex.printStackTrace();
            }
            if (response != null) {
                return new Result(null, response);
            }
            return new Result(Error.DATA);
        }

        return new Result(Error.NETWORK);
    }

    public Result like(String fileId, String type){
        Response response = null;
        if (!Utils.isOnline(mContext)) {
            return new Result(Error.OFFLINE);
        }
        Map<String, String> params = new HashMap<>();
        params.put("link_id", fileId);
        params.put("link_type", type);
        Http.Request request = Http.request(mContext)
                .url(REQUST_LIKE_URL)
                .post(params)
                .tag(LIKE_TAG);
        Result result = request.send();
        if (result.isSuccess()) {
            String responseText = (String) result.getResult();
            try {
                response = App.gson().fromJson(responseText, Response.class);
            } catch (JsonSyntaxException ex) {
                ex.printStackTrace();
            }
            if (response != null) {
                return new Result(null, response);
            }
            return new Result(Error.DATA);
        }
        return new Result(Error.NETWORK);
    }

    public Result getReviews(int targetId, String url){
        String tempUrl = (url == null)?"http://step4work.com/api/talent/review/get?page=1"/*"https://www.dropbox.com/s/27kd9l94vt00cm4/reviews_response.txt?dl=1"*/:url;
        tempUrl = tempUrl + "&target_id="+targetId;
        ReviewResponse response = null;
        if (!Utils.isOnline(mContext)) {
            return new Result(Error.OFFLINE);
        }
        Http.Request request = Http.request(mContext)
                .url(tempUrl)
                .tag(REVIEW_TAG);
        Result result = request.send();
        if (result.isSuccess()) {
            String responseText = (String) result.getResult();
            try {
                response = App.gson().fromJson(responseText, ReviewResponse.class);
            } catch (JsonSyntaxException ex) {
                ex.printStackTrace();
            }
            if (response != null) {
                return new Result(null, response);
            }
            return new Result(Error.DATA);
        }

        return new Result(Error.NETWORK);
    }


    public static String getRole(Context context){
        SharedPreferences sharedPref = context.getSharedPreferences("dojob",Context.MODE_PRIVATE);
        return sharedPref.getString("USER_ROLE", "talent");
    }
    public static void setRole(Context context, String albums){
        SharedPreferences sharedPref = context.getSharedPreferences("dojob",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("USER_ROLE", albums);
        editor.apply();
    }


}
