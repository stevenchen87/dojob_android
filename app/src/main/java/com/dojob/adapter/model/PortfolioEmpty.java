package com.dojob.adapter.model;

import android.view.Gravity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dojob.R;

public class PortfolioEmpty {
    public static final int LAYOUT_ID = R.layout.layout_empty;
    private final String mContent;
    private boolean mIsWrapContent;

    public PortfolioEmpty(String content) {
        mContent = content;
    }

    public PortfolioEmpty(String content, boolean isWrapContent) {
        mContent = content;
        mIsWrapContent =  isWrapContent;
    }

    public void bindViewHolder(ViewHolder holder, int position){
        if(holder.description != null){
            if(mIsWrapContent){
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)holder.description.getLayoutParams();
                params.width = RelativeLayout.LayoutParams.WRAP_CONTENT;
                holder.description.setLayoutParams(params);
                holder.description.setGravity(Gravity.START);
            }
            holder.description.setText(mContent);
        }
    }

    public static class ViewHolder{
        public TextView description;

        public ViewHolder(View view){
            description = view.findViewById(R.id.portfolio_empty_content);
        }
    }
}
