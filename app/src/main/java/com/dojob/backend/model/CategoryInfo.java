package com.dojob.backend.model;

import com.dojob.Additions.Cat;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryInfo {

    @SerializedName("sub_cat")
    @Expose
    private SubCat subCat;
    @SerializedName("cat")
    @Expose
    private Categ cat;

    public SubCat getSubCat() {
        return subCat;
    }

    public void setSubCat(SubCat subCat) {
        this.subCat = subCat;
    }

    public Categ getCat() {
        return cat;
    }

    public void setCat(Categ cat) {
        this.cat = cat;
    }

}