package com.dojob.backend.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Categ {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("category_type")
    @Expose
    private String categoryType;
    @SerializedName("category_image")
    @Expose
    private String categoryImage;
    @SerializedName("translation_key")
    @Expose
    private String translationKey;
    @SerializedName("popularity")
    @Expose
    private String popularity;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(String categoryType) {
        this.categoryType = categoryType;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(String categoryImage) {
        this.categoryImage = categoryImage;
    }

    public String getTranslationKey() {
        return translationKey;
    }

    public void setTranslationKey(String translationKey) {
        this.translationKey = translationKey;
    }

    public String getPopularity() {
        return popularity;
    }

    public void setPopularity(String popularity) {
        this.popularity = popularity;
    }

}