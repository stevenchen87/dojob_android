package com.dojob.Additions;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dojob.R;
import com.dojob.adapter.PortfolioAdapter;
import com.dojob.adapter.model.PortfolioOverflowScroll;
import com.dojob.fragment.PortfolioFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.MyViewHolder> {


    private Context context;

    private List<RecommendedTalent> recommendedTalentList;

    private String MAIN_FRAGMENT_TAG = "main_fragment";

    private String PREVIEW_PROFILE_FRAGMENT_TAG = "profile_preview_fragment";

    private FragmentActivity activity;

    private PortfolioOverflowScroll.Listener mListener;


    public ItemsAdapter(Context context, List<RecommendedTalent> recommendedTalentList, FragmentActivity activity) {
        this.context = context;
        this.recommendedTalentList = recommendedTalentList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_feed_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        RecommendedTalent recommendedTalent = recommendedTalentList.get(position);

        /*App.picasso().setLoggingEnabled(true);

        App.picasso().load(recommendedTalent.getProfileImage()).into(holder.feedImageView);*/
        if (recommendedTalent.getName() != null)
            holder.feedName.setText(recommendedTalent.getName());

        if (recommendedTalent.getProfileImage() != null) {
            Uri uri = Uri.parse(recommendedTalent.getProfileImage());


            Log.i("URI", recommendedTalent.getProfileImage());
           /* Glide.with(context)

                    .load(uri)


                    .into(holder.feedImage);*/
            Glide.with(context)
                    .load(uri)
                    .apply(RequestOptions.fitCenterTransform())
                    .into(holder.feedImage);
            /*App.picasso().load(uri).fit().centerCrop().into(holder.feedImage);*/
        }

        if (holder.tvcountryCode != null) {
            if (recommendedTalent.countrycode != null) {
                holder.tvcountryCode.setText(recommendedTalent.countrycode);
                holder.tvcountryCode.setVisibility(View.VISIBLE);
            }else
                holder.tvcountryCode.setVisibility(View.GONE);
            holder.tvcountryCode.setTextSize(context.getResources().getDimension(R.dimen._5sdp));
        }

        holder.linearLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.green));
        if (recommendedTalent.getCategoryInfo() != null)
            if (recommendedTalent.getCategoryInfo().getSubCat() != null)
                if (recommendedTalent.getCategoryInfo().getSubCat().getName() != null)
                    holder.feedPosition.setText(recommendedTalent.getCategoryInfo().getSubCat().getName());

    }

    @Override
    public int getItemCount() {
        return recommendedTalentList.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.clickable)
        LinearLayout itemLinearLayout;

        @BindView(R.id.feed_image)
        ImageView feedImage;

        @BindView(R.id.feed_name)
        TextView feedName;

        @BindView(R.id.feed_position)
        TextView feedPosition;

        @BindView(R.id.card_view)
        CardView cardView;

        @BindView(R.id.content)
        LinearLayout linearLayout;

        @BindView(R.id.tvcountryCode)
        TextView tvcountryCode;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(340, 400);
            cardView.setLayoutParams(layoutParams);
            feedName.setTextColor(context.getResources().getColor(R.color.white));
            feedPosition.setTextColor(context.getResources().getColor(R.color.white));
            cardView.setOnClickListener(this);
            cardView.setPreventCornerOverlap(false);
            if (Build.VERSION.SDK_INT >= 21) {
                // Call some material design APIs here
                cardView.setElevation(0);
            }

        }

        @Override
        public void onClick(View v) {

            int position = getAdapterPosition();
            RecommendedTalent recommendedTalent = recommendedTalentList.get(position);
            Log.i("TALENTURL", recommendedTalent.getUrl());
            Bundle bundle = new Bundle();
            bundle.putInt(PortfolioFragment.ARGS_PORTFOLIO_TYPE, PortfolioAdapter.DEFAULT_TYPE);
            bundle.putString(PortfolioFragment.ARGS_PORTFOLIO_URL, recommendedTalent.getUrl().replace("https", "http"));
            PortfolioFragment portfolioFragment = PortfolioFragment.newInstance(bundle);
            activity.getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content, portfolioFragment, PREVIEW_PROFILE_FRAGMENT_TAG)
                    .addToBackStack(MAIN_FRAGMENT_TAG)
                    .commit();


        }
    }

}
