package com.dojob.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.dojob.adapter.model.AlbumsCover;
import com.dojob.adapter.model.AlbumsItem;
import com.dojob.adapter.model.AlbumsTitle;
import com.dojob.adapter.model.PhotosAddBar;
import com.dojob.adapter.model.PhotosItem;
import com.dojob.adapter.model.PortfolioEmpty;
import com.dojob.adapter.model.VideosItem;

public class GalleryViewHolder extends RecyclerView.ViewHolder{
    public AlbumsCover.ViewHolder albumsCover;
    public AlbumsItem.ViewHolder albumsItem;
    public AlbumsTitle.ViewHolder albumsTitle;
    public PhotosAddBar.ViewHolder photosAddBar;
    public PhotosItem.ViewHolder photosItem;
    public VideosItem.ViewHolder videosItem;
    public PortfolioEmpty.ViewHolder empty;

    public GalleryViewHolder(View itemView) {
        super(itemView);
        albumsCover = new AlbumsCover.ViewHolder(itemView);
        albumsItem = new AlbumsItem.ViewHolder(itemView);
        albumsTitle = new AlbumsTitle.ViewHolder(itemView);
        photosAddBar = new PhotosAddBar.ViewHolder(itemView);
        photosItem = new PhotosItem.ViewHolder(itemView);
        videosItem = new VideosItem.ViewHolder(itemView);
        empty = new PortfolioEmpty.ViewHolder(itemView);
    }
}
