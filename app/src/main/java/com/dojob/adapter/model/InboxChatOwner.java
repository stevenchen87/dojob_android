package com.dojob.adapter.model;

import android.text.format.DateUtils;
import android.view.View;
import android.widget.TextView;

import com.dojob.R;
import com.dojob.backend.model.InboxMessageResponse;

import java.text.SimpleDateFormat;

public class InboxChatOwner {
    public static final int LAYOUT_ID = R.layout.layout_inbox_chat_own_item;
    private final InboxMessageResponse mResponse;

    public InboxChatOwner(InboxMessageResponse response) {
        mResponse = response;
    }

    public void bindViewHolder(ViewHolder holder, int position){
        if(holder.content != null){
            holder.content.setText(mResponse.msg_body);
        }
        if(holder.date != null){
            String value = new SimpleDateFormat("hh:mm a").format(new java.util.Date(mResponse.created_at * 1000));
            if(!DateUtils.isToday(mResponse.created_at * 1000)){
                value = new SimpleDateFormat("dd/MM").format(new java.util.Date(mResponse.created_at * 1000));
            }
            holder.date.setText(value);
        }

        if(holder.badge != null){
            holder.badge.setVisibility((mResponse.status.equals("0"))?View.VISIBLE:View.GONE);
        }
    }

    public static class ViewHolder{
        public TextView date;
        public TextView content;
        public View badge;
        public TextView time;

        public ViewHolder(View view){
            date = view.findViewById(R.id.inbox_chat_own_time);
            content = view.findViewById(R.id.inbox_chat_own_content);
            badge = view.findViewById(R.id.inbox_chat_own_badge);
        }
    }
}
