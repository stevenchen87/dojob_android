package com.dojob.Additions;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dojob.R;
import com.dojob.activity.CountryActivity;
import com.dojob.adapter.CountrysAdapter;
import com.dojob.databinding.FragmentCountryBinding;

import java.util.ArrayList;

public class CountryFragment extends Fragment {

    public static String COUNTRY_KEY = "country";
    public static String COUNTRY_NAME = "countryname";
    private ArrayList<CountryData> countrylist;
    FragmentCountryBinding binding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_country, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            countrylist = (ArrayList<CountryData>) getArguments().getSerializable(COUNTRY_KEY);
            LinearLayoutManager verticalLayoutManager = new LinearLayoutManager(getContext());
            binding.countryRecycleview.setLayoutManager(verticalLayoutManager);
            binding.countryRecycleview.setItemAnimator(new DefaultItemAnimator());
            CountrysAdapter mCountrysAdapter = new CountrysAdapter(getContext(), countrylist);
            binding.countryRecycleview.setAdapter(mCountrysAdapter);
            binding.countryRecycleview.addOnItemTouchListener(
                    new RecyclerItemClickListener(getContext(), binding.countryRecycleview, new RecyclerItemClickListener.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {

                            onListItemClick(position);
                        }

                        @Override
                        public void onLongItemClick(View view, int position) {
                            // do whatever
                        }
                    }));
        }
    }

    private void onListItemClick(int position) {
        Bundle bundle = new Bundle();
        bundle.putString(COUNTRY_NAME, countrylist.get(position).name);
        if (getActivity() != null && getActivity() instanceof CountryActivity)
            ((CountryActivity) getActivity()).onCountryClick(bundle);
    }
}
