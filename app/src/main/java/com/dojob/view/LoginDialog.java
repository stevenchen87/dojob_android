package com.dojob.view;

import android.content.Intent;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.dojob.R;
import com.dojob.activity.IntroActivity;
import com.dojob.activity.MainActivity;
import com.dojob.backend.LoginApi;
import com.dojob.backend.ProfileApi;
import com.dojob.backend.RegisterApi;
import com.dojob.backend.model.LoginResponse;
import com.dojob.core.Authentication;
import com.dojob.core.BackgroundWorker;
import com.dojob.core.Logger;
import com.dojob.core.Result;
import com.dojob.util.AppSP;
import com.dojob.util.ResourcesUtil;
import com.dojob.util.Utils;
import com.google.firebase.auth.FirebaseUser;

public class LoginDialog extends AppCompatDialogFragment implements View.OnClickListener,
        Authentication.Listener,
        BackgroundWorker.Callbacks,
        RadioGroup.OnCheckedChangeListener {
    private final String TAG = "LoginDialog";
    private View mRegisterButton;
    private TextView mLoginButton, tvforgotPass;
    private View mClose;
    private Listener mListener;
    private BackgroundWorker mWorker;
    private LoginApi mLoginApi;
    private AppCompatButton mGoogleButton;
    private AppCompatButton mFacebookButton;
    private RadioGroup mToggle;
    private String mRole;
    private String idToken;
    private AlertDialog progressDialog;

    public static LoginDialog newInstance() {
        Bundle args = new Bundle();

        LoginDialog fragment = new LoginDialog();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            fragment.setStyle(AppCompatDialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Material_Light_Dialog_NoActionBar);
        } else {
            fragment.setStyle(AppCompatDialogFragment.STYLE_NO_TITLE, android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar);
        }
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null) {
            mWorker = new BackgroundWorker(getActivity());
            mLoginApi = (LoginApi) mWorker.get(LoginApi.LOGIN_TAG);
            if (mLoginApi == null) {
                mLoginApi = new LoginApi(getContext());
                mWorker.put(LoginApi.LOGIN_TAG, mLoginApi);
            }
            if (getArguments() != null)
                mRole = getArguments().getString("role", "talent");
            else
                mRole = "talent";

            if (getContext() != null) ProfileApi.setRole(getContext(), mRole);
        }
        Authentication.init(getActivity(), this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getActivity() != null) {
            mWorker = new BackgroundWorker(getActivity());
            mLoginApi = (LoginApi) mWorker.get(LoginApi.LOGIN_TAG);
            if (mLoginApi == null) {
                mLoginApi = new LoginApi(getContext());
                mWorker.put(LoginApi.LOGIN_TAG, mLoginApi);
            }
            if (getArguments() != null) mRole = getArguments().getString("role", "talent");
        }

        View view = inflater.inflate(R.layout.dialog_login, container);
        Window window = getDialog().getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }

        mToggle = view.findViewById(R.id.toggle);
        mFacebookButton = view.findViewById(R.id.login_facebook_button);
        Drawable fbBackgroundDrawable = DrawableCompat.wrap(mFacebookButton.getBackground()).mutate();
        DrawableCompat.setTint(fbBackgroundDrawable, ResourcesUtil.getColor(R.color.fb_blue));
        mGoogleButton = view.findViewById(R.id.login_google_button);
        Drawable googleBackgroundDrawable = DrawableCompat.wrap(mGoogleButton.getBackground()).mutate();
        DrawableCompat.setTint(googleBackgroundDrawable, ResourcesUtil.getColor(R.color.google_red));

        mLoginButton = (TextView) view.findViewById(R.id.login_email_button);
        tvforgotPass = (TextView) view.findViewById(R.id.tvforgotPass);
        mRegisterButton = view.findViewById(R.id.signup_email_button);
        mClose = view.findViewById(R.id.close);

        mLoginButton.setPaintFlags(mLoginButton.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tvforgotPass.setPaintFlags(tvforgotPass.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Authentication.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mToggle != null) {
            mToggle.setOnCheckedChangeListener(this);
        }
        if (mLoginButton != null) {
            mLoginButton.setOnClickListener(this);
        }
        if (mRegisterButton != null) {
            mRegisterButton.setOnClickListener(this);
        }
        if (mFacebookButton != null) {
            mFacebookButton.setOnClickListener(this);
        }
        if (mGoogleButton != null) {
            mGoogleButton.setOnClickListener(this);
        }
        if (mClose != null) {
            mClose.setOnClickListener(this);
        }
        if (tvforgotPass != null) {
            tvforgotPass.setOnClickListener(this);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mToggle != null) {
            mToggle.setOnCheckedChangeListener(null);
        }

        if (mLoginButton != null) {
            mLoginButton.setOnClickListener(null);
        }
        if (mRegisterButton != null) {
            mRegisterButton.setOnClickListener(null);
        }
        if (mFacebookButton != null) {
            mFacebookButton.setOnClickListener(null);
        }
        if (mGoogleButton != null) {
            mGoogleButton.setOnClickListener(null);
        }
        if (mClose != null) {
            mClose.setOnClickListener(null);
        }
        if (tvforgotPass != null) {
            tvforgotPass.setOnClickListener(null);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        /* dismiss();*/
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_email_button:
                // dismiss();
                dismiss();
                if (mListener != null) {
                    mListener.onOpenEmailLoginDialog(mRole);
                }
                break;
            case R.id.signup_email_button:
                dismiss();
                if (mListener != null) {
                    mListener.onOpenEmailSignUpDialog(mRole);
                }
                break;
            case R.id.close:
                dismiss();
                break;
            case R.id.login_google_button:
                if (getActivity() != null && !getActivity().isFinishing()) {
                    Authentication.google(getActivity());
                }

                break;
            case R.id.login_facebook_button:
                if (getActivity() != null && !getActivity().isFinishing()) {
                    Authentication.facebook(getActivity());
                }

                break;
            case R.id.tvforgotPass:
                dismiss();
                if (mListener != null) {
                    mListener.onOpenEmailForgotPassDialog(mRole);
                }

                break;
        }
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }

    ///Authentication.Listener

    @Override
    public void onComplete(FirebaseUser user, String accessToken) {
        Log.d(TAG, "\nname:" + user.getDisplayName() + "\nemail:" + user.getEmail() + "\nphote:" + user.getPhotoUrl());
        Log.d(TAG, "accessToken:" + accessToken);
        this.idToken = accessToken;
        if (!accessToken.isEmpty()) {
            showLoadingProgress();
            Log.i(TAG, "accessToken:" + "NOT_EMPTY");
            if (mWorker != null) mWorker.executeNewTask(LoginApi.LOGIN_TAG, null, this);
        } else {
            Log.i(TAG, "accessToken:" + "EMPTY");
        }
    }

    @Override
    public void onError(Exception e) {
        dismissLoadingProgress();
        e.printStackTrace();
    }

    private class LongOperation extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mLoginApi.request(idToken, mRole, Utils.getDeviceToken(getActivity()));
            return null;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    @Override
    public Result executeTaskInBackground(String id, Bundle args) {
        Log.i("REQUEST_LOG_IN", "CALLED");
        return mLoginApi.request(idToken, mRole, Utils.getDeviceToken(getActivity()));
    }

    @Override
    public void onBackgroundTaskCompleted(String id, Result result) {
        switch (id) {
            case LoginApi.LOGIN_TAG:
                Log.i("EmailLogin", "result,");
                if (Utils.isFromLogin != 1) {
                    if (result.isSuccess()) {
                        LoginResponse response = (LoginResponse) result.getResult();
                        Log.d("EmailLogin.status", "" + response.status + "," + response.status.equals("ok"));
                        Log.d("EmailLogin.token", "" + response.token);
                        Log.d("EmailLogin.mListener", "" + mListener);

                        if (response != null && response.status.equals("ok")) {
                            Utils.isRefreshRequired = 1;
                            if (response.token != null && response.token.length() > 0 && response.data.verified == 1) {
                                RegisterApi.saveToken(this.getActivity(), response.token);
                                AppSP sp = AppSP.getInstance(this.getActivity());
                                sp.savePreferences(Utils.USER_ROLE,response.data.position);
                                sp.savePreferences(Utils.ANNOUNCEMENT_BIGGEST_ID,response.data.announeMentID);
                            }
                            if(getActivity() != null) ((MainActivity) this.getActivity()).switchSignUpButton(true);
                            if (mListener != null) ((MainActivity) this.getActivity()).refresh();

                            if (response.data.setup_profile != 0) {
                                if (!response.data.isTutorialShown.equalsIgnoreCase("1")) {
                                    Intent intent1 = new Intent(getActivity(), IntroActivity.class);
                                    getActivity().startActivity(intent1);
                                }
                            }
                            dismiss();
                        }
                    }
                }
                dismissLoadingProgress();
                break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (mToggle.getCheckedRadioButtonId()) {
            case R.id.talent_radio:
                mRole = "talent";
                break;
            case R.id.recruiter_radio:
                mRole = "recruiter";
                break;
            default:
                mRole = "talent";
                break;
        }
        if (getContext() != null) ProfileApi.setRole(getContext(), mRole);
    }

    public interface Listener {
        void onOpenEmailLoginDialog(String role);

        void onOpenEmailSignUpDialog(String role);

        void onOpenEmailForgotPassDialog(String role);
    }

    private void showLoadingProgress() {
        if (getActivity() != null) {
            AlertDialog.Builder progressDialogBuilder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.layout_loading, null);
            progressDialogBuilder.setView(dialogView);
            progressDialog = progressDialogBuilder.create();
            progressDialog.show();
        }
    }

    private void dismissLoadingProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }
}

