package com.dojob.adapter;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.dojob.backend.GalleryApi;
import com.dojob.backend.model.FileResponse;
import com.dojob.backend.model.MediaResponse;
import com.dojob.backend.model.UserResponse;
import com.dojob.fragment.AlbumsFragment;
import com.dojob.fragment.GalleryTabFragment;

import java.util.ArrayList;
import java.util.List;

public class GalleryAdapter extends FragmentStatePagerAdapter {

    public final static String GALLERY_ALBUM_DATA = "gallery_album_data";

    private final int PHOTOS_TYPE = 0;
    private final int ALBUMS_TYPE = 1;
    private final int VIDEOS_TYPE = 2;
    private List<Fragment> mItems;


    public GalleryAdapter(FragmentManager fm, UserResponse response) {
        super(fm);
        mItems = new ArrayList<>();
        Bundle bundle = new Bundle();
        bundle.putInt("user_id", response.profile.id);
        ArrayList<String> list = new ArrayList<>();
        for(MediaResponse photoResponse: response.talentPhoto ){
            list.add(photoResponse.url);
        }
        Bundle photo = new Bundle();
        photo.putAll(bundle);
        photo.putInt(GalleryTabFragment.TYPE_GALLERY, GalleryApi.TYPE_GALLERY_PHOTO);
        mItems.add(GalleryTabFragment.newInstance(photo));
        Bundle album = new Bundle();
        album.putAll(bundle);
        album.putInt(AlbumsFragment.ALBUM_TYPE, AlbumsFragment.TYPE_ALL);
        mItems.add(AlbumsFragment.newInstance(album));
        if(response.talentVideo == null){
            response.talentVideo = new FileResponse[0];
        }
        Bundle video = new Bundle();
        video.putAll(bundle);
        video.putInt(GalleryTabFragment.TYPE_GALLERY, GalleryApi.TYPE_GALLERY_VIDEO);
        mItems.add(GalleryTabFragment.newInstance(video));
    }

    @Override
    public Fragment getItem(int position) {
        if (mItems.size() == 0) {
            return null;
        }
        if (position > mItems.size()) {
            return null;
        }
        return mItems.get(position);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {}

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case PHOTOS_TYPE:
                return "PHOTOS";
            case ALBUMS_TYPE:
                return "ALBUMS";
            case VIDEOS_TYPE:
                return "VIDEOS";
        }
        return super.getPageTitle(position);
    }
}
