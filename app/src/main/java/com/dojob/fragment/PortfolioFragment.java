package com.dojob.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.dojob.Additions.ProfileCategoryAdapter;
import com.dojob.R;
import com.dojob.activity.MainActivity;
import com.dojob.adapter.PortfolioAdapter;
import com.dojob.adapter.layout.PortfolioSpanSizeLookup;
import com.dojob.backend.FavouriteApi;
import com.dojob.backend.GalleryApi;
import com.dojob.backend.LoginApi;
import com.dojob.backend.ProfileApi;
import com.dojob.backend.RegisterApi;
import com.dojob.backend.model.Comment;
import com.dojob.backend.model.Response;
import com.dojob.backend.model.Review;
import com.dojob.backend.model.UserResponse;
import com.dojob.core.App;
import com.dojob.core.BackgroundWorker;
import com.dojob.core.Logger;
import com.dojob.core.Result;
import com.dojob.util.ResourcesUtil;
import com.dojob.util.Utils;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;

public class PortfolioFragment extends Fragment implements
        BackgroundWorker.Callbacks,
        PortfolioAdapter.Listener {
    public final static String ARGS_PORTFOLIO_TYPE = "args_portfolio_type";
    public final static String ARGS_PORTFOLIO_URL = "args_portfolio_url";
    public final static String ARGS_PORTFOLIO_DATA = "args_portfolio_data";
    public final static String NO_TOOLBAR = "args_no_back_toolbar";
    public final static String PROFILE_COMMENT_FILED_LIKED = "comment_filed_liked";

    private final String OBJ_GALLERY_API = "obj_gallery_api";
    private final String OBJ_PROFILE_API = "obj_profile_api";
    private final String TASK_REQUEST_USER_PROFILE = "task_request_user_profile";
    private final String TASK_REQUEST_PROFILE = "task_request_profile";
    private final String TASK_REQUEST_RECOMMENDED = "task_request_recommended";
    private final String TASK_REVIEW = "task_review";
    private final String TASK_LIKE = "task_like";

    private BackgroundWorker mWorker;
    private GalleryApi mGalleryApi;
    private ProfileApi mProfileApi;
    private PortfolioAdapter mAdapter;
    private SpinKitView mProgress;
    private RecyclerView mRecyclerView;
    private int mType;
    private String url;
    private UserResponse mResponse;
    private int mPhotoPosition;
    private boolean mNoToolbar;
    // for favourite api
    private FavouriteApi mFavouriteApi;
    private final String OBJ_FAVOURITE_API = "obj_favourite_api";
    private final String TASK_REQUEST_USER_FAVOURITE = "task_request_user_favourite";
    private final String TASK_REQUEST_USER_REMOVE_FAVOURITE = "task_request_user_remove_favourite";
    private int favouriteId;
    public static int NeedNewData = 0;

    public static PortfolioFragment newInstance(Bundle args) {
        PortfolioFragment fragment = new PortfolioFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        if (getArguments() != null) {
            if (getArguments().containsKey(NO_TOOLBAR)) {
                mNoToolbar = getArguments().getBoolean(NO_TOOLBAR);
            }
            if (getArguments().containsKey(ARGS_PORTFOLIO_TYPE)) {
                mType = getArguments().getInt(ARGS_PORTFOLIO_TYPE);
            }
            if (getArguments().containsKey(ARGS_PORTFOLIO_URL)) {
                url = getArguments().getString(ARGS_PORTFOLIO_URL);
            }
            if (getArguments().containsKey(ARGS_PORTFOLIO_DATA)) {
                mResponse = App.gson().fromJson(getArguments().getString(ARGS_PORTFOLIO_DATA), UserResponse.class);
            }
        }

        if (activity != null) {
            if (activity instanceof MainActivity && !mNoToolbar) {
                ActionBar actionbar = activity.getSupportActionBar();
                if (actionbar != null) {
                    actionbar.setDisplayHomeAsUpEnabled(true);
                    actionbar.setDisplayShowHomeEnabled(true);
                }
                ((MainActivity) activity).isSettingPage(false);
                if (mType == PortfolioAdapter.PREVIEW_TYPE) {
                    // will be used in fragment with back button
                    ((MainActivity) activity).setTitle("Profile Preview");
                } else {
                    ((MainActivity) activity).setTitle("Profile");
                }
                ((MainActivity) activity).cameraButtonHide();
            }
            mWorker = new BackgroundWorker(getActivity());
            mGalleryApi = (GalleryApi) mWorker.get(OBJ_GALLERY_API);
            if (mGalleryApi == null) {
                mGalleryApi = new GalleryApi(getContext());
                mWorker.put(OBJ_GALLERY_API, mGalleryApi);
            }
            mProfileApi = (ProfileApi) mWorker.get(OBJ_PROFILE_API);
            if (mProfileApi == null) {
                Log.i("PROFILE_API", "NULL");
                mProfileApi = new ProfileApi(getContext());
                mWorker.put(OBJ_PROFILE_API, mProfileApi);
            } else {
                Log.i("PROFILE_API", "NOT_NULL");
            }
            mFavouriteApi = (FavouriteApi) mWorker.get(OBJ_FAVOURITE_API);
            if (mFavouriteApi == null) {
                mFavouriteApi = new FavouriteApi(getContext());
                mWorker.put(OBJ_FAVOURITE_API, mFavouriteApi);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        if (activity != null) {
            if (activity instanceof MainActivity && !mNoToolbar) {
                ActionBar actionbar = activity.getSupportActionBar();
                if (mType == PortfolioAdapter.PREVIEW_TYPE) {
                    // will be used in fragment with back button
                    if (actionbar != null) {
                        actionbar.setDisplayHomeAsUpEnabled(true);
                        actionbar.setDisplayShowHomeEnabled(true);
                    }
                    ((MainActivity) activity).setTitle("Profile Preview");
                    // ((MainActivity) activity).setJobDoneClose();
                } else {
                    ((MainActivity) activity).setTitle("Profile");
                    // ((MainActivity) activity).setJobDoneClose();
                }
//                ((MainActivity) getActivity()).setBasicToolbar();
                if (getActivity() != null && getActivity() instanceof MainActivity){
                    ((MainActivity)getActivity()).cameraButtonHide();
                }
            }
        }

        if (mResponse != null && NeedNewData != 1) {
            updateUI();
        } else
            if (url == null) {
            if (mWorker != null) mWorker.executeNewTask(TASK_REQUEST_USER_PROFILE, null, this);
        } else {
            Log.i("TASK", "REQUEST_PROFILE");
            if (mWorker != null) {
                Log.i("TASK", "WORKER_NOT_NULL");
                mWorker.executeNewTask(TASK_REQUEST_PROFILE, null, this);
            } else {
                Log.i("TASK", "WORKER_NULL");
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        NeedNewData = 0;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_portfolio, container, false);
        Utils.hideKeyBoard(getActivity());
        mProgress = view.findViewById(R.id.progress);
        mProgress.setVisibility(View.VISIBLE);
        mRecyclerView = view.findViewById(R.id.recycle_view);
        mRecyclerView.setItemViewCacheSize(20);
        mRecyclerView.setDrawingCacheEnabled(true);
        mRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        mRecyclerView.setVisibility(View.GONE);
        mAdapter = new PortfolioAdapter(getContext(), mType, getActivity());
        mAdapter.setListener(getActivity(), this);
        mRecyclerView.setAdapter(mAdapter);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
        gridLayoutManager.setSpanSizeLookup(new PortfolioSpanSizeLookup(mAdapter));
        mRecyclerView.setLayoutManager(gridLayoutManager);
//        if (mResponse != null) {
//            updateUI();
//        } else if (url == null) {
//            if (mWorker != null) mWorker.executeNewTask(TASK_REQUEST_USER_PROFILE, null, this);
//        } else {
//            Log.i("TASK", "REQUEST_PROFILE");
//            if (mWorker != null) {
//                Log.i("TASK", "WORKER_NOT_NULL");
//                mWorker.executeNewTask(TASK_REQUEST_PROFILE, null, this);
//            } else {
//                Log.i("TASK", "WORKER_NULL");
//            }
//        }
        if (getContext() != null)
            LocalBroadcastManager.getInstance(getContext()).registerReceiver(updateLikeComment, new IntentFilter(PROFILE_COMMENT_FILED_LIKED));

        return view;
    }

    @Override
    public Result executeTaskInBackground(String id, Bundle args) {
        Log.i("METHOD", "OKAY");
        switch (id) {
            case TASK_REQUEST_USER_PROFILE:
                return mProfileApi.request();
            case TASK_REQUEST_PROFILE:
                Log.i("REQUEST_PROFILE", "OKAY");
                return mProfileApi.request(url);
            case TASK_REQUEST_RECOMMENDED:
                return mProfileApi.requestRecommended();
            case TASK_REVIEW:
                return mProfileApi.rate(String.valueOf(mResponse.profile.id), args.getString("rating_msg"), args.getString("rate"));
            case TASK_LIKE:
                return mProfileApi.like(args.getString("file_id"), args.getString("file_type"));
            case TASK_REQUEST_USER_FAVOURITE:
                return mFavouriteApi.requestDoFavourite(favouriteId);
            case TASK_REQUEST_USER_REMOVE_FAVOURITE:
                return mFavouriteApi.requestRemoveFavourite(getJsonArray(favouriteId));
        }
        return null;
    }

    private String getJsonArray(int favouriteId) {

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("1", String.valueOf(favouriteId));

        Gson gson = new Gson();
        Type type = new TypeToken<HashMap<String, String>>() {
        }.getType();
        return gson.toJson(hashMap, type);
    }

    @Override
    public void onBackgroundTaskCompleted(String id, Result result) {
        switch (id) {
            case TASK_REQUEST_USER_PROFILE:
            case TASK_REQUEST_PROFILE:
                if (result.isSuccess() && mAdapter != null) {
                    mResponse = (UserResponse) result.getResult();
                    if (mResponse != null) {
                        updateUI();
                        if (getActivity() != null && LoginApi.isUser(getContext(), mResponse.profile.id) && mResponse.profile.setup_profile == 0) {
                            ((MainActivity) getActivity()).onOpenProfileSetting();
                        }
                    }
                }
                break;
            case TASK_REQUEST_RECOMMENDED:
                if (mResponse != null && result.isSuccess()) {
                    UserResponse response = (UserResponse) result.getResult();
                    mResponse.recommended = response.recommended;
                    updateUI();
                }
                break;
            case TASK_REVIEW:
            case TASK_LIKE:
                if (result.isSuccess()) {
                    Response response = (Response) result.getResult();
                    if (getActivity() != null) {
                        if (response.status.equals("ok")) {
                            if (mWorker != null) {
                                String temp = url == null ? TASK_REQUEST_USER_PROFILE : TASK_REQUEST_PROFILE;
                                mWorker.executeNewTask(temp, null, this);
                            }
                        }
                        if (response.msg != null) {
                            Utils.showSnackbar(getActivity(), response.msg);
                        }
                    }
                }
                break;
            case TASK_REQUEST_USER_FAVOURITE:
                if (result.isSuccess()) {
                    Response response = (Response) result.getResult();
                    if (getActivity() != null) {
                        if (response.status.equals("ok")) {
                            if (response.msg != null)
                                Utils.showSnackbar(getActivity(), response.msg);
                        } else {
                            if (response.msg != null)
                                Utils.showSnackbar(getActivity(), response.msg);
                        }
                    }
                }
                break;
            case TASK_REQUEST_USER_REMOVE_FAVOURITE:
                if (result.isSuccess()) {
                    Response response = (Response) result.getResult();
                    if (getActivity() != null) {
                        if (response.status.equals("ok")) {
                            if (response.msg != null)
                                Utils.showSnackbar(getActivity(), response.msg);
                        } else {
                            if (response.msg != null)
                                Utils.showSnackbar(getActivity(), response.msg);
                        }
                    }
                }
                break;
        }
    }

    private void updateUI() {
        mAdapter.updateItem(mResponse, mPhotoPosition);
        mRecyclerView.setVisibility(View.VISIBLE);
        mProgress.setVisibility(View.GONE);
    }

    @Override
    public void onOpenAllGallery(int page, UserResponse response) {
        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        if (activity != null) {
            if (activity instanceof MainActivity) {
                ((MainActivity) activity).openGalleryPage(page, response);
            }
        }
    }

    @Override
    public void onEnqueryClick(UserResponse response) {
        if (RegisterApi.isLogin(getActivity())) {
            AppCompatActivity activity = ((AppCompatActivity) getActivity());
            if (activity != null) {
                if (activity instanceof MainActivity) {
                    ((MainActivity) activity).openChatRoom(response.profile.id, response.profile.name, null);
                }
            }
        } else {
            onLoginDialogOpen();
        }
    }

    @Override
    public void onEditProfileClick() {
        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        if (activity != null) {
            if (activity instanceof MainActivity) {
                ((MainActivity) activity).onOpenProfileSetting();
            }
        }
    }

    @Override
    public void onRateClick() {
        showRatingDialog();
    }

    @Override
    public void onBookmarkedClick(boolean result, int id) {
        Log.d("PortfolioFragment", "clicked");
        if (RegisterApi.isLogin(getActivity())) {
            favouriteId = id;
            if (result) {
                if (mWorker != null)
                    mWorker.executeNewTask(TASK_REQUEST_USER_FAVOURITE, null, this);
            } else {
                if (mWorker != null)
                    mWorker.executeNewTask(TASK_REQUEST_USER_REMOVE_FAVOURITE, null, this);
            }
        } else {
            onLoginDialogOpen();
        }
    }

    @Override
    public void onListBulletClick() {
        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        if (activity != null) {
            if (activity instanceof MainActivity) {
                ((MainActivity) activity).onOpenFavouriteFragment();
            }
        }
    }

    @Override
    public void onLoginDialogOpen() {
        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        if (activity != null) {
            if (activity instanceof MainActivity) {
                ((MainActivity) activity).openLoginDialog();
            }
        }
    }

    @Override
    public void onOpenCommentPage(UserResponse response, String type, int position) {
        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        if (activity != null) {
            if (activity instanceof MainActivity) {
//                    ((MainActivity) activity).onOpenCommentsPage(response, type, position);
                if (type == CommentFragment.TYPE_IMAGE) {
                    if (response.talentPhoto.length>position)
                    ((MainActivity) activity).openCommentFragment(type, position, (int) response.talentPhoto[position].id);
                } else {
                    if (response.profile.featuredVideo.length>position)
                    ((MainActivity) activity).openCommentFragment(type, position, (int) response.profile.featuredVideo[position].id);

                }
            }
        }
    }

    @Override
    public void onOpenCommentFragment(String type, int position, int fileId) {
        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        if (activity != null) {
            if (activity instanceof MainActivity) {
                ((MainActivity) activity).openCommentFragment(type, position, fileId);
            }
        }
    }

    @Override
    public void onSaveComment(String query) {

    }

    @Override
    public void onOpenViewReply(Comment[] comments) {

    }

    @Override
    public void onBackClick() {
        if (getActivity() != null) {
            if (getActivity() instanceof MainActivity)
                getActivity().getSupportFragmentManager().popBackStack();
            else
                getActivity().finish();
        }
    }

    @Override
    public void onOpenReviewsPage(UserResponse response) {
        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        if (activity != null) {
            if (activity instanceof MainActivity) {
                ((MainActivity) activity).openReviewsPage(response);
            }
        }
    }

    @Override
    public void onCoverImageSwiped(int position) {
        mPhotoPosition = position;
    }

    @Override
    public void onCoverLike(String type, int position) {
        if (RegisterApi.isLogin(getActivity())) {
            Bundle bundle = new Bundle();
            bundle.putString("file_type", type);
            if (type.equals(CommentFragment.TYPE_IMAGE)) {
                bundle.putString("file_id", String.valueOf(mResponse.talentPhoto[position].id));
            } else {
                bundle.putString("file_id", String.valueOf(mResponse.profile.featuredVideo[position].id));
            }

            if (mWorker != null) mWorker.executeNewTask(TASK_LIKE, bundle, this);
        } else {
            onLoginDialogOpen();
        }
    }

    @Override
    public void onCoverClick(String type, int position) {
        if (type.equals(CommentFragment.TYPE_IMAGE)) {

        } else {
            Utils.openVideoPlayer(getContext(), mResponse.profile.featuredVideo[position].url);
        }
    }

    private void showRatingDialog() {
        if (getContext() != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.AlertDialog));
            final View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_rate, null);
            final EditText ratingMsg = view.findViewById(R.id.portfolio_rating_msg);
            final AppCompatRatingBar rate = view.findViewById(R.id.portfolio_rating);

            if (mResponse.profile.review != null && mResponse.profile.review.length > 0) {
                for (Review review : mResponse.profile.review) {
                    if (LoginApi.isUser(getContext(), review.review_by)) {
                        ratingMsg.setText(review.review);
                        rate.setRating(review.rating);
                    }
                }
            }
            builder.setView(view);
            builder.setPositiveButton("Send Review", null);
            builder.setNegativeButton("Cancel", null);
            final AlertDialog dialog = builder.create();
            dialog.show();
            final Button positiveDialog = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
            // name edit text on landing is empty
            if (TextUtils.isEmpty(ratingMsg.getText())) {
                positiveDialog.setEnabled(false);
                positiveDialog.setTextColor(Color.GRAY);
            }
            positiveDialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putString("rating_msg", ratingMsg.getText().toString());
                    bundle.putString("rate", String.valueOf((int) rate.getRating()));

                    if (mWorker != null)
                        mWorker.executeNewTask(TASK_REVIEW, bundle, PortfolioFragment.this);
                    Utils.hideKeyboard(getActivity());
                    ratingMsg.setText("");
                    dialog.dismiss();
                }
            });
            ratingMsg.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (TextUtils.isEmpty(editable)) {
                        // Disable ok button
                        positiveDialog.setTextColor(Color.GRAY);
                        positiveDialog.setEnabled(false);
                    } else {
                        // Something into edit text. Enable the button.
                        positiveDialog.setTextColor(ResourcesUtil.getColor(R.color.yellow));
                        positiveDialog.setEnabled(true);
                    }
                }
            });
        }
    }

    private BroadcastReceiver updateLikeComment = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
//            getArguments().putAll(intent.getExtras());
            NeedNewData = 1;
        }

    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getContext() != null)
            LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(updateLikeComment);
    }

}
