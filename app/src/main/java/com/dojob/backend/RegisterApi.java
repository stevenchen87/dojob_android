package com.dojob.backend;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.dojob.backend.model.RegisterResponse;
import com.dojob.core.App;
import com.dojob.core.Error;
import com.dojob.core.Http;
import com.dojob.core.Logger;
import com.dojob.core.Result;
import com.dojob.util.Utils;
import com.google.gson.JsonSyntaxException;

import java.util.HashMap;
import java.util.Map;

import static com.dojob.backend.LoginApi.LOG_IN;

public class RegisterApi {
    public final static String REGISTER_TAG = "register_tag";
    private final Context mContext;
    public static int logInType;

    public RegisterApi(Context context) {
        mContext = context;
    }

    public Result request(String email, String password, String password_confirmation, String role,String deviceToken) {
        RegisterResponse response = null;
        if (!Utils.isOnline(mContext)) {
            return new Result(Error.OFFLINE);
        }
        Map<String, String> params = new HashMap<>();
        Http.Request request = Http.request(mContext)
                .url("http://step4work.com/api/talent/register")
                .tag(REGISTER_TAG);
        Map<String, String> data = new HashMap<String, String>();
        data.put("email", email);
        data.put("password", password);
        data.put("password_confirmation", password_confirmation);

        data.put("role", role); //talent/recruiter

        data.put("push_token", deviceToken);
        data.put("device_token",deviceToken);
        data.put("device_model",Utils.getDeviceModel());
        data.put("device_os",Utils.getOsVersion());
        data.put("device_Manuf",Utils.getManufracturer());
        data.put("app_version",Utils.getAppVersion());
        request.post(data);

        Result result = request.send();
        if (result.isSuccess()) {
            String responseText = (String) result.getResult();

            try {
                response = App.gson().fromJson(responseText, RegisterResponse.class);
                if (response.status.equals("ok")){
                    if (response.token != null && response.token.length()>0){
                        LoginApi.setEmail(mContext, email);
                        LoginApi.setPassword(mContext, password);
                        RegisterApi.saveToken(mContext, response.token);
                        SetupApi.saveId(mContext, response.data.id);
                        SetupApi.saveUserData(mContext, response.data);
                        PreferenceManager.getDefaultSharedPreferences(mContext).edit().putInt(LOG_IN,1).apply();
                    }
                }
            } catch (JsonSyntaxException ex) {
                ex.printStackTrace();
            }
            if (response != null) {
                return new Result(null, response);
            }
            return new Result(Error.DATA);
        }

        return new Result(Error.NETWORK);
    }
    public static boolean isLogin(Context context){
        String token = getToken(context);
        return token != null && !token.isEmpty();
    }
    public static void deleteToken(Context context){
        SharedPreferences sharedPref = context.getSharedPreferences("dojob",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("LOGIN_TOKEN", "");
        editor.apply();
    }
    public static void saveToken(Context context, String token){
        if(context != null){
            SharedPreferences sharedPref = context.getSharedPreferences("dojob",Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("LOGIN_TOKEN", token);
            editor.apply();
        }
    }
    public static String getToken(Context context){
        SharedPreferences sharedPref = context.getSharedPreferences("dojob",Context.MODE_PRIVATE);
        return sharedPref.getString("LOGIN_TOKEN", "");
    }
}
