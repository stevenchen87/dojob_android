package com.dojob.Additions.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WebResponseTutorial {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("userData")
    @Expose
    private Object userData;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getUserData() {
        return userData;
    }

    public void setUserData(Object userData) {
        this.userData = userData;
    }

}