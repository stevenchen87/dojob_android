package com.dojob.backend.model;

public class ChatRoomResponse {
    public int id;
    public String name;
    public String profile_image;
    public String url;
    public String review_status; // prompt_job_done, prompt_review, completed
}
