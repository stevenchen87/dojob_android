package com.dojob.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.dojob.R;
import com.dojob.adapter.PortfolioAdapter;
import com.dojob.core.App;
import com.dojob.core.Logger;
import com.dojob.fragment.PortfolioFragment;
import com.dojob.util.ResourcesUtil;

public class PreviewActivity extends AppCompatActivity{
    private FragmentManager mManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if(actionbar!=null) {
            actionbar.setHomeAsUpIndicator(ResourcesUtil.getDrawable(R.drawable.baseline_navigate_before_black_24));
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setDisplayShowHomeEnabled(true);
        }
        TextView title = findViewById(R.id.toolbar_title);
        title.setVisibility(View.VISIBLE);
        title.setText("Preview");

        mManager = getSupportFragmentManager();
        String response = getIntent().getExtras().getString("preview_response");
        if(response != null){
            int maxLogSize = 1000;
            for(int i = 0; i <= response.length() / maxLogSize; i++) {
                int start = i * maxLogSize;
                int end = (i+1) * maxLogSize;
                end = end > response.length() ? response.length() : end;
                Log.v("previewActivity", response.substring(start, end));
            }
            Bundle bundle = new Bundle();
            bundle.putInt(PortfolioFragment.ARGS_PORTFOLIO_TYPE, PortfolioAdapter.PREVIEW_TYPE);
            bundle.putString(PortfolioFragment.ARGS_PORTFOLIO_DATA, response);
            PortfolioFragment portfolioFragment = PortfolioFragment.newInstance(bundle);
            mManager.beginTransaction()
                    .replace(R.id.content, portfolioFragment, "preview_page")
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
