package com.dojob.Additions;

import com.google.gson.annotations.SerializedName;

public class ChangePassword {

    @SerializedName("password")
    String password;

    @SerializedName("password_confirmation")
    String confirmPassword;

    @SerializedName("current_password")
    String oldPassword;


    public ChangePassword(String password, String confirmPassword, String oldPassword) {
        this.password = password;
        this.confirmPassword = confirmPassword;
        this.oldPassword = oldPassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }
}
