package com.dojob.backend;

import android.content.Context;
import android.os.Message;
import android.util.SparseArray;

import com.dojob.backend.model.ChatRoomResponse;
import com.dojob.backend.model.FeedsResponse;
import com.dojob.backend.model.InboxMessageResponse;
import com.dojob.backend.model.InboxResponse;
import com.dojob.backend.model.MessageResponse;
import com.dojob.core.App;
import com.dojob.core.Error;
import com.dojob.core.Http;
import com.dojob.core.Logger;
import com.dojob.core.Result;
import com.dojob.database.MessagesTable;
import com.dojob.util.Utils;
import com.google.gson.JsonSyntaxException;
import com.orm.SugarRecord;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class InboxApi {
    private final static String INBOX_TAG = "inbox_tag";
    private final static String MESSAGE_TAG = "message_tag";
    private final static String SEND_MESSAGE_TAG = "send_message_tag";
    private final static String JOB_DONE_TAG = "job_done_tag";
    private final Context mContext;
    private String getMessageUrl = "http://step4work.com/api/talent/message/get?";
    private String sendMessageUrl = "http://step4work.com/api/talent/message/send?";
    private String jobDoneUrl = "http://step4work.com/api/talent/review/confirmJobDone?";

    public InboxApi(Context context) {
        mContext = context;
    }

    //not sure what it is
    public Result sendMessage(String messageBody, String targetId) {
        MessageResponse response = null;
        if (!Utils.isOnline(mContext)) {
            return new Result(Error.OFFLINE);
        }
        Map<String, String> params = new HashMap<>();
        params.put("msg_body",messageBody);
        params.put("target_id",targetId);
        params.put("biggest_id",String.valueOf(getBiggestId()));
        Http.Request request = Http.request(mContext)
                .url(sendMessageUrl)
                .post(params)
                .tag(SEND_MESSAGE_TAG);
        Result result = request.send();
        if (result.isSuccess()) {
            String responseText = (String) result.getResult();
            try {
                response = App.gson().fromJson(responseText, MessageResponse.class);
            } catch (JsonSyntaxException ex) {
                ex.printStackTrace();
            }
            if (response != null) {
                if(response.status.equals("ok")){
                    saveMessages(response, true);
                    InboxResponse[] inboxResponses = processData(response);
                    if(inboxResponses != null){
                        return new Result(null, inboxResponses);
                    }
                }
            }
            return new Result(Error.DATA);
        }

        return new Result(Error.NETWORK);
    }

    public Result getMessage() {
        MessageResponse response = null;
        if (!Utils.isOnline(mContext)) {
            return new Result(Error.OFFLINE);
        }
        String url = getMessageUrl + "biggest_id="+ getBiggestId();
        Http.Request request = Http.request(mContext)
                .url(url)
                .tag(MESSAGE_TAG);
        Result result = request.send();
        if (result.isSuccess()) {
            String responseText = (String) result.getResult();
            try {
                response = App.gson().fromJson(responseText, MessageResponse.class);
            } catch (JsonSyntaxException ex) {
                ex.printStackTrace();
            }
            if (response != null) {
                if(response.status.equals("ok") && response.msgDataAry.length > 0){
                    saveMessages(response, false);
                    InboxResponse[] inboxResponses = processData(response);
                    if(inboxResponses != null){
                        return new Result(null, inboxResponses);
                    }
                }
            }
            return new Result(Error.DATA);
        }

        return new Result(Error.NETWORK);
    }

    private Long getBiggestId(){
       MessagesTable tables = Select.from(MessagesTable.class).orderBy("id desc").first();
       return (tables != null)?tables.getId():0L;
    }

    private void saveMessages(MessageResponse response, boolean isRead){
        if(response.msgDataAry.length > 0){
            MessagesTable[] tables = new MessagesTable[response.msgDataAry.length];
            int i = 0;
            for(InboxMessageResponse item: response.msgDataAry){
                MessagesTable row = MessagesTable.findById(MessagesTable.class, item.id);
                if(row == null){
                    row = new MessagesTable();
                    row.setId((long)item.id);
                }
                row.setUser_id(item.user_id);
                row.setSender_id(item.sender_id);
                row.setChatroom_id(item.chatroom_id);
                row.setMsg_body(item.msg_body);
                row.setStatus(item.status);
                row.setCreated_at(item.created_at);
                row.setChatroom_user_id(item.chatroomData.id);
                row.setChatroomData(item.chatroomData);
                if(row.getIsRead() == 0 && isRead){
                    row.setIsRead(1);
                }
                else if(!isRead){
                    row.setIsRead(Integer.valueOf(item.status));
                }
                tables[i] = row;
                i++;
            }
            SugarRecord.saveInTx(tables);
        }
    }

    public void updateReadStatus(InboxMessageResponse messageResponse){
        List<MessagesTable> chatRoomMessages = Select.from(MessagesTable.class)
                .where(Condition.prop("id").eq(messageResponse.id))
                .list();
        for(MessagesTable table: chatRoomMessages){
            table.setIsRead(1);
            table.save();
        }
    }

    private void updateChatroomData(ChatRoomResponse chatroomData){
        List<MessagesTable> chatRoomMessages = Select.from(MessagesTable.class)
                .where(Condition.prop("chatroomid").eq(chatroomData.id))
                .list();
        for(MessagesTable table: chatRoomMessages){
            table.setChatroomData(chatroomData);
            table.update();
        }
    }

    private InboxResponse[] processData(MessageResponse response) {
        if(response.msgDataAry != null && response.msgDataAry.length > 0){
            return getChatRoomMessages();
        }
        return new InboxResponse[0];
    }

    public InboxMessageResponse[] getMessages(int userId){
        List<MessagesTable> tables = Select.from(MessagesTable.class)
                .where(Condition.prop("chatroomuserid")
                .eq(userId)).orderBy("createdat desc").list();
        ArrayList<InboxMessageResponse> arrayList = new ArrayList<>();
        for(MessagesTable messagesTable: tables){
            InboxMessageResponse temp = new InboxMessageResponse();
            temp.id = messagesTable.getId().intValue();
            temp.user_id = messagesTable.getUser_id();
            temp.sender_id = messagesTable.getSender_id();
            temp.chatroom_id = messagesTable.getChatroom_id();
            temp.msg_body = messagesTable.getMsg_body();
            temp.status = messagesTable.getStatus();
            temp.created_at = messagesTable.getCreated_at();
            temp.chatroomData = messagesTable.getChatroomData();
            temp.status = String.valueOf(messagesTable.getIsRead());
            arrayList.add(temp);
        }

        return arrayList.toArray(new InboxMessageResponse[arrayList.size()]);

    }

    public InboxResponse[] getChatRoomMessages(){
        ArrayList<InboxResponse> list = new ArrayList<>();
        try {
            List<MessagesTable> tables = Select.from(MessagesTable.class).groupBy("chatroomid").orderBy("createdat desc").list();
            if(tables != null){
                for(MessagesTable item: tables){
                    List<MessagesTable> chatRoomMessages = Select.from(MessagesTable.class)
                            .where(Condition.prop("chatroomid").eq(item.getChatroom_id()))
                            .orderBy("createdat desc")
                            .list();
                    InboxResponse inbox = new InboxResponse();
                    inbox.name =  String.valueOf(item.getChatroom_id());
                    ArrayList<InboxMessageResponse> arrayList = new ArrayList<>();
                    int chatIsRead = 0;
                    for(MessagesTable messagesTable: chatRoomMessages){
                        InboxMessageResponse temp = new InboxMessageResponse();
                        temp.id = messagesTable.getId().intValue();
                        temp.user_id = messagesTable.getUser_id();
                        temp.sender_id = messagesTable.getSender_id();
                        temp.chatroom_id = messagesTable.getChatroom_id();
                        temp.msg_body = messagesTable.getMsg_body();
                        temp.status = messagesTable.getStatus();
                        temp.created_at = messagesTable.getCreated_at();
                        temp.chatroomData = messagesTable.getChatroomData();
                        temp.status = String.valueOf(messagesTable.getIsRead());
                        if(messagesTable.getIsRead() == 0){
                            chatIsRead++;
                        }
                        arrayList.add(temp);
                    }
                    inbox.messages = arrayList.toArray(new InboxMessageResponse[arrayList.size()]);
                    if(inbox.messages.length>0){
                        InboxMessageResponse inboxLastMessage = inbox.messages[0];
                        inbox.last_message = inboxLastMessage.msg_body;
                        inbox.target_id = inboxLastMessage.sender_id;
                        inbox.last_seen = inboxLastMessage.created_at;
                        inbox.chatroom_id = inboxLastMessage.chatroom_id;
                        inbox.chatIsRead = chatIsRead;
                    }
                    list.add(inbox);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }


        return list.toArray(new InboxResponse[list.size()]);
    }

    public Result requestJobDone(String chatroomId){
        InboxMessageResponse response = null;
        if (!Utils.isOnline(mContext)) {
            return new Result(Error.OFFLINE);
        }
        Map<String, String> params = new HashMap<>();
        params.put("chatroom_id",chatroomId);
        Http.Request request = Http.request(mContext)
                .url(jobDoneUrl)
                .post(params)
                .tag(JOB_DONE_TAG);
        Result result = request.send();
        if (result.isSuccess()) {
            String responseText = (String) result.getResult();
            try {
                response = App.gson().fromJson(responseText, InboxMessageResponse.class);
            } catch (JsonSyntaxException ex) {
                ex.printStackTrace();
            }
            if (response != null) {
                if(response.status.equals("ok")){
                    updateChatroomData(response.chatroomData);
                }
                return new Result(null, response);
            }
            return new Result(Error.DATA);
        }

        return new Result(Error.NETWORK);
    }


}
