package com.dojob.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.dojob.R;
import com.dojob.activity.MainActivity;
import com.dojob.util.Utils;
import com.github.ybq.android.spinkit.SpinKitView;

public class FAQFragment extends Fragment {
    String url = "https://step4work.com/faq";
    SpinKitView progressBar;

    public static FAQFragment newInstance() {

        Bundle args = new Bundle();

        FAQFragment fragment = new FAQFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        if (activity != null) {
            ActionBar actionbar = activity.getSupportActionBar();
            if (actionbar != null) {
                actionbar.setDisplayHomeAsUpEnabled(true);
            }
        }
        View view = inflater.inflate(R.layout.fragment_faq, container, false);

        progressBar = view.findViewById(R.id.progress);
        final WebView myWebView = view.findViewById(R.id.webView1);
        myWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
               // return super.shouldOverrideUrlLoading(view, request);
                String url = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    url = request.getUrl().toString();
                    if (url.startsWith("mailto:"))
                    {
                        url = url.replace("mailto:", "");

                        Intent mail = new Intent(Intent.ACTION_SEND);
                        mail.setType("application/octet-stream");
                        mail.putExtra(Intent.EXTRA_EMAIL, new String[] { url.split("//?")[0] });
                        startActivity(mail);
                        return true;
                    }

                }


                return false;
            }
        });
        myWebView.loadUrl(url);


        ((MainActivity) this.getActivity()).setTitle("FAQ");
        Utils.hideKeyBoard(getActivity());

//        shouldOverrideUrlLoading(myWebView,url);
        return view;
    }

    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (url.startsWith("tel:")) {
            Intent tel = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
            startActivity(tel);
            return true;
        }
        else if (url.contains("mailto:")) {
            view.getContext().startActivity(
                    new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
            return true;

        }else {
            view.loadUrl(url);
            return true;
        }
    }



}
