package com.dojob.view;

import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import com.dojob.R;
import com.dojob.activity.MainActivity;
import com.dojob.backend.LoginApi;
import com.dojob.backend.RegisterApi;
import com.dojob.backend.model.LoginResponse;
import com.dojob.backend.model.RegisterResponse;
import com.dojob.core.BackgroundWorker;
import com.dojob.core.Result;
import com.dojob.util.AppSP;
import com.dojob.util.Utils;

public class EmailSignUpDialog extends DialogFragment implements View.OnClickListener, BackgroundWorker.Callbacks {

    private View mClose;
    private AppCompatButton mCreateProfileButton;
    private Listener mListener;
    private BackgroundWorker mWorker;
    private RegisterApi mRegisterApi;
    private LoginApi mLoginApi;
    private TextView textEmail, textPassword;
    private EditText email, password, password_confirmation;
    private String mRole;

    public static EmailSignUpDialog newInstance(Bundle args) {
        EmailSignUpDialog fragment = new EmailSignUpDialog();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            fragment.setStyle(AppCompatDialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Material_Light_Dialog_NoActionBar);
        } else {
            fragment.setStyle(AppCompatDialogFragment.STYLE_NO_TITLE, android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar);
        }
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(getActivity() != null){
            mWorker = new BackgroundWorker(getActivity());
            mRegisterApi = (RegisterApi) mWorker.get(RegisterApi.REGISTER_TAG);
            if (mRegisterApi == null) {
                mRegisterApi = new RegisterApi(getContext());
                mWorker.put(RegisterApi.REGISTER_TAG, mRegisterApi);
            }
            mLoginApi = (LoginApi) mWorker.get(LoginApi.LOGIN_TAG);
            if (mLoginApi == null) {
                mLoginApi = new LoginApi(getContext());
                mWorker.put(LoginApi.LOGIN_TAG, mLoginApi);
            }

            if(getArguments() != null) mRole = getArguments().getString("role", "talent");
        }
        View view = inflater.inflate(R.layout.dialog_email_sign_up, container);
        Window window = getDialog().getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }
        mCreateProfileButton = view.findViewById(R.id.login_create_profile_button);
        mClose = view.findViewById(R.id.close);
        email = view.findViewById(R.id.login_email);
        textEmail =  view.findViewById(R.id.text_email);
        textPassword =  view.findViewById(R.id.text_password);
        password = view.findViewById(R.id.login_password);
        password_confirmation = view.findViewById(R.id.login_repeated_password);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(mCreateProfileButton != null){
            mCreateProfileButton.setOnClickListener(this);
        }
        if(mClose != null){
            mClose.setOnClickListener(this);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(mCreateProfileButton != null){
            mCreateProfileButton.setOnClickListener(null);
        }
        if(mClose != null){
            mClose.setOnClickListener(null);
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        Utils.hideKeyBoard(getActivity(),getView());
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.login_create_profile_button:
                if(mWorker != null) mWorker.executeNewTask(RegisterApi.REGISTER_TAG, null, this);
                break;
            case R.id.close:
                dismiss();
                break;
        }
    }

    public void setListener(Listener listener){
        mListener = listener;
    }

    @Override
    public Result executeTaskInBackground(String id, Bundle args) {
        switch (id){
            case LoginApi.LOGIN_TAG:
                showHideErrorView();
                return mLoginApi.request(email.getText().toString(), password.getText().toString(), mRole,Utils.getDeviceToken(getActivity()));
            case RegisterApi.REGISTER_TAG:
                showHideErrorView();
                return mRegisterApi.request(email.getText().toString(), password.getText().toString(), password_confirmation.getText().toString(), mRole,Utils.getDeviceToken(getActivity()));
        }
        return null;
    }

    @Override
    public void onBackgroundTaskCompleted(String id, Result result) {
        switch (id){
            case LoginApi.LOGIN_TAG:
            if(result.isSuccess() ){
                LoginResponse response = (LoginResponse) result.getResult();
                Log.d("EmailLogin.status", ""+response.status+","+response.status.equals("ok"));
                Log.d("EmailLogin.token", ""+response.token );
                Log.d("EmailLogin.mListener", ""+mListener);


                if (response.status.equals("ok") && getActivity() != null){
                    Utils.isRefreshRequired = 1;
                    if(response.data.verified == 1) {
                        if (response.token != null && response.token.length()>0){
                            RegisterApi.saveToken(this.getActivity(), response.token);
                            AppSP sp = AppSP.getInstance(this.getActivity());
                            sp.savePreferences(Utils.USER_ROLE,response.data.position);
                            sp.savePreferences(Utils.ANNOUNCEMENT_BIGGEST_ID,response.data.announeMentID);
                        }
                        ((MainActivity) this.getActivity()).refresh();
                    } else if(mListener != null) mListener.onOpenVerification();
                    textEmail.setVisibility(View.GONE);
                    textPassword.setVisibility(View.GONE);
                    Utils.hideKeyBoard(getActivity(), getView());
                    dismiss();
                }
            }else {
                Log.i("LOG_IN_STATUS",result.getErrorMessage());
            }
            break;
            case RegisterApi.REGISTER_TAG:
                if(result.isSuccess() ){
                    RegisterResponse response = (RegisterResponse) result.getResult();
                    Log.d("EmailLogin.status", ""+response.status);
                    Log.d("EmailLogin.token", ""+response.token);

                    textEmail.setVisibility(View.GONE);
                    textPassword.setVisibility(View.GONE);
                    if (response.status.equals("ok")){
                        Utils.isRefreshRequired = 1;
                        if(response.data.verified == 1 && getActivity() != null) {
                            ((MainActivity) this.getActivity()).refresh();
                            AppSP sp = AppSP.getInstance(this.getActivity());
                            sp.savePreferences(Utils.USER_ROLE,response.data.position);
                            sp.savePreferences(Utils.ANNOUNCEMENT_BIGGEST_ID,response.data.announeMentID);
                        } else if(mListener != null) mListener.onOpenVerification();
                        dismiss();
                        Utils.hideKeyBoard(getActivity(), getView());
                    }
                    else{
                        try {
                            String email_error = response.error.email[0];
                            textEmail.setText(email_error);
                            textEmail.setVisibility(View.VISIBLE);
                            if(mWorker != null) mWorker.executeNewTask(LoginApi.LOGIN_TAG, null, this);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        try {
                            String password_error = response.error.password[0];
                            textPassword.setText(password_error);
                            textPassword.setVisibility(View.VISIBLE);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                break;
        }
    }

    private void showHideErrorView(){
        if (getActivity()!= null){
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    textPassword.setVisibility(View.GONE);
                }
            });
        }
    }

    public interface Listener{
        void onOpenProfileSetting(); // might need to pass email and password back to
        void onOpenVerification();
    }
}
