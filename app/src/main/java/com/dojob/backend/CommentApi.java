package com.dojob.backend;

import android.content.Context;
import android.database.sqlite.SQLiteException;

import com.dojob.backend.model.Comment;
import com.dojob.backend.model.CommentsInboxResponse;
import com.dojob.backend.model.CommentsResponse;
import com.dojob.backend.model.InboxMessageResponse;
import com.dojob.backend.model.MessageResponse;
import com.dojob.core.App;
import com.dojob.core.Error;
import com.dojob.core.Http;
import com.dojob.core.Result;
import com.dojob.database.CommentsTable;
import com.dojob.database.MessagesTable;
import com.dojob.util.Utils;
import com.google.gson.JsonSyntaxException;
import com.orm.SugarRecord;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CommentApi {
    private final static String COMMENT_TAG = "comment_tag";
    private final static String SEND_COMMENT_TAG = "send_comment_tag";
    private final Context mContext;
    private String getCommentsUrl = "http://step4work.com/api/talent/comment/get?";
    private String sendCommentUrl = "http://step4work.com/api/talent/comment/save?";

    public CommentApi(Context context) {
        mContext = context;
    }

    public Result request(int postId, String postType) {
        CommentsResponse response = null;
        if (!Utils.isOnline(mContext)) {
            return new Result(Error.OFFLINE);
        }
        Map<String, String> params = new HashMap<>();
        params.put("post_id", String.valueOf(postId));
        params.put("post_type", postType);
        params.put("biggest_ID", String.valueOf(0));
        Http.Request request = Http.request(mContext)
                .url(getCommentsUrl+Utils.hashMapToString(params))
                .tag(COMMENT_TAG);
        Result result = request.send();
        if (result.isSuccess()) {
            String responseText = (String) result.getResult();
            try {
                response = App.gson().fromJson(responseText, CommentsResponse.class);
            } catch (JsonSyntaxException ex) {
                ex.printStackTrace();
            }
            if (response != null) {
                if(response.status.equals("ok")){
                    saveComments(response);
                    return new Result(null, getComments(postId));
                }
            }
            return new Result(Error.DATA);
        }

        return new Result(Error.NETWORK);
    }

    public CommentsInboxResponse[] getInboxComments(int fileId){
        ArrayList<CommentsInboxResponse> list = new ArrayList<>();
        List<CommentsTable> tables = Select.from(CommentsTable.class)
                .where(Condition.prop("postid").eq(fileId))
                .groupBy("userid")
                .orderBy("updatedat desc")
                .list();
        if(tables != null){
            for(CommentsTable item: tables){
                List<CommentsTable> chatRoomMessages = Select.from(CommentsTable.class)
                        .where(Condition.prop("userid").eq(item.getUser_id()))
                        .orderBy("updatedat desc")
                        .list();
                CommentsInboxResponse inbox = new CommentsInboxResponse();
                inbox.name =  String.valueOf(item.getName());
                ArrayList<Comment> arrayList = new ArrayList<>();
                for(CommentsTable commentsTable: chatRoomMessages){
                    Comment temp = new Comment();
                    temp.id = commentsTable.getId().intValue();
                    temp.user_id = commentsTable.getUser_id();
                    temp.parent_path = commentsTable.getParent_path();
                    temp.post_id = commentsTable.getPost_id();
                    temp.msg_body = commentsTable.getMsg_body();
                    temp.post_type = commentsTable.getPost_type();
                    temp.created_at = commentsTable.getCreated_at();
                    temp.updated_at = commentsTable.getUpdated_at();
                    temp.name = commentsTable.getName();
                    temp.profile_image = commentsTable.getProfile_image();
                    arrayList.add(temp);
                }
                inbox.comments = arrayList.toArray(new Comment[arrayList.size()]);
                if(inbox.comments.length>0){
                    Comment inboxLastMessage = inbox.comments[0];
                    inbox.msg_body = inboxLastMessage.msg_body;
                    inbox.id = inboxLastMessage.id;
                    inbox.name = inboxLastMessage.name;
                    inbox.post_id = inboxLastMessage.post_id;
                    inbox.profile_image = inboxLastMessage.profile_image;
                    inbox.vip = inboxLastMessage.vip;
                    inbox.user_id = inboxLastMessage.user_id;
                    inbox.updated_at = inboxLastMessage.updated_at;
                }
                list.add(inbox);
            }
        }

        return list.toArray(new CommentsInboxResponse[list.size()]);
    }

    public Comment[] getComments(int fileId){
        ArrayList<Comment> list = new ArrayList<>();
        List<CommentsTable> tables = null;
        try {
            tables = Select.from(CommentsTable.class)
                    .where(Condition.prop("postid").eq(fileId))
//                .and(Condition.prop("userid").eq(userId))
                    .orderBy("updatedat desc")
                    .list();
        }catch (SQLiteException e){
            e.printStackTrace();
        }

        if(tables != null){
            for(CommentsTable commentsTable: tables){
                Comment temp = new Comment();
                temp.id = commentsTable.getId().intValue();
                temp.user_id = commentsTable.getUser_id();
                temp.parent_path = commentsTable.getParent_path();
                temp.post_id = commentsTable.getPost_id();
                temp.msg_body = commentsTable.getMsg_body();
                temp.post_type = commentsTable.getPost_type();
                temp.created_at = commentsTable.getCreated_at();
                temp.updated_at = commentsTable.getUpdated_at();
                temp.name = commentsTable.getName();
                temp.profile_image = commentsTable.getProfile_image();
                list.add(temp);
            }
        }

        return list.toArray(new Comment[list.size()]);
    }


    private void saveComments(CommentsResponse response){
        if(response.comments.length > 0){
            CommentsTable[] tables = new CommentsTable[response.comments.length];
            int i = 0;
            for(Comment item: response.comments){
                CommentsTable row = MessagesTable.findById(CommentsTable.class, item.id);
                if(row == null){
                    row = new CommentsTable();
                    row.setId((long)item.id);
                }
                row.setUser_id(item.user_id);
                row.setParent_path(item.parent_path);
                row.setPost_id(item.post_id);
                row.setMsg_body(item.msg_body);
                row.setPost_type(item.post_type);
                row.setCreated_at(item.created_at);
                row.setMsg_body(item.msg_body);
                row.setUpdated_at((item.updated_at));
                row.setProfile_image(item.profile_image);
                row.setVip(item.vip);
                row.setName(item.name);
                tables[i] = row;
                i++;
            }
            SugarRecord.saveInTx(tables);
        }
    }

    /*
     * return Result - response (Comment[])
     * */

    public Result sendMessage(int postId, String postType, String msg){
        CommentsResponse response = null;
        if (!Utils.isOnline(mContext)) {
            return new Result(Error.OFFLINE);
        }
        Map<String, String> params = new HashMap<>();
        params.put("post_id", String.valueOf(postId));
        params.put("post_type", postType);
        params.put("msg_body", msg);
        params.put("biggest_id",String.valueOf(0));
        Http.Request request = Http.request(mContext)
                .url(sendCommentUrl)
                .post(params)
                .tag(SEND_COMMENT_TAG);
        Result result = request.send();
        if (result.isSuccess()) {
            String responseText = (String) result.getResult();
            try {
                response = App.gson().fromJson(responseText, CommentsResponse.class);
            } catch (JsonSyntaxException ex) {
                ex.printStackTrace();
            }
            if (response != null) {
                if(response.status.equals("ok")){
                    saveComments(response);
                    return new Result(null, getComments(postId));
//                    if(isReplyComment){
//                        return new Result(null, getComments(postId));
//                    }
//                    else{
//                        return new Result(null, getInboxComments(postId));
//                    }
                }
            }
            return new Result(Error.DATA);
        }

        return new Result(Error.NETWORK);
    }

}
