package com.dojob.core;

public enum Error {
    DATA,
    NETWORK,
    OFFLINE,
    CUSTOM,
    EMPTY
}
