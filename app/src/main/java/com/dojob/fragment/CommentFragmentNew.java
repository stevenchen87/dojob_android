package com.dojob.fragment;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dojob.Additions.ApiInterface;
import com.dojob.Additions.ServiceGenerator;
import com.dojob.Additions.local.CommentDatum;
import com.dojob.Additions.local.FileData;
import com.dojob.Additions.responses.WebResponseComment;
import com.dojob.GlideApp;
import com.dojob.R;
import com.dojob.activity.MainActivity;
import com.dojob.adapter.CommentsAdapter;
import com.dojob.adapter.model.InboxItem;
import com.dojob.backend.CommentApi;
import com.dojob.backend.ProfileApi;
import com.dojob.backend.RegisterApi;
import com.dojob.backend.SetupApi;
import com.dojob.backend.model.Comment;
import com.dojob.backend.model.ProfileResponse;
import com.dojob.core.App;
import com.dojob.core.BackgroundWorker;
import com.dojob.core.Result;
import com.dojob.databinding.FragmentCommentBinding;
import com.dojob.util.ResourcesUtil;
import com.dojob.util.Utils;

import java.util.ArrayList;

import retrofit2.Callback;
import retrofit2.Response;

public class CommentFragmentNew extends Fragment implements BackgroundWorker.Callbacks, View.OnClickListener {

    public static final String COMMENTS_TYPE = "comments_type";
    public static final String TYPE_IMAGE = "image";
    public static final String TYPE_VIDEO = "video";
    public static final String FILE_ID = "file_id";
    public static final String LIKE_BROADCAST = "like_broadcast";

    public static final String COMMENT_FILE_POSITION = "comment_file_position";
    private final String TASK_SEND_COMMENT = "task_send_profile";
    private final String TASK_LIKE = "task_like";

    FragmentCommentBinding binding;
    private BackgroundWorker mWorker;
    private CommentApi mCommetApi;
    private CommentsAdapter mAdapter;
    private String mType;
    private ProfileApi mProfileApi;
    private FileData fileData;
    private ArrayList<CommentDatum> commentDatumArrayList = new ArrayList<>();
    private int file_ID;

    public static CommentFragmentNew newInstance(Bundle args) {
        CommentFragmentNew fragment = new CommentFragmentNew();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatActivity activity = ((AppCompatActivity) getActivity());

        if (getArguments() != null) {
            if (getArguments().containsKey(COMMENTS_TYPE))
                mType = getArguments().getString(COMMENTS_TYPE);
            if (getArguments().containsKey(FILE_ID))
                file_ID = getArguments().getInt(FILE_ID);
        }
        if (activity != null) {
            settingTitle();
            mWorker = new BackgroundWorker(activity);
            String OBJ_COMMENT_API = "obj_comment_api";
            mCommetApi = (CommentApi) mWorker.get(OBJ_COMMENT_API);
            if (mCommetApi == null) {
                mCommetApi = new CommentApi(getContext());
                mWorker.put(OBJ_COMMENT_API, mCommetApi);
            }
            String OBJ_PROFILE_API = "obj_profile_api";
            mProfileApi = (ProfileApi) mWorker.get(OBJ_PROFILE_API);
            if (mProfileApi == null) {
                mProfileApi = new ProfileApi(getContext());
                mWorker.put(OBJ_PROFILE_API, mProfileApi);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_comment, container, false);
        return binding.getRoot();

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE
                    | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        }
        Utils.hideKeyBoard(getActivity());
        binding.recycleView.setItemViewCacheSize(20);
        binding.recycleView.setDrawingCacheEnabled(true);
        binding.recycleView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        binding.recycleView.setVisibility(View.VISIBLE);
        mAdapter = new CommentsAdapter(getContext(), commentDatumArrayList);
        binding.recycleView.setAdapter(mAdapter);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        binding.recycleView.setLayoutManager(llm);

        binding.portfolioCover.setOnClickListener(this);
        binding.portfolioCoverFavourite.setOnClickListener(this);
        binding.portfolioCoverShare.setOnClickListener(this);
        binding.portfolioCoverPlay.setOnClickListener(this);
        binding.btnViewProfile.setOnClickListener(this);
        binding.portfolioCoverFormat.setOnClickListener(this);

        callApiCommentData();
        sendComment();
    }

    @Override
    public void onResume() {
        super.onResume();
        settingTitle();
    }

    private void settingTitle() {
        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        if (activity != null) {
            if (activity instanceof MainActivity) {
                ActionBar actionbar = activity.getSupportActionBar();
                if (actionbar != null) {
                    actionbar.setDisplayHomeAsUpEnabled(true);
                    actionbar.setDisplayShowHomeEnabled(true);
                }
                ((MainActivity) activity).setTitle("Comments");
            }
        }
    }

    private void showData() {
        if (fileData != null) {
            if (fileData.getUrl() != null) {
                String imgeurl = "";
                if (mType.equalsIgnoreCase(TYPE_VIDEO)) {
                    binding.portfolioCoverPlay.setVisibility(View.VISIBLE);
                    binding.portfolioCover.setOnClickListener(null);
                    imgeurl = fileData.getThumbnail();
                } else {
                    binding.portfolioCoverPlay.setVisibility(View.GONE);
                    binding.portfolioCover.setOnClickListener(this);
                    imgeurl = fileData.getUrl();
                }

                if (getActivity() != null)
                    if (imgeurl != null && !imgeurl.equalsIgnoreCase(""))
                        GlideApp.with(getActivity())
                                .load(imgeurl)
                                .placeholder(R.drawable.placeholder_1)
                                .apply(RequestOptions.fitCenterTransform())
                                .into(binding.portfolioCover);
            }
            updateLikeButton(fileData.getLikeStatus(), fileData.getLikeCount());
        }


        if (getActivity() != null) {
            ProfileResponse response = SetupApi.getUserData(getActivity());
            if (response != null) {
                if (response.profile_picture != null) {
                    Glide.with(this)
                            .load(response.profile_picture)
                            .apply(RequestOptions.fitCenterTransform())
                            .into(binding.commentUserImage);
                }
            }
        }
    }

    private void updateLikeButton(int isliked, int likeCounter) {
        if (isliked == 1) {
            App.picasso().load(R.drawable.baseline_favorite_black_24).into(binding.portfolioCoverFavourite);
            binding.portfolioCoverFavourite.setColorFilter(ResourcesUtil.getColor(R.color.red), PorterDuff.Mode.SRC_IN);
        } else {
            App.picasso().load(R.drawable.baseline_favorite_border_black_24).into(binding.portfolioCoverFavourite);
            binding.portfolioCoverFavourite.setColorFilter(ResourcesUtil.getColor(R.color.green), PorterDuff.Mode.SRC_IN);
        }
        binding.tvlikeCounter.setText(String.valueOf(likeCounter));
    }

    private void sendComment() {
        if (RegisterApi.isLogin(getActivity())) {
            binding.etComment.setImeOptions(EditorInfo.IME_ACTION_DONE);
            binding.etComment.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        if (!binding.etComment.getText().toString().equalsIgnoreCase("")) {
                            apiCallingForComment(binding.etComment.getText().toString());
                        }
                    }
                    return true;
                }
            });
        } else {
            binding.etComment.setFocusable(false);
            binding.etComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onLoginDialogOpen();
                }
            });
        }
    }

    private void apiCallingForComment(String msg) {
        if (RegisterApi.isLogin(getActivity())) {
            binding.progress.setVisibility(View.VISIBLE);
            Bundle bundle = new Bundle();
            bundle.putString("query", msg);
            if (mWorker != null) mWorker.executeNewTask(TASK_SEND_COMMENT, bundle, this);
        } else {
            onLoginDialogOpen();
        }
        if (getActivity() != null) Utils.hideKeyBoard(getActivity());
    }


    private void callApiCommentData() {
        binding.progress.setVisibility(View.VISIBLE);
        ServiceGenerator serviceGenerator = new ServiceGenerator();
        ApiInterface apiInterface = serviceGenerator.createService(ApiInterface.class);
        String token = "";
        if (getActivity() != null) token = "Bearer " + RegisterApi.getToken(getActivity());
        retrofit2.Call<WebResponseComment> call = apiInterface.getCommentData(token, String.valueOf(file_ID));
        call.enqueue(new Callback<WebResponseComment>() {
            @Override
            public void onResponse(retrofit2.Call<WebResponseComment> call, Response<WebResponseComment> response) {
                binding.progress.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    Log.i("RESPONSE", "OKAY");
                    if (response.body() != null) {
                        String status = response.body().getStatus();
                        if (status.equalsIgnoreCase("ok")) {

                            fileData = response.body().getFileData();
                            showData();
                            commentDatumArrayList = (ArrayList<CommentDatum>) response.body().getCommentData();
                            mAdapter.setCommentData(commentDatumArrayList);

                        } else {
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Log.i("RESPONSE", response.message());
                    Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<WebResponseComment> call, Throwable t) {
                Log.i("RESPONSE", t.getMessage());
                binding.progress.setVisibility(View.GONE);
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public Result executeTaskInBackground(String id, Bundle args) {
        switch (id) {
//            case TASK_REQUEST_COMMENT:
//                return mCommetApi.request(file_ID, mType);
            case TASK_SEND_COMMENT:
                return mCommetApi.sendMessage(file_ID, mType, args.getString("query"));
            case TASK_LIKE:
                return mProfileApi.like(args.getString("file_id"), args.getString("file_type"));
        }
        return null;
    }

    @Override
    public void onBackgroundTaskCompleted(String id, Result result) {
        binding.progress.setVisibility(View.GONE);
        // int type = (mType.equals(TYPE_IMAGE)) ? PortfolioCover.TYPE_COVER : PortfolioCover.TYPE_VIDOES;
        switch (id) {
            case TASK_SEND_COMMENT:
                if (result.isSuccess() && mAdapter != null) {
                    binding.etComment.setText("");
                    Comment[] mCommentResponse = (Comment[]) result.getResult();
                    if (mCommentResponse != null && mCommentResponse.length > 0) {
                        CommentDatum commentDatum = new CommentDatum();
                        Comment comment = mCommentResponse[0];
                        commentDatum.setId(comment.id);
                        commentDatum.setMsgBody(comment.msg_body);
                        commentDatum.setName(comment.name);
                        commentDatum.setCreatedAt(Integer.parseInt(comment.created_at));
                        commentDatum.setProfileImage(comment.profile_image);
                        commentDatum.setUserId(comment.user_id);
                        commentDatum.setPostType(comment.post_type);
                        mAdapter.setItemComment(commentDatum);
                        binding.recycleView.setVisibility(View.VISIBLE);
                        binding.progress.setVisibility(View.GONE);
                    }
                }
                break;
            case TASK_LIKE:
                if (result.isSuccess()) {
                    //com.dojob.backend.model.Response response = (com.dojob.backend.model.Response) result.getResult();
                    if (fileData != null) {
                        if (fileData.getLikeStatus() == 0)
                            fileData.setLikeCount(fileData.getLikeCount() + 1);
                        else
                            fileData.setLikeCount(fileData.getLikeCount() - 1);

                        updateLikeButton(fileData.getLikeStatus() == 0 ? 1 : 0, fileData.getLikeCount());
                        if (fileData.getLikeStatus() == 0)
                            fileData.setLikeStatus(1);
                        else fileData.setLikeStatus(0);
                    }
                    PortfolioFragment.NeedNewData = 1;
                    //getContext().sendBroadcast(new Intent(PortfolioFragment.PROFILE_COMMENT_FILED_LIKED));
                }
                break;
        }
    }

    public void onLikePressed() {
        if (RegisterApi.isLogin(getActivity())) {
            Bundle bundle = new Bundle();
            bundle.putString("file_type", mType);
            bundle.putString("file_id", String.valueOf(file_ID));
            binding.progress.setVisibility(View.VISIBLE);
            if (mWorker != null) mWorker.executeNewTask(TASK_LIKE, bundle, this);
        } else {
            onLoginDialogOpen();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.portfolio_cover_favourite:
                onLikePressed();
                break;
            case R.id.portfolio_cover_share:
                if (fileData != null)
                    if (getActivity() != null)
                        Utils.share(getActivity(), fileData.getShareUrl());
                break;
            case R.id.portfolio_cover_play:
                if (fileData != null) {
                    Utils.openVideoPlayer(getContext(), fileData.getUrl());
                }
                break;
            case R.id.portfolio_cover:
                if (fileData != null) {
                    AppCompatActivity activity = ((AppCompatActivity) getActivity());
                    if (activity != null) {
                        if (activity instanceof MainActivity) {
                            ((MainActivity) activity).openImageViewer(fileData.getUrl());
                        }
                    }
                }
                break;
            case R.id.btnViewProfile:
                onViewProfile();
                break;
            case R.id.portfolio_cover_format:
                onFavouriteOPen();
                break;
            default:
                break;
        }
    }

    public void onViewProfile() {
        if (fileData != null) {
            if (RegisterApi.isLogin(getActivity())) {
                int userid = fileData.getOwner_id();
                String url = "http://step4work.com/api/profile/get?uid=" + userid;
                AppCompatActivity activity = ((AppCompatActivity) getActivity());
                if (activity != null) {
                    if (activity instanceof MainActivity) {
                        ((MainActivity) activity).onOpenPreviewProfilePage(url);
                    }
                }
            } else {
                onLoginDialogOpen();
            }
        }
    }

    public void onFavouriteOPen() {
        if (RegisterApi.isLogin(getActivity())) {
            AppCompatActivity activity1 = ((AppCompatActivity) getActivity());
            if (activity1 != null) {
                if (activity1 instanceof MainActivity) {
                    ((MainActivity) activity1).onOpenFavouriteFragment();
                }
            }
        } else {
            onLoginDialogOpen();
        }
    }

    public void onLoginDialogOpen() {
        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        if (activity != null) {
            if (activity instanceof MainActivity) {
                ((MainActivity) activity).openLoginDialog();
            }
        }
    }
}