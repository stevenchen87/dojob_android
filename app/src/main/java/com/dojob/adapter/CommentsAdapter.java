package com.dojob.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dojob.Additions.local.CommentDatum;
import com.dojob.R;
import com.dojob.util.Utils;

import java.util.ArrayList;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.CommentHolder> {
    private final Context mContext;
    private ArrayList<CommentDatum> commentData;

    public CommentsAdapter(Context context, ArrayList<CommentDatum> commentData) {
        mContext = context;
        this.commentData = commentData;
    }

    public void setCommentData(ArrayList<CommentDatum> commentData) {
        this.commentData = commentData;
        notifyDataSetChanged();
    }

    public void setItemComment(CommentDatum commentDatu) {
        this.commentData.add(commentDatu);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CommentsAdapter.CommentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater != null ? inflater.inflate(R.layout.layout_comment_other, parent, false) : null;
        return new CommentHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentHolder holder, int position) {
        CommentDatum commentDatum = commentData.get(position);
        if (commentDatum != null) {
            Glide.with(mContext)
                    .load(commentDatum.getProfileImage())
                    .apply(RequestOptions.fitCenterTransform().dontAnimate())
                    .into(holder.commentUserImage);

            holder.userCommnet.setText(commentDatum.getName());
            holder.comment_other_msg.setText(commentDatum.getMsgBody());

            long remainingTime = (Utils.nowTimestamp() - commentDatum.getCreatedAt()) * 1000;
            holder.date.setText(Utils.getRemainingTime(remainingTime));
        }
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        if (commentData != null) {
            return commentData.size();
        }
        return 0;
    }


    class CommentHolder extends RecyclerView.ViewHolder {
        ImageView commentUserImage;
        TextView userCommnet, comment_other_msg, date;

        CommentHolder(View itemView) {
            super(itemView);
            commentUserImage = itemView.findViewById(R.id.comment_other_image);
            userCommnet = itemView.findViewById(R.id.comment_other_name);
            comment_other_msg = itemView.findViewById(R.id.comment_other_msg);
            date = itemView.findViewById(R.id.comment_other_last_reply);
        }
    }
}
