package com.dojob.adapter.model;

import android.content.Context;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dojob.R;
import com.dojob.util.ResourcesUtil;

public class PortfolioTitleBar {
    public static final int LAYOUT_ID = R.layout.layout_portfolio_title_bar;

    public static final int DEFAULT_TYPE = 1;
    public static final int ALL_ALBUMS_TYPE = 2;
    public static final int ALL_VIDEOS_TYPE = 3;
    public static final int ALL_REVIEW_TYPE = 4;
    public static final int RECRUITER_RECOMMENDED = 5;
    public static final int RECRUITER_VIEWED = 6;
    private Context mContext;
    private int mType;
    private String mTitle;
    private boolean mMore;

    public PortfolioTitleBar(int type, String title, boolean more, Context context) {
        mType = type;
        mTitle = title;
        mMore = more;
        this.mContext = context;
    }

    public PortfolioTitleBar(int type, String title,Context context) {
        mType = type;
        mTitle = title;
        this.mContext = context;
    }

    public void bindViewHolder(ViewHolder holder, int position){
        if(holder.title != null && mTitle != null){
            holder.title.setText(mTitle);
        }
        if(holder.allGallery != null) {
            holder.allGallery.setVisibility(View.GONE);
        }
        if(holder.allReviews != null){
            holder.allReviews.setVisibility(View.GONE);
        }
        switch (mType){
            case DEFAULT_TYPE:
                break;
            case ALL_ALBUMS_TYPE:
                if(holder.allGallery != null) {
                    holder.allGallery.setVisibility(View.GONE);

                }
                if(holder.allReviews != null){
                    holder.allReviews.setVisibility(View.VISIBLE);
                    holder.allReviews.setTypeface(null,Typeface.BOLD);
                    holder.allReviews.setText(mContext.getResources().getString(R.string.see_album_title));
                }
                break;
            case ALL_VIDEOS_TYPE:
                if(holder.allGallery != null) {
                    holder.allGallery.setVisibility(View.GONE);

                }
                if(holder.allReviews != null){
                    holder.allReviews.setVisibility(View.VISIBLE);
                    holder.allReviews.setTypeface(null,Typeface.BOLD);
                    holder.allReviews.setText(mContext.getResources().getString(R.string.see_videos_title));
                }
                break;
            case ALL_REVIEW_TYPE:
                if(holder.allGallery != null) {
                    holder.allGallery.setVisibility(View.GONE);                }
                if(holder.allReviews != null){
                    holder.allReviews.setVisibility(mMore?View.VISIBLE:View.GONE);
                }
                break;
            case RECRUITER_RECOMMENDED:
                holder.title.setText("Recommended Talents");
                holder.title.setTextColor(ResourcesUtil.getColor(R.color.yellow));
                holder.title.setGravity(Gravity.LEFT);
                break;
            case RECRUITER_VIEWED:
                holder.title.setText("Recently interacted with");
                holder.title.setTextColor(ResourcesUtil.getColor(R.color.yellow));
                holder.title.setGravity(Gravity.LEFT);
                break;
        }

        if(holder.allGallery != null){
            holder.allGallery.setTag(position);
        }

        if(holder.allReviews != null){
            holder.allReviews.setTag(position);
        }
    }

    public int getType(){
        return mType;
    }

    public static class ViewHolder{
        public TextView title;
        public ImageView allGallery;
        public TextView allReviews;

        public ViewHolder(View view){
            title = view.findViewById(R.id.portfolio_titlebar_name);
            allGallery = view.findViewById(R.id.portfolio_titlebar_all_gallery);
            allReviews = view.findViewById(R.id.portfolio_titlebar_all_reviews);
        }
    }
}
