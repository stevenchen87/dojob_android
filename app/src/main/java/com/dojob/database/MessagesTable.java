package com.dojob.database;

import com.dojob.backend.model.ChatRoomResponse;
import com.dojob.core.App;
import com.orm.SugarRecord;
import com.orm.dsl.Unique;

public class MessagesTable extends SugarRecord{

    public int user_id;
    public int sender_id;
    public int chatroom_id;
    public int chatroom_user_id;
    public String msg_body;
    public String status;
    public long created_at;
    public String chatroomData;
    private int isRead;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getSender_id() {
        return sender_id;
    }

    public void setSender_id(int sender_id) {
        this.sender_id = sender_id;
    }

    public int getChatroom_id() {
        return chatroom_id;
    }

    public void setChatroom_id(int chatroom_id) {
        this.chatroom_id = chatroom_id;
    }

    public String getMsg_body() {
        return msg_body;
    }

    public void setMsg_body(String msg_body) {
        this.msg_body = msg_body;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getCreated_at() {
        return created_at;
    }

    public void setCreated_at(long created_at) {
        this.created_at = created_at;
    }

    public ChatRoomResponse getChatroomData() {
        return App.gson().fromJson(chatroomData, ChatRoomResponse.class);
    }

    public void setChatroomData(ChatRoomResponse chatroomData) {
        this.chatroomData = App.gson().toJson(chatroomData);
    }

    public int getChatroom_user_id() {
        return chatroom_user_id;
    }

    public void setChatroom_user_id(int chatroom_user_id) {
        this.chatroom_user_id = chatroom_user_id;
    }

    public int getIsRead() {
        return isRead;
    }

    public void setIsRead(int isRead) {
        this.isRead = isRead;
    }
}
