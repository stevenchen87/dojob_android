package com.dojob.listener;

import android.net.Uri;

public interface CompressCallbacbkListener {
    void onStart();
    void onSuccess(Uri uri);
    void onError();
    void onProgress(float percent);
}

