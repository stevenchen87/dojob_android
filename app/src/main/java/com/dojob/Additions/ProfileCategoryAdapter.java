package com.dojob.Additions;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dojob.R;
import com.dojob.activity.CategoryActivity;
import com.dojob.fragment.ProfileSettingFragment;
import com.dojob.fragment.SearchFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.dojob.activity.MainActivity.ENABLE_TOOLBAR_TITLE;

public class ProfileCategoryAdapter extends RecyclerView.Adapter<ProfileCategoryAdapter.MyViewHolder> {


    private Activity activity;

    private String PROFILE_SETTING_FRAGMENT_TAG = "profile_setting_fragment";

    private String MAIN_FRAGMENT_TAG = "main_fragment";

    private List<Object> allCatList;

    private FragmentActivity fragmentActivity;

    public static String SUB_CAT_KEY = "sub_cat";

    public ProfileCategoryAdapter(Activity activity, List<Object> allCatList, FragmentActivity fragmentActivity) {
        this.activity = activity;
        this.allCatList = allCatList;
        this.fragmentActivity = fragmentActivity;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category,parent,false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        Object object = allCatList.get(position);

        if (object instanceof SubCategory){
            SubCategory subCategory = (SubCategory) object;
            holder.categoryItemTextView.setText(subCategory.name);
            holder.categoryItemTextView.setPadding(50,10,0,10);
            holder.textArrow.setPadding(0,0,20,0);
        }else {
            holder.categoryItemTextView.setPadding(30,10,0,10);
            CategoryWIthID categoryWIthID = (CategoryWIthID) object;
            holder.categoryItemTextView.setText(categoryWIthID.getTitle());
            holder.view.setBackgroundColor(activity.getResources().getColor(R.color.light_grey));
            holder.categoryItemTextView.setTypeface(null,Typeface.BOLD);
            holder.categoryItemTextView.setTextColor(activity.getResources().getColor(R.color.green));
            holder.textArrow.setVisibility(View.GONE);
            holder.categoryItemTextView.setTextSize(15);
        }

    }

    @Override
    public int getItemCount() {
        return allCatList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class MyViewHolder extends  RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.category_item_text_view)
        TextView categoryItemTextView;

        @BindView(R.id.total_view)
        LinearLayout view;

        @BindView(R.id.line_view)
        View lineView;

        @BindView(R.id.text_arrow)
        TextView textArrow;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            lineView.setVisibility(View.VISIBLE);
            view.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {

            int i = getAdapterPosition();
            Object object = allCatList.get(i);
            if (object instanceof SubCategory){
                SubCategory subCategory = (SubCategory) object;
                Bundle bundle = new Bundle();
                bundle.putBoolean(ENABLE_TOOLBAR_TITLE , true);
                bundle.putParcelable(SUB_CAT_KEY,subCategory);
                ((CategoryActivity)activity).OnCategoryClick(bundle);
            }
        }
    }


}
