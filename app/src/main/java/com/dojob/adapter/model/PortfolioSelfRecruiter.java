package com.dojob.adapter.model;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatRatingBar;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.dojob.GlideApp;
import com.dojob.R;
import com.dojob.activity.MainActivity;
import com.dojob.backend.LoginApi;
import com.dojob.backend.model.UserResponse;
import com.dojob.core.App;
import com.dojob.core.Logger;
import com.dojob.util.ResourcesUtil;
import com.dojob.util.Utils;
import com.squareup.picasso.Picasso;

import java.io.File;

public class PortfolioSelfRecruiter {
    public static final int LAYOUT_ID = R.layout.layout_portfolio_self_recruiter;
    private final Context mContext;
    private UserResponse mResponse;

    public PortfolioSelfRecruiter(Context context, UserResponse response) {
        mContext = context;
        mResponse = response;
    }

    public void bindViewHolder(ViewHolder holder, int position){
        if(holder.userImage != null && mResponse.profile.profile_picture != null){
            if(URLUtil.isNetworkUrl(mResponse.profile.profile_picture)){
                GlideApp.with(mContext)
                        .load(mResponse.profile.profile_picture)
                        .placeholder(R.drawable.placeholder_1)
                        .apply(RequestOptions.fitCenterTransform())
                        .skipMemoryCache(false)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.userImage);
            }
            else{
                GlideApp.with(mContext)
                        .load(Uri.fromFile(new File(mResponse.profile.profile_picture)))
                        .placeholder(R.drawable.placeholder_1)
                        .apply(RequestOptions.fitCenterTransform())
                        .skipMemoryCache(false)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.userImage);
            }
        }
        if(holder.enquiry != null){
            holder.enquiry.setTag(position);
        }
        holder.email.setVisibility(View.GONE);
        holder.bullet.setVisibility(View.GONE);
        if(LoginApi.isLoggedIn(mContext) && LoginApi.isUser(mContext, mResponse.profile.id)){
            if(holder.enquiry != null){
                holder.enquiry.setVisibility(View.GONE);
            }
            if(holder.editProfile != null){
                App.picasso().load(R.drawable.btn_edit_profile_x3).into(holder.editProfile);
                holder.editProfile.setVisibility(View.VISIBLE);
            }
            if (holder.bullet != null){
                holder.bullet.setVisibility(View.VISIBLE);
                holder.bullet.setTag(position);
            }
            holder.email.setVisibility(View.VISIBLE);
        }
        else if(!LoginApi.isLoggedIn(mContext)){
            if(holder.enquiry != null){
                holder.enquiry.getBackground().setColorFilter(ResourcesUtil.getColor(R.color.grey), PorterDuff.Mode.SRC_ATOP);
                holder.enquiry.setEnabled(false);
            }
        }
        if(holder.position != null){
            if(mResponse.profile!=null)
                if (mResponse.profile.category_info!=null)
                    if (mResponse.profile.category_info.sub_cat!=null)
                        if (mResponse.profile.category_info.sub_cat.name!=null)
                            holder.position.setText(mResponse.profile.category_info.sub_cat.name);
        }

        if (holder.tv_country != null){
            if (mResponse.profile!= null && mResponse.profile.country != null)
                holder.tv_country.setText(mResponse.profile.country);
        }
        if(holder.name != null) holder.name.setText(mResponse.profile.name);
        if(holder.email != null){
            SpannableString content = new SpannableString(mResponse.profile.email);
            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
            holder.email.setText(content);
        }

        holder.userImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mResponse.profile.profile_picture != null) {
                    String url = mResponse.profile.profile_picture;
                    ((MainActivity) mContext).openImageViewer(url);

                }
            }
        });
    }

    public static class ViewHolder{
        public ImageView userImage;
        public TextView name;
        public AppCompatButton enquiry;
        public TextView position;
        public TextView email;
        public AppCompatImageView editProfile;
        public AppCompatImageView bullet;
        public TextView tv_country;


        public ViewHolder(View view){
            userImage = view.findViewById(R.id.portfolio_self_image);
            name = view.findViewById(R.id.portfolio_self_recruiter_name);
            enquiry = view.findViewById(R.id.portfolio_self_enquiry);
            position = view.findViewById(R.id.portfolio_self_recruiter_position);
            email = view.findViewById(R.id.portfolio_self_recruiter_email);
            editProfile = view.findViewById(R.id.portfolio_self_edit_profile);
            bullet = view.findViewById(R.id.portfolio_self_list_bullet);
            tv_country = view.findViewById(R.id.tv_country);
        }
    }

}
