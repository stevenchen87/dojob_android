package com.dojob.Additions;

import android.os.Parcel;
import android.os.Parcelable;

public class CountryData implements Parcelable {
    public int id;
    public String name;
    public String code_alpha3;


    public CountryData(Parcel in) {
        id = in.readInt();
        name = in.readString();
        code_alpha3 = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(code_alpha3);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CountryData> CREATOR = new Creator<CountryData>() {
        @Override
        public CountryData createFromParcel(Parcel in) {
            return new CountryData(in);
        }

        @Override
        public CountryData[] newArray(int size) {
            return new CountryData[size];
        }
    };
}
