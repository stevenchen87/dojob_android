package com.dojob.util;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;

import java.util.Locale;

public class ResourcesUtil {

    private static Context sContext;
    private static Resources sRes;

    public static void init(Context context) {
        sContext = context;
        sRes = context.getResources();
    }

    public static String getString(String id) {
        int resId = sRes.getIdentifier(id, "string", sContext.getPackageName());
        if (resId == 0) {
            return id;
        }
        return sRes.getString(resId);
    }

    public static String getString(int id) {
        return sContext.getString(id);
    }

    public static String getLocalString(int id, String locale) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            Configuration config = new Configuration(sContext.getResources().getConfiguration());
            config.setLocale(new Locale(locale));
            return sContext.createConfigurationContext(config).getText(id).toString();
        }
        return getString(id);

    }

    public static CharSequence getText(int id) {
        return sContext.getText(id);
    }

    public static int getDrawable(String id) {
        return sRes.getIdentifier(id, "drawable", sContext.getPackageName());
    }

    public static Drawable getDrawable(int id) {
        return ContextCompat.getDrawable(sContext, id);
    }

    public static int getColor(int id) {
        if (Build.VERSION.SDK_INT >= 23) {
            return sRes.getColor(id, sContext.getTheme());
        }

        return sRes.getColor(id);
    }
}
