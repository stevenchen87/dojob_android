package com.dojob.backend.model;

public class FileResponse extends MediaResponse{
    public int linkID;
    public String linkType;
    public String fileType;
    public String caption;
    public int album;
    public String data;
    public long updated_at;
    public long created_at;
}
