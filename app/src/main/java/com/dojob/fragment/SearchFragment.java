package com.dojob.fragment;

import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dojob.R;
import com.dojob.activity.MainActivity;
import com.dojob.adapter.FeedAdapter;
import com.dojob.adapter.layout.FeedSpanSizeLookup;
import com.dojob.backend.FeedsApi;
import com.dojob.backend.SearchApi;
import com.dojob.backend.model.SearchResponse;
import com.dojob.core.BackgroundWorker;
import com.dojob.core.Logger;
import com.dojob.core.Result;
import com.dojob.util.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchFragment extends Fragment implements BackgroundWorker.Callbacks{
    public final static String QUERY = "search_toolbar_title";
    private final static String TAG = "SearchFragment";
    private final String OBJ_SEARCH_API = "obj_search_api";
    private final String TASK_REQUEST_SEARCH = "task_request_search";
    private final String TASK_REQUEST_CATS = "tas_request_cats";
    private BackgroundWorker mWorker;
    private RecyclerView mRecyclerView;
    private ProgressBar mItemProgressBar;
    private AppCompatActivity mActivity;
    private View mProgress;
    private FeedAdapter mAdapter;
    private boolean mNextPage;
    private SearchApi mSearchApi;
    private String mQuery;
    private TextView mError;
    private int ID;
    public static String SEARCH_FRAGMENT_TYPE;
    public static String SEARCH_FRAGMENT_SEARCH_TYPE = "search";
    public static String SEARCH_FRAGMENT_CATEGORY_TYPE = "cate";
    public static String CATE_NAME = "name";
    public static String CATE_ID = "id";


    private List<Map<String,String>> catList;
    private FeedsApi mFeedsApi;

    public static SearchFragment newInstance(Bundle args) {
        SearchFragment fragment = new SearchFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getActivity() != null){
            mWorker = new BackgroundWorker(getActivity());
            mSearchApi = (SearchApi) mWorker.get(OBJ_SEARCH_API);
            if (mSearchApi == null) {
                mSearchApi = new SearchApi(getContext());
                mWorker.put(OBJ_SEARCH_API, mSearchApi);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        Utils.hideKeyBoard(getActivity());
        AppCompatActivity activity = ((AppCompatActivity)getActivity());
        if(activity != null) {
            ActionBar actionbar = activity.getSupportActionBar();
            if (actionbar != null) {
                actionbar.setDisplayHomeAsUpEnabled(true);
            }
            if(activity instanceof MainActivity && getArguments() != null){
                if (getArguments().getString(SEARCH_FRAGMENT_TYPE)==null){
                    mQuery = getArguments().getString(QUERY);
                    ((MainActivity) activity).setTitle(mQuery);
                }else if (getArguments().getString(SEARCH_FRAGMENT_TYPE ).equals(SEARCH_FRAGMENT_CATEGORY_TYPE)){
                    ID = getArguments().getInt(CATE_ID,1);
                    ((MainActivity) activity).setTitle("Search Category");
                }else {
                    mQuery = getArguments().getString(QUERY);
                    ((MainActivity) activity).setTitle(mQuery);
                }
            }
        }
        mError = view.findViewById(R.id.error);
        mProgress = view.findViewById(R.id.progress);
        mProgress.setVisibility(View.VISIBLE);
        mRecyclerView = view.findViewById(R.id.recycler_view);
        mRecyclerView.setItemViewCacheSize(20);
        mRecyclerView.setDrawingCacheEnabled(true);
        mRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        mRecyclerView.setVisibility(View.GONE);

        if (getArguments().getString(SEARCH_FRAGMENT_TYPE)==null){
            mAdapter = new FeedAdapter(getActivity(),activity.getSupportFragmentManager(),0,null);
        }else if (getArguments().getString(SEARCH_FRAGMENT_TYPE ).equals(SEARCH_FRAGMENT_CATEGORY_TYPE)){
            String title;
            if (getArguments().getString(CATE_NAME)!=null){
                title = getArguments().getString(CATE_NAME);
            }else {
                title = " ";
            }
            mAdapter = new FeedAdapter(getActivity(),activity.getSupportFragmentManager(),1,title);
        }else {
            mAdapter = new FeedAdapter(getActivity(),activity.getSupportFragmentManager(),0,null);
        }

        mAdapter.setListener(this.getActivity());
        mAdapter.setHasStableIds(true);
        mRecyclerView.setAdapter(mAdapter);
//        mRecyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
//            @Override
//            public void onLoadMore() {
//                if(mNextPage){
//                    if(mWorker != null) mWorker.executeNewTask(TASK_REQUEST_SEARCH, null, SearchFragment.this);
//                }
//            }
//        });
        if (getArguments().getString(SEARCH_FRAGMENT_TYPE)==null){
            GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 4);
            gridLayoutManager.setSpanSizeLookup(new FeedSpanSizeLookup(mAdapter));
            mRecyclerView.setLayoutManager(gridLayoutManager);
        }else if (getArguments().getString(SEARCH_FRAGMENT_TYPE ).equals(SEARCH_FRAGMENT_CATEGORY_TYPE)){
            GridLayoutManager mLayoutManager = new GridLayoutManager(getContext(), 4);
            mLayoutManager.setSpanSizeLookup(new FeedSpanSizeLookup(mAdapter));
            mRecyclerView.setLayoutManager(mLayoutManager);
//            mRecyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
//            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mRecyclerView.setLayoutManager(mLayoutManager);
        }else {
            GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 4);
            gridLayoutManager.setSpanSizeLookup(new FeedSpanSizeLookup(mAdapter));
            mRecyclerView.setLayoutManager(gridLayoutManager);
        }
        if (getArguments().getString(SEARCH_FRAGMENT_TYPE)==null){
            if(mWorker != null) mWorker.executeNewTask(TASK_REQUEST_SEARCH, null, this);
        }else if (getArguments().getString(SEARCH_FRAGMENT_TYPE ).equals(SEARCH_FRAGMENT_CATEGORY_TYPE)){
            if (mWorker!=null) mWorker.executeNewTask(TASK_REQUEST_CATS,null,this);
        }else {
            if(mWorker != null) mWorker.executeNewTask(TASK_REQUEST_SEARCH, null, this);
        }

        return view;
    }
    private void addCats(){
        Map<String, String> map = new HashMap<String, String>();
        map.put("foo", "bar");
        catList.add(map);
        map.put("iuashd","iuasbx");
        catList.add(map);
        map.put("ash","wjqhx");
        catList.add(map);
    }

    @Override
    public Result executeTaskInBackground(String id, Bundle args) {
        switch (id){
            case TASK_REQUEST_SEARCH:
                return mSearchApi.request(mQuery);
            case TASK_REQUEST_CATS:
                return mSearchApi.SearchRequest(ID);

        }
        return null;
    }

    @Override
    public void onBackgroundTaskCompleted(String id, Result result) {
        switch (id){
            case TASK_REQUEST_SEARCH:
                if(result.isSuccess() && mAdapter != null){
                    SearchResponse response = (SearchResponse) result.getResult();
                    if(response.status.equals("ok")){
                        if(response.nameSearch.length > 0 || response.categorySearch.length > 0){
                            mAdapter.updateItem(response);
                            mRecyclerView.setVisibility(View.VISIBLE);
                            mProgress.setVisibility(View.GONE);
                            mError.setVisibility(View.GONE);
                        }
                        else{
                            mError.setText("No result found");
                            mError.setVisibility(View.VISIBLE);
                            mRecyclerView.setVisibility(View.GONE);
                            mProgress.setVisibility(View.GONE);
                        }
                    }
                }
                break;
            /*case TASK_REQUEST_CATS:
                if (result.isSuccess() && mAdapter != null){
                    CategoryResponse response = (CategoryResponse) result.getResult();
                    String[] responseList = response.catData;
                    if (responseList.length>0){
                        for(int i=0;i<responseList.length;i++){
                            catList.add(responseList[i]);
                        }
                    }
                    Toast.makeText(getContext(),String.valueOf(responseList.length),Toast.LENGTH_SHORT).show();
                    catList.addAll(Arrays.asList(responseList));
                    categoryAdapter.notifyDataSetChanged();

                }*/
            case TASK_REQUEST_CATS:
                if(result.isSuccess() && mAdapter != null){
                    SearchResponse resposne = (SearchResponse) result.getResult();
                    if(resposne.status.equals("ok")){
                        if(resposne.categorySearch.length > 0){
//                            mNextPage = resposne.nextPage;
//                            mAdapter.updateItem(resposne.search_result, mNextPage);
                            mAdapter.updateItem(resposne);
                            mRecyclerView.setVisibility(View.VISIBLE);
                            mProgress.setVisibility(View.GONE);
                            mError.setVisibility(View.GONE);
                        }
                        else{
                            mError.setText("No result found");
                            mError.setVisibility(View.VISIBLE);
                            mRecyclerView.setVisibility(View.GONE);
                            mProgress.setVisibility(View.GONE);
                        }
                    }
                }
        }
    }
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

}
