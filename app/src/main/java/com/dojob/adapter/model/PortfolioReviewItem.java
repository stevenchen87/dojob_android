package com.dojob.adapter.model;

import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.text.format.DateUtils;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import com.dojob.R;
import com.dojob.backend.model.Review;
import com.dojob.util.Utils;

import java.text.SimpleDateFormat;

public class PortfolioReviewItem {
    public static final int LAYOUT_ID = R.layout.layout_portfolio_reviews_item;
    private Review mReviewResponse;
    private boolean mWrapHeight;

    public PortfolioReviewItem(Review reviewResponse) {
        mReviewResponse = reviewResponse;
    }

    public PortfolioReviewItem(Review reviewResponse, boolean wrapHeight) {
        mReviewResponse = reviewResponse;
        mWrapHeight = wrapHeight;
    }

    public void bindViewHolder(final ViewHolder holder, int position){
        if(holder.container != null){
            if(mWrapHeight){
                RecyclerView.LayoutParams params = (RecyclerView.LayoutParams)holder.container.getLayoutParams();
                params.width = RecyclerView.LayoutParams.MATCH_PARENT;
                params.height = RecyclerView.LayoutParams.WRAP_CONTENT;
                params.leftMargin = Utils.dpToPx(8);
                params.rightMargin = Utils.dpToPx(8);
                params.topMargin = Utils.dpToPx(4);
                params.bottomMargin = Utils.dpToPx(4);
                holder.container.setLayoutParams(params);
                holder.container.setMinimumHeight(0);
            }
            holder.container.setTag(position);
        }
        if(holder.reviewTitle != null){
            if(mReviewResponse.title != null){
                holder.reviewTitle.setText(mReviewResponse.title);
                holder.reviewTitle.setVisibility(View.VISIBLE);
            }
            else{
                holder.reviewTitle.setVisibility(View.GONE);
            }

        }
        if(holder.reviewRating != null){
            holder.reviewRating.setRating(mReviewResponse.rating);
        }
        if(holder.reviewDate != null){
            String value = new SimpleDateFormat("hh:mm a").format(new java.util.Date(mReviewResponse.created_at * 1000));
            if(!DateUtils.isToday(mReviewResponse.created_at * 1000)){
                value = new SimpleDateFormat("d MMM").format(new java.util.Date(mReviewResponse.created_at * 1000));
            }
            holder.reviewDate.setText(value);
        }
        if(holder.reviewReviewer != null){
            holder.reviewReviewer.setText(mReviewResponse.review_by_user.name);
        }
        if(holder.reviewContent != null){
            holder.reviewContent.setText(mReviewResponse.review);
        }
        if(holder.reviewMore != null){
            final ViewTreeObserver vto = holder.reviewContent.getViewTreeObserver();
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    Layout l = holder.reviewContent .getLayout();
                    if ( l != null){
                        int lines = l.getLineCount();
                        if ( lines > 0)
                            holder.reviewMore.setVisibility(( ( l.getEllipsisCount(lines-1) > 0))?View.VISIBLE:View.INVISIBLE);
                    }
                    holder.reviewContent.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });
            holder.reviewMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(holder.reviewContent != null){
                        holder.reviewContent.setMaxLines(Integer.MAX_VALUE);
                        holder.reviewMore.setVisibility(View.INVISIBLE);
                    }
                }
            });
        }
    }

    public Review getReview(){
        return mReviewResponse;
    }

    public static class ViewHolder{
        public View container;
        public TextView reviewTitle;
        public AppCompatRatingBar reviewRating;
        public TextView reviewDate;
        public TextView reviewReviewer;
        public TextView reviewContent;
        public AppCompatButton reviewMore;

        public ViewHolder(View view){
            container = view.findViewById(R.id.review_container);
            reviewTitle = view.findViewById(R.id.portfolio_reviews_title);
            reviewRating = view.findViewById(R.id.portfolio_reviews_rating);
            reviewDate = view.findViewById(R.id.portfolio_reviews_date);
            reviewReviewer = view.findViewById(R.id.portfolio_reviews_reviewer);
            reviewContent = view.findViewById(R.id.portfolio_reviews_content);
            reviewMore = view.findViewById(R.id.portfolio_reviews_more);
        }
    }
}
