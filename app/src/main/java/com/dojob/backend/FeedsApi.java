package com.dojob.backend;

import android.content.Context;

import com.dojob.backend.model.CategoryResponse;
import com.dojob.backend.model.FeedsResponse;
import com.dojob.core.App;
import com.dojob.core.Http;
import com.dojob.core.Result;
import com.dojob.core.Error;
import com.dojob.util.Utils;
import com.google.gson.JsonSyntaxException;

import java.util.HashMap;
import java.util.Map;

import okhttp3.internal.Util;

public class FeedsApi {
    public final static String FEEDS_TAG = "feeds_tag";
    private final static String CAT_TAG = "category_tag";
    public final static String TALENT_TAG = "talent_tag";
    private final Context mContext;

    public FeedsApi(Context context) {
        mContext = context;
    }

    public Result request() {
        FeedsResponse response = null;
        if (!Utils.isOnline(mContext)) {
            return new Result(Error.OFFLINE);
        }
        Map<String, String> params = new HashMap<>();
        Http.Request request = Http.request(mContext)
                .url("http://step4work.com/api/home")//.url("https://www.dropbox.com/s/ocugljvfo2msx5u/feeds_response.json?dl=1")
                .tag(FEEDS_TAG);
        Result result = request.send();
        if (result.isSuccess()) {
            String responseText = (String) result.getResult();

            try {
                response = App.gson().fromJson(responseText, FeedsResponse.class);
            } catch (JsonSyntaxException ex) {
                ex.printStackTrace();
            }
            if (response != null) {
                return new Result(null, response);
            }
            return new Result(Error.DATA);
        }

        return new Result(Error.NETWORK);
    }

    public Result requestTalent(String url) {
        String tempUrl = (url == null)?"http://step4work.com/api/talent/all?page=1":url;
        FeedsResponse response = null;
        if (!Utils.isOnline(mContext)) {
            return new Result(Error.OFFLINE);
        }
        Http.Request request = Http.request(mContext)
                .url(tempUrl)
                .tag(TALENT_TAG);
        Result result = request.send();
        if (result.isSuccess()) {
            String responseText = (String) result.getResult();

            try {
                response = App.gson().fromJson(responseText, FeedsResponse.class);
            } catch (JsonSyntaxException ex) {
                ex.printStackTrace();
            }
            if (response != null) {
                return new Result(null, response);
            }
            return new Result(Error.DATA);
        }

        return new Result(Error.NETWORK);
    }

    public Result catRequest() {
        CategoryResponse response = null;
        if (!Utils.isOnline(mContext)) {
            return new Result(Error.OFFLINE);
        }
        Http.Request request = Http.request(mContext)
                .url("http://step4work.com/api/getSearchPage")
                .tag(CAT_TAG);
        Result result = request.send();
        if (result.isSuccess()) {
            String responseText = (String) result.getResult();

            try {
                response = App.gson().fromJson(responseText, CategoryResponse.class);
            } catch (JsonSyntaxException ex) {
                ex.printStackTrace();
            }
            if (response != null) {
                return new Result(null, response);
            }

            return new Result(Error.DATA);
        }
        return new Result(Error.NETWORK);
    }

}
