package com.dojob.listener;

import android.net.Uri;

public interface SocialItemClickListener {
    void onSocialItemClick(String fileType, int fileId);
}

