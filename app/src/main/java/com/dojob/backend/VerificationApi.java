package com.dojob.backend;

import android.content.Context;

import com.dojob.backend.model.FeedsResponse;
import com.dojob.backend.model.Response;
import com.dojob.core.App;
import com.dojob.core.Error;
import com.dojob.core.Http;
import com.dojob.core.Result;
import com.dojob.util.Utils;
import com.google.gson.JsonSyntaxException;

import java.util.HashMap;
import java.util.Map;

public class VerificationApi {
    private final static String VERIFICATION_CONFIRM_TAG = "confirm_tag";
    private final static String VERIFICATION_RESEND_TAG = "resend_tag";
    private final Context mContext;

    public VerificationApi(Context context) {
        mContext = context;
    }

    public Result confirm(String code) {
        Response response = null;
        if (!Utils.isOnline(mContext)) {
            return new Result(Error.OFFLINE);
        }
        Map<String, String> params = new HashMap<>();
        params.put("code", code);
        Http.Request request = Http.request(mContext)
                .url("http://step4work.com/api/verify_account")
                .post(params)
                .tag(VERIFICATION_CONFIRM_TAG);
        Result result = request.send();
        if (result.isSuccess()) {
            String responseText = (String) result.getResult();

            try {
                response = App.gson().fromJson(responseText, Response.class);
            } catch (JsonSyntaxException ex) {
                ex.printStackTrace();
            }
            if (response != null) {
                return new Result(null, response);
            }
            return new Result(Error.DATA);
        }

        return new Result(Error.NETWORK);
    }

    public Result resend() {
        Response response = null;
        if (!Utils.isOnline(mContext)) {
            return new Result(Error.OFFLINE);
        }
        Http.Request request = Http.request(mContext)
                .url("http://step4work.com/api/resend_verification_code")
                .post(new HashMap<String, String>())
                .tag(VERIFICATION_RESEND_TAG);
        Result result = request.send();
        if (result.isSuccess()) {
            String responseText = (String) result.getResult();

            try {
                response = App.gson().fromJson(responseText, Response.class);
            } catch (JsonSyntaxException ex) {
                ex.printStackTrace();
            }
            if (response != null) {
                return new Result(null, response);
            }
            return new Result(Error.DATA);
        }

        return new Result(Error.NETWORK);
    }

}
