package com.dojob.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.dojob.Additions.local.SocialFeed;
import com.dojob.Additions.local.SocialFeedParent;
import com.dojob.GlideApp;
import com.dojob.R;
import com.dojob.listener.SocialItemClickListener;

import java.util.ArrayList;
import java.util.Random;


public class SocialFeedAdapter2 extends RecyclerView.Adapter<SocialFeedAdapter2.ViewHolder> {
    private ArrayList<SocialFeedParent> mDataSet;
    private Context mContext;
    private Random mRandom = new Random();
    private static final int TYPE_NORMAL = 0;
    private static final int TYPE_LEFT = 1;
    private static final int TYPE_RIGHT = 2;

    private String ALL_FILTER = "all";
    private String IMAGE_FILTER = "image";
    private String VIDEO_FILTER = "video";
    private String SELECTED_FILTER = ALL_FILTER;
    private SocialItemClickListener socialItemListner;

    public SocialFeedAdapter2(Context context, ArrayList<SocialFeedParent> mDataSet, String SELECTED_FILTER) {
        this.mDataSet = mDataSet;
        mContext = context;
        this.SELECTED_FILTER = SELECTED_FILTER;
    }

    public void resetAdapterData(ArrayList<SocialFeedParent> mDataSet, String SELECTED_FILTER) {
        this.mDataSet = mDataSet;
        this.SELECTED_FILTER = SELECTED_FILTER;
        notifyDataSetChanged();
    }

    public void updateAdapterData(ArrayList<SocialFeedParent> mDataSet, String SELECTED_FILTER) {
        this.mDataSet.addAll(mDataSet);
        this.SELECTED_FILTER = SELECTED_FILTER;
        notifyDataSetChanged();
    }

    public void setSocialItemListner(SocialItemClickListener socialItemListner) {
        this.socialItemListner = socialItemListner;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image1, image2, image3;
        LinearLayout video_indicator1, video_indicator2, video_indicator3;
        FrameLayout frameLayout1, frameLayout2, frameLayout3;
        LinearLayout layoutParent;

        public ViewHolder(View v) {
            super(v);
            image1 = (ImageView) v.findViewById(R.id.image1);
            image2 = (ImageView) v.findViewById(R.id.image2);
            image3 = (ImageView) v.findViewById(R.id.image3);
            video_indicator1 = (LinearLayout) v.findViewById(R.id.portfolio_cover_play1);
            video_indicator2 = (LinearLayout) v.findViewById(R.id.portfolio_cover_play2);
            video_indicator3 = (LinearLayout) v.findViewById(R.id.portfolio_cover_play3);
            frameLayout1 = (FrameLayout) v.findViewById(R.id.frameLayout1);
            frameLayout2 = (FrameLayout) v.findViewById(R.id.frameLayout2);
            frameLayout3 = (FrameLayout) v.findViewById(R.id.frameLayout3);
            layoutParent = (LinearLayout) v.findViewById(R.id.layoutParent);
        }
    }

    @Override
    public SocialFeedAdapter2.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_NORMAL) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_social_normal, parent, false);
            return new ViewHolder(v);
        } else if (viewType == TYPE_LEFT) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_social_left, parent, false);
            return new ViewHolder(v);
        } else if (viewType == TYPE_RIGHT) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_social_right, parent, false);
            return new ViewHolder(v);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        SocialFeedParent socialFeedParent = mDataSet.get(position);
        if (socialFeedParent.getArrayList().size() > 0 && socialFeedParent.getArrayList().get(0) != null) {
            holder.frameLayout1.setTag(socialFeedParent.getArrayList().get(0));
            if (socialFeedParent.getArrayList().get(0).getFileType().equalsIgnoreCase("image")) {
                GlideApp.with(mContext)
                        .load(socialFeedParent.getArrayList().get(0).getUrl())
                        .placeholder(R.drawable.placeholder_1)
                        .apply(RequestOptions.fitCenterTransform())
                        .skipMemoryCache(false)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.image1);
                holder.video_indicator1.setVisibility(View.GONE);
            } else {
                GlideApp.with(mContext)
                        .load(socialFeedParent.getArrayList().get(0).getThumbnail())
                        .placeholder(R.drawable.placeholder_1)
                        .apply(RequestOptions.fitCenterTransform())
                        .skipMemoryCache(false)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.image1);
                holder.video_indicator1.setVisibility(View.VISIBLE);
            }
        }

        if (socialFeedParent.getArrayList().size() > 1 && socialFeedParent.getArrayList().get(1) != null) {
            holder.frameLayout2.setTag(socialFeedParent.getArrayList().get(1));
            if (socialFeedParent.getArrayList().get(1).getFileType().equalsIgnoreCase("image")) {
                GlideApp.with(mContext)
                        .load(socialFeedParent.getArrayList().get(1).getUrl())
                        .placeholder(R.drawable.placeholder_1)
                        .apply(RequestOptions.fitCenterTransform())
                        .skipMemoryCache(false)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.image2);
                holder.video_indicator2.setVisibility(View.GONE);
            } else {
                GlideApp.with(mContext)
                        .load(socialFeedParent.getArrayList().get(1).getThumbnail())
                        .placeholder(R.drawable.placeholder_1)
                        .apply(RequestOptions.fitCenterTransform())
                        .skipMemoryCache(false)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.image2);
                holder.video_indicator2.setVisibility(View.VISIBLE);
            }
        }

        if (socialFeedParent.getArrayList().size() > 2 && socialFeedParent.getArrayList().get(2) != null) {
            holder.frameLayout3.setTag(socialFeedParent.getArrayList().get(2));
            if (socialFeedParent.getArrayList().get(2).getFileType().equalsIgnoreCase("image")) {
                GlideApp.with(mContext)
                        .load(socialFeedParent.getArrayList().get(2).getUrl())
                        .placeholder(R.drawable.placeholder_1)
                        .apply(RequestOptions.fitCenterTransform())
                        .skipMemoryCache(false)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.image3);
                holder.video_indicator3.setVisibility(View.GONE);
            } else {
                GlideApp.with(mContext)
                        .load(socialFeedParent.getArrayList().get(2).getThumbnail())
                        .placeholder(R.drawable.placeholder_1)
                        .apply(RequestOptions.fitCenterTransform())
                        .skipMemoryCache(false)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.image3);
                holder.video_indicator3.setVisibility(View.VISIBLE);
            }
        }

        holder.frameLayout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getTag() != null) {
                    SocialFeed socialFeed = (SocialFeed) v.getTag();
                    if (socialItemListner != null && socialFeed != null) {
                        socialItemListner.onSocialItemClick(socialFeed.getFileType(), socialFeed.getId());
                    }
                }
            }
        });

        holder.frameLayout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getTag() != null) {
                    SocialFeed socialFeed = (SocialFeed) v.getTag();
                    if (socialItemListner != null && socialFeed != null) {
                        socialItemListner.onSocialItemClick(socialFeed.getFileType(), socialFeed.getId());
                    }
                }
            }
        });

        holder.frameLayout3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getTag() != null) {
                    SocialFeed socialFeed = (SocialFeed) v.getTag();
                    if (socialItemListner != null && socialFeed != null) {
                        socialItemListner.onSocialItemClick(socialFeed.getFileType(), socialFeed.getId());
                    }
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (SELECTED_FILTER.equalsIgnoreCase(ALL_FILTER)) {
            if (position == 0 || position % 6 == 0) {
                return TYPE_RIGHT;
            } else if (position == 3 || position % 6 == 3) {
                return TYPE_LEFT;
            } else {
                return TYPE_NORMAL;
            }
        } else if (SELECTED_FILTER.equalsIgnoreCase(IMAGE_FILTER) ||
                SELECTED_FILTER.equalsIgnoreCase(VIDEO_FILTER)) {
            return TYPE_NORMAL;
        }
        return TYPE_NORMAL;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

//    // Custom method to get a random number between a range
//    protected int getRandomIntInRange(int max, int min) {
//        return mRandom.nextInt((max - min) + min) + min;
//    }
//
//    // Custom method to generate random HSV color
//    protected int getRandomHSVColor() {
//        // Generate a random hue value between 0 to 360
//        int hue = mRandom.nextInt(361);
//        // We make the color depth full
//        float saturation = 1.0f;
//        // We make a full bright color
//        float value = 1.0f;
//        // We avoid color transparency
//        int alpha = 255;
//        // Finally, generate the color
//        int color = Color.HSVToColor(alpha, new float[]{hue, saturation, value});
//        // Return the color
//        return color;
//    }
}