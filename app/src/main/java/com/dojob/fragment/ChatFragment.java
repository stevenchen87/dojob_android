package com.dojob.fragment;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.dojob.Additions.ApiInterface;
import com.dojob.Additions.ServiceGenerator;
import com.dojob.Additions.local.ForgotPassResult;
import com.dojob.Additions.posts.RatingPost;
import com.dojob.Additions.responses.WebResponseOk;
import com.dojob.R;
import com.dojob.activity.MainActivity;
import com.dojob.adapter.InboxAdapter;
import com.dojob.backend.InboxApi;
import com.dojob.backend.LoginApi;
import com.dojob.backend.ProfileApi;
import com.dojob.backend.RegisterApi;
import com.dojob.backend.model.ChatRoomResponse;
import com.dojob.backend.model.ChatStatuResponse;
import com.dojob.backend.model.InboxMessageResponse;
import com.dojob.backend.model.InboxResponse;
import com.dojob.backend.model.MessageResponse;
import com.dojob.backend.model.Review;
import com.dojob.core.App;
import com.dojob.core.BackgroundWorker;
import com.dojob.core.Logger;
import com.dojob.core.Result;
import com.dojob.util.AndroidBug5497Workaround;
import com.dojob.util.AppSP;
import com.dojob.util.ResourcesUtil;
import com.dojob.util.Utils;
import com.github.ybq.android.spinkit.SpinKitView;

import me.leolin.shortcutbadger.ShortcutBadger;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatFragment extends Fragment implements BackgroundWorker.Callbacks, View.OnClickListener, MainActivity.ToolBarClickedCallback {
    private final String TAG = "ChatFragment";
    private final String OBJ_MESSAGE_API = "obj_message_api";
    private final String TASK_REQUEST_MESSAGE = "task_request_message";
    private final String TASK_SEND_MESSAGE = "task_send_message";
    private final String TASK_JOB_DONE = "task_job_done";
    public static final String USER_ID = "chat_user_id";
    public static final String TITLE = "chat_title";
    private final String TASK_REVIEW = "task_review";
    public static final String MESSAGES_RESPONSE = "chat_messages_response";
    private String mTitle;
    private InboxMessageResponse[] mMessageResponse;
    private InboxAdapter mAdapter;
    private BackgroundWorker mWorker;
    private InboxApi mInboxApi;
    private RecyclerView mRecyclerView;
    private SpinKitView mProgress;
    private View mChatSend;
    private EditText mMessageBody;
    private int mId;
    private String mChatRoomStatus;

    public static ChatFragment newInstance(Bundle args) {
        ChatFragment fragment = new ChatFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null) {
            mWorker = new BackgroundWorker(getActivity());
            mInboxApi = (InboxApi) mWorker.get(OBJ_MESSAGE_API);
            if (mInboxApi == null) {
                mInboxApi = new InboxApi(getContext());
                mWorker.put(OBJ_MESSAGE_API, mInboxApi);
            }
            Bundle args = getArguments();
            if (args != null) {
                mId = args.getInt(USER_ID, 0);
                mTitle = args.getString(TITLE, "");
                if (getArguments().containsKey(MESSAGES_RESPONSE)) {
                    mMessageResponse = App.gson().fromJson(getArguments().getString(MESSAGES_RESPONSE), InboxMessageResponse[].class);
                } else {
                    mMessageResponse = mInboxApi.getMessages(mId);
                }
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_inbox_chat_room, container, false);
        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        if (activity != null) {
            ActionBar actionbar = activity.getSupportActionBar();
            if (actionbar != null) {
                actionbar.setDisplayHomeAsUpEnabled(true);
                actionbar.setDisplayShowHomeEnabled(true);
            }
            if (activity instanceof MainActivity) {
                ((MainActivity) activity).setTitle(mTitle);
            }
            mProgress = view.findViewById(R.id.progress);
            mProgress.setVisibility(View.GONE);
            mRecyclerView = view.findViewById(R.id.recycle_view);
            mRecyclerView.setItemViewCacheSize(20);
            mRecyclerView.setNestedScrollingEnabled(false);
            mRecyclerView.setDrawingCacheEnabled(true);
            mRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
            mAdapter = new InboxAdapter(getContext());
            mAdapter.setHasStableIds(true);
            mRecyclerView.setAdapter(mAdapter);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
            linearLayoutManager.setReverseLayout(true);
            linearLayoutManager.setStackFromEnd(true);
            mRecyclerView.setLayoutManager(linearLayoutManager);
            mChatSend = view.findViewById(R.id.chat_send);
            mMessageBody = view.findViewById(R.id.chat_message);
            updateKeyboard();
            if (mMessageResponse == null)
                if (mWorker != null) mWorker.executeNewTask(TASK_REQUEST_MESSAGE, null, this);


        }
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setToolbarStatus("");
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE
                | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        if (mMessageResponse != null) {
//            if (mMessageResponse.length > 0)
//                setToolbarStatus(mMessageResponse[0].chatroomData.review_status);
            mAdapter.updateItem(mMessageResponse);
            mRecyclerView.scrollToPosition(0);

            if (mMessageResponse.length > 0)
                getChatApi(mMessageResponse[0].chatroom_id + "");
        }

        if (getActivity() != null) {
            AppSP.getInstance(getActivity()).savePreferences(Utils.NOFICATION_NUMBER, 0);
            ShortcutBadger.removeCount(getActivity());
        }
        updateRecylcerview();
    }

    private void updateRecylcerview() {
        if (Build.VERSION.SDK_INT >= 11) {
            mRecyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                @Override
                public void onLayoutChange(View v,
                                           int left, int top, int right, int bottom,
                                           int oldLeft, int oldTop, int oldRight, int oldBottom) {
                    if (bottom < oldBottom) {
                        mRecyclerView.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mRecyclerView.smoothScrollToPosition(0);
                            }
                        }, 100);
                    }
                }
            });
        }
    }

    private void updateKeyboard() {
        mMessageBody.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    mRecyclerView.scrollToPosition(0);
                } else {
                    mRecyclerView.scrollToPosition(0);
                }
            }
        });
    }

    private void getChatApi(String chatRoomID) {
        ServiceGenerator serviceGenerator = new ServiceGenerator();
        ApiInterface apiInterface = serviceGenerator.createService(ApiInterface.class);
        String token = "Bearer " + RegisterApi.getToken(getActivity());
        retrofit2.Call<ChatStatuResponse> call = apiInterface.getChatRoomStatu(token, chatRoomID);
        call.enqueue(new Callback<ChatStatuResponse>() {
            @Override
            public void onResponse(retrofit2.Call<ChatStatuResponse> call, Response<ChatStatuResponse> response) {
                if (response.isSuccessful()) {
                    Log.i("RESPONSE", "OKAY");
                    if (response.body() != null) {
                        ChatRoomResponse chatRoomResponse = response.body().getChatroomData();
                        Log.i("ChatRoom", "Review Status : " + chatRoomResponse.review_status);
                        setToolbarStatus(chatRoomResponse.review_status);

                    }

                } else {
                    Log.i("RESPONSE", response.message());
                    if (getActivity() != null)
                        Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<ChatStatuResponse> call, Throwable t) {
                Log.i("RESPONSE", t.getMessage());
                if (getActivity() != null)
                    Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mChatSend.setOnClickListener(this);
        if (getActivity() != null) ((MainActivity) getActivity()).setChatToolbarCallback(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mChatSend.setOnClickListener(null);
        if (getActivity() != null) ((MainActivity) getActivity()).setChatToolbarCallback(null);
        for (InboxMessageResponse inboxMessageResponse : mMessageResponse) {
            mInboxApi.updateReadStatus(inboxMessageResponse);
        }
    }

    @Override
    public Result executeTaskInBackground(String id, Bundle args) {
        switch (id) {
            case TASK_REQUEST_MESSAGE:
                return mInboxApi.getMessage();
            case TASK_SEND_MESSAGE:
                return mInboxApi.sendMessage(args.getString("message"), String.valueOf(mId));
            case TASK_JOB_DONE:
                return mInboxApi.requestJobDone(String.valueOf(mMessageResponse[0].chatroom_id));
        }
        return null;
    }

    @Override
    public void onBackgroundTaskCompleted(String id, Result result) {
        switch (id) {
            case TASK_REQUEST_MESSAGE:
                mProgress.setVisibility(View.GONE);
                if (result.isSuccess() && mAdapter != null) {
                    InboxResponse[] response = (InboxResponse[]) result.getResult();
                    if (response.length > 0) {
                        for (InboxResponse inbox : response) {
                            if (inbox.messages.length > 0) {
                                if (mId > 0 && inbox.messages[0].chatroomData.id == mId) {
                                    setToolbarStatus(inbox.messages[0].chatroomData.review_status);
                                    mAdapter.updateItem(inbox.messages);
                                    mRecyclerView.scrollToPosition(0);
                                    for (InboxMessageResponse inboxMessageResponse : inbox.messages) {
                                        mInboxApi.updateReadStatus(inboxMessageResponse);
                                    }
                                }
                            }
                        }
                    }
                }
                break;
            case TASK_SEND_MESSAGE:
                if (result.isSuccess() && mAdapter != null) {
                    InboxResponse[] response = (InboxResponse[]) result.getResult();
                    int userId = (mMessageResponse != null && mMessageResponse.length > 0) ? mMessageResponse[0].chatroomData.id : mId;
                    mProgress.setVisibility(View.GONE);
                    for (InboxResponse inbox : response) {
                        if (inbox.messages.length > 0 && userId > 0 && inbox.messages[0].chatroomData.id == userId) {
                            setToolbarStatus(inbox.messages[0].chatroomData.review_status);
                            mAdapter.updateItem(inbox.messages);
                            mRecyclerView.scrollToPosition(0);
                            return;
                        }
                    }
                }
                break;
            case TASK_JOB_DONE:
                if (result.isSuccess()) {
                    InboxMessageResponse response = (InboxMessageResponse) result.getResult();
                    if (response.status.equals("ok")) {
                        setToolbarStatus(response.chatroomData.review_status);
                    }
                }
                break;
        }
    }

    public void setToolbarStatus(String status) {
        mChatRoomStatus = status;
        String statusButton = "";
        if (status.equals("prompt_job_done")) {
            statusButton = "Job Done";
        } else if (status.equals("prompt_review")) {
            statusButton = "Review";
        } else if (status.equals("complete")) {
            statusButton = "Complete";
        }

        if (getActivity() != null)
            ((MainActivity) getActivity()).setChatToolbarStatus(statusButton);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.chat_send:
                // called InboxApi.sendMessage
                mProgress.setVisibility(View.VISIBLE);
                if (mMessageBody.getText().length() > 0) {
                    Bundle bundle = new Bundle();
                    bundle.putString("message", mMessageBody.getText().toString());
                    if (mWorker != null) mWorker.executeNewTask(TASK_SEND_MESSAGE, bundle, this);
                    mMessageBody.getText().clear();
                    if (getActivity() != null) Utils.hideKeyboard(getActivity());
                }
                break;
        }
    }

    @Override
    public void onRefreshClicked() {
        mProgress.setVisibility(View.VISIBLE);
        if (mWorker != null) mWorker.executeNewTask(TASK_REQUEST_MESSAGE, null, this);
    }

    @Override
    public void onChatButtonClicked() {
        if (mChatRoomStatus != null) {
            if (mChatRoomStatus.equals("prompt_job_done")) {
                if (mWorker != null) mWorker.executeNewTask(TASK_JOB_DONE, null, this);
            } else if (mChatRoomStatus.equals("prompt_review")) {
                showRatingDialog();
//                if (getActivity() != null)
//                    ((MainActivity) getActivity()).onOpenPreviewProfilePage(ProfileApi.getUserProfile(String.valueOf(mId)));
            }
        }
    }

    private void apiSubmitRating(String reviews, int rating) {

        mProgress.setVisibility(View.VISIBLE);
        RatingPost ratingPost = new RatingPost();
        ratingPost.setRating(rating);
        ratingPost.setReview(reviews);
        ratingPost.setTargetId(mId);

        ServiceGenerator serviceGenerator = new ServiceGenerator();
        ApiInterface apiInterface = serviceGenerator.createService(ApiInterface.class);
        String token = "Bearer " + RegisterApi.getToken(getActivity());
        retrofit2.Call<WebResponseOk> call = apiInterface.submitRating(token, ratingPost);
        call.enqueue(new Callback<WebResponseOk>() {
            @Override
            public void onResponse(retrofit2.Call<WebResponseOk> call, Response<WebResponseOk> response) {
                mProgress.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    Log.i("RESPONSE", "OKAY");
                    if (response.body() != null) {
                        String status = response.body().getStatus();
                        if (status.equalsIgnoreCase("ok")) {
                            if (mMessageResponse.length > 0)
                                getChatApi(mMessageResponse[0].chatroom_id + "");
                        } else {
                            if (getActivity() != null)
                                Toast.makeText(getActivity(), response.body().getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (getActivity() != null)
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Log.i("RESPONSE", response.message());
                    if (getActivity() != null)
                        Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<WebResponseOk> call, Throwable t) {
                Log.i("RESPONSE", t.getMessage());
                mProgress.setVisibility(View.GONE);
                if (getActivity() != null)
                    Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void showRatingDialog() {
        if (getContext() != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.AlertDialog));
            final View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_rate, null);
            final EditText ratingMsg = view.findViewById(R.id.portfolio_rating_msg);
            final AppCompatRatingBar rate = view.findViewById(R.id.portfolio_rating);

            builder.setView(view);
            builder.setPositiveButton("Send Review", null);
            builder.setNegativeButton("Cancel", null);
            final AlertDialog dialog = builder.create();
            dialog.show();
            final Button positiveDialog = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
            // name edit text on landing is empty
            if (TextUtils.isEmpty(ratingMsg.getText())) {
                positiveDialog.setEnabled(false);
                positiveDialog.setTextColor(Color.GRAY);
            }
            positiveDialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String reviews = ratingMsg.getText().toString();
                    int ratings = (int) rate.getRating();
                    if (reviews.equalsIgnoreCase("")) {
                        Toast.makeText(getActivity(), "Please add reviews", Toast.LENGTH_SHORT).show();
                    } else if (ratings == 0) {
                        Toast.makeText(getActivity(), "Please rate first", Toast.LENGTH_SHORT).show();
                    } else {
                        apiSubmitRating(reviews, ratings);
                        Utils.hideKeyboard(getActivity());
                        ratingMsg.setText("");
                        dialog.dismiss();
                    }
                }
            });
            ratingMsg.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (TextUtils.isEmpty(editable)) {
                        // Disable ok button
                        positiveDialog.setTextColor(Color.GRAY);
                        positiveDialog.setEnabled(false);
                    } else {
                        // Something into edit text. Enable the button.
                        positiveDialog.setTextColor(ResourcesUtil.getColor(R.color.yellow));
                        positiveDialog.setEnabled(true);
                    }
                }
            });
        }
    }
}
