package com.dojob.backend.model;


public class SearchResponse {
    public String status;
    public FeedItemResponse[] nameSearch;
    public FeedItemResponse[] categorySearch;
    public boolean nextPage;

}
