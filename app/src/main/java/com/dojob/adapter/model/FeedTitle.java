package com.dojob.adapter.model;

import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.dojob.R;
import com.dojob.util.ResourcesUtil;

public class FeedTitle {
    public static final int LAYOUT_ID = R.layout.layout_feed_title;
    public static final int HOTTEST_TALENT = 0;
    public static final int TOP_PICK = 1;
    public static final int SEARCH_TALENT = 2;
    public static final int SEARCH_CATEGORY = 3;

    private int mType;
    private String catTitle;


    public FeedTitle(int type) {
        mType = type;
    }

    public FeedTitle(int mType, String catTitle) {
        this.mType = mType;
        this.catTitle = catTitle;
    }

    public void bindViewHolder(ViewHolder holder, int position){
        if(holder.feedTitle != null){
            switch (mType){
                case HOTTEST_TALENT:
                    holder.feedTitle.setText("Ten Hottest Talents");
                    holder.feedTitle.setTextColor(ResourcesUtil.getColor(R.color.yellow));
                    holder.feedTitle.setGravity(Gravity.CENTER);
                    break;
                case TOP_PICK:
                    holder.feedTitle.setText("Top Picks");
                    holder.feedTitle.setTextColor(ResourcesUtil.getColor(R.color.green));
                    holder.feedTitle.setGravity(Gravity.CENTER);
                    break;
                case SEARCH_TALENT:
                    holder.feedTitle.setText("Recommended Talents and Services");
                    holder.feedTitle.setTextColor(ResourcesUtil.getColor(R.color.green_search));
                    holder.feedTitle.setGravity(Gravity.LEFT);
                    break;
                case SEARCH_CATEGORY:
                    if (catTitle!=null)
                    holder.feedTitle.setText(catTitle);
                    holder.feedTitle.setTextColor(ResourcesUtil.getColor(R.color.green_search));
                    holder.feedTitle.setGravity(Gravity.LEFT);
                    break;

            }
        }
    }

    public static class ViewHolder{

        public TextView feedTitle;

        public ViewHolder(View view){
            feedTitle = view.findViewById(R.id.feed_title);
        }
    }
}
