package com.dojob.util;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.graphics.Palette;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dojob.BuildConfig;
import com.dojob.R;
import com.dojob.activity.FullScreenVideoActivity;
import com.dojob.activity.FullScreenVideoActivity2;
import com.dojob.listener.CompressCallbacbkListener;
import com.dojob.service.FirebaseNotificationService;
import com.vincent.videocompressor.VideoCompress;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import hb.xvideoplayer.MxVideoPlayerWidget;
import za.co.riggaroo.materialhelptutorial.TutorialItem;
import za.co.riggaroo.materialhelptutorial.tutorial.MaterialTutorialActivity;


public class Utils {

    public static int isFromLogin = 0;
    private static AlertDialog compressDialog;
    private static TextView compressProgress;
    public static int isRefreshRequired = 0;

    public static final String BASEURL = "https://step4work.com/api/";
    //    public static int NOTIFICATION_COUNTER = 0;
    public static final String NOFICATION_NUMBER = "notificationCounter";
    public static final String USER_ROLE = "userRole";
    public static final String TALENT_ROLE = "talent";
    public static final String RECRUITEE_ROLE = "recruiter";
    public static final String ANNOUNCEMENT_BIGGEST_ID = "announcementBigID";
    public static final String ISSOCIALFEED_OPENED = "socialFeed_Opened";

    public static boolean isOnline(Context context) {
        if (context != null) {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo ni = cm.getActiveNetworkInfo();
            return ni != null && ni.isConnected();
        }
        return false;
    }


    private static int calendarToTimestamp(Calendar calendar) {
        return millisToSecond(calendar.getTimeInMillis());
    }

    public static int nowTimestamp() {
        return calendarToTimestamp(Calendar.getInstance());
    }

    private static int millisToSecond(long millis) {
        return (int) (millis / 1000.0);
    }

    public static int getDominantImageColor(Bitmap bitmap) {
        List<Palette.Swatch> swatchesTemp = Palette.from(bitmap).generate().getSwatches();
        List<Palette.Swatch> swatches = new ArrayList<Palette.Swatch>(swatchesTemp);
        Collections.sort(swatches, new Comparator<Palette.Swatch>() {
            @Override
            public int compare(Palette.Swatch swatch1, Palette.Swatch swatch2) {
                return swatch2.getPopulation() - swatch1.getPopulation();
            }
        });
        return swatches.size() > 0 ? swatches.get(0).getRgb() : ResourcesUtil.getColor(R.color.white);
    }

    public static int dpToPx(int value) {
        return (int) (value * Resources.getSystem().getDisplayMetrics().density);
    }

    public static void share(Context context, String data) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, data);
        context.startActivity(Intent.createChooser(shareIntent, "Share using"));
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void showKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static void greyScale(ImageView img) {
        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);  //0 means grayscale
        ColorMatrixColorFilter cf = new ColorMatrixColorFilter(matrix);
        img.setColorFilter(cf);
        img.setImageAlpha(128);
    }


    public static String hashMapToString(Map<String, String> map) {
        StringBuilder stringBuilder = new StringBuilder();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            stringBuilder.append(entry.getKey())
                    .append("=")
                    .append(entry.getValue())
                    .append("&");
        }
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        return stringBuilder.toString();
    }

    public static long bytesToMB(long size) {
        long fileSizeInKB = size / 1024;
        return fileSizeInKB / 1024;
    }

    private static final int SECOND = 1000;        // no. of ms in a second
    private static final int MINUTE = SECOND * 60; // no. of ms in a minute
    private static final int HOUR = MINUTE * 60;   // no. of ms in an hour
    private static final int DAY = HOUR * 24;      // no. of ms in a day
    private static final int WEEK = DAY * 7;       // no. of ms in a week

    public static String getRemainingTime(long remaining) {
        int weeks = (int) (remaining / WEEK);
        if (weeks > 0) {
            return String.format(Locale.US, "%d weeks ago", weeks);
        }
        int days = (int) ((remaining % WEEK) / DAY);
        if (days > 0) {
            return String.format(Locale.US, "%d days ago", days);
        }
        int hours = (int) ((remaining % DAY) / HOUR);
        if (hours > 0) {
            return String.format(Locale.US, "%d hours ago", hours);
        }
        int minutes = (int) ((remaining % HOUR) / MINUTE);
        if (minutes > 0) {
            return String.format(Locale.US, "%d minutes ago", minutes);
        }
        int seconds = (int) ((remaining % MINUTE) / SECOND);
        if (seconds > 0) {
            return String.format(Locale.US, "%d seconds ago", seconds);
        }
        if (seconds == 0) {
            return "just now";
        }

        return "";
    }

    private static int tempPercent;

    public static void compressVideo(final Activity activity, Uri uri, final CompressCallbacbkListener listener) {
        final File compressFile;
        File file = new File(uri.getPath());
        final long fileSizeInBytes = file.length();
        if (Utils.bytesToMB(fileSizeInBytes) <= 20) {
            listener.onSuccess(uri);
            return;
        }
//        if (Utils.bytesToMB(fileSizeInBytes) > 50) {
//            listener.onError();
//            Toast.makeText(activity, "File is invalid to upload.", Toast.LENGTH_LONG).show();
//            return;
//        }
        try {
            File compressCachePath = activity.getExternalCacheDir();
            compressCachePath.mkdir();
            compressFile = File.createTempFile("compress", ".mp4", compressCachePath);
        } catch (IOException e) {
            Toast.makeText(activity, "Failed to create temporary file.", Toast.LENGTH_LONG).show();
            return;
        }

        VideoCompress.VideoCompressTask task = VideoCompress.compressVideoLow(uri.getPath(), compressFile.getAbsolutePath(), new VideoCompress.CompressListener() {
            @Override
            public void onStart() {
                //Start Compress
                Log.d("VideoCompress", "Start Compress");
                listener.onStart();
                tempPercent = 0;
            }

            @Override
            public void onSuccess() {
                //Finish successfully
                tempPercent = 0;
                if (compressDialog != null) dismissCompressDialog();
                long fileSize = Utils.bytesToMB(compressFile.length());
                Log.d("","after compress Size "+fileSize);
                if (compressFile.exists() && Utils.bytesToMB(compressFile.length()) <= 50) {
                    Log.d("VideoCompress", "onSuccess");
                    listener.onSuccess(Uri.fromFile(compressFile));
                    return;
                }
                listener.onError();
                Toast.makeText(activity, "The media file that you have selected is larger than 50MB.", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFail() {
                //Failed
                if (compressDialog != null) dismissCompressDialog();
                Log.d("VideoCompress", "onFail");
                listener.onError();
                tempPercent = 0;
                Toast.makeText(activity, "File is invalid to upload.", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onProgress(final float percent) {
                //Progress
                if (tempPercent != (int) (percent)) {
                    Log.d("VideoCompress", "onProgress:" + percent);
                    compressProgress.post(new Runnable() {
                        @Override
                        public void run() {
                            compressProgress.setText(String.format(Locale.US, "Compress %d%%...", (int) (percent)));
                        }
                    });

                    listener.onProgress(percent);
                    tempPercent = (int) (percent);
                }
            }
        });
        showCompressDialog(task, activity, listener);

    }

    private static void showCompressDialog(final VideoCompress.VideoCompressTask task, Activity context, final CompressCallbacbkListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.AlertDialog));
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_upload_progress, null);
        compressProgress = view.findViewById(R.id.upload_total);
        compressProgress.setText("Compress 0%");
        View uploadPercent = view.findViewById(R.id.upload_item_percent);
        uploadPercent.setVisibility(View.GONE);
        builder.setTitle("Compress Video");
        builder.setView(view);
        builder.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                task.cancel(true);
                listener.onError();
            }
        });
        compressDialog = builder.create();
        compressDialog.setCancelable(false);
        compressDialog.show();
    }

    private static void dismissCompressDialog() {
        if (compressDialog != null) {
            compressProgress = null;
            compressDialog.dismiss();
        }
    }

    public static void openVideoPlayer(Context context, String url) {
//        String replaceString = url.replace("https", "http");
//        MKPlayerActivity.configPlayer((Activity) context).play(replaceString);
//        MxVideoPlayer.startFullscreen(context, MxVideoPlayerWidget.class, replaceString, "");
//        MxVideoPlayerWidget.startFullscreen(context, MxVideoPlayerWidget.class, replaceString, "");



        Intent intent = new Intent(context, FullScreenVideoActivity.class);
        intent.putExtra("url",url);
        context.startActivity(intent);
//        SimpleExoPlayer simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(context);
//        DashMediaSource mediaSource = new DashMediaSource();
//
//       ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
//        MediaSource mediaSource = new ExtractorMediaSource(Uri.parse(url),null
//                , extractorsFactory, null, null);
//
//        simpleExoPlayer.prepare(mediaSource);
//        simpleExoPlayer.setPlayWhenReady(true);

//        GiraffePlayer.play(context, new VideoInfo(url));

    }

    public static void showSnackbar(Activity activity, String msg) {
        Snackbar.make(activity.findViewById(android.R.id.content), msg,
                Snackbar.LENGTH_LONG).show();
    }

    public static boolean isValidEmailId(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }


    public static void loadTutorial(Context context) {
        Intent mainAct = new Intent(context, MaterialTutorialActivity.class);
        mainAct.putParcelableArrayListExtra(MaterialTutorialActivity.MATERIAL_TUTORIAL_ARG_TUTORIAL_ITEMS, getTutorialItems(context));
        context.startActivity(mainAct);

    }

    private static ArrayList<TutorialItem> getTutorialItems(Context context) {
        TutorialItem tutorialItem1 = new TutorialItem("", "",
                R.color.yellow, R.drawable.tut_page_1, R.drawable.tut_page_1);

        TutorialItem tutorialItem2 = new TutorialItem("", "",
                R.color.yellow, R.drawable.tut_page_2, R.drawable.tut_page_2);

        TutorialItem tutorialItem3 = new TutorialItem("", "",
                R.color.yellow, R.drawable.tut_page_1, R.drawable.tut_page_1);

        TutorialItem tutorialItem4 = new TutorialItem("", "",
                R.color.yellow, R.drawable.tut_page_2, R.drawable.tut_page_2);

        TutorialItem tutorialItem5 = new TutorialItem("", "",
                R.color.yellow, R.drawable.tut_page_1, R.drawable.tut_page_1);


//        // You can also add gifs, [IN BETA YET] (because Glide is awesome!)
//        TutorialItem tutorialItem1 = new TutorialItem(context.getString(R.string.slide_1_african_story_books), context.getString(R.string.slide_1_african_story_books_subtitle),
//                R.color.slide_1, R.drawable.gif_drawable, true);

        ArrayList<TutorialItem> tutorialItems = new ArrayList<>();
        tutorialItems.add(tutorialItem1);

        return tutorialItems;
    }


    public static String getDeviceModel() {

        return Build.MODEL;
    }

    public static String getManufracturer() {
        return Build.MANUFACTURER;
    }

    public static String getOsVersion() {
        return Build.VERSION.RELEASE + " " + Build.VERSION_CODES.class.getFields()[android.os.Build.VERSION.SDK_INT].getName();
    }

    public static String getDeviceToken(Context context) {
        if (context != null) {
            if (FirebaseNotificationService.getToken(context) != null) {
                return FirebaseNotificationService.getToken(context);
            }
            return "";
        }
        return "";
    }

    public static String getAppVersion() {
        return BuildConfig.VERSION_NAME;
    }

    public static void hideKeyBoard(Activity context) {
        if (context == null)
            return;
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            View view = context.getCurrentFocus();
            if (view == null) {
                view = new View(context);
            }
            imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public static void hideKeyBoard(Context context, View view) {
        if (context == null)
            return;
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            if (view == null) {
                view = new View(context);
            }
            imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}
