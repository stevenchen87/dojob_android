package com.dojob.adapter.model;

import android.text.format.DateUtils;
import android.view.View;
import android.widget.TextView;

import com.dojob.R;
import com.dojob.backend.model.InboxMessageResponse;
import com.dojob.core.Logger;

import java.text.SimpleDateFormat;
import java.util.Date;

public class InboxChatOther {
    public static final int LAYOUT_ID = R.layout.layout_inbox_chat_other_item;
    private final InboxMessageResponse mResponse;

    public InboxChatOther(InboxMessageResponse response) {
        mResponse = response;
    }

    public void bindViewHolder(ViewHolder holder, int position){
        if(holder.content != null){
            holder.content.setText(mResponse.msg_body);
        }
        if(holder.date != null){
            long time = mResponse.created_at * 1000;

            String value = new SimpleDateFormat("hh:mm a").format(new Date(time));
            if(!DateUtils.isToday(time)){
                value = new SimpleDateFormat("dd/MM").format(new Date(time));
            }
            holder.date.setText(value);
        }

        if(holder.badge != null){
            holder.badge.setVisibility((mResponse.status.equals("0"))?View.VISIBLE:View.GONE);
        }
    }

    public static class ViewHolder{
        public TextView date;
        public TextView content;
        public View badge;
        public TextView time;

        public ViewHolder(View view){
            date = view.findViewById(R.id.inbox_chat_other_time);
            content = view.findViewById(R.id.inbox_chat_other_content);
            badge = view.findViewById(R.id.inbox_chat_other_badge);
        }
    }
}
