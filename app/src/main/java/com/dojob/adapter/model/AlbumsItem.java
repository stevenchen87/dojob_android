package com.dojob.adapter.model;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.dojob.GlideApp;
import com.dojob.R;
import com.dojob.backend.model.GalleryAlbumsItemResponse;
import com.dojob.util.ResourcesUtil;
import com.dojob.util.Utils;

public class AlbumsItem {
    public static final int LAYOUT_ID = R.layout.layout_albums_item;
    public static final int ALBUMS_DEFAULT = 0;
    public static final int ALBUMS_IMAGES = 1;
    public static final int ALBUMS_IMAGES_PORTFOLIO = 2;

    private final GalleryAlbumsItemResponse mResponse;

    private int mType;

    private long id;
    private Context mContext;

    public AlbumsItem(int type, GalleryAlbumsItemResponse response, Context context) {
        mType = type;
        mResponse = response;
        this.mContext = context;
        if (response != null){
            this.id = response.id;
        }
    }

    public GalleryAlbumsItemResponse getItem(){
        return mResponse;
    }


    public void bindViewHolder(final ViewHolder holder, int position){
        if(holder.container != null){
            holder.container.setTag(position);
            if(mType == ALBUMS_IMAGES_PORTFOLIO){
                /*holder.cardView.setBackgroundColor(mContext.getResources().getColor(R.color.yellow));
                holder.DoJobCardView.setBackgroundColor(mContext.getResources().getColor(R.color.yellow));*/
               /* holder.cardView.setElevation(0);
                holder.DoJobCardView.setElevation(0);*/
                ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) holder.cardView.getLayoutParams();
              marginParams.rightMargin=0;marginParams.leftMargin=0;marginParams.topMargin=0;marginParams.bottomMargin = 0;

                holder.gradient.setVisibility(View.GONE);

                holder.container.setBackgroundColor(mContext.getResources().getColor(R.color.yellow));
                holder.container.getLayoutParams().height = Utils.dpToPx(100);
                holder.container.getLayoutParams().width = Utils.dpToPx(100);
                ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) holder.container.getLayoutParams();
                 params.bottomMargin = 35; params.topMargin = 35;params.leftMargin=10;params.rightMargin=10;
            /*    holder.container.setPadding(10,40,10,40);*/
            }
        }
        switch (mType){
            case ALBUMS_DEFAULT:
                if(holder.image != null){
//                    holder.image.setBackgroundColor(ResourcesUtil.getColor(R.color.yellow));
                    holder.image.setBackground(ResourcesUtil.getDrawable(R.drawable.create_albums_x3));
                }
                if(holder.title != null){
                    holder.title.setText("Create Album");
                }
                if(holder.gradient != null){
                    holder.gradient.setVisibility(View.GONE);
                }
                if(holder.desc != null){
                    holder.desc.setVisibility(View.GONE);
                }
//                if(holder.add != null){
//                    holder.add.setVisibility(View.VISIBLE);
//                }
                break;
            case ALBUMS_IMAGES:
            case ALBUMS_IMAGES_PORTFOLIO:
                if(mResponse != null){
                    if(holder.image != null){
                        if(mResponse.files != null && mResponse.files.length > 0){
//                            holder.gradient.setBackgroundColor(ResourcesUtil.getColor(R.color.grey));
                            if(mResponse.album_type.equals("video")){
                                GlideApp.with(mContext)
                                        .load(mResponse.files[0].thumbnail)
                                        .placeholder(R.drawable.placeholder_1)
                                        .apply(RequestOptions.fitCenterTransform())
                                        .skipMemoryCache(false)
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .into(holder.image);
                               /* App.picasso().load(mResponse.files[0].thumbnail).fit().centerCrop().into(holder.image);*/
                            }
                            else{
                                Log.i("IMAGES","OKAY");
                                GlideApp.with(mContext)
                                        .load(mResponse.files[0].url)
                                        .placeholder(R.drawable.placeholder_1)
                                        .apply(RequestOptions.fitCenterTransform())
                                        .skipMemoryCache(false)
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .into(holder.image);
                                /*App.picasso().load(mResponse.files[0].url).fit().centerCrop().into(holder.image);*/
                            }

                            if(holder.gradient != null){
                                GradientDrawable drawable = new GradientDrawable(
                                        GradientDrawable.Orientation.BOTTOM_TOP, new int[] {/*Utils.getDominantImageColor(bitmap)*/ ResourcesUtil.getColor(R.color.grey), ResourcesUtil.getColor(R.color.white)
                                });
                                holder.gradient.setBackground(drawable);
                            }
                        }
                    }
                    if(holder.title != null){
                        holder.title.setText(mResponse.name);
                    }
                    if(holder.desc != null){
                        holder.desc.setText(mResponse.description);
                    }
                    if(holder.add != null){
                        holder.add.setVisibility(View.GONE);
                    }
                }
                break;
        }
    }

    public int getType(){
        return mType;
    }
    public long getId(){
        return this.id;
    }
    public static class ViewHolder{

        public View container;
        public ImageView image;
        public TextView title;
        public TextView desc;
        public View add;
        public View gradient;
        public View DoJobCardView;
        public CardView cardView;

        public ViewHolder(View view){
            container = view.findViewById(R.id.album_container);
            image = view.findViewById(R.id.album_image);
            title = view.findViewById(R.id.album_title);
            desc = view.findViewById(R.id.album_desc);
            DoJobCardView = view.findViewById(R.id.do_job_card_view);
            cardView = view.findViewById(R.id.album_card_view);

//            add = view.findViewById(R.id.album_add);
            gradient = view.findViewById(R.id.album_gradient);
        }
    }
}
