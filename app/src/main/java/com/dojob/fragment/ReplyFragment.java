//package com.dojob.fragment;
//
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.support.design.widget.BottomSheetDialogFragment;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//
//import com.dojob.R;
//import com.dojob.adapter.PortfolioAdapter;
//import com.dojob.backend.CommentApi;
//import com.dojob.backend.model.Comment;
//import com.dojob.backend.model.UserResponse;
//import com.dojob.core.App;
//import com.dojob.core.BackgroundWorker;
//import com.dojob.core.Result;
//import com.github.ybq.android.spinkit.SpinKitView;
//
//public class ReplyFragment extends BottomSheetDialogFragment implements
//    BackgroundWorker.Callbacks,
//    PortfolioAdapter.Listener {
//    public static final String COMMENTS_RESPONSE = "comment_response";
//    public static final String COMMENTS_FILE_ID = "comment_file_id";
//    public static final String COMMENTS_USER_ID = "comment_user_id";
//
//    private final String OBJ_COMMENT_API = "obj_comment_api";
//    private final String TASK_REQUEST_COMMENT = "task_request_comment";
//    private final String TASK_SEND_COMMENT = "task_send_profile";
//
//    private BackgroundWorker mWorker;
//    private CommentApi mCommetApi;
//    private PortfolioAdapter mAdapter;
//    private SpinKitView mProgress;
//    private RecyclerView mRecyclerView;
//    private String mType;
//    private int mFileId;
//    private Comment[] mCommentResponse;
//    private int mUserId;
//
//    public static ReplyFragment newInstance(Bundle args) {
//        ReplyFragment fragment = new ReplyFragment();
//        fragment.setArguments(args);
//        return fragment;
//    }
//
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        AppCompatActivity activity = ((AppCompatActivity) getActivity());
//        if (getArguments() != null) {
//            if(getArguments().containsKey(CommentFragment.COMMENTS_TYPE))
//                mType = getArguments().getString(CommentFragment.COMMENTS_TYPE);
//            if(getArguments().containsKey(COMMENTS_FILE_ID))
//                mFileId = getArguments().getInt(COMMENTS_FILE_ID);
//            if(getArguments().containsKey(COMMENTS_RESPONSE)) {
//                mCommentResponse = App.gson().fromJson(getArguments().getString(COMMENTS_RESPONSE), Comment[].class);
//                if(mCommentResponse.length > 0){
//                    mUserId = mCommentResponse[0].user_id;
//                }
//            }
//        }
//
//        if (activity != null) {
//            mWorker = new BackgroundWorker(activity);
//            mCommetApi = (CommentApi) mWorker.get(OBJ_COMMENT_API);
//            if (mCommetApi == null) {
//                mCommetApi = new CommentApi(getContext());
//                mWorker.put(OBJ_COMMENT_API, mCommetApi);
//            }
//        }
//    }
//
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.fragment_portfolio, container, false);
//        mProgress = view.findViewById(R.id.progress);
//        mProgress.setVisibility(View.GONE);
//        mRecyclerView = view.findViewById(R.id.recycle_view);
//        mRecyclerView.setItemViewCacheSize(20);
//        mRecyclerView.setDrawingCacheEnabled(true);
//        mRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
//        mRecyclerView.setVisibility(View.VISIBLE);
//        mAdapter = new PortfolioAdapter(getContext());
//        mAdapter.setListener(getActivity(), this);
//        mRecyclerView.setAdapter(mAdapter);
//        LinearLayoutManager llm = new LinearLayoutManager(getContext());
//        llm.setOrientation(LinearLayoutManager.VERTICAL);
//        mRecyclerView.setLayoutManager(llm);
////        mAdapter.updateItem(mCommentResponse);
//        return view;
//    }
//
//    @Override
//    public Result executeTaskInBackground(String id, Bundle args) {
//        switch (id) {
//            case TASK_SEND_COMMENT:
//                return mCommetApi.sendMessage(mFileId, mType,args.getString("query"));
//        }
//        return null;
//    }
//
//    @Override
//    public void onBackgroundTaskCompleted(String id, Result result) {
//        switch (id) {
//            case TASK_SEND_COMMENT:
//                if(result.isSuccess() && mAdapter != null){
//                    Comment[] response = (Comment[]) result.getResult();
//                    if(response != null){
////                        mAdapter.updateItem(response, );
//                        mRecyclerView.setVisibility(View.VISIBLE);
//                        mProgress.setVisibility(View.GONE);
//                    }
//                }
//                break;
//        }
//    }
//
//    @Override
//    public void onOpenAllGallery(int page, UserResponse response) { }
//
//    @Override
//    public void onEnqueryClick(com.dojob.backend.model.UserResponse response) {}
//
//    @Override
//    public void onEditProfileClick() {}
//
//    @Override
//    public void onRateClick() {
//
//    }
//
//    @Override
//    public void onLoginDialogOpen() {}
//
//    @Override
//    public void onOpenCommentPage(com.dojob.backend.model.UserResponse response, String type, int position) { }
//
//    @Override
//    public void onSaveComment(String query) {
//        mProgress.setVisibility(View.VISIBLE);
//        Bundle bundle = new Bundle();
//        bundle.putString("query", query);
//        if(mWorker != null) mWorker.executeNewTask(TASK_SEND_COMMENT, bundle, this);
//    }
//
//    @Override
//    public void onOpenViewReply(Comment[] comments) {}
//
//    @Override
//    public void onBackClick() {
//
//    }
//
//    @Override
//    public void onOpenReviewsPage(UserResponse response) {
//
//    }
//
//    @Override
//    public void onCoverImageSwiped(int position) {
//
//    }
//}
