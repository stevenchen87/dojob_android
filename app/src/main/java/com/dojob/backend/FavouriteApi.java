package com.dojob.backend;

import android.content.Context;

import com.dojob.backend.model.FavouriteListResponse;
import com.dojob.backend.model.Response;
import com.dojob.core.App;
import com.dojob.core.Http;
import com.dojob.core.Result;
import com.dojob.util.Utils;
import com.dojob.core.Error;
import com.google.gson.JsonSyntaxException;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Admin on 29-Nov-18.
 */
public class FavouriteApi {
    private final static String TAG = "favourite_tag";
    private final Context mContext;

    public FavouriteApi(Context context) {
        mContext = context;
    }

    public Result requestDoFavourite(int target_id) {
        Response response = null;
        if (!Utils.isOnline(mContext)) {
            return new Result(Error.OFFLINE);
        }

        Http.Request request = Http.request(mContext)
                .url("http://step4work.com/api/favourite")
                .tag(TAG);

        Map<String, String> data = new HashMap<String, String>();
        data.put("target_id", String.valueOf(target_id));
        request.post(data);

        Result result = request.send();
        if (result.isSuccess()) {
            String responseText = (String) result.getResult();

            try {
                response = App.gson().fromJson(responseText, Response.class);
            } catch (JsonSyntaxException ex) {
                ex.printStackTrace();
            }
            if (response != null) {
                return new Result(null, response);
            }
            return new Result(Error.DATA);
        }

        return new Result(Error.NETWORK);
    }

    public Result requestRemoveFavourite(String target_id) {
        Response response = null;
        if (!Utils.isOnline(mContext)) {
            return new Result(Error.OFFLINE);
        }

        Http.Request request = Http.request(mContext)
                .url("http://step4work.com/api/removeFavourite")
                .tag(TAG);

        Map<String, String> data = new HashMap<String, String>();
        data.put("target_id", target_id);
        request.post(data);

        Result result = request.send();
        if (result.isSuccess()) {
            String responseText = (String) result.getResult();

            try {
                response = App.gson().fromJson(responseText, Response.class);
            } catch (JsonSyntaxException ex) {
                ex.printStackTrace();
            }
            if (response != null) {
                return new Result(null, response);
            }
            return new Result(Error.DATA);
        }

        return new Result(Error.NETWORK);
    }

    public Result getFavouriteList(int page) {
        FavouriteListResponse response = null;
        if (!Utils.isOnline(mContext)) {
            return new Result(Error.OFFLINE);
        }

        Http.Request request = Http.request(mContext)
                .url("http://step4work.com/api/getFavouriteList?page=" + page)
                .tag(TAG);

        Result result = request.send();
        if (result.isSuccess()) {
            String responseText = (String) result.getResult();

            try {
                response = App.gson().fromJson(responseText, FavouriteListResponse.class);
            } catch (JsonSyntaxException ex) {
                ex.printStackTrace();
            }
            if (response != null) {
                return new Result(null, response);
            }
            return new Result(Error.DATA);
        }

        return new Result(Error.NETWORK);
    }

}
