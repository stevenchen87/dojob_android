package com.dojob.adapter.model;

import android.view.View;

import com.dojob.R;

public class PhotosAddBar {
    public static final int LAYOUT_ID = R.layout.layout_add_bar;

    public PhotosAddBar() {
    }

    public void bindViewHolder(ViewHolder holder, int position){
        if(holder.container != null){
            holder.container.setTag(position);
        }
    }

    public static class ViewHolder{

        public View container;

        public ViewHolder(View view){
            container = view.findViewById(R.id.photos_add_bar);
        }
    }
}
