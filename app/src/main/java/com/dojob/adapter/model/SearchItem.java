package com.dojob.adapter.model;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.dojob.GlideApp;
import com.dojob.R;
import com.dojob.backend.SetupApi;
import com.dojob.backend.model.FeedItemResponse;
import com.dojob.core.App;
import com.dojob.core.Logger;
import com.dojob.util.ResourcesUtil;
import com.dojob.util.Utils;
import com.squareup.picasso.Picasso;

public class SearchItem {

    public static final int LAYOUT_ID = R.layout.layout_search_item;
    public static final int SEARCH_RESULT = 0;
    private final Context mContext;
    private int mType;
    private FeedItemResponse mSearchResponse;

    public SearchItem(Context context, int type, FeedItemResponse response) {
        mSearchResponse = response;
        mType = type;
        mContext = context;
    }

    public FeedItemResponse getData(){
        return mSearchResponse;
    }

    public void bindViewHolder(ViewHolder holder, int position){
        if(holder.clickable != null){
            holder.clickable.setTag(position);
        }
        if(holder.image != null){
            GlideApp.with(mContext)
                    .load(mSearchResponse.image)
                    .placeholder(R.drawable.placeholder_1)
                    .apply(RequestOptions.fitCenterTransform())
                    .skipMemoryCache(false)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.image);
            /*App.picasso().load(mSearchResponse.image).fit().centerCrop().into(holder.image);*/
        }
        if(holder.name != null){
            holder.name.setText(mSearchResponse.name);
        }

        if (holder.tvcountryCode != null) {
            if (mSearchResponse.countrycode != null) {
                holder.tvcountryCode.setText(mSearchResponse.countrycode);
                holder.tvcountryCode.setVisibility(View.VISIBLE);
            }else
                holder.tvcountryCode.setVisibility(View.GONE);

        }
        if(holder.position != null){
            if( mSearchResponse.category_info!=null){
                holder.position.setText(mSearchResponse.category_info.sub_cat.name);
            }
        }
        if(holder.container != null){
            LinearLayout.LayoutParams topParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, Utils.dpToPx(180));
            topParams.setMargins(Utils.dpToPx(2), Utils.dpToPx(2), Utils.dpToPx(2), Utils.dpToPx(2));
            holder.container.setLayoutParams(topParams);
        }
    }

    public static class ViewHolder{
        public CardView container;
        public ImageView image;
        public TextView name;
        public TextView position;
        public View content;
        public View clickable;
        public TextView tvcountryCode;
        public ViewHolder(View view){
            container = view.findViewById(R.id.card_view);
            image = view.findViewById(R.id.search_image);
            name = view.findViewById(R.id.search_name);
            position = view.findViewById(R.id.search_position);
            content = view.findViewById(R.id.content);
            clickable = view.findViewById(R.id.clickable);
            tvcountryCode = view.findViewById(R.id.tvcountryCode);
        }
    }
}
