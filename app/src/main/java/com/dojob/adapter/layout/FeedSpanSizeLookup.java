package com.dojob.adapter.layout;

import android.support.v7.widget.GridLayoutManager;

import com.dojob.adapter.FeedAdapter;
import com.dojob.adapter.model.FeedItem;
import com.dojob.adapter.model.LoadingFooter;
import com.dojob.adapter.model.SearchItem;

public class FeedSpanSizeLookup extends GridLayoutManager.SpanSizeLookup  {

    private FeedAdapter mAdapter;

    public FeedSpanSizeLookup(FeedAdapter adapter) {
        mAdapter = adapter;
    }

    @Override
    public int getSpanSize(int position) {
        Object item = mAdapter.getItem(position);
        if(item instanceof FeedItem || item instanceof SearchItem){
            return 2;
        }

        return 4;
    }
}
