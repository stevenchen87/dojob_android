package com.dojob.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dojob.R;
import com.dojob.adapter.holder.InboxVIewHolder;
import com.dojob.adapter.model.InboxChatOther;
import com.dojob.adapter.model.InboxChatOwner;
import com.dojob.adapter.model.InboxItem;
import com.dojob.backend.LoginApi;
import com.dojob.backend.model.InboxMessageResponse;
import com.dojob.backend.model.InboxResponse;

import java.util.ArrayList;
import java.util.List;

public class InboxAdapter extends RecyclerView.Adapter<InboxVIewHolder> implements View.OnClickListener {
    private static final int TYPE_INBOX_ITEM = 1;
    private static final int TYPE_CHAT_OWNER = 2;
    private static final int TYPE_CHAT_OTHER = 3;
    private Context mContext;

    private List<Object> mItems = new ArrayList<>();
    private InboxResponse[] mInboxResponse;
    private InboxMessageResponse[] mInboxRoomResponse;
    private Listener mListener;

    public InboxAdapter(Context context) {
        mContext = context;
        setHasStableIds(true);
    }

    public void updateItem(InboxResponse[] response) {
        mInboxResponse = response;
        updateItem();
    }

    public void updateItem(InboxMessageResponse[] response) {
        mInboxRoomResponse = response;
        updateItem();
    }

    private void updateItem() {
        mItems.clear();
        if (mInboxResponse != null && mInboxResponse.length > 0) {
            for (InboxResponse item : mInboxResponse) {
                mItems.add(new InboxItem(item));
            }
        } else if (mInboxRoomResponse != null && mInboxRoomResponse.length > 0) {
//            Arrays.sort(mInboxRoomResponse, new Comparator<InboxMessageResponse>() {
//                @Override
//                public int compare(InboxMessageResponse a, InboxMessageResponse b) {
//                    return  -1 * ((Long)b.created_at).compareTo(a.created_at);
//                }
//            });
//            Arrays.sort(mInboxRoomResponse, new Comparator<InboxMessageResponse>() {
//                @Override
//                public int compare(InboxMessageResponse a, InboxMessageResponse b) {
//                    Long msg1 = a.created_at;
//                    Long msg2 = b.created_at;
//                    return  msg1.compareTo(msg2);
//                }
//            });

            for (InboxMessageResponse item : mInboxRoomResponse) {
                if (LoginApi.isUser(mContext, item.sender_id)) {
                    mItems.add(new InboxChatOwner(item));
                } else {
                    mItems.add(new InboxChatOther(item));
                }
            }

        }

        notifyDataSetChanged();
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }

    @NonNull
    @Override
    public InboxVIewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        int layout = 0;
        switch (viewType) {
            case TYPE_INBOX_ITEM:
                layout = InboxItem.LAYOUT_ID;
                break;
            case TYPE_CHAT_OWNER:
                layout = InboxChatOwner.LAYOUT_ID;
                break;
            case TYPE_CHAT_OTHER:
                layout = InboxChatOther.LAYOUT_ID;
                break;
        }
        View view = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        InboxVIewHolder holder = new InboxVIewHolder(view);
        if (holder.inboxItem.container != null) {
            holder.inboxItem.container.setOnClickListener(this);
        }
        if (holder.inboxItem.avatar != null) {
            holder.inboxItem.avatar.setOnClickListener(this);
        }

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull InboxVIewHolder holder, int position) {
        Object item = getItem(position);
        int type = getItemViewType(position);
        switch (type) {
            case TYPE_INBOX_ITEM:
                ((InboxItem) item).bindViewHolder(holder.inboxItem, position);
                break;
            case TYPE_CHAT_OWNER:
                ((InboxChatOwner) item).bindViewHolder(holder.owner, position);
                break;
            case TYPE_CHAT_OTHER:
                ((InboxChatOther) item).bindViewHolder(holder.other, position);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        Object item = getItem(position);
        if (item instanceof InboxItem) {
            return TYPE_INBOX_ITEM;
        } else if (item instanceof InboxChatOwner) {
            return TYPE_CHAT_OWNER;
        } else if (item instanceof InboxChatOther) {
            return TYPE_CHAT_OTHER;
        }
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        if (mItems != null) {
            return mItems.size();
        }
        return 0;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onClick(View view) {
        Object tag = view.getTag();
        if (tag == null) {
            return;
        }
        Object item = getItem((int) view.getTag());
        switch (view.getId()) {
            case R.id.inbox_avatar:
                if (item instanceof InboxItem) {
                    int userid = ((InboxItem) item).getId();
                    String url = "http://step4work.com/api/profile/get?uid="+userid;
                    mListener.onOpenUserProfile(url,userid);
                }
                break;
            case R.id.inbox_container:
                if (item instanceof InboxItem) {
                    mListener.onOpenChatRoom(((InboxItem) item).getId(), ((InboxItem) item).getName(), ((InboxItem) item).getMessages());
                }
                break;
            default:
                break;

        }
    }

    public Object getItem(int position) {
        int size = mItems.size();
        if (size > 0 && position < size) {
            return mItems.get(position);
        }
        return null;
    }

    public interface Listener {
        void onOpenChatRoom(int id, String name, InboxMessageResponse[] response);

        void onOpenUserProfile(String url,int userId);
    }
}
