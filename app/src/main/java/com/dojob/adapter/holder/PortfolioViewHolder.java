package com.dojob.adapter.holder;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.dojob.adapter.model.CommentOtherItem;
import com.dojob.adapter.model.CommentUserItem;
import com.dojob.adapter.model.Devider;
import com.dojob.adapter.model.MainSearch;
import com.dojob.adapter.model.PortfolioCover;
import com.dojob.adapter.model.PortfolioDescription;
import com.dojob.adapter.model.PortfolioEmpty;
import com.dojob.adapter.model.PortfolioOverflowScroll;
import com.dojob.adapter.model.PortfolioPreview;
import com.dojob.adapter.model.PortfolioReviewItem;
import com.dojob.adapter.model.PortfolioSelf;
import com.dojob.adapter.model.PortfolioSelfRecruiter;
import com.dojob.adapter.model.PortfolioSkillsItem;
import com.dojob.adapter.model.PortfolioTitleBar;
import com.dojob.adapter.model.ReviewHeader;

public class PortfolioViewHolder extends RecyclerView.ViewHolder{
    public MainSearch.ViewHolder search;
    public PortfolioCover.ViewHolder cover;
    public PortfolioTitleBar.ViewHolder titleBar;
    public PortfolioDescription.ViewHolder description;
    public PortfolioSelf.ViewHolder self;
    public PortfolioSelfRecruiter.ViewHolder selfRecruiter;
    public PortfolioSkillsItem.ViewHolder skill;
    public PortfolioOverflowScroll.ViewHolder overFlowScroll;
    public Devider.ViewHolder devider;
    public PortfolioPreview.ViewHolder preview;
    public CommentUserItem.ViewHolder commentUser;
    public CommentOtherItem.ViewHolder commentOther;
    public PortfolioEmpty.ViewHolder empty;
    public PortfolioReviewItem.ViewHolder review;
    public ReviewHeader.ViewHolder reviewHeader;
    private FragmentActivity fragmentActivity;


    public PortfolioViewHolder(View itemView, FragmentActivity fragmentActivity) {
        super(itemView);
        this.fragmentActivity = fragmentActivity;
        search = new MainSearch.ViewHolder(itemView);
        cover = new PortfolioCover.ViewHolder(itemView);
        titleBar = new PortfolioTitleBar.ViewHolder(itemView);
        description = new PortfolioDescription.ViewHolder(itemView);
        self = new PortfolioSelf.ViewHolder(itemView,fragmentActivity);
        selfRecruiter = new PortfolioSelfRecruiter.ViewHolder(itemView);
        skill = new PortfolioSkillsItem.ViewHolder(itemView);
        overFlowScroll = new PortfolioOverflowScroll.ViewHolder(itemView);
        devider = new Devider.ViewHolder(itemView);
        preview = new PortfolioPreview.ViewHolder(itemView);
        commentUser = new CommentUserItem.ViewHolder(itemView);
        commentOther = new CommentOtherItem.ViewHolder(itemView);
        empty = new PortfolioEmpty.ViewHolder(itemView);
        review = new PortfolioReviewItem.ViewHolder(itemView);
        reviewHeader = new ReviewHeader.ViewHolder(itemView);
    }

}
