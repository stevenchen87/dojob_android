package com.dojob.backend.model;

import com.google.gson.annotations.SerializedName;

public class ProfileResponse {
    public int id;
    public String name;
    public String email;
    @SerializedName("tutorial")
    public String isTutorialShown;
    @SerializedName("role")
    public String position;
    public int category;
    @SerializedName("profile_image")
    public String profile_picture;
    @SerializedName("talentDescription")
    public String talent;
    public String cover_photo;
    public String url;
    @SerializedName("country")
    public String country;
    public int verified;
    @SerializedName("announcement_id")
    public int announeMentID;
    @SerializedName("featured_video")
    public MediaResponse[] featuredVideo;
    public String share_url;
    public CategoryInfo category_info;
    public Review[] review;
    public boolean hot;
    public boolean vip;
    public int rating;
    public int favourite_status;
    public int setup_profile;

    public class CategoryInfo{
        public Category sub_cat;
        public Category cat;
    }
    public class Category{
        public int id;
        public String name;
        public String category_type;
        public int category_id;
        public String translation_key;
    }

}

