package com.dojob.adapter.model;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.dojob.R;
import com.dojob.adapter.PortfolioOverflowAdapter;
import com.dojob.backend.model.FeedItemResponse;
import com.dojob.backend.model.GalleryAlbumsItemResponse;
import com.dojob.backend.model.ProfileResponse;
import com.dojob.backend.model.Review;
import com.dojob.backend.model.ReviewResponse;
import com.dojob.backend.model.UserResponse;

public class PortfolioOverflowScroll implements PortfolioOverflowAdapter.Listener {
    public static final int LAYOUT_ID = R.layout.layout_portfolio_overflow_scroll;
    public static final int REVIEWS_TYPE = 1;
    public static final int ALBUMS_TYPE = 2;
    public static final int FEED_TYPE = 3;
    public static final int SEARCH_TYPE = 4;
    public static final int RECRUITER_TYPE = 5;
    public static final int TYPE_VIDEO_ALBUM = 6;

    private static Context newContext;

    private Context mContext;
    private UserResponse mUserResponse;
    private FeedItemResponse[] mFeedItems;
    private ProfileResponse[] mRecruiterItems;
    private int mType;
    private PortfolioOverflowAdapter mAdapter;
    private Listener mListener;
    private static int mType_new;


    public PortfolioOverflowScroll(Context context, int type, UserResponse userResponse) {
        mContext = context;
        mUserResponse = userResponse;
        mType = type;
        newContext = context;
        mType_new=type;

    }

    public PortfolioOverflowScroll(Context context, int type, FeedItemResponse[] response) {
        mContext = context;
        mFeedItems = response;
        mType = type;
        newContext = context;
        mType_new=type;
    }

    public PortfolioOverflowScroll(Context context, int type, ProfileResponse[] response) {
        mContext = context;
        mRecruiterItems = response;
        mType = type;
        newContext = context;
        mType_new=type;
    }

    public void bindViewHolder(ViewHolder holder, int position) {
        if (mAdapter == null) {
            if(mFeedItems != null){
                mAdapter = new PortfolioOverflowAdapter(mContext, mType, mFeedItems);
            }
            else if(mRecruiterItems != null){
                mAdapter = new PortfolioOverflowAdapter(mContext, mType, mRecruiterItems);
            }
            else{
                mAdapter = new PortfolioOverflowAdapter(mContext, mType, mUserResponse);
            }
            mAdapter.setListener(this);
        }

        if (holder.list != null ) {
            holder.list.setAdapter(mAdapter);
            holder.list.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            holder.list.setNestedScrollingEnabled(false);
        }
    }

    public void setListener(Listener listener){
        mListener = listener;
    }

    @Override
    public void onAlbumClick(GalleryAlbumsItemResponse album) {
        mListener.onAlbumClick(album);
    }

    @Override
    public void onReviewClick(Review review) {
        mListener.onReviewClick(review);
    }

    @Override
    public void onFeedItemClick(String url) {
        mListener.onFeedItemClick(url);
    }

    public interface Listener{
        void onAlbumClick(GalleryAlbumsItemResponse album);
        void onReviewClick(Review review);
        void onFeedItemClick(String url);
    }

    public static class ViewHolder {
        public RecyclerView list;
        public ViewHolder(View view) {
            list = view.findViewById(R.id.list);
            if (list!=null)
                if ((mType_new != REVIEWS_TYPE))
                    if ((mType_new != SEARCH_TYPE))
                        if ((mType_new != FEED_TYPE)) {
                            Log.i("MTYPE_ORANGE",mType_new+"");
                            list.setBackgroundColor(newContext.getResources().getColor(R.color.yellow));
                        }else {
                            Log.i("MTYPE",mType_new+"");
                        }


        }
    }
}
