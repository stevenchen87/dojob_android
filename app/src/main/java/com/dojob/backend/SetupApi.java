package com.dojob.backend;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;

import com.dojob.backend.model.ProfileResponse;
import com.dojob.backend.model.RegisterResponse;
import com.dojob.backend.model.SetupResponse;
import com.dojob.backend.model.UserResponse;
import com.dojob.core.App;
import com.dojob.core.Error;
import com.dojob.core.Http;
import com.dojob.core.Logger;
import com.dojob.core.Result;
import com.dojob.util.Utils;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SetupApi {
    public final static String SETUP_TAG = "setup_tag";
    public final static String SETUP_GET_TAG = "setup_get_tag";
    private final Context mContext;
    public final static String TALENT_URL = "http://step4work.com/api/talent/setupProfile/get";
    public final static String RECRUITER_URL = "http://step4work.com/api/recruiter/setupProfile/get";

    public final static String SET_TALENT_URL = "http://step4work.com/api/talent/setupProfile/set";
    public final static String SET_RECRUITER_URL = "http://step4work.com/api/recruiter/setupProfile/set";
    private static HashMap<String, String> mCategories;

    public SetupApi(Context context) {
        mContext = context;
    }

    public Result get() {
        SetupResponse response = null;
        if (!Utils.isOnline(mContext)) {
            return new Result(Error.OFFLINE);
        }
        Http.Request request = Http.request(mContext)
                .url(ProfileApi.getRole(mContext).equals("talent") ? TALENT_URL : RECRUITER_URL)
                .tag(SETUP_GET_TAG);

        Result result = request.send();
        if (result.isSuccess()) {
            String responseText = (String) result.getResult();
            try {
                response = App.gson().fromJson(responseText, SetupResponse.class);
                if (response.status.equals("ok")) {
                    saveId(mContext, response.profile.id);
                    /*saveCategories(mContext, response.category);*/
                    ProfileApi.setRole(mContext, response.profile.position);
                    saveUserData(mContext, response.profile);
                }
            } catch (JsonSyntaxException ex) {
                ex.printStackTrace();
            }
            if (response != null) {
                return new Result(null, response);
            }
            return new Result(Error.DATA);
        }

        return new Result(Error.NETWORK);
    }

    public Result post(Map<String, String> formData, Map<String, Uri> fileData) {
        SetupResponse response = null;
        if (!Utils.isOnline(mContext)) {
            return new Result(Error.OFFLINE);
        }
        Map<String, String> params = new HashMap<>();
        Http.Request request = Http.request(mContext)
                .url(ProfileApi.getRole(mContext).equals("talent") ? SET_TALENT_URL : SET_RECRUITER_URL)
                .tag(SETUP_TAG);
        request.postMultiPartToServer(mContext, formData, fileData);

        Result result = request.send();
        if (result.isSuccess()) {
            String responseText = (String) result.getResult();

            try {
                response = App.gson().fromJson(responseText, SetupResponse.class);
            } catch (JsonSyntaxException ex) {
                ex.printStackTrace();
            }
            if (response != null) {
                return new Result(null, response);
            }
            return new Result(Error.DATA);
        }

        return new Result(Error.NETWORK);
    }

    public static void saveId(Context context, int id) {
        SharedPreferences sharedPref = context.getSharedPreferences("dojob", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("PROFILE_ID", id);
        editor.apply();
    }

    public static boolean isUserRecruiter(Context context) {
        return ProfileApi.getRole(context).equals("recruiter");
    }

    public static void saveUserData(Context context, ProfileResponse data) {
        SharedPreferences sharedPref = context.getSharedPreferences("dojob", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("USER_DATA", App.gson().toJson(data));
        editor.apply();
    }

    public static ProfileResponse getUserData(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("dojob", Context.MODE_PRIVATE);
        String response = sharedPref.getString("USER_DATA", null);
        try {
            return response != null ? App.gson().fromJson(response, ProfileResponse.class) : null;
        } catch (Exception ex) {
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.remove("USER_DATA");
            editor.apply();
            return null;
        }
    }

    public static int getId(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("dojob", Context.MODE_PRIVATE);
        return sharedPref.getInt("PROFILE_ID", 0);
    }

    public static void deleteId(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("dojob", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("PROFILE_ID", 0);
        editor.apply();
    }

   /* public static void saveCategories(Context context, HashMap<String, String> category) {
        SharedPreferences sharedPref = context.getSharedPreferences("dojob",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("PROFILE_CATEGORIES", App.gson().toJson(category));
        editor.apply();
    }*/

    public static String getCategories(Context context, String categoryId) {
        if (mCategories == null) {
            SharedPreferences sharedPref = context.getSharedPreferences("dojob", Context.MODE_PRIVATE);
            String storedCategories = sharedPref.getString("PROFILE_CATEGORIES", "");
            if (storedCategories.isEmpty()) {
                return "";
            }
            java.lang.reflect.Type type = new TypeToken<HashMap<String, String>>() {
            }.getType();
            mCategories = App.gson().fromJson(storedCategories, type);
        }
        return mCategories.get(categoryId);
    }

    public static void savePreviewSetupResponse(Context context, String response) {
        SharedPreferences sharedPref = context.getSharedPreferences("dojob", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("PREVIEW_RESPONSE", response);
        editor.apply();
    }

    public static SetupResponse getPreviewSetupResponse(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("dojob", Context.MODE_PRIVATE);
        String storedCategories = sharedPref.getString("PREVIEW_RESPONSE", "");
        if (!storedCategories.isEmpty())
            return App.gson().fromJson(storedCategories, SetupResponse.class);
        return null;
    }
}
