package com.dojob.backend.model;

public class SearchCategorySubInfoResponse {
    public int id;
    public String name;
    public int category_id;
    public String translation_key;
}
