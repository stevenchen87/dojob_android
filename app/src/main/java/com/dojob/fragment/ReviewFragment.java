package com.dojob.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dojob.R;
import com.dojob.activity.MainActivity;
import com.dojob.adapter.InboxAdapter;
import com.dojob.adapter.PortfolioAdapter;
import com.dojob.backend.ProfileApi;
import com.dojob.backend.model.ReviewResponse;
import com.dojob.backend.model.UserResponse;
import com.dojob.core.App;
import com.dojob.core.BackgroundWorker;
import com.dojob.core.Result;
import com.dojob.listener.EndlessRecyclerOnScrollListener;
import com.dojob.util.Utils;
import com.github.ybq.android.spinkit.SpinKitView;

public class ReviewFragment extends Fragment implements BackgroundWorker.Callbacks {
    public final static String REVIEW_RESPONSE_DATA = "review_response_data";
    private final static String TASK_REQUEST_REVIEWS = "tasl_request_reviews";
    private UserResponse mUserResponse;
    private SpinKitView mProgress;
    private RecyclerView mRecyclerView;
    private PortfolioAdapter mAdapter;
    private ProfileApi mProfileApi;
    private BackgroundWorker mWorker;
    private boolean mNextPage;
    private String mNextUrl;

    public static ReviewFragment newInstance(Bundle args) {
        ReviewFragment fragment = new ReviewFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getActivity() != null && getArguments() != null){
            if(getArguments().containsKey(REVIEW_RESPONSE_DATA)){
                mUserResponse = App.gson().fromJson(getArguments().getString(REVIEW_RESPONSE_DATA), UserResponse.class);
            }
        }
        mWorker = new BackgroundWorker(getActivity());
        mProfileApi = (ProfileApi) mWorker.get(ProfileApi.TAG);
        if (mProfileApi == null) {
            Log.i("PROFILE_API", "NULL");
            mProfileApi = new ProfileApi(getContext());
            mWorker.put(ProfileApi.TAG, mProfileApi);
        } else {
            Log.i("PROFILE_API", "NOT_NULL");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_albums, container, false);
        Utils.hideKeyBoard(getActivity());
        AppCompatActivity activity = ((AppCompatActivity)getActivity());
        if(activity != null){
            ActionBar actionbar = activity.getSupportActionBar();
            if (actionbar != null) {
                actionbar.setDisplayHomeAsUpEnabled(true);
                actionbar.setDisplayShowHomeEnabled(true);
            }
            if(activity instanceof MainActivity){
                ((MainActivity) activity).setTitle("Rating and Reviews");
            }
            mProgress = view.findViewById(R.id.progress);
            mProgress.setVisibility(View.VISIBLE);
            mRecyclerView = view.findViewById(R.id.recycle_view);
            mRecyclerView.setVisibility(View.VISIBLE);
            mRecyclerView.setItemViewCacheSize(20);
            mRecyclerView.setDrawingCacheEnabled(true);
            mRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
            mAdapter = new PortfolioAdapter(getContext(),0);
            mAdapter.setHasStableIds(true);
            mRecyclerView.setAdapter(mAdapter);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            if(mWorker != null) mWorker.executeNewTask(TASK_REQUEST_REVIEWS, new Bundle(), ReviewFragment.this);
            mRecyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
                @Override
                public void onLoadMore() {
                    if(mNextPage && mNextUrl != null){
                        Bundle bundle = new Bundle();
                        bundle.putString("reviews_url", mNextUrl);
                        if(mWorker != null) mWorker.executeNewTask(TASK_REQUEST_REVIEWS, bundle, ReviewFragment.this);
                    }
                }
            });
        }
        return view;
    }


    @Override
    public Result executeTaskInBackground(String id, Bundle args) {
        switch (id){
            case TASK_REQUEST_REVIEWS:
                return mProfileApi.getReviews(mUserResponse.profile.id, args.getString("reviews_url", null));

        }
        return null;
    }

    @Override
    public void onBackgroundTaskCompleted(String id, Result result) {
        switch (id){
            case TASK_REQUEST_REVIEWS:
                if(result.isSuccess() && mAdapter != null){
                    ReviewResponse response = (ReviewResponse) result.getResult();
                    mNextPage = response.currentPage < response.lastPage;
                    if(mNextPage){
                        mNextUrl = response.nextPageUrl;
                    }
                    else{
                        mNextUrl = null;
                    }
                    mAdapter.updateReviewItems(mUserResponse, response);
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mProgress.setVisibility(View.GONE);
                }
                break;
        }
    }
}
