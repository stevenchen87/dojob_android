package com.dojob.adapter.model;

import android.text.format.DateUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dojob.R;
import com.dojob.backend.model.InboxResponse;
import com.dojob.backend.model.InboxMessageResponse;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;

public class InboxItem {
    public static final int LAYOUT_ID = R.layout.layout_inbox_item;
    private final InboxResponse mInboxResponse;

    public InboxItem(InboxResponse response) {
        mInboxResponse = response;
    }

    public void bindViewHolder(ViewHolder holder, int position){
        if(holder.container != null){
            holder.container.setTag(position);
        }
        if (holder.avatar != null){
            holder.avatar.setTag(position);
        }

        InboxMessageResponse response = null;
        if( mInboxResponse.messages.length > 0){
            response =  mInboxResponse.messages[0];
        }
        if(holder.name != null){
            holder.name.setText((response != null)?response.chatroomData.name:mInboxResponse.name);
        }
        if(holder.content != null){
            holder.content.setText(mInboxResponse.last_message);
        }
        if(holder.avatar != null){
            Picasso.get().load((response != null)?response.chatroomData.profile_image:mInboxResponse.avatar).into(holder.avatar);
        }
        if(holder.time != null){
            String value = new SimpleDateFormat("hh:mm a").format(new java.util.Date(mInboxResponse.last_seen * 1000));
            if(!DateUtils.isToday(mInboxResponse.last_seen * 1000)){
                value = new SimpleDateFormat("dd/MM").format(new java.util.Date(mInboxResponse.last_seen * 1000));
            }
            holder.time.setText(value);
        }

        if(holder.badge != null){
            holder.badge.setVisibility((mInboxResponse.chatIsRead > 0 ?View.VISIBLE:View.GONE));
        }

        if(holder.unread != null){
            holder.unread.setText(String.valueOf(mInboxResponse.chatIsRead));
        }
    }

    public InboxMessageResponse[] getMessages(){
        return mInboxResponse.messages;
    }

    public String getName(){
        return mInboxResponse.messages[0].chatroomData.name;
    }

    public int getId(){
        return mInboxResponse.messages[0].chatroomData.id;
    }

    public static class ViewHolder{
        public View badge;
        public View container;
        public TextView name;
        public TextView content;
        public ImageView avatar;
        public TextView time;
        public TextView unread;

        public ViewHolder(View view){
            container = view.findViewById(R.id.inbox_container);
            avatar = view.findViewById(R.id.inbox_avatar);
            name = view.findViewById(R.id.inbox_name_sent);
            content = view.findViewById(R.id.inbox_content_sent);
            time = view.findViewById(R.id.inbox_time_last_sent);
            badge = view.findViewById(R.id.inbox_avatar_badge);
            unread = view.findViewById(R.id.inbox_avatar_total_unread);
        }
    }
}
