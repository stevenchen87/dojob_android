package com.dojob.fragment;

import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.asksira.bsimagepicker.BSImagePicker;
import com.dojob.BuildConfig;
import com.dojob.R;
import com.dojob.activity.MainActivity;
import com.dojob.adapter.AlbumsAdapter;
import com.dojob.adapter.layout.AlbumsSpanSizeLookup;
import com.dojob.backend.GalleryApi;
import com.dojob.backend.ProfileApi;
import com.dojob.backend.SetupApi;
import com.dojob.backend.model.CommentsInboxResponse;
import com.dojob.backend.model.FileResponse;
import com.dojob.backend.model.GalleryAlbumsItemResponse;
import com.dojob.backend.model.GalleryResponse;
import com.dojob.backend.model.UserResponse;
import com.dojob.core.App;
import com.dojob.core.BackgroundWorker;
import com.dojob.core.Logger;
import com.dojob.core.Result;
import com.dojob.listener.CompressCallbacbkListener;
import com.dojob.util.ResourcesUtil;
import com.dojob.util.Utils;
import com.github.ybq.android.spinkit.SpinKitView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static com.dojob.fragment.PortfolioFragment.ARGS_PORTFOLIO_DATA;

public class AlbumsFragment extends Fragment implements AlbumsAdapter.Listener,
        BSImagePicker.OnMultiImageSelectedListener,
        BackgroundWorker.Callbacks,
        BSImagePicker.OnSingleImageSelectedListener{
    public final static String ALBUM_ITEM_RESPONSE = "album_item_response";
    public final static String ALBUMS_RESPONSE = "albums_response";

    public final static String ALBUM_TYPE = "album_type";
    public final static int TYPE_ALL = 0;
    public final static int TYPE_ITEM = 1;

    private final String OBJ_ALBUM_API = "obj_album_api";
    private final String TASK_SET_ALBUM = "task_set_album";
    private final String TASK_DELETE_ALBUM = "task_delete_album";
    private final String TASK_ADD_FILE = "task_add_file";
    private final String TASK_GET_ALBUM = "task_get_album";

    private SpinKitView mProgress;
    private RecyclerView mRecyclerView;
    private AlbumsAdapter mAdapter;
    private GalleryAlbumsItemResponse mAlbumsItemResponse;
    private GalleryAlbumsItemResponse[] mAlbumsResponse;
    private BSImagePicker multiSelectionPicker;
    private BackgroundWorker mWorker;
    private GalleryApi mAlbumApi;
    private HashMap<String, Uri> file;
    private HashMap<String, String> data;
    private int mType;
    private boolean mUploading;
    private int mUserId;

    public static AlbumsFragment newInstance(Bundle args) {
        AlbumsFragment fragment = new AlbumsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            if(getArguments().containsKey("user_id"))
                mUserId = getArguments().getInt("user_id");
            if(getArguments().containsKey(ALBUM_TYPE)){
                mType = getArguments().getInt(ALBUM_TYPE);
            }
            if(getArguments().containsKey(ALBUM_ITEM_RESPONSE)){
                mAlbumsItemResponse = App.gson().fromJson(getArguments().getString(ALBUM_ITEM_RESPONSE), GalleryAlbumsItemResponse.class);
                getArguments().remove(ALBUM_ITEM_RESPONSE);
            }
            else if(getArguments().containsKey(ALBUMS_RESPONSE)){
                mAlbumsResponse =  App.gson().fromJson(getArguments().getString(ALBUMS_RESPONSE), GalleryAlbumsItemResponse[].class);
            }
        }

        if(getActivity() != null){
            mWorker = new BackgroundWorker(getActivity());
            mAlbumApi = (GalleryApi) mWorker.get(OBJ_ALBUM_API);
            if (mAlbumApi == null) {
                mAlbumApi = new GalleryApi(getContext());
                mWorker.put(OBJ_ALBUM_API, mAlbumApi);
            }

        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_albums, container, false);
        AppCompatActivity activity = ((AppCompatActivity)getActivity());
        if(activity != null && mAlbumsItemResponse != null) {
            ActionBar actionbar = activity.getSupportActionBar();
            if (actionbar != null) {
                actionbar.setDisplayHomeAsUpEnabled(true);
                actionbar.setDisplayShowHomeEnabled(true);
            }
        }
        Utils.hideKeyBoard(getActivity());
        mRecyclerView = view.findViewById(R.id.recycle_view);
        mRecyclerView.setItemViewCacheSize(20);
        mRecyclerView.setDrawingCacheEnabled(true);
        mRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        Log.d("AlbumsFragment", "onCreateView:"+ mAlbumsItemResponse!=null?"ALBUM_ITEM_LIST_TYPE":"ALBUM LIST TYPE");
        if(mAlbumsItemResponse != null){
            mAdapter = new AlbumsAdapter(getContext(), mUserId, AlbumsAdapter.ALBUM_ITEM_LIST_TYPE, mWorker, this);
        }
        else{
            mAdapter = new AlbumsAdapter(getContext(), mUserId, AlbumsAdapter.ALBUMS_LIST_TYPE, mWorker, this);
        }
        mAdapter.setListener(this);
        mAdapter.setHasStableIds(true);
        mRecyclerView.setAdapter(mAdapter);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 6);
        gridLayoutManager.setSpanSizeLookup(new AlbumsSpanSizeLookup(mAdapter));
        mRecyclerView.setLayoutManager(gridLayoutManager);
        mProgress = view.findViewById(R.id.progress);
        return view;
    }
    public void reloadData(){
        if(mWorker != null) mWorker.executeNewTask(TASK_GET_ALBUM, null, this);
    }
    @Override
    public void onResume() {
        super.onResume();
        if(mType == TYPE_ALL && getContext() != null){
            if(mAlbumsResponse != null){
                mRecyclerView.setVisibility(View.VISIBLE);
                mProgress.setVisibility(View.GONE);

                mAdapter.updateItem(mAlbumsResponse);
            }
            else{
                reloadData();
            }
        }
        else if(mType == TYPE_ITEM && mAlbumsItemResponse != null && !mUploading){
            mRecyclerView.setVisibility(View.VISIBLE);
            mProgress.setVisibility(View.GONE);
            mAdapter.updateItem(mAlbumsItemResponse);
            if(mAlbumsItemResponse.files != null && mAlbumsItemResponse.files.length == 0){
                openPickerToAddAlbum();
            }
        }
    }

    @Override
    public void onOpenAlbumClicked(GalleryAlbumsItemResponse item) {
        AppCompatActivity activity = ((AppCompatActivity)getActivity());
        if(activity != null){
            if(activity instanceof MainActivity){
                if (item != null) {
                    ((MainActivity) activity).openAlbumPage(item);
                }
                else {
                    showCreateAlbumDialog();
                }
            }
        }
    }

    @Override
    public void onOpenImageViewer(String url) {
        AppCompatActivity activity = ((AppCompatActivity)getActivity());
        if(activity != null){
            if(activity instanceof MainActivity){
                ((MainActivity) activity).openImageViewer(url);
            }
        }
    }

    @Override
    public void onOpenVideoPlayer(String url) {
        Utils.openVideoPlayer(getContext(), url);
    }

    @Override
    public void onOpenMediaPicker() {
        openPickerToAddFile();
    }

    @Override
    public void onOpenCommentFragment(String type, int position, int fileId) {
        AppCompatActivity activity = ((AppCompatActivity)getActivity());
        if(activity != null){
            if(activity instanceof MainActivity){
                ((MainActivity) activity).openCommentFragment(type,position,fileId);
            }
        }

    }

    private void showCreateAlbumDialog(){
        if(getContext() != null){
            AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.AlertDialog));
            builder.setTitle("Create Album");
            final View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_create_albums, null);
            final TextInputEditText name = view.findViewById(R.id.input_name);
            final TextInputEditText desc = view.findViewById(R.id.input_description);
            final RadioGroup radioGroup =  view.findViewById(R.id.radio);
            builder.setView(view);
            builder.setPositiveButton("Create", null);
            builder.setNegativeButton("Cancel", null);
            final AlertDialog dialog = builder.create();
            dialog.show();
            final Button positiveDialog = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
            // name edit text on landing is empty
            if (TextUtils.isEmpty(name.getText())) {
                positiveDialog.setEnabled(false);
                positiveDialog.setTextColor(Color.GRAY);
            }
            positiveDialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    RadioButton radioButton = view.findViewById(radioGroup.getCheckedRadioButtonId());
                    setUpAlbum(name.getText().toString(), desc.getText().toString(), radioButton.getText().toString());
                    if(name.hasFocus()){
                        name.clearFocus();
                    }
                    if(desc.hasFocus()){
                        desc.clearFocus();
                    }
                    dialog.dismiss();
                }
            });
            name.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if (TextUtils.isEmpty(editable)) {
                        // Disable ok button
                        positiveDialog.setTextColor(Color.GRAY);
                        positiveDialog.setEnabled(false);
                    } else {
                        // Something into edit text. Enable the button.
                        positiveDialog.setTextColor(ResourcesUtil.getColor(R.color.yellow));
                        positiveDialog.setEnabled(true);
                    }
                }
            });
        }
    }

    private void setUpAlbum(String name, String desc, String albumType){
        GalleryAlbumsItemResponse itemResponse = new GalleryAlbumsItemResponse();
        itemResponse.name = name;
        itemResponse.description = desc;
        itemResponse.album_type = albumType;
        itemResponse.created_at = Utils.nowTimestamp();
        itemResponse.updated_at = Utils.nowTimestamp();
        itemResponse.files = new FileResponse[0];
        onOpenAlbumClicked(itemResponse);
    }

    public void openPickerToAddAlbum(){//to add album
        openMediaPicker("add_album");
    }

    public void openPickerToAddFile(){ //to add file
        openMediaPicker("add_file");
    }

    public void openMediaPicker(String tag){
        if(mAlbumsItemResponse.album_type.equals("video")){
            multiSelectionPicker = new BSImagePicker.Builder(BuildConfig.APPLICATION_ID + ".provider")
                    .setTag(tag)
                    .hideCameraTile()
                    .hideGalleryTile()
                    .isLoadVideo(true)
                    .setMultiSelectBarBgColor(android.R.color.white) //Default: #FFFFFF. You can also set it to a translucent color.
                    .setMultiSelectTextColor(R.color.primary_text) //Default: #212121(Dark grey). This is the message in the multi-select bottom bar.
                    .setMultiSelectDoneTextColor(R.color.green) //Default: #388e3c(Green). This is the color of the "Done" TextView.
                    .setOverSelectTextColor(R.color.error_text) //Default: #b71c1c. This is the color of the message shown when user tries to select more than maximum select count.
                    .disableOverSelectionMessage() //You can also decide not to show this over select message.
                    .build();
        }
        else{
            multiSelectionPicker = new BSImagePicker.Builder(BuildConfig.APPLICATION_ID + ".provider")
                    .setTag(tag)
                    .isLoadVideo(false)
                    .isMultiSelect() //Set this if you want to use multi selection mode.
                    .setMinimumMultiSelectCount(1) //Default: 1.
                    .setMaximumMultiSelectCount(5) //Default: Integer.MAX_VALUE (i.e. User can select as many images as he/she wants)
                    .setMultiSelectBarBgColor(android.R.color.white) //Default: #FFFFFF. You can also set it to a translucent color.
                    .setMultiSelectTextColor(R.color.primary_text) //Default: #212121(Dark grey). This is the message in the multi-select bottom bar.
                    .setMultiSelectDoneTextColor(R.color.green) //Default: #388e3c(Green). This is the color of the "Done" TextView.
                    .setOverSelectTextColor(R.color.error_text) //Default: #b71c1c. This is the color of the message shown when user tries to select more than maximum select count.
                    .disableOverSelectionMessage() //You can also decide not to show this over select message.
                    .build();
        }

        multiSelectionPicker.show(getChildFragmentManager(), tag);
    }

    @Override
    public Result executeTaskInBackground(String id, Bundle args) {
        switch (id){
            case TASK_DELETE_ALBUM:
                final HashMap<String, String> deletedFileId = new HashMap<>();
                deletedFileId.put("0", args.get("id").toString());
                return mAlbumApi.deleteAlbum(deletedFileId);
            case TASK_SET_ALBUM:
                return mAlbumApi.setAlbum(data, file);
            case TASK_ADD_FILE:
                return mAlbumApi.addFile(data, file);
            case TASK_GET_ALBUM:
                return mAlbumApi.getGallery(mType, String.valueOf(mUserId));
        }
        return null;
    }

    @Override
    public void onBackgroundTaskCompleted(String id, Result result) {
        switch (id){
            case TASK_DELETE_ALBUM:
                reloadData();
                break;
            case TASK_SET_ALBUM:
            case TASK_ADD_FILE:
                mUploading = false;
                if(result.isSuccess() && mAdapter != null){
                    GalleryResponse response = (GalleryResponse) result.getResult();
                    if(response.status.equals("ok") && getContext() != null){
                        mRecyclerView.setVisibility(View.VISIBLE);
                        mProgress.setVisibility(View.GONE);
                        if(mAlbumsResponse == null){
                            mAlbumsResponse = new GalleryAlbumsItemResponse[0];
                        }
                        List<GalleryAlbumsItemResponse> albums = new ArrayList<>(Arrays.asList(mAlbumsResponse));
                        albums.add(response.albumData);
                        mAlbumsResponse = albums.toArray(new GalleryAlbumsItemResponse[albums.size()]);
                        mAdapter.updateItem(response.albumData);
                    }
                }
                if(mProgress != null){
                    mProgress.setVisibility(View.GONE);
                }
                break;
            case TASK_GET_ALBUM:
                mUploading = false;
                if(result.isSuccess() && mAdapter != null){
                    UserResponse response = (UserResponse) result.getResult();
                    if(response.status.equals("ok") && getContext() != null){
                        mAdapter.updateItem(response.albumData);
                        mRecyclerView.setVisibility(View.VISIBLE);
                        mProgress.setVisibility(View.GONE);
                        mAlbumsResponse = response.albumData;
                    }
                }
                if(mProgress != null){
                    mProgress.setVisibility(View.GONE);
                }
                break;
        }
    }

    @Override
    public void onSingleImageSelected(Uri uri, final String tag) {
        setupParam(tag);
        mUploading = true;
        Utils.compressVideo(getActivity(), uri, new CompressCallbacbkListener() {
            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(Uri uri) {
                file.put("albumContent[0]",uri);
                request(tag);
            }

            @Override
            public void onError() {
            }

            @Override
            public void onProgress(final float percent) { }
        });
        multiSelectionPicker.dismiss();
    }

    @Override
    public void onMultiImageSelected(List<Uri> uriList, String tag) {
        Log.d("AlbumsFragment", tag+", onMultiImageSelected"+uriList.toString());
        mUploading = true;
        setupParam(tag);
        if(uriList.size() > 0){
            for(int i = 0; i< uriList.size(); i++){
                Uri uri = uriList.get(i);
                file.put("albumContent["+i+"]",uri);
            }
            request(tag);
        }
    }

    private void setupParam(String tag){
        data = new HashMap<>();
        if(tag.equals("add_album")){
            data.put("albumName", mAlbumsItemResponse.name);
            data.put("albumDescription", mAlbumsItemResponse.description);
            data.put("albumType", mAlbumsItemResponse.album_type);
        }
        else if(tag.equals("add_file")){
            data.put("albumID", String.valueOf(mAlbumsItemResponse.id));
        }
        //data.put("albumDesc", JSON);
        file = new HashMap<>();
    }

    private void request(String tag){
        if(tag.equals("add_album")){
            if(mWorker != null) mWorker.executeNewTask(TASK_SET_ALBUM, null, this);
        }
        else if(tag.equals("add_file")){
            if(mWorker != null) mWorker.executeNewTask(TASK_ADD_FILE, null, this);
        }

        if(mProgress != null){
            mProgress.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBSDestroy() {
    }
}
