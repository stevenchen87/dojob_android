package com.dojob.adapter.model;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.dojob.GlideApp;
import com.dojob.R;
import com.dojob.backend.model.Comment;
import com.dojob.backend.model.CommentsInboxResponse;
import com.dojob.backend.model.CommentsResponse;
import com.dojob.core.App;
import com.dojob.util.Utils;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class CommentOtherItem {
    public static final int LAYOUT_ID = R.layout.layout_comment_other;
    private Comment mResponse;
    private Comment[] mInboxResponse;
    private Context mContext;

    public CommentOtherItem(CommentsInboxResponse response, Context context) {
        mInboxResponse = response.comments;
        this.mContext = context;
        if(response.comments != null && response.comments.length > 0){
            mResponse = response.comments[response.comments.length-1];
            return;
        }

        mResponse = null;
    }

    public CommentOtherItem(Comment response,Context context) {
        this.mContext = context;
        mResponse = response;
    }

    public void bindViewHolder(ViewHolder holder, int position){
        if(mResponse != null){
            if(holder.date != null){
                long remainingTime = (Utils.nowTimestamp() - mResponse.updated_at) * 1000;
                holder.date.setText(Utils.getRemainingTime(remainingTime));
            }
            if(holder.content != null){
                holder.content.setText(mResponse.msg_body);
            }
            if(holder.image != null){
                GlideApp.with(mContext)
                        .load(mResponse.profile_image)
                        .placeholder(R.drawable.profile_header_x3)
                        .apply(RequestOptions.fitCenterTransform())
                        .skipMemoryCache(false)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.image);
               /* App.picasso().load(mResponse.profile_image).fit().centerCrop().into(holder.image);*/
            }
            if(holder.name != null){
                holder.name.setText(mResponse.name);
            }
            if(holder.status != null){
                if(mResponse.vip){

                    App.picasso().load(R.drawable.vip_x3).into(holder.status);
                }else{
                    App.picasso().load(R.drawable.regular_x3).into(holder.status);
                }
            }
            if(holder.moreReply != null){
                if(mInboxResponse != null && mInboxResponse.length > 0){
                    if(mInboxResponse.length > 1){
                        holder.moreReply.setText("View Replies");
                    }
                    else{
                        holder.moreReply.setText("View Reply");
                    }
                    holder.moreReply.setVisibility(View.VISIBLE);
                    holder.moreReply.setTag(position);
                }
                else{
                    holder.moreReply.setVisibility(View.GONE);
                }
            }
        }
    }

    public Comment[] getComments(){
        return mInboxResponse;
    }

    public static class ViewHolder{
        public TextView moreReply;
        public ImageView status;
        public TextView date;
        public TextView content;
        public ImageView image;
        public TextView name;

        public ViewHolder(View view){
            date = view.findViewById(R.id.comment_other_last_reply);
            content = view.findViewById(R.id.comment_other_msg);
            image = view.findViewById(R.id.comment_other_image);
            name = view.findViewById(R.id.comment_other_name);
            status = view.findViewById(R.id.comment_other_status);
            moreReply = view.findViewById(R.id.comment_other_view_reply);
        }
    }
}
