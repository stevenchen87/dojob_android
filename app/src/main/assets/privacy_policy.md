# PRIVACY POLICY


## Privacy Statement
Step4work respects and takes matters pertaining to privacy very seriously, therefore we would suggest that users read our privacy policy carefully so that you may understand how Step4work uses your personal data.
The purpose of Step4work is to solely act as a platform to enable potential Recruiters to get their special work done and for Talents to earn extra income with their passions. Regarding the personal information that you share with us, you may choose whether or not to share or the extent to which you would like to share, with a minimum requirement of your name and email (and for Talent profiles, a profile photo and at least one Talent photo). We will not share any of your private information without your prior permission.
We do not sell or distribute any personal information or data that you have provided Step4work without your prior permission.

## Types of Information That We Collect
Information You Provide
We collect content and other information that you provide when you use our Services, including when you register for an account, create or share, message or communicate with others. This includes information such as service created or date of photo uploaded. We also collect information about how you use the Step4work app, such as the type of content you view or engage with or the frequency and duration of your activities.

## Information Others Provide
We collect content and information that other people provide when they use the Step4work app, including information about you, such as when they share a photo of yours or send a message to you.

## Information from Third Parties
We receive information about you and your activities on and off the Step4work app from third-party services (e.g., Facebook). They may send us information such as your registration and profile information from that service. This information varies and is controlled by that service or as authorised by you via your privacy settings at that service.

## How We Use This Information
We use, store and process information about you to provide, understand, improve and develop the Step4work app, and to create and maintain a trusted and safer environment. Examples of such use include

   • Verify or authenticate information or identifications provided by you (such as when we send a confirmation to your registered email account for account validation). You may opt to register with your Facebook social media account for your convenience.
    
   • Enhance the security and safety of the Step4work app and experience, by performing analytics and conducting research.
    
   • Provide customer services such as sending service or support messages, updates, security alerts, and account notifications.
    
   • Personalise or otherwise customise your experience by, among other things, ranking search results or showing ads based on your search, booking history, and preferences.
    
   • Send you promotional messages, marketing, advertising and other information that may be of interest to you based on your communication preferences.
    
   • Feature service posting on our social media page (Facebook) for content purposes and to promote the use of our portal.

## Editing Personal Details
You may change your personal details from your profile settings page at any time by adding or subtracting details but requiring a minimum of your display name and email address (and for Talent profiles, a profile photo and at least one Talent photo).

## Cookies
Cookies in your browser enable us to better serve you by suggesting posts or services that might be of interest to you. If you wish to disable the cookies, you may adjust it in your browser settings.

## Privacy Policy Changes
Step4work’s Privacy Policy and Terms of Use may be revised periodically or whenever necessary and will be updated on our website for your reference.












