package com.dojob.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.dojob.Additions.CategoryFragment;
import com.dojob.R;
import com.dojob.backend.ProfileApi;
import com.dojob.util.ResourcesUtil;

import static com.dojob.fragment.ProfileSettingFragment.PROFILE_SETTING_CATEGORY_CHOOSED;

public class CategoryActivity extends AppCompatActivity {
    private FragmentManager mManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setHomeAsUpIndicator(ResourcesUtil.getDrawable(R.drawable.baseline_navigate_before_black_24));
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setDisplayShowHomeEnabled(true);
        }
        TextView title = findViewById(R.id.toolbar_title);
        title.setVisibility(View.VISIBLE);
        if (ProfileApi.getRole(this).equals("recruiter")) {
            title.setText(getString(R.string.industry));
        } else {
            title.setText("Category");
        }

        mManager = getSupportFragmentManager();
        CategoryFragment categoryFragment = new CategoryFragment();
        categoryFragment.setArguments(getIntent().getExtras());
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content, categoryFragment)
                .commit();
    }

    public void OnCategoryClick(Bundle bundle) {
        Intent localIntent = new Intent(PROFILE_SETTING_CATEGORY_CHOOSED);
        localIntent.putExtras(bundle);
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
