package com.dojob.Additions.local;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AnnoucncementResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("announcement_data")
    @Expose
    private List<AnnouncementItem> announcementData = null;
    @SerializedName("biggest_announcement_id")
    @Expose
    private Integer biggestAnnouncementId;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<AnnouncementItem> getAnnouncementData() {
        return announcementData;
    }

    public void setAnnouncementData(List<AnnouncementItem> announcementData) {
        this.announcementData = announcementData;
    }

    public Integer getBiggestAnnouncementId() {
        return biggestAnnouncementId;
    }

    public void setBiggestAnnouncementId(Integer biggestAnnouncementId) {
        this.biggestAnnouncementId = biggestAnnouncementId;
    }

}