package com.dojob.backend.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 30-Nov-18.
 */
public class FavouriteUserResponse {

    public int id;
    public String name;
    public String url;
    public String cover_photo;
    public CategoryInfo category_info;
    @SerializedName("country_alpha3")
    public String countrycode;

}
