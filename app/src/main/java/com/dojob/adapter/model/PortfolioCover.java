package com.dojob.adapter.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.dojob.GlideApp;
import com.dojob.R;
import com.dojob.backend.LoginApi;
import com.dojob.backend.model.MediaResponse;
import com.dojob.backend.model.UserResponse;
import com.dojob.backend.model.VideosResponse;
import com.dojob.core.App;
import com.dojob.core.Logger;
import com.dojob.fragment.CommentFragment;
import com.dojob.util.ResourcesUtil;
import com.pixelcan.inkpageindicator.InkPageIndicator;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

public class PortfolioCover{
    public static final int TYPE_COVER = 1; // photo
    public static final int TYPE_VIDOES = 2;

    public static final int LAYOUT_ID = R.layout.layout_portfolio_cover;
    private UserResponse mResponse;
    private Context mContext;
    private int mType;
    private int mPosition;
    private ViewPager mImageViewPager;
    private MediaResponse[] gallery;

    public PortfolioCover(Context context, UserResponse response, int type) {
        mResponse = response;
        mContext = context;
        mType = type;
        mPosition = 0;
    }

    public PortfolioCover(Context context, UserResponse response, int type, int position) {
        mResponse = response;
        mContext = context;
        mType = type;
        mPosition = position;
    }

    public void bindViewHolder(final ViewHolder holder, int position) {
        if (holder.hotImage != null) {
            holder.hotImage.setVisibility(mResponse.profile.hot && mType == TYPE_COVER ? View.VISIBLE : View.GONE);
        }
        if (holder.moreImage != null) {
            holder.moreImage.setVisibility((mResponse.talentPhoto != null &&  mResponse.talentPhoto.length > 0)?View.VISIBLE:View.GONE);
            holder.moreImage.setTag(position);
        }
        if (holder.vp != null && holder.indicator != null) {
            mImageViewPager = holder.vp;
            if(mType == TYPE_COVER &&  mResponse.talentPhoto != null &&  mResponse.talentPhoto.length > 0){
                ArrayList<String> list = new ArrayList<>();
                gallery = mResponse.talentPhoto;
                for(MediaResponse photoResponse: gallery){
                    list.add(photoResponse.url);
                }
                holder.vp.setAdapter(new ImagesPagerAdapter(mContext, mType, list.toArray(new String[list.size()])));
                holder.vp.setCurrentItem(mPosition);
                holder.indicator.setViewPager(holder.vp);
                holder.vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                    }

                    @Override
                    public void onPageSelected(int position) {
                        updateLikeButton(holder,position);
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {
                    }
                });
            }
            else if(mType == TYPE_VIDOES &&  mResponse.profile.featuredVideo != null &&  mResponse.profile.featuredVideo.length > 0){
                gallery = mResponse.profile.featuredVideo;
                ArrayList<String> thumbnails = new ArrayList<>();
                for(MediaResponse video: mResponse.profile.featuredVideo){
                    if(video.thumbnail != null){
                        thumbnails.add(video.thumbnail);
                    }
                    else {
                        thumbnails.add(video.url);
                    }
                }
                holder.vp.setAdapter(new ImagesPagerAdapter(mContext, mType, thumbnails.toArray(new String[thumbnails.size()])));
                holder.indicator.setViewPager(holder.vp);
                holder.vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                    }

                    @Override
                    public void onPageSelected(int position) {
                        updateLikeButton(holder,position);
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {
                    }
                });
            }
            holder.vp.setTag(position);
        }



        if(holder.playButton != null){
            holder.playButton.setVisibility( mType == TYPE_VIDOES?View.VISIBLE : View.GONE);
            if( mType == TYPE_VIDOES){
                holder.playButton.setTag(position);
            }
        }

        if(holder.commentEditText != null){
            holder.commentEditText.setTag(position);
        }

        if(holder.favImage != null){
            holder.favImage.setTag(position);
            updateLikeButton(holder, mPosition);
        }

        if(holder.shareImage != null){
            holder.shareImage.setTag(position);
        }

        if(holder.formatImage != null){
            holder.formatImage.setTag(position);
        }
    }

    private Bitmap getBitmap(Uri uri){
        return ThumbnailUtils.createVideoThumbnail(uri.getPath() , MediaStore.Video.Thumbnails.MINI_KIND);
    }

    public void updateLikeButton(ViewHolder holder, int position){
        if (holder.favImage != null) {
            gallery = (mType == 1)?mResponse.talentPhoto:mResponse.profile.featuredVideo;
            if (gallery!= null && gallery.length > 0 && gallery.length > position && gallery[position].like_status == 1) {
                App.picasso().load(R.drawable.baseline_favorite_black_24).into(holder.favImage);
                holder.favImage.setColorFilter(ResourcesUtil.getColor(R.color.red), PorterDuff.Mode.SRC_IN);
            }
            else{
                App.picasso().load(R.drawable.baseline_favorite_border_black_24).into(holder.favImage);
                holder.favImage.setColorFilter(ResourcesUtil.getColor(R.color.green), PorterDuff.Mode.SRC_IN);
            }
        }

        if (gallery != null && gallery.length>position){
            if (holder.tvlikeCounter != null)
            holder.tvlikeCounter.setText(gallery[position].like_count+"");
        }
    }

    public int getCurrentPosition(){
        return mImageViewPager.getCurrentItem();
    }

    public String getType(){
        String type = "";
        if(mType == TYPE_COVER){
            return CommentFragment.TYPE_IMAGE;
        }
        else if(mType == TYPE_VIDOES){
            return CommentFragment.TYPE_VIDEO;
        }
        return CommentFragment.TYPE_IMAGE;
    }

    public String getCurrentCoverImage(){
        if(mImageViewPager != null){
            String[] images = new String[]{mResponse.profile.cover_photo};
            return images[mImageViewPager.getCurrentItem()];
        }

        return "http://default image link";
    }

    public static class ViewHolder{

        public View container;
        public ViewPager vp;
        public ImageView hotImage;
        public ImageView moreImage;
        public TextView commentEditText;
        public ImageView favImage;
        public ImageView shareImage;
        public ImageView formatImage;
        public InkPageIndicator indicator;
        public View playButton;
        public TextView tvlikeCounter;

        public ViewHolder(View view){
            vp = view.findViewById(R.id.portfolio_cover_photo);
            hotImage = view.findViewById(R.id.portfolio_cover_hot);
            moreImage = view.findViewById(R.id.portfolio_cover_more);
            commentEditText = view.findViewById(R.id.portfolio_cover_comment);
            favImage = view.findViewById(R.id.portfolio_cover_favourite);
            shareImage = view.findViewById(R.id.portfolio_cover_share);
            formatImage = view.findViewById(R.id.portfolio_cover_format);
            indicator = view.findViewById(R.id.portfolio_indicator);
            playButton = view.findViewById(R.id.portfolio_cover_play);
            container = view.findViewById(R.id.cover_container);
            tvlikeCounter = view.findViewById(R.id.tvlikeCounter);
        }
    }

    private class ImagesPagerAdapter extends PagerAdapter  {
        int MAX = 10;
        LayoutInflater mInflater;
        int mNumberPages;
        String[] mPhotos;
        int mType;

        public ImagesPagerAdapter(Context context, int type, String[] photos) {
            mNumberPages = photos.length;
            mPhotos = photos;
            mInflater = LayoutInflater.from(context);
            mType = type;
        }

        @Override
        public int getCount() {
            return mNumberPages >= MAX? MAX:mNumberPages;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(@NonNull ViewGroup collection, int position) {
            ImageView iv = (ImageView)mInflater.inflate(R.layout.layout_portfolio_cover_image, collection, false);
            if(URLUtil.isNetworkUrl(mPhotos[position])){
                GlideApp.with(mContext)
                        .load(mPhotos[position])
                        .placeholder(R.drawable.placeholder_1)
                        .apply(RequestOptions.fitCenterTransform())
                        .skipMemoryCache(false)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(iv);
            }
            else{
                if(mType == TYPE_COVER){
                    GlideApp.with(mContext)
                            .load(Uri.fromFile(new File(mPhotos[position])))
                            .placeholder(R.drawable.placeholder_1)
                            .apply(RequestOptions.fitCenterTransform())
                            .skipMemoryCache(false)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(iv);
                }
                else{
                    GlideApp.with(mContext)
                            .asBitmap()
                            .load(getBitmap(Uri.parse(mPhotos[position])))
                            .placeholder(R.drawable.placeholder_1)
                            .apply(RequestOptions.fitCenterTransform())
                            .skipMemoryCache(false)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(iv);
                }
            }
            collection.addView(iv);
            return iv;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup collection, int position, @NonNull Object view) {
            collection.removeView((View) view);
        }

    }
}
