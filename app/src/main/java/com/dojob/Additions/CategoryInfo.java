package com.dojob.Additions;

import com.google.gson.annotations.SerializedName;

public class CategoryInfo {

    @SerializedName("cat")
    Cat cat;

    @SerializedName("sub_cat")
    Cat subCat;
    public Cat getCat() {
        return cat;
    }

    public Cat getSubCat() {
        return subCat;
    }

    public void setCat(Cat cat) {
        this.cat = cat;
    }
}
