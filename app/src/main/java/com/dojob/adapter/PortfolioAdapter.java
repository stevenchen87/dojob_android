package com.dojob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.dojob.R;
import com.dojob.activity.MainActivity;
import com.dojob.adapter.holder.PortfolioViewHolder;
import com.dojob.adapter.model.CommentOtherItem;
import com.dojob.adapter.model.CommentUserItem;
import com.dojob.adapter.model.Devider;
import com.dojob.adapter.model.MainSearch;
import com.dojob.adapter.model.PortfolioCover;
import com.dojob.adapter.model.PortfolioDescription;
import com.dojob.adapter.model.PortfolioEmpty;
import com.dojob.adapter.model.PortfolioOverflowScroll;
import com.dojob.adapter.model.PortfolioPreview;
import com.dojob.adapter.model.PortfolioRecruiterItem;
import com.dojob.adapter.model.PortfolioReviewItem;
import com.dojob.adapter.model.PortfolioSelf;
import com.dojob.adapter.model.PortfolioSelfRecruiter;
import com.dojob.adapter.model.PortfolioSkillsItem;
import com.dojob.adapter.model.PortfolioTitleBar;
import com.dojob.adapter.model.ReviewHeader;
import com.dojob.backend.LoginApi;
import com.dojob.backend.RegisterApi;
import com.dojob.backend.model.Comment;
import com.dojob.backend.model.CommentsInboxResponse;
import com.dojob.backend.model.GalleryAlbumsItemResponse;
import com.dojob.backend.model.Review;
import com.dojob.backend.model.ReviewResponse;
import com.dojob.backend.model.SkillResponse;
import com.dojob.backend.model.UserResponse;
import com.dojob.core.Logger;
import com.dojob.fragment.CommentFragment;
import com.dojob.util.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import okhttp3.internal.http2.Header;

import static android.content.ContentValues.TAG;
import static com.google.zxing.common.detector.MathUtils.distance;

public class PortfolioAdapter extends RecyclerView.Adapter<PortfolioViewHolder> implements View.OnClickListener,
        PortfolioOverflowScroll.Listener {
    public static final int DEFAULT_TYPE = 0;
    public static final int PREVIEW_TYPE = 1;
    private static final int SEARCH_BAR_TYPE = 0;
    private static final int COVER_TYPE = 1;
    private static final int TITLE_BAR_TYPE = 2;
    private static final int DESCRIPTION_TYPE = 3;
    private static final int SELF_TYPE = 4;
    private static final int SKILLS_TYPE = 5;
    private static final int OVERFLOW_SCROLL_TYPE = 6;
    private static final int DEVIDER_TYPE = 7;
    private static final int PORTFOLIO_PREVIEW_TYPE = 8;
    private static final int PORTFOLIO_SELF_RECRUITER_TYPE = 9;
    private static final int COMMENT_USER_TYPE = 10;
    private static final int COMMENT_OTHER_TYPE = 11;
    private static final int TYPE_PORTFOLIO_EMPTY = 12;
    private static final int TYPE_REVIEW = 13;
    private static final int TYPE_REVIEW_HEADER = 14;

    private int mType = 0;
    private final Context mContext;
    private UserResponse mUserResponse;
    private List<Object> mItems = new ArrayList<>();
    private Listener mListener;
    private Activity mActivity;
    private CommentsInboxResponse[] mCommentsResponse;
    private EditText commentEditText;
    private int previousState;
    private boolean userScrollChange;
    private FragmentActivity fragmentActivity;
    public static int currentPage;

    private int viewHolderType;
    private ReviewHeader mReviewHeader;
    private boolean isPhotoAvailable;
    private boolean isVideoAvailable;

    public PortfolioAdapter(Context context, int viewHolderType) {
        this.viewHolderType = viewHolderType;
        mContext = context;
    }

    public PortfolioAdapter(Context context, int type, FragmentActivity fragmentActivity) {
        mContext = context;
        mType = type;
        this.fragmentActivity = fragmentActivity;
    }

//    private float distance(float x1, float y1, float x2, float y2) {
//        float dx = x1 - x2;
//        float dy = y1 - y2;
//        float distanceInPx = (float) Math.sqrt(dx * dx + dy * dy);
//        return pxToDp(distanceInPx);
//    }
//
//    private float pxToDp(float px) {
//        return px / mContext.getResources().getDisplayMetrics().density;
//    }

    public void updateItem(UserResponse userResponse, int typeCover, Comment[] commentsResponse, int position) {
        mItems.clear();
        mUserResponse = userResponse;
        mItems.add(new MainSearch());
        mItems.add(new PortfolioCover(mContext, userResponse, typeCover, position));
        mItems.add(new CommentUserItem(mContext));
        mItems.add(new Devider());
        if (commentsResponse != null) {
            if (commentsResponse.length > 0) {
                for (Comment comment : commentsResponse) {
                    mItems.add(new CommentOtherItem(comment, mContext));
                    mItems.add(new Devider());
                }
                mItems.remove(mItems.size() - 1);
            }
        }
        notifyDataSetChanged();
    }

    public void updateItem(UserResponse userResponse, CommentsInboxResponse[] commentsInboxResponse) {
        mItems.clear();
        mUserResponse = userResponse;
        mItems.add(new MainSearch());
        mItems.add(new PortfolioCover(mContext, userResponse, PortfolioCover.TYPE_COVER));
        mItems.add(new CommentUserItem(mContext));
        mItems.add(new Devider());
        if (commentsInboxResponse != null) {
            mCommentsResponse = commentsInboxResponse;
            if (commentsInboxResponse.length > 0) {
                for (CommentsInboxResponse comment : commentsInboxResponse) {
                    mItems.add(new CommentOtherItem(comment, mContext));
                    mItems.add(new Devider());
                }
                mItems.remove(mItems.size() - 1);
            } else {
                mItems.add(new PortfolioEmpty("No Review"));
            }
        }
        notifyDataSetChanged();
    }

    public void updateReviewItems(UserResponse userResponse, ReviewResponse response) {
        if (!mItems.contains(mReviewHeader)) {
            mReviewHeader = new ReviewHeader(userResponse);
            mItems.add(mReviewHeader);
        }
        if (response.review.length > 0) {
            for (Review review : response.review) {
                mItems.add(new PortfolioReviewItem(review, true));
            }
        }
        notifyDataSetChanged();
    }

    public void updateItem(UserResponse userResponse, int coverPosition) {
        mUserResponse = userResponse;
        mItems.clear();
        mItems.add(new MainSearch());
        if (mUserResponse.profile != null && mUserResponse.profile.position.equals("talent")) {
            mItems.add(new PortfolioCover(mContext, mUserResponse, PortfolioCover.TYPE_COVER, coverPosition));
            mItems.add(new PortfolioTitleBar(PortfolioTitleBar.DEFAULT_TYPE, String.format(Locale.US, "About %s", mUserResponse.profile.name), mContext));
            mItems.add(new PortfolioDescription(mUserResponse.profile.talent));
            mItems.add(new Devider());
            mItems.add(new PortfolioSelf(mContext, mUserResponse, fragmentActivity));
            if (mUserResponse.skill != null) {
                mItems.add(new PortfolioTitleBar(PortfolioTitleBar.DEFAULT_TYPE, "Skills", mContext));
                for (SkillResponse skill : mUserResponse.skill) {
                    mItems.add(new PortfolioSkillsItem(skill));
                }

            }

//            mItems.add(new PortfolioTitleBar(PortfolioTitleBar.ALL_REVIEW_TYPE, "Reviews", mUserResponse.profile.review.length > 0, mContext)); // TODO now it will always displayed more
//            if (mUserResponse.profile.review.length > 0) {
//                PortfolioOverflowScroll reviewAdapter = new PortfolioOverflowScroll(mContext, PortfolioOverflowScroll.REVIEWS_TYPE, mUserResponse);
//                reviewAdapter.setListener(this);
//                mItems.add(reviewAdapter);
//            } else {
//                mItems.add(new PortfolioEmpty("No Review", true));
//            }
//            mItems.add(new Devider());
            if (mUserResponse.albums != null && mUserResponse.albums.length > 0) {
                mItems.add(new Devider());
                mItems.add(new PortfolioTitleBar(PortfolioTitleBar.ALL_ALBUMS_TYPE, "Photo Albums", mContext));
                PortfolioOverflowScroll albumsAdapter = new PortfolioOverflowScroll(mContext, PortfolioOverflowScroll.ALBUMS_TYPE, mUserResponse);
                albumsAdapter.setListener(this);
                mItems.add(albumsAdapter);

            }

            if (mUserResponse.profile.featuredVideo != null) {
                if (mUserResponse.profile.featuredVideo.length > 0) {
                    mItems.add(new Devider());
                    Log.i("VIDEO_OKAY", mUserResponse.profile.featuredVideo.length + "");
                    mItems.add(new PortfolioTitleBar(PortfolioTitleBar.ALL_VIDEOS_TYPE, "Videos/Audios", mContext));
//                    PortfolioOverflowScroll albumsAdapter = new PortfolioOverflowScroll(mContext, PortfolioOverflowScroll.TYPE_VIDEO_ALBUM, mUserResponse);
//                    albumsAdapter.setListener(this);
//                    mItems.add(albumsAdapter);
                    mItems.add(new PortfolioCover(mContext, mUserResponse, PortfolioCover.TYPE_VIDOES, coverPosition));
                }
            } else {
                Log.i("VIDEO", mUserResponse.videoAlbums.length + "");
            }
            //        mItems.add(new Devider());

            //        mItems.add(new PortfolioTitleBar(PortfolioTitleBar.ALL_VIDEOS_TYPE, "Video/Audio"));
            //        mItems.add(new PortfolioCover(mContext, mUserResponse, PortfolioCover.TYPE_VIDOES));
        } else {
            mItems.add(new PortfolioSelfRecruiter(mContext, mUserResponse));

            if (mUserResponse.recommended != null && mUserResponse.recommended.length > 0) {
                mItems.add(new Devider());
                mItems.add(new PortfolioTitleBar(PortfolioTitleBar.RECRUITER_RECOMMENDED, null, mContext));
                PortfolioOverflowScroll recommendedAdapter = new PortfolioOverflowScroll(mContext, PortfolioRecruiterItem.TYPE_RECOMMENDED, mUserResponse.recommended);
                recommendedAdapter.setListener(this);
                mItems.add(recommendedAdapter);
            }

            if (mUserResponse.viewed != null && mUserResponse.viewed.length > 0) {
                mItems.add(new Devider());
                mItems.add(new PortfolioTitleBar(PortfolioTitleBar.RECRUITER_VIEWED, null, mContext));
                PortfolioOverflowScroll viewedAdapter = new PortfolioOverflowScroll(mContext, PortfolioRecruiterItem.TYPE_VIEWED, mUserResponse.viewed);
                viewedAdapter.setListener(this);
                mItems.add(viewedAdapter);
            }
        }

        if (mType == PREVIEW_TYPE) {
            mItems.add(new PortfolioPreview());
        }

        notifyDataSetChanged();
    }

    public void setListener(Activity activity, Listener listener) {
        mActivity = activity;
        mListener = listener;
    }

//    public void setListener(Activity activity, GalleryListener listener){
//        mActivity = activity;
//        mGalleryListener = listener;
//    }

    @NonNull
    @Override
    public PortfolioViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        int layout = 0;
        switch (viewType) {
            case SEARCH_BAR_TYPE:
                layout = MainSearch.LAYOUT_ID;
                break;
            case COVER_TYPE:
                layout = PortfolioCover.LAYOUT_ID;
                break;
            case TITLE_BAR_TYPE:
                layout = PortfolioTitleBar.LAYOUT_ID;
                break;
            case DESCRIPTION_TYPE:
                layout = PortfolioDescription.LAYOUT_ID;
                break;
            case SELF_TYPE:
                layout = PortfolioSelf.LAYOUT_ID;
                break;
            case SKILLS_TYPE:
                layout = PortfolioSkillsItem.LAYOUT_ID;
                break;
            case OVERFLOW_SCROLL_TYPE:
                layout = PortfolioOverflowScroll.LAYOUT_ID;
                break;
            case DEVIDER_TYPE:
                layout = Devider.LAYOUT_ID;
                break;
            case PORTFOLIO_PREVIEW_TYPE:
                layout = PortfolioPreview.LAYOUT_ID;
                break;
            case PORTFOLIO_SELF_RECRUITER_TYPE:
                layout = PortfolioSelfRecruiter.LAYOUT_ID;
                break;
            case COMMENT_USER_TYPE:
                layout = CommentUserItem.LAYOUT_ID;
                break;
            case COMMENT_OTHER_TYPE:
                layout = CommentOtherItem.LAYOUT_ID;
                break;
            case TYPE_PORTFOLIO_EMPTY:
                layout = PortfolioEmpty.LAYOUT_ID;
                break;
            case TYPE_REVIEW:
                layout = PortfolioReviewItem.LAYOUT_ID;
                break;
            case TYPE_REVIEW_HEADER:
                layout = ReviewHeader.LAYOUT_ID;
                break;
        }

        View view = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        final PortfolioViewHolder holder = new PortfolioViewHolder(view, fragmentActivity);
        if (holder.cover.vp != null) {
            if (viewHolderType == 1) {
                Log.i("COMMENT", "TRUE");
                holder.cover.favImage.setVisibility(View.GONE);
                holder.cover.commentEditText.setVisibility(View.GONE);
                holder.cover.formatImage.setVisibility(View.GONE);
                holder.cover.indicator.setVisibility(View.GONE);
            }
        }

        if (mType != PREVIEW_TYPE) {
            if (holder.titleBar.allGallery != null) {
                holder.titleBar.allGallery.setOnClickListener(this);
            }

            if (holder.titleBar.allReviews != null) {
                holder.titleBar.allReviews.setOnClickListener(this);
            }

            if (holder.search.container != null) {
               /* holder.search.container.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        String query = v.getText().toString();
                        v.clearFocus();
                        v.setText(null);
                        ((MainActivity)mActivity).openSearchPage(query);
                        return false;
                    }
                });*/
                holder.search.container.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ((MainActivity) mActivity).openPreSearchPage();
                    }
                });

                holder.search.container.setFocusableInTouchMode(false);
                holder.search.container.setCursorVisible(false);
                holder.search.container.setImeActionLabel("Search", KeyEvent.KEYCODE_ENTER);
            }

            //Comment
            if (holder.commentUser.msg != null) {
                commentEditText = holder.commentUser.msg;
                holder.commentUser.msg.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        String query = v.getText().toString();
                        v.clearFocus();
                        v.setText(null);
                        mListener.onSaveComment(query);
                        return true;
                    }
                });
                holder.commentUser.msg.setImeActionLabel("Send", KeyEvent.KEYCODE_ENTER);
            }

            //Cover
            if (holder.cover.commentEditText != null) {
                holder.cover.commentEditText.setOnClickListener(this);
            }
            if (holder.cover.playButton != null) {
                holder.cover.playButton.setOnClickListener(this);
            }
            if (holder.cover.moreImage != null) {
                holder.cover.moreImage.setOnClickListener(this);
            }
            if (holder.cover.favImage != null) {
                holder.cover.favImage.setOnClickListener(this);
            }
            if (holder.cover.formatImage != null) {
                holder.cover.formatImage.setOnClickListener(this);
            }
            if (holder.cover.shareImage != null) {
                holder.cover.shareImage.setOnClickListener(this);
            }
            if (holder.cover.vp != null) {
                final TapGestureListener tapGestureDetector = new TapGestureListener(holder.cover.vp);

                holder.cover.vp.setOnTouchListener(new View.OnTouchListener() {
                    private static final int MAX_CLICK_DISTANCE = 15;

                    private float distance(float x1, float y1, float x2, float y2) {
                        float dx = x1 - x2;
                        float dy = y1 - y2;
                        float distanceInPx = (float) Math.sqrt(dx * dx + dy * dy);
                        return pxToDp(distanceInPx);
                    }

                    private float pxToDp(float px) {
                        return px / mContext.getResources().getDisplayMetrics().density;
                    }

                    private static final int MAX_CLICK_DURATION = 1000;

                    /**
                     * Max allowed distance to move during a "click", in DP.
                     */


                    private long pressStartTime;
                    private float pressedX;
                    private float pressedY;
                    private boolean stayedWithinClickDistance;

                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        switch (motionEvent.getAction()) {
                            case MotionEvent.ACTION_DOWN: {
                                pressStartTime = System.currentTimeMillis();
                                pressedX = motionEvent.getX();
                                pressedY = motionEvent.getY();
                                stayedWithinClickDistance = true;
                                break;
                            }
                            case MotionEvent.ACTION_MOVE: {
                                if (stayedWithinClickDistance && distance(pressedX, pressedY, motionEvent.getX(), motionEvent.getY()) > MAX_CLICK_DISTANCE) {
                                    stayedWithinClickDistance = false;
                                }
                                break;
                            }
                            case MotionEvent.ACTION_UP: {
                                long pressDuration = System.currentTimeMillis() - pressStartTime;
                                if (pressDuration < MAX_CLICK_DURATION && stayedWithinClickDistance) {
                                    // Click event has occurred
                                    onClick(view);

                                }
                            }
                        }
                        return false;
                    }
                });

                holder.cover.vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                    }

                    @Override
                    public void onPageSelected(int position) {
                        if (userScrollChange && mListener != null) {
                            mListener.onCoverImageSwiped(position);
                        }
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {
                        if (previousState == ViewPager.SCROLL_STATE_DRAGGING && state == ViewPager.SCROLL_STATE_SETTLING) {
                            userScrollChange = true;
                        } else if (previousState == ViewPager.SCROLL_STATE_SETTLING && state == ViewPager.SCROLL_STATE_IDLE) {
                            userScrollChange = false;
                        }
                        previousState = state;
                    }
                });
            }

            //Self
            if (holder.self.enquiry != null) {
                holder.self.enquiry.setOnClickListener(this);
            }
            if (holder.self.editProfile != null) {
                /*holder.self.editProfile.setOnClickListener(this);*/
            }
            if (holder.self.rate != null) {
                holder.self.rate.setOnClickListener(this);
            }
            if (holder.self.bookmark != null) {
                holder.self.bookmark.setOnClickListener(this);
            }
            if (holder.self.bullet != null) {
                holder.self.bullet.setOnClickListener(this);
            }
            if (holder.self.share != null) {
                holder.self.share.setOnClickListener(this);
            }

            if (holder.commentOther.moreReply != null) {
                holder.commentOther.moreReply.setOnClickListener(this);
            }
        } else {
            if (holder.preview.back != null) {
                holder.preview.back.setOnClickListener(this);
            }
            if (holder.search.container != null) {
                holder.search.container.setFocusableInTouchMode(false);
                holder.search.container.setCursorVisible(false);
            }
            if (holder.self.editProfile != null) {
                holder.self.editProfile.setOnClickListener(null);
            }
        }

        return holder;
    }

    private int getCurrentPageIndex(ViewPager vp) {
        int first, second, id1, id2 = 0, left;
        id1 = first = second = 99999999;
        View v;
        for (int i = 0, k = vp.getChildCount(); i < k; ++i) {
            left = vp.getChildAt(i).getLeft();
            if (left < second) {
                if (left < first) {
                    second = first;
                    id2 = id1;
                    first = left;
                    id1 = i;
                } else {
                    second = left;
                    id2 = i;
                }
            }
        }
        return id2;
    }

    private static class PageListener extends ViewPager.SimpleOnPageChangeListener {
        public void onPageSelected(int position) {
            Log.i(TAG, "page selected " + position);
            currentPage = position;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        Object item = getItem(position);
        if (item instanceof MainSearch) {
            return SEARCH_BAR_TYPE;
        } else if (item instanceof PortfolioCover) {
            return COVER_TYPE;
        } else if (item instanceof PortfolioTitleBar) {
            return TITLE_BAR_TYPE;
        } else if (item instanceof PortfolioDescription) {
            return DESCRIPTION_TYPE;
        } else if (item instanceof PortfolioSelf) {
            return SELF_TYPE;
        } else if (item instanceof PortfolioSkillsItem) {
            return SKILLS_TYPE;
        } else if (item instanceof PortfolioOverflowScroll) {
            return OVERFLOW_SCROLL_TYPE;
        } else if (item instanceof Devider) {
            return DEVIDER_TYPE;
        } else if (item instanceof PortfolioPreview) {
            return PORTFOLIO_PREVIEW_TYPE;
        } else if (item instanceof PortfolioSelfRecruiter) {
            return PORTFOLIO_SELF_RECRUITER_TYPE;
        } else if (item instanceof CommentUserItem) {
            return COMMENT_USER_TYPE;
        } else if (item instanceof CommentOtherItem) {
            return COMMENT_OTHER_TYPE;
        } else if (item instanceof PortfolioEmpty) {
            return TYPE_PORTFOLIO_EMPTY;
        } else if (item instanceof PortfolioReviewItem) {
            return TYPE_REVIEW;
        } else if (item instanceof ReviewHeader) {
            return TYPE_REVIEW_HEADER;
        }

        return super.getItemViewType(position);
    }

    @Override
    public void onBindViewHolder(@NonNull PortfolioViewHolder holder, int position) {
        Object item = getItem(position);
        int type = getItemViewType(position);
        switch (type) {
            case SEARCH_BAR_TYPE:
                ((MainSearch) item).bindViewHolder(holder.search, position);
                break;
            case COVER_TYPE:
                ((PortfolioCover) item).bindViewHolder(holder.cover, position);
                break;
            case TITLE_BAR_TYPE:
                ((PortfolioTitleBar) item).bindViewHolder(holder.titleBar, position);
                break;
            case DESCRIPTION_TYPE:
                ((PortfolioDescription) item).bindViewHolder(holder.description, position);
                break;
            case SELF_TYPE:
                ((PortfolioSelf) item).bindViewHolder(holder.self, position);
                break;
            case SKILLS_TYPE:
                ((PortfolioSkillsItem) item).bindViewHolder(holder.skill, position);
                break;
            case OVERFLOW_SCROLL_TYPE:
                /*holder.overFlowScroll.list.setLayoutManager(new GridLayoutManager(mContext,3));*/
                ((PortfolioOverflowScroll) item).bindViewHolder(holder.overFlowScroll, position);
                break;
            case DEVIDER_TYPE:
                ((Devider) item).bindViewHolder(holder.devider, position);
                break;
            case PORTFOLIO_PREVIEW_TYPE:
                ((PortfolioPreview) item).bindViewHolder(holder.preview, position);
                break;
            case PORTFOLIO_SELF_RECRUITER_TYPE:
                ((PortfolioSelfRecruiter) item).bindViewHolder(holder.selfRecruiter, position);
                break;
            case COMMENT_USER_TYPE:
                ((CommentUserItem) item).bindViewHolder(holder.commentUser, position);
                break;
            case COMMENT_OTHER_TYPE:
                ((CommentOtherItem) item).bindViewHolder(holder.commentOther, position);
                break;
            case TYPE_PORTFOLIO_EMPTY:
                ((PortfolioEmpty) item).bindViewHolder(holder.empty, position);
                break;
            case TYPE_REVIEW:
                ((PortfolioReviewItem) item).bindViewHolder(holder.review, position);
                break;
            case TYPE_REVIEW_HEADER:
                ((ReviewHeader) item).bindViewHolder(holder.reviewHeader, position);
                break;
        }
    }

    @Override
    public int getItemCount() {
        if (mItems != null) {
            return mItems.size();
        }
        return 0;
    }

    public Object getItem(int position) {
        int size = mItems.size();
        if (size > 0 && position < size) {
            return mItems.get(position);
        }
        return null;
    }

    @Override
    public void onClick(View view) {
        Object tag = view.getTag();
        if (tag == null) {
            return;
        }
        final Object item = getItem((int) view.getTag());
        switch (view.getId()) {

            case R.id.portfolio_titlebar_all_gallery:
                if (item instanceof PortfolioTitleBar) {
                    mListener.onOpenAllGallery(((PortfolioTitleBar) item).getType() - 1, mUserResponse);
                }
                break;
            case R.id.portfolio_titlebar_all_reviews:
                if (item instanceof PortfolioTitleBar) {
                    TextView textView = (TextView) view;
                    if (textView.getText().equals(mContext.getResources().getString(R.string.see_album_title))) {
                        mListener.onOpenAllGallery(((PortfolioTitleBar) item).getType() - 1, mUserResponse);
                    } else if (textView.getText().equals(mContext.getString(R.string.see_videos_title))) {
                        mListener.onOpenAllGallery(((PortfolioTitleBar) item).getType() - 1, mUserResponse);
                    } else {
                        mListener.onOpenReviewsPage(mUserResponse);
                    }
                }
                break;
            case R.id.portfolio_cover_more:
                mListener.onOpenAllGallery(0, mUserResponse);
                break;
            case R.id.portfolio_cover_favourite:
                Log.d("PortfolioCover", "cover favourite clicked");
                isPhotoAvailable = mUserResponse.talentPhoto.length > 0 && ((PortfolioCover) item).getType().equals(CommentFragment.TYPE_IMAGE);
                isVideoAvailable = mUserResponse.profile.featuredVideo.length > 0 && ((PortfolioCover) item).getType().equals(CommentFragment.TYPE_VIDEO);
                if (mListener != null && (isPhotoAvailable || isVideoAvailable)) {
                    mListener.onCoverLike(((PortfolioCover) item).getType(), ((PortfolioCover) item).getCurrentPosition());
                }
                break;
            case R.id.portfolio_cover_play:
                isPhotoAvailable = mUserResponse.talentPhoto.length > 0 && ((PortfolioCover) item).getType().equals(CommentFragment.TYPE_IMAGE);
                isVideoAvailable = mUserResponse.profile.featuredVideo.length > 0 && ((PortfolioCover) item).getType().equals(CommentFragment.TYPE_VIDEO);
                if (mListener != null && (isPhotoAvailable || isVideoAvailable)) {
                    mListener.onCoverClick(((PortfolioCover) item).getType(), ((PortfolioCover) item).getCurrentPosition());
                }
                break;
            case R.id.portfolio_cover_share:
                Log.d("PortfolioCover", "cover share clicked");
                if (mUserResponse != null)
                    Utils.share(mContext, mUserResponse.profile.share_url);
                break;
            case R.id.portfolio_self_edit_profile:
                mListener.onEditProfileClick();
                break;
            case R.id.portfolio_self_enquiry:
                // if (LoginApi.isLoggedIn(mContext)) {
                mListener.onEnqueryClick(mUserResponse);
//                } else {
//                    mListener.onLoginDialogOpen();
//                }
                Log.d("PortfolioSelf", "self enquiry clicked");
                break;
            case R.id.portfolio_self_rate:
                Log.d("PortfolioSelf", "self rate clicked");
                mListener.onRateClick();
                break;
            case R.id.portfolio_self_bookmark:
                Log.d("PortfolioSelf", "self bookmark clicked");
                ImageView bookmark = view.findViewById(R.id.portfolio_self_bookmark);
                if (mUserResponse != null) {
                    if (mUserResponse.profile.favourite_status == 0) {
                        mUserResponse.profile.favourite_status = 1;
                        bookmark.setImageResource(R.drawable.drawable_bookmark_selected);
                        mListener.onBookmarkedClick(true, mUserResponse.profile.id);
                    } else if (mUserResponse.profile.favourite_status == 1) {
                        mUserResponse.profile.favourite_status = 0;
                        mListener.onBookmarkedClick(false, mUserResponse.profile.id);
                        bookmark.setImageResource(R.drawable.drawable_bookmark);
                    }
                }
                break;
            case R.id.portfolio_self_list_bullet:
                Log.d("PortfolioSelf", "self bullet clicked");
                mListener.onListBulletClick();
                break;
            case R.id.portfolio_self_share:
                Log.d("PortfolioSelf", "self share clicked");
                if (mUserResponse != null)
                    Utils.share(mContext, mUserResponse.profile.share_url);
                break;
            case R.id.portfolio_cover_comment:
            case R.id.portfolio_cover_format:
            case R.id.cover_container:
            case R.id.portfolio_cover_photo:
                if (mCommentsResponse != null && commentEditText != null) {
                    commentEditText.setFocusableInTouchMode(true);
                    commentEditText.requestFocus();
                    Utils.showKeyboard(mActivity);
                } else {
//                    if (RegisterApi.isLogin(mContext)){
                    mListener.onOpenCommentPage(mUserResponse, ((PortfolioCover) item).getType(), ((PortfolioCover) item).getCurrentPosition());
//                    }
                    // mListener.onOpenCommentFragment(((PortfolioCover) item).getType(),0,mUserResponse.profile.cover_photo)
                }

                break;
            case R.id.comment_other_view_reply:
                if (item instanceof CommentOtherItem)
                    mListener.onOpenViewReply(((CommentOtherItem) item).getComments());
                break;
            case R.id.portfolio_preview_back:
                mListener.onBackClick();
                break;

        }
    }

    @Override
    public void onAlbumClick(GalleryAlbumsItemResponse album) {
        Log.d("PortfolioOverflowScroll", "album clicked");
        ((MainActivity) mActivity).openAlbumPage(album);
    }

    @Override
    public void onReviewClick(Review review) {
        Log.d("PortfolioOverflowScroll", "review clicked");
    }

    @Override
    public void onFeedItemClick(String url) {
        Log.d("PortfolioOverflowScroll", "feed item clicked");
        ((MainActivity) mActivity).onOpenPreviewProfilePage(url);
    }

    public interface Listener {
        void onOpenAllGallery(int page, UserResponse response);

        void onEnqueryClick(UserResponse response);

        void onEditProfileClick();

        void onRateClick();

        void onBookmarkedClick(boolean result, int id);

        void onListBulletClick();

        void onLoginDialogOpen();

        void onOpenCommentPage(UserResponse response, String type, int position);

        void onOpenCommentFragment(String type, int position, int fileId);

        void onSaveComment(String query);

        void onOpenViewReply(Comment[] comments);

        void onBackClick();

        void onOpenReviewsPage(UserResponse response);

        void onCoverImageSwiped(int position);

        void onCoverLike(String type, int position);

        void onCoverClick(String type, int position);
    }

    class TapGestureListener extends GestureDetector.SimpleOnGestureListener {

        private ViewPager viewHolder;
        private PortfolioCover portfolioCover;

        public TapGestureListener(ViewPager viewHolder) {
            this.viewHolder = viewHolder;
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            Log.i("CURRENT_ITEM", viewHolder.getCurrentItem() + "");

            if (RegisterApi.isLogin(mContext))
                mListener.onOpenCommentPage(mUserResponse, portfolioCover.getType(), viewHolder.getCurrentItem());
            return true;

        }
    }


}

