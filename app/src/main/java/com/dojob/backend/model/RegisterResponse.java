package com.dojob.backend.model;

import com.google.gson.JsonObject;

import org.json.JSONObject;

public class RegisterResponse {
    public String status;
    public String token;
    public ProfileResponse data;
    public boolean tutorialFlag;
    public Error error;

}
