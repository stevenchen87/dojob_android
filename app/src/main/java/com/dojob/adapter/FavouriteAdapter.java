package com.dojob.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.dojob.GlideApp;
import com.dojob.R;
import com.dojob.activity.MainActivity;
import com.dojob.backend.model.FavouriteItemResponse;
import com.dojob.util.ResourcesUtil;
import com.dojob.util.Utils;

import java.util.ArrayList;

/**
 * Created by Admin on 30-Nov-18.
 */
public class FavouriteAdapter extends RecyclerView.Adapter<FavouriteAdapter.MyHolder> {

    private Activity activity;
    private ArrayList<FavouriteItemResponse> favourite_data;
    private boolean isEdit = false;

    public FavouriteAdapter(Activity activity, ArrayList<FavouriteItemResponse> favourite_data) {
        this.activity = activity;
        this.favourite_data = favourite_data;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_feed_item, parent, false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyHolder holder, final int position) {

        final FavouriteItemResponse model = favourite_data.get(position);

        holder.content.setBackgroundColor(ResourcesUtil.getColor(R.color.green));
        CardView.LayoutParams topParams = new CardView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Utils.dpToPx(180));
        topParams.setMargins(Utils.dpToPx(2), Utils.dpToPx(2), Utils.dpToPx(2), Utils.dpToPx(2));
        holder.container.setLayoutParams(topParams);

        holder.feed_name.setText(model.fav_user_data.name);
        holder.feed_position.setText(model.fav_user_data.category_info.getSubCat().getName());

        GlideApp.with(activity)
                .load(model.fav_user_data.cover_photo)
                .placeholder(R.drawable.placeholder_1)
                .apply(RequestOptions.fitCenterTransform())
                .skipMemoryCache(false)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.feed_image);

        if (holder.tvcountryCode != null) {
            if (model.fav_user_data.countrycode != null) {
                holder.tvcountryCode.setText(model.fav_user_data.countrycode);
                holder.tvcountryCode.setVisibility(View.VISIBLE);
            }else
                holder.tvcountryCode.setVisibility(View.GONE);
        }

        if (model.isSelected) {
            setSelected(holder, position);
        } else {
            setUnSelected(holder, position);
        }

        holder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (model.isSelected) {
                    setUnSelected(holder, position);
                } else {
                    setSelected(holder, position);
                }
            }
        });

        if (isEdit)
            holder.checkbox.setVisibility(View.VISIBLE);
        else
            holder.checkbox.setVisibility(View.GONE);

    }

    private void setSelected(final MyHolder holder, final int position) {
        holder.checkbox.setButtonDrawable(R.drawable.drawable_radio_checked);
        favourite_data.get(position).isSelected = true;
    }

    private void setUnSelected(final MyHolder holder, final int position) {
        holder.checkbox.setButtonDrawable(R.drawable.drawable_radio_unchecked);
        favourite_data.get(position).isSelected = false;
    }

    public void showCheckBox(boolean result) {
        isEdit = result;
        notifyDataSetChanged();
    }

    public void updateNewData(ArrayList<FavouriteItemResponse> favourite_data) {
        this.favourite_data = favourite_data;
        notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
    @Override
    public int getItemCount() {
        return favourite_data.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView feed_image;
        public TextView feed_name, feed_position;
        public CheckBox checkbox;
        public LinearLayout content;
        public CardView container;
        public TextView tvcountryCode;


        public MyHolder(View view) {
            super(view);

            feed_image = view.findViewById(R.id.feed_image);
            feed_name = view.findViewById(R.id.feed_name);
            feed_position = view.findViewById(R.id.feed_position);
            checkbox = view.findViewById(R.id.checkbox);
            content = view.findViewById(R.id.content);
            container = view.findViewById(R.id.card_view);
            tvcountryCode = view.findViewById(R.id.tvcountryCode);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            ((MainActivity) activity).onOpenPreviewProfilePage(favourite_data.get(getAdapterPosition()).fav_user_data.url);
        }
    }

}
