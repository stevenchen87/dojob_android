package com.dojob.Additions;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dojob.R;
import com.dojob.fragment.ProfileSettingFragment;
import com.dojob.fragment.SearchFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SkillsAdapter extends RecyclerView.Adapter<SkillsAdapter.MyViewHolder> {

    private Context mContext;

    private List<Skill> skillsList;

    private ProfileSettingFragment profileSettingFragment;

    public SkillsAdapter(Context mContext, List<Skill> skillsList,ProfileSettingFragment profileSettingFragment) {
        this.mContext = mContext;
        this.skillsList = skillsList;
        this.profileSettingFragment = profileSettingFragment;
        Log.i("SKILLS_LIST",skillsList.toString());
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_skill,parent,false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        Skill skill = skillsList.get(position);

        if (skill!=null) {
                holder.skillNameTextView.setText( skill.getValue());
                Log.i("SKILL_INSTANCE",skill.getValue());
            Log.i("SKILL_INSTANCE",skill.getKey()+"");

        }



    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return skillsList.size();
    }

    public class MyViewHolder extends  RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R.id.skill_name_text_view)
        TextView skillNameTextView;

        @BindView(R.id.skill_close_image_view)
        ImageView skillCloseImageView;


        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            skillCloseImageView.setOnClickListener(this);



        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            Skill skill = skillsList.get(position);
            switch (v.getId()){
                case R.id.skill_close_image_view:
                    if (skill.getKey()==0){
                        skillsList.remove(skill);
                        profileSettingFragment.removeItem(skill,0);

                        notifyDataSetChanged();

                    }else {
                        profileSettingFragment.removeItem(skill,1);
                        skillsList.remove(skill);
                        notifyDataSetChanged();

                    }

            }
        }
    }
}
