package com.dojob.adapter.model;

import android.view.View;
import android.widget.ImageView;

import com.dojob.R;
import com.squareup.picasso.Picasso;

public class AlbumsCover {
    public static final int LAYOUT_ID = R.layout.layout_albums_cover;

    private String mUrl;

    public AlbumsCover(String url) {
        mUrl = url;
    }

    public void bindViewHolder(ViewHolder holder, int position){
        if(holder.image != null){
            Picasso.get().load(mUrl).into(holder.image);
        }
    }

    public static class ViewHolder{

        public ImageView image;
        public ViewHolder(View view){
            image = view.findViewById(R.id.albums_cover);
        }
    }
}
