package com.dojob.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dojob.R;
import com.dojob.activity.MainActivity;
import com.dojob.adapter.holder.PortfolioOverflowViewHolder;
import com.dojob.adapter.model.AlbumsItem;
import com.dojob.adapter.model.FeedItem;
import com.dojob.adapter.model.NewVideosItem;
import com.dojob.adapter.model.PortfolioRecruiterItem;
import com.dojob.adapter.model.PortfolioReviewItem;
import com.dojob.adapter.model.SearchItem;
import com.dojob.backend.model.FeedItemResponse;
import com.dojob.backend.model.GalleryAlbumsItemResponse;
import com.dojob.backend.model.MediaResponse;
import com.dojob.backend.model.ProfileResponse;
import com.dojob.backend.model.Review;
import com.dojob.backend.model.UserResponse;
import com.dojob.util.Utils;

import java.util.ArrayList;
import java.util.List;


public class PortfolioOverflowAdapter extends RecyclerView.Adapter<PortfolioOverflowViewHolder> implements
        View.OnClickListener{
    private static final int TYPE_REVIEW = 1;
    private static final int TYPE_ALBUMS = 2;
    private static final int TYPE_FEED = 3;
    private static final int TYPE_SEARCH = 4;
    private static final int TYPE_RECRUITER = 5;
    private static final int TYPE_VIDEO_ALBUM = 6;
    private List<Object> mItems = new ArrayList<>();
    private Listener mListener;
    private Context mContext;

    public PortfolioOverflowAdapter(Context context, int type, UserResponse userResponse) {
        this.mContext = context;
        mItems.clear();
        switch (type){
            case TYPE_REVIEW:
                if(userResponse.profile != null && userResponse.profile.review != null && userResponse.profile.review.length > 0){
                    for(Review review: userResponse.profile.review){
                        mItems.add(new PortfolioReviewItem(review));
                    }
                }
                break;
            case TYPE_ALBUMS:
                if(userResponse.profile != null && userResponse.albums != null && userResponse.albums.length>0){
                    for(GalleryAlbumsItemResponse album: userResponse.albums){
                        mItems.add(new AlbumsItem(AlbumsItem.ALBUMS_IMAGES_PORTFOLIO, album,context));
                    }
                }

                break;
            case TYPE_VIDEO_ALBUM:
                if (userResponse.profile!=null&&userResponse.profile.featuredVideo!=null&&userResponse.profile.featuredVideo.length>0){
                    for (MediaResponse videosResponse : userResponse.profile.featuredVideo){
                        mItems.add(new NewVideosItem(context,videosResponse,1));
                    }
                }
        }
    }

    public PortfolioOverflowAdapter(Context context, int type, FeedItemResponse[] response) {
        mItems.clear();
        if(response != null && response.length>0){
            for(FeedItemResponse item  : response){
                if(type == TYPE_FEED){
                    mItems.add(new FeedItem(context, FeedItem.HOTTEST_TALENT, item));
                }
                else{
                    mItems.add(new SearchItem(context, SearchItem.SEARCH_RESULT, item));
                }
            }
        }
    }

    public PortfolioOverflowAdapter(Context context, int type, ProfileResponse[] response) {
        mItems.clear();
        if(response != null && response.length>0){
            for(ProfileResponse item  : response){
                mItems.add(new PortfolioRecruiterItem(context, type, item));
            }
        }
    }

    @NonNull
    @Override
    public PortfolioOverflowViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        int layout = 0;
        if (viewType == TYPE_REVIEW) {
            layout = PortfolioReviewItem.LAYOUT_ID;
        } else if (viewType == TYPE_ALBUMS) {
            layout = AlbumsItem.LAYOUT_ID;
        } else if(viewType == TYPE_FEED){
            layout = FeedItem.LAYOUT_ID;
        } else if(viewType == TYPE_SEARCH){
            layout = SearchItem.LAYOUT_ID;
        } else if(viewType == TYPE_RECRUITER){
            layout = PortfolioRecruiterItem.LAYOUT_ID;
        }else if (viewType == TYPE_VIDEO_ALBUM){
            layout = NewVideosItem.LAYOUT_ID;
        }
        View view = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        PortfolioOverflowViewHolder holder = new PortfolioOverflowViewHolder(view);
        if(holder.review.container != null) {
            holder.review.container.setOnClickListener(this);
        }
        if(holder.albums.container != null) {
            holder.albums.container.setOnClickListener(this);
        }
        if(holder.feeds.clickable != null){
            holder.feeds.clickable.setOnClickListener(this);
        }

        if(holder.search.clickable != null){
            holder.search.clickable.setOnClickListener(this);
        }
        if(holder.recruiter.clickable != null){
            holder.recruiter.clickable.setOnClickListener(this);
        }
        if (holder.videos.container != null){
            holder.videos.container.setOnClickListener(this);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull PortfolioOverflowViewHolder holder, int position) {
        Object item = getItem(position);
        if (item == null) {
            return;
        }
        int type = getItemViewType(position);
        if (type == TYPE_REVIEW) {
            ((PortfolioReviewItem) item).bindViewHolder(holder.review, position);
        } else if (type == TYPE_ALBUMS) {

            ((AlbumsItem) item).bindViewHolder(holder.albums, position);
        } else if(type == TYPE_FEED){
            ((FeedItem) item).bindViewHolder(holder.feeds, position);
        } else if(type == TYPE_SEARCH){
            ((SearchItem) item).bindViewHolder(holder.search, position);
        } else if(type == TYPE_RECRUITER){
            ((PortfolioRecruiterItem) item).bindViewHolder(holder.recruiter, position);
        }else if (type == TYPE_VIDEO_ALBUM){
            ((NewVideosItem) item).bindViewHolder(holder.videos,position);
        }
    }

    public Object getItem(int position) {
        int size = mItems.size();
        if (size > 0 && position < size) {
            return mItems.get(position);
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        Object item = getItem(position);
        if (item != null) {
            if (item instanceof PortfolioReviewItem) {
                return TYPE_REVIEW;
            } else if (item instanceof AlbumsItem) {
                return TYPE_ALBUMS;
            } else if(item instanceof FeedItem){
                return TYPE_FEED;
            } else if(item instanceof SearchItem){
                return TYPE_SEARCH;
            } else if(item instanceof PortfolioRecruiterItem){
                return TYPE_RECRUITER;
            }else if (item instanceof NewVideosItem){
                return TYPE_VIDEO_ALBUM;
            }
        }
        return super.getItemViewType(position);
    }


    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public void onClick(View view) {
        Object tag = view.getTag();
        if (tag == null) {
            return;
        }
        final Object item = getItem((int) view.getTag());
        switch (view.getId()) {
            case R.id.album_container:
                mListener.onAlbumClick(((AlbumsItem)item).getItem());

                break;
            case R.id.review_container:
                mListener.onReviewClick(((PortfolioReviewItem)item).getReview());
                break;
            case R.id.clickable:
                if(item instanceof FeedItem){
                    mListener.onFeedItemClick(((FeedItem)item).getData().url);
                    Log.i("FEED_ITEM_URL",((FeedItem)item).getData().url);

                }
                else if(item instanceof SearchItem){
                    mListener.onFeedItemClick(((SearchItem)item).getData().url);
                }
                else if(item instanceof PortfolioRecruiterItem){
                    mListener.onFeedItemClick(((PortfolioRecruiterItem)item).getData().url);
                }
                break;
            case R.id.video_container :
                Utils.openVideoPlayer(mContext,((NewVideosItem)item).getUrl());
        }
    }

    public void setListener(Listener listener){
        mListener = listener;
    }

    public interface Listener{
        void onAlbumClick(GalleryAlbumsItemResponse album);
        void onReviewClick(Review review);
        void onFeedItemClick(String url);
    }
}
