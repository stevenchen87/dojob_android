package com.dojob.Additions;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.dojob.R;
import com.dojob.activity.MainActivity;
import com.github.ybq.android.spinkit.SpinKitView;

public class PrivacyFragment extends Fragment {
    String url = "https://step4work.com/privacy-policy";
    SpinKitView progressBar;

    public static com.dojob.fragment.TermOfUseFragment newInstance() {

        Bundle args = new Bundle();

        com.dojob.fragment.TermOfUseFragment fragment = new com.dojob.fragment.TermOfUseFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        if (activity != null) {
            ActionBar actionbar = activity.getSupportActionBar();
            if (actionbar != null) {
                actionbar.setDisplayHomeAsUpEnabled(true);
            }
        }
        View view = inflater.inflate(R.layout.fragment_term_of_use, container, false);

        progressBar = view.findViewById(R.id.progress);
        WebView myWebView = view.findViewById(R.id.webView1);
        myWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                progressBar.setVisibility(View.GONE);
            }
        });
        myWebView.loadUrl(url);


        ((MainActivity) this.getActivity()).setTitle(getResources().getString(R.string.privacy_title));
        return view;
    }


}

