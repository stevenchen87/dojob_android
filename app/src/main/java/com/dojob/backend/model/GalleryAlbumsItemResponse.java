package com.dojob.backend.model;

public class GalleryAlbumsItemResponse {

    public int id;
    public int linkID;
    public String name;
    public String description;
    public int user_id;
    public String album_type;
    public long created_at;
    public long updated_at;

    public FileResponse[] files;
}
