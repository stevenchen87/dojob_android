package com.dojob.backend;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.dojob.backend.model.FeedsResponse;
import com.dojob.backend.model.LoginResponse;
import com.dojob.backend.model.RegisterResponse;
import com.dojob.backend.model.Response;
import com.dojob.core.App;
import com.dojob.core.Error;
import com.dojob.core.Http;
import com.dojob.core.Logger;
import com.dojob.core.Result;
import com.dojob.service.FirebaseNotificationService;
import com.dojob.util.Utils;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonSyntaxException;

import java.util.HashMap;
import java.util.Map;

public class LoginApi {
    public final static String LOGIN_TAG = "login_tag";
    public final static String LOGOUT_TAG = "logout_tag";
    private final Context mContext;

    public static String LOG_IN = "LOGIN";

    public LoginApi(Context context) {
        mContext = context;
    }

    public static boolean isUser(Context context, int id){
        return SetupApi.getId(context) == id;
    }

    public static boolean isLoggedIn(Context context){
        return SetupApi.getId(context) > 0;
    }

    public Result request(String email, String password, String role,String deviceToken){
        return requestLogin(email, password, "", role, deviceToken,"http://step4work.com/api/talent/login");
    }
    public Result request(String token, String role,String deviceToken){
        Log.i("REQUEST_LOG_IN","REQUEST");
        return requestLogin(null, null, token, role, deviceToken,"http://step4work.com/api/talent/firebaseLogin");
    }
    public Result requestLogin(String email, String password, String token, String role,String deviceToken, String url) {
        LoginResponse response = null;
        if (!Utils.isOnline(mContext)) {
            return new Result(Error.OFFLINE);
        }
        Map<String, String> params = new HashMap<>();
        Http.Request request = Http.request(mContext)
                .url(url)
                .tag(LOGIN_TAG);

        Map<String, String> data = new HashMap<String, String>();
        if(email!=null){
            data.put("email", email);
            data.put("password", password);
        }
        else{
            data.put("idTokenString", token );
        }

        data.put("push_token", deviceToken);
        data.put("role", role); //talent/recruiter
        data.put("device_token",deviceToken);
        data.put("device_model",Utils.getDeviceModel());
        data.put("device_os",Utils.getOsVersion());
        data.put("device_Manuf",Utils.getManufracturer());
        data.put("app_version",Utils.getAppVersion());

        request.post(data);

        Result result = request.send();
        if (result.isSuccess()) {
            String responseText = (String) result.getResult();

            try {
                response = App.gson().fromJson(responseText, LoginResponse.class);
                if (response.status.equals("ok")){
                    if (response.token != null && response.token.length()>0){
                        LoginApi.setEmail(mContext, email);
                        LoginApi.setPassword(mContext, password);
                        RegisterApi.saveToken(mContext, response.token);
                        SetupApi.saveId(mContext, response.data.id);
                        SetupApi.saveUserData(mContext, response.data);
                        RegisterApi.logInType = 1;
                        PreferenceManager.getDefaultSharedPreferences(mContext).edit().putInt(LOG_IN,1).commit();

                    }
                }

            } catch (JsonSyntaxException ex) {
                ex.printStackTrace();
                RegisterApi.deleteToken(mContext);
            }
            if (response != null) {
                return new Result(null, response);
            }
            RegisterApi.deleteToken(mContext);
            return new Result(Error.DATA);
        }

        return new Result(Error.NETWORK);
    }

    public Result logout() {
        Response response = null;
        if (!Utils.isOnline(mContext)) {
            return new Result(Error.OFFLINE);
        }
        Map<String, String> params = new HashMap<>();
        Http.Request request = Http.request(mContext)
                .url("http://step4work.com/api/logout")//.url("https://www.dropbox.com/s/ocugljvfo2msx5u/feeds_response.json?dl=1")
                .post(new HashMap<String, String>())
                .tag(LOGOUT_TAG);
        Result result = request.send();
        if (result.isSuccess()) {
            String responseText = (String) result.getResult();

            try {
                response = App.gson().fromJson(responseText, Response.class);
            } catch (JsonSyntaxException ex) {
                ex.printStackTrace();
            }
            if (response != null) {
                return new Result(null, response);
            }
            return new Result(Error.DATA);
        }

        return new Result(Error.NETWORK);
    }

    public static String getEmail(Context context){
        SharedPreferences sharedPref = context.getSharedPreferences("dojob",Context.MODE_PRIVATE);
        return sharedPref.getString("LOGIN_EMAIL", "");
    }
    public static String getPassword(Context context){
        SharedPreferences sharedPref = context.getSharedPreferences("dojob",Context.MODE_PRIVATE);
        return sharedPref.getString("LOGIN_PASSWORD", "");
    }
    public static void setEmail(Context context, String token){
        SharedPreferences sharedPref = context.getSharedPreferences("dojob",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("LOGIN_EMAIL", token);
        editor.apply();
    }
    public static void setPassword(Context context, String token){
        SharedPreferences sharedPref = context.getSharedPreferences("dojob",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("LOGIN_PASSWORD", token);
        editor.apply();
    }
}
