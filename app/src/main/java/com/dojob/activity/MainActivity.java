package com.dojob.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.asksira.bsimagepicker.BSImagePicker;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.dojob.Additions.ApiInterface;
import com.dojob.Additions.ForgotPasswordFragment;
import com.dojob.Additions.PreSearchFragment;
import com.dojob.Additions.PrivacyFragment;
import com.dojob.Additions.ServiceGenerator;
import com.dojob.Additions.local.AnnoucncementResponse;
import com.dojob.Additions.local.AnnouncementItem;
import com.dojob.BuildConfig;
import com.dojob.GlideApp;
import com.dojob.R;
import com.dojob.adapter.PortfolioAdapter;
import com.dojob.backend.LoginApi;
import com.dojob.backend.RegisterApi;
import com.dojob.backend.SetupApi;
import com.dojob.backend.model.GalleryAlbumsItemResponse;
import com.dojob.backend.model.InboxMessageResponse;
import com.dojob.backend.model.ProfileResponse;
import com.dojob.backend.model.UserResponse;
import com.dojob.core.App;
import com.dojob.core.BackgroundWorker;
import com.dojob.core.Result;
import com.dojob.fragment.AlbumsFragment;
import com.dojob.fragment.ChatFragment;
import com.dojob.fragment.CommentFragment;
import com.dojob.fragment.CommentFragmentNew;
import com.dojob.fragment.FAQFragment;
import com.dojob.fragment.FavouriteFragment;
import com.dojob.fragment.FeedFragment;
import com.dojob.fragment.GalleryFragment;
import com.dojob.fragment.ImageViewerFragment;
import com.dojob.fragment.MainFragment;
import com.dojob.fragment.PortfolioFragment;
import com.dojob.fragment.ProfileSettingFragment;
import com.dojob.fragment.QuickPostingFragment;
import com.dojob.fragment.ReviewFragment;
import com.dojob.fragment.SearchFragment;
import com.dojob.fragment.TermOfUseFragment;
import com.dojob.fragment.VerificationFragment;
import com.dojob.listener.CompressCallbacbkListener;
import com.dojob.util.AppSP;
import com.dojob.util.ResourcesUtil;
import com.dojob.util.Utils;
import com.dojob.view.AnnouncementDialog;
import com.dojob.view.EmailLoginDialog;
import com.dojob.view.EmailSignUpDialog;
import com.dojob.view.ForgotPassMessageDialog;
import com.dojob.view.ForgotPasswordDialog;
import com.dojob.view.LoginDialog;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.security.MessageDigest;
import java.util.List;

import hb.xvideoplayer.MxVideoPlayerWidget;
import me.leolin.shortcutbadger.ShortcutBadger;
import retrofit2.Callback;
import retrofit2.Response;

//import hb.xvideoplayer.MxVideoPlayer;


public class MainActivity extends AppCompatActivity implements
        BackgroundWorker.Callbacks,
        View.OnClickListener,
        LoginDialog.Listener,
        EmailLoginDialog.Listener,
        EmailSignUpDialog.Listener,
        ProfileSettingFragment.Listener,
        ForgotPasswordDialog.Listener,
        BSImagePicker.OnMultiImageSelectedListener,
        BSImagePicker.OnSingleImageSelectedListener,
        FavouriteFragment.Listner {

    public final static String SEARCH_TEXT_RECEIVER = "search_text_receiver";
    public final static String ENABLE_TOOLBAR_TITLE = "enable_toolbar_title";
    private FragmentManager mManager;
    private View rightToolbar;
    private ImageView toolbar_camera;
    private AppCompatButton mLoginButton, mProfileButton;
    private String MAIN_FRAGMENT_TAG = "main_fragment";
    private String SOCIAL_FRAGMENT_TAG = "socailfeed_fragment";
    private String PROFILE_SETTING_FRAGMENT_TAG = "profile_setting_fragment";
    private String PROFILE_QUICK_POST_TAG = "quick_post_fragment";
    private String PREVIEW_PROFILE_FRAGMENT_TAG = "profile_preview_fragment";
    private String PREVIEW_PROFILE_USER_FRAGMENT_TAG = "profile_preview_user_fragment";
    private String ALBUM_PAGE_FRAGMENT_TAG = "album_page_fragment";
    public static String SEARCH_PAGE_FRAGMENT_TAG = "search_page_fragment";
    public static String PRE_SEARCH_PAGE_FRAGMENT_TAG = "pre_search_page_fragment";
    private String GALLERY_FRAGMENT_TAG = "gallery_fragment";
    private String CHAT_FRAGMENT_TAG = "chat_fragment";
    private String COMMENT_FRAGMENT_NEW_TAG = "comment_fragment_new";
    private String VERIFICATION_PAGE_FRAGMENT_TAG = "verification_fragment";
    private String COMMENT_FRAGMENT_TAG = "comment_fragment";
    private String REVIEW_PAGE_FRAGMENT_TAG = "review_fragment";
    private String FAVOURITE_FRAGMENT_TAG = "favourite_fragment";
    private String TALENT_FRAGMENT_TAG = "talent_fragment";
    private BackgroundWorker mWorker;
    private LoginApi mLoginApi;
    private View mToolbarProfile;
    private ToolBarClickedCallback mInboxCallback;
    private View mToolbarChatContainer;
    private AppCompatImageView mChatRefreshButton;
    private AppCompatButton mChatRoomButton;
    private ToolBarClickedCallback mChatCallback;
    private LinearLayout recruiter_layout_Button;
    private int IMAGE_PICKER_SELECT = 110;
    private int IMAGE_CAPTURE_SELECT = 111;
    private int VIDEO_CAPTURE_SELECT = 112;
    private final static int MY_PERMISSIONS_REQUEST_RERORD_VIDEO = 114;

    BSImagePicker singleSelectionPicker, multiSelectionPicker, videoSelectionPicker;

    //used as splash screen if needed

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        printHashKey(this);
        setContentView(R.layout.activity_main);

        StrictMode.VmPolicy.Builder newbuilder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(newbuilder.build());

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        rightToolbar = findViewById(R.id.toolbar_right);
        mLoginButton = findViewById(R.id.toolbar_login_button);
        mProfileButton = findViewById(R.id.toolbar_profile_button);
        mToolbarProfile = findViewById(R.id.toolbar_profile);
        recruiter_layout_Button = findViewById(R.id.toolbar_recuriter_button);
        toolbar_camera = findViewById(R.id.toolbar_camera);

        mToolbarChatContainer = findViewById(R.id.toolbar_chatRoom);
        mChatRefreshButton = findViewById(R.id.toolbar_refresh);
        mChatRoomButton = findViewById(R.id.toolbar_chatRoom_button);

        if (getSupportActionBar() != null)
            getSupportActionBar().setHomeAsUpIndicator(ResourcesUtil.getDrawable(R.drawable.baseline_navigate_before_black_24));
        mManager = getSupportFragmentManager();
        mManager.beginTransaction()
                .replace(R.id.content, MainFragment.newInstance(), MAIN_FRAGMENT_TAG)
                .commit();

        mWorker = new BackgroundWorker(this);
        mLoginApi = (LoginApi) mWorker.get(LoginApi.LOGIN_TAG);
        if (mLoginApi == null) {
            mLoginApi = new LoginApi(this);
            mWorker.put(LoginApi.LOGIN_TAG, mLoginApi);
        }

        callAnnouncmentsApi();

        switchSignUpButton(RegisterApi.isLogin(this));
        setIcon();

        String reqString = Build.MANUFACTURER
                + " " + Build.MODEL + " " + Build.VERSION.RELEASE
                + " " + Build.VERSION_CODES.class.getFields()[android.os.Build.VERSION.SDK_INT].getName();

        Log.d("MainActivty", "Model Request" + reqString);

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String newToken = instanceIdResult.getToken();
                Log.e("newToken", newToken);
                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("dojob", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("FIREBASE_TOKEN", newToken);
                editor.apply();
            }
        });
    }

    private void callAnnouncmentsApi() {
        if (RegisterApi.isLogin(this)) {
            AppSP sp = AppSP.getInstance(this);
            String bigId = String.valueOf(sp.readInt(Utils.ANNOUNCEMENT_BIGGEST_ID, 0));
//            String bigId = "1";//sp.readString(Utils.ANNOUNCEMENT_BIGGEST_ID, "");
            String token = "";
            token = "Bearer " + RegisterApi.getToken(this);
            ServiceGenerator serviceGenerator = new ServiceGenerator();
            ApiInterface apiInterface = serviceGenerator.createService(ApiInterface.class);
            retrofit2.Call<AnnoucncementResponse> call = apiInterface.getAnnounceMents(token, bigId);
            call.enqueue(new Callback<AnnoucncementResponse>() {
                @Override
                public void onResponse(retrofit2.Call<AnnoucncementResponse> call, Response<AnnoucncementResponse> response) {
                    if (response.isSuccessful()) {
                        Log.i("RESPONSE", "OKAY");
                        if (response.body() != null) {
                            String status = response.body().getStatus();
                            if (status.equalsIgnoreCase("ok")) {
                                AnnoucncementResponse annoucncementResponse = response.body();
                                showAnnouncements(annoucncementResponse);
                            } else {
                                Toast.makeText(MainActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(MainActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Log.i("RESPONSE", response.message());
                        Toast.makeText(MainActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(retrofit2.Call<AnnoucncementResponse> call, Throwable t) {
                    Log.i("RESPONSE", t.getMessage());
                    Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void showAnnouncements(AnnoucncementResponse annoucncementResponse) {
        if (annoucncementResponse != null) {
            if (annoucncementResponse.getAnnouncementData().size() > 0) {
                List<AnnouncementItem> itemArrayList = annoucncementResponse.getAnnouncementData();
                openAnnouncementDialog(itemArrayList);
            }
            AppSP sp = AppSP.getInstance(this);
            sp.savePreferences(Utils.ANNOUNCEMENT_BIGGEST_ID, annoucncementResponse.getBiggestAnnouncementId());
        }
    }

    public void openAnnouncementDialog(List<AnnouncementItem> itemArrayList) {
        if (getSupportFragmentManager() != null) {
            AnnouncementDialog dialog = AnnouncementDialog.newInstance();
            dialog.setAnnounceMentData(itemArrayList);
            dialog.show(getSupportFragmentManager(), "AnnounceDialog");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {

            if (resultCode == RESULT_OK) {
                if (requestCode == IMAGE_PICKER_SELECT) {
                    Uri selectedMediaUri = data.getData();
                    if (selectedMediaUri.toString().contains("image")) {
                        //handle image
                        String path = getPathFromURI(selectedMediaUri);
                        Bundle bundle = new Bundle();
                        bundle.putString(QuickPostingFragment.ARGS_FILE_URL, path);
                        bundle.putInt(QuickPostingFragment.ARGS_FILE_TYPE, 0);
                        bundle.putBoolean(QuickPostingFragment.NO_TOOLBAR, true);
                        openQuickPost(bundle);
                    } else if (selectedMediaUri.toString().contains("video")) {
                        String path = getPathFromURI(selectedMediaUri);
                        Bundle bundle = new Bundle();
                        bundle.putString(QuickPostingFragment.ARGS_FILE_URL, path);
                        bundle.putInt(QuickPostingFragment.ARGS_FILE_TYPE, 1);
                        bundle.putBoolean(QuickPostingFragment.NO_TOOLBAR, true);
                        openQuickPost(bundle);
                    }
                } else if (requestCode == VIDEO_CAPTURE_SELECT) {
                    Uri selectedMediaUri = data.getData();

//                    String filePath = null;
//                    Uri _uri = data.getData();
//                    Log.d("", "URI = " + _uri);
//                    if (_uri != null && "content".equals(_uri.getScheme())) {
//                        Cursor cursor = this
//                                .getContentResolver()
//                                .query(_uri,
//                                        new String[] { android.provider.MediaStore.Video.VideoColumns.DATA },
//                                        null, null, null);
//                        cursor.moveToFirst();
//                        filePath = cursor.getString(0);
//                        cursor.close();
//                    } else {
//                        filePath = _uri.getPath();
//                    }
//                    Log.d("", "Chosen path = " + filePath);
//                    if (!file.exists()) {
//                        copyFile(filePath, file.toString());
//                        Log.i("COPY", "Copy: " + videoUri.toString() + " a "
//                                + file.toString());
//                    }
//                    Log.i("uriVid", videoUri.getPath());
                    // String path  =getRealPathFromUri(this,selectedMediaUri);
//                    Bundle bundle = new Bundle();
//                    bundle.putString(QuickPostingFragment.ARGS_FILE_URL, selectedMediaUri.getPath());
//                    bundle.putString(QuickPostingFragment.ARGS_FILE_URI, selectedMediaUri.toString());
//                    bundle.putInt(QuickPostingFragment.ARGS_FILE_TYPE, 1);
//                    bundle.putBoolean(QuickPostingFragment.NO_TOOLBAR, true);
//                    openQuickPost(bundle);

                    // copyFile(selectedMediaUri.getPath(),"");

                    String path = getRealPathFromURIPath(selectedMediaUri, this);
                    File file = new File(path);
                    Uri userFile = Uri.parse(file.getPath());

                    onSingleImageSelected(userFile, "video");
                } else {
                    List<Fragment> fragments = getSupportFragmentManager().getFragments();
                    if (fragments != null) {

                        for (Fragment fragment : fragments) {
                            if (Utils.isFromLogin == 1) {
                                if (fragment instanceof EmailLoginDialog)
                                    fragment.onActivityResult(requestCode, resultCode, data);
                            } else {
                                fragment.onActivityResult(requestCode, resultCode, data);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View view = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (view instanceof EditText) {
            View w = getCurrentFocus();
            if (w != null) {
                int scrcoords[] = new int[2];
                w.getLocationOnScreen(scrcoords);
                float x = event.getRawX() + w.getLeft() - scrcoords[0];
                float y = event.getRawY() + w.getTop() - scrcoords[1];

                if (event.getAction() == MotionEvent.ACTION_UP
                        && (x < w.getLeft() || x >= w.getRight()
                        || y < w.getTop() || y > w.getBottom())) {
                    Utils.hideKeyboard(this);
                }
            }
        }
        return ret;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mLoginButton.setOnClickListener(this);
        mProfileButton.setOnClickListener(this);
        mToolbarProfile.setOnClickListener(this);
        mChatRefreshButton.setOnClickListener(this);
        mChatRoomButton.setOnClickListener(this);
        toolbar_camera.setOnClickListener(this);
        AppSP.getInstance(this).savePreferences(Utils.NOFICATION_NUMBER, 0);
        ShortcutBadger.removeCount(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mLoginButton.setOnClickListener(null);
        mProfileButton.setOnClickListener(null);
        mToolbarProfile.setOnClickListener(null);
        mChatRefreshButton.setOnClickListener(null);
        mChatRoomButton.setOnClickListener(null);
        toolbar_camera.setOnClickListener(null);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            mManager.popBackStack();
        }
        return super.onOptionsItemSelected(item);
    }

    public void cameraButtonActive() {
        if (LoginApi.isLoggedIn(this)) {
            AppSP sp = AppSP.getInstance(this);
            if (sp.readString(Utils.USER_ROLE).equalsIgnoreCase(Utils.TALENT_ROLE)) {
                if (toolbar_camera != null) {
                    toolbar_camera.setVisibility(View.VISIBLE);
//                    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content);
//                    if (fragment != null && fragment.isVisible()){
//                        if (fragment instanceof FeedFragment){
//                            toolbar_camera.setVisibility(View.VISIBLE);
//                        }else if(fragment instanceof SocialFeedFragment){
//                            toolbar_camera.setVisibility(View.VISIBLE);
//                        }else if(fragment instanceof MainFragment){
//                            toolbar_camera.setVisibility(View.VISIBLE);
//                        }else{
//                            toolbar_camera.setVisibility(View.GONE);
//                        }
//                    }else{
//                        toolbar_camera.setVisibility(View.GONE);
//                    }
                }
            }
        }
    }

    public void cameraButtonHide() {
        if (toolbar_camera != null) {
            toolbar_camera.setVisibility(View.GONE);
        }
    }

    public void openLoginDialog() {
        if (getSupportFragmentManager() != null) {
            LoginDialog dialog = LoginDialog.newInstance();
            dialog.setListener(this);
            dialog.show(getSupportFragmentManager(), "login_dialog");
        }
    }

    public void setIcon() {
        ImageView icon = findViewById(R.id.toolbar_image);
        icon.setVisibility(View.VISIBLE);
        Picasso.get().load(R.drawable.app_icon_transparent)/*.resize(0, Utils.dpToPx(60) )*/.into(icon);
        TextView title = findViewById(R.id.toolbar_title);
        title.setVisibility(View.GONE);
    }

    public void openMainFragment() {
        mManager.beginTransaction()
                .replace(R.id.content, MainFragment.newInstance(), MAIN_FRAGMENT_TAG)
                .commit();
    }

    public void setTitle(String message, boolean isMargin) {
        setToolbarTitle(message, isMargin);
    }

    public void setTitle(String message) {
        setToolbarTitle(message, false);
    }

    public void setJobDoneClose() {
        mChatRoomButton.setVisibility(View.GONE);
    }


    public void setBasicToolbar() {
        switchSignUpButton(RegisterApi.isLogin(getApplicationContext()));
        if (mToolbarChatContainer != null) mToolbarChatContainer.setVisibility(View.GONE);
    }

    public void setInboxToolbar() {
        if (mToolbarProfile != null) mToolbarProfile.setVisibility(View.GONE);
        if (mToolbarChatContainer != null)
            mToolbarChatContainer.setVisibility(View.GONE);
        if (mChatRoomButton != null)
            mChatRoomButton.setVisibility(View.GONE);
    }

    public void setInboxToolbarCallback(ToolBarClickedCallback listener) {
        mInboxCallback = listener;
        if (mChatRoomButton != null) mChatRoomButton.setVisibility(View.GONE);
        if (mChatRefreshButton != null) mChatRefreshButton.setVisibility(View.GONE);
        if (mToolbarProfile != null) mToolbarProfile.setVisibility(View.GONE);
        if (mToolbarChatContainer != null) mToolbarChatContainer.setVisibility(View.VISIBLE);
    }

    public void setChatToolbarCallback(ToolBarClickedCallback listener) {
        mChatCallback = listener;
        if (listener != null) {
            if (mChatRefreshButton != null)
                mChatRefreshButton.setVisibility(View.VISIBLE);
            if (mToolbarChatContainer != null)
                mToolbarChatContainer.setVisibility(View.VISIBLE);
            if (mToolbarProfile != null) mToolbarProfile.setVisibility(View.GONE);
        }
        recruiter_layout_Button.setVisibility(View.GONE);
    }

    public void setChatToolbarStatus(String status) {
        if (mChatRoomButton != null)
            if (status.isEmpty())
                mChatRoomButton.setVisibility(View.GONE);
            else {
                mChatRoomButton.setVisibility(View.VISIBLE);
                mChatRoomButton.setText(status);
            }
    }

    public void setToolbarTitle(String message, boolean isMargin) {
        ImageView icon = findViewById(R.id.toolbar_image);
        if (icon != null) icon.setVisibility(View.GONE);
        TextView title = findViewById(R.id.toolbar_title);
        if (title != null) {
            title.setVisibility(View.VISIBLE);
            title.setText(message);
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) title.getLayoutParams();
            if (isMargin) {
                params.setMargins(Utils.dpToPx(16), 0, 0, 0);
            } else {
                params.setMargins(Utils.dpToPx(0), 0, 0, 0);
            }
            title.setLayoutParams(params);
        }
        updateRecruiterStatus();
    }

    private void updateRecruiterStatus() {
        if (RegisterApi.isLogin(this)) {
            AppSP sp = AppSP.getInstance(this);
            String role = sp.readString(Utils.USER_ROLE, "");
            if (role.equalsIgnoreCase(Utils.RECRUITEE_ROLE)) {
                recruiter_layout_Button.setVisibility(View.VISIBLE);
            } else {
                recruiter_layout_Button.setVisibility(View.GONE);
            }
        } else {
            recruiter_layout_Button.setVisibility(View.GONE);
        }
    }

    public void openReviewsPage(UserResponse response) {
        cameraButtonHide();
        Bundle bundle = new Bundle();
        bundle.putString(ReviewFragment.REVIEW_RESPONSE_DATA, App.gson().toJson(response));
        mManager.beginTransaction()
                .replace(R.id.content, ReviewFragment.newInstance(bundle), REVIEW_PAGE_FRAGMENT_TAG)
                .addToBackStack(MAIN_FRAGMENT_TAG)
                .commit();
    }

    public void openPasswordChnageFragment() {
        cameraButtonHide();
        ForgotPasswordFragment forgotPasswordFragment = new ForgotPasswordFragment();
        mManager.beginTransaction()
                .replace(R.id.content, forgotPasswordFragment)
                .addToBackStack(null)
                .commit();
    }

    public void openNewFragment() {
        cameraButtonHide();
        PreSearchFragment preSearchFragment = new PreSearchFragment();
        mManager.beginTransaction()
                .replace(R.id.content, preSearchFragment, PRE_SEARCH_PAGE_FRAGMENT_TAG)
                .addToBackStack(MAIN_FRAGMENT_TAG)
                .commit();
    }

    public void openPreSearchPage() {
        cameraButtonHide();
        /*Bundle bundle = new Bundle();
        bundle.putString(SearchFragment.QUERY, query);
        Fragment searchFragment = getSupportFragmentManager().findFragmentByTag(SEARCH_PAGE_FRAGMENT_TAG);
        if(searchFragment == null){
            mManager.beginTransaction()
                    .replace(R.id.content, SearchFragment.newInstance(bundle), SEARCH_PAGE_FRAGMENT_TAG)
                    .addToBackStack(MAIN_FRAGMENT_TAG)
                    .commit();
        }
        else{
            searchFragment.setArguments(bundle);
            mManager.beginTransaction()
                    .detach(searchFragment)
                    .attach(searchFragment)
                    .commit();
        }*/
        PreSearchFragment preSearchFragment = new PreSearchFragment();

        mManager.beginTransaction()
                .replace(R.id.content, preSearchFragment, PRE_SEARCH_PAGE_FRAGMENT_TAG)
                .addToBackStack(MAIN_FRAGMENT_TAG)
                .commit();
    }

    public void searchOnlySecond(String query) {
        cameraButtonHide();
        Bundle bundle = new Bundle();
        bundle.putString(SearchFragment.QUERY, query);
        Fragment searchFragment = getSupportFragmentManager().findFragmentByTag(SEARCH_PAGE_FRAGMENT_TAG);

        if (searchFragment == null || !searchFragment.isVisible()) {
            mManager.beginTransaction()
                    .replace(R.id.content, SearchFragment.newInstance(bundle), SEARCH_PAGE_FRAGMENT_TAG)
                    .addToBackStack(MAIN_FRAGMENT_TAG)
                    .commit();
        } else {
            searchFragment.setArguments(bundle);
            mManager.beginTransaction()
                    .detach(searchFragment)
                    .attach(searchFragment)
                    .commit();
        }
    }

    public void openAlbumPage(GalleryAlbumsItemResponse item) {
        cameraButtonHide();
        Bundle bundle = new Bundle();
        bundle.putInt(AlbumsFragment.ALBUM_TYPE, AlbumsFragment.TYPE_ITEM);
        bundle.putString(AlbumsFragment.ALBUM_ITEM_RESPONSE, App.gson().toJson(item));
        mManager.beginTransaction()
                .replace(R.id.content, AlbumsFragment.newInstance(bundle), ALBUM_PAGE_FRAGMENT_TAG)
                .addToBackStack(MAIN_FRAGMENT_TAG)
                .commit();
    }

    public void openGalleryPage(int page, UserResponse response) {
        cameraButtonHide();
        Bundle bundle = new Bundle();
        bundle.putInt(GalleryFragment.GALLERY_PAGE, page);
        bundle.putString(GalleryFragment.GALLERY_USER_RESPONSE, App.gson().toJson(response));
        GalleryFragment fragment = GalleryFragment.newInstance(bundle);
        mManager.beginTransaction()
                .replace(R.id.content, fragment, GALLERY_FRAGMENT_TAG)
                .addToBackStack(MAIN_FRAGMENT_TAG)
                .commit();
    }


//    public void openAlbumItem(GalleryAlbumsItemResponse album) {
//        Bundle bundle = new Bundle();
//        bundle.putInt(GalleryFragment.GALLERY_PAGE, album.id);
//        AlbumsFragment fragment = AlbumsFragment.newInstance(bundle);
//        mManager.beginTransaction()
//                .replace(R.id.content, fragment, ALBUM_PAGE_FRAGMENT_TAG)
//                .addToBackStack(MAIN_FRAGMENT_TAG)
//                .commit();
//    }

    public void setNewUnreadBadge() {
        MainFragment mainFragment = (MainFragment) getSupportFragmentManager().findFragmentByTag(MAIN_FRAGMENT_TAG);
        if (mainFragment != null) {
            mainFragment.setMessageBadge();
        }
    }

    public void openChatRoom(int id, String name, InboxMessageResponse[] response) {
        cameraButtonHide();
        Bundle bundle = new Bundle();
        bundle.putInt(ChatFragment.USER_ID, id);
        bundle.putString(ChatFragment.TITLE, name);
        if (response != null) {
            bundle.putString(ChatFragment.MESSAGES_RESPONSE, App.gson().toJson(response));
        }
        ChatFragment fragment = ChatFragment.newInstance(bundle);
        mManager.beginTransaction()
                .replace(R.id.content, fragment, CHAT_FRAGMENT_TAG)
                .addToBackStack(MAIN_FRAGMENT_TAG)
                .commit();
        recruiter_layout_Button.setVisibility(View.GONE);
    }

    public void openImageViewer(String url) {
        cameraButtonHide();
        Intent intent = new Intent(this, ImageViewerActivty.class);
        intent.putExtra(ImageViewerFragment.IMAGE_URL, url);
        startActivity(intent);
    }

    public void openCommentFragment(String type, int position, int fileId) {
        cameraButtonHide();
        Bundle bundle = new Bundle();
        bundle.putString(CommentFragmentNew.COMMENTS_TYPE, type);
        bundle.putInt(CommentFragmentNew.FILE_ID, fileId);
        bundle.putInt(CommentFragmentNew.COMMENT_FILE_POSITION, position);
        CommentFragmentNew commentFragment = CommentFragmentNew.newInstance(bundle);
        mManager.beginTransaction()
                .replace(R.id.content, commentFragment, COMMENT_FRAGMENT_NEW_TAG)
                .addToBackStack(MAIN_FRAGMENT_TAG)
                .commit();

    }

    public void openCommentFragment2(String type, int position, int fileId) {

        Bundle bundle = new Bundle();
        bundle.putString(CommentFragmentNew.COMMENTS_TYPE, type);
        bundle.putInt(CommentFragmentNew.FILE_ID, fileId);
        bundle.putInt(CommentFragmentNew.COMMENT_FILE_POSITION, position);
        CommentFragmentNew commentFragment = CommentFragmentNew.newInstance(bundle);
        mManager.beginTransaction()
                .replace(R.id.content, commentFragment, COMMENT_FRAGMENT_NEW_TAG)
                .addToBackStack(null)
                .commit();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toolbar_login_button:
                openLoginDialog();
                break;

            case R.id.toolbar_camera:
                selectImage();
                break;

            case R.id.toolbar_profile_button:
                onOpenProfileSetting();
                break;
            case R.id.toolbar_profile:
                onOpenPreviewProfilePage(PortfolioAdapter.DEFAULT_TYPE, null);
                break;
            case R.id.toolbar_refresh:
                if (mChatCallback != null) {
                    mChatCallback.onRefreshClicked();
                } else if (mInboxCallback != null) {
                    mInboxCallback.onRefreshClicked();
                }
                break;
            case R.id.toolbar_chatRoom_button:
                if (mChatCallback != null) {
                    mChatCallback.onChatButtonClicked();
                }
                break;
        }
    }

    @Override
    public void onOpenEmailLoginDialog(String role) {
        if (getSupportFragmentManager() != null) {
            Bundle bundle = new Bundle();
            bundle.putString("role", role);
            EmailLoginDialog emailLoginDialog = EmailLoginDialog.newInstance(bundle);
            emailLoginDialog.setListener(this);

            emailLoginDialog.show(getSupportFragmentManager(), "email_login_dialog");
        }
    }

    @Override
    public void onOpenEmailSignUpDialog(String role) {
        if (getSupportFragmentManager() != null) {
            Bundle bundle = new Bundle();
            bundle.putString("role", role);
            EmailSignUpDialog emailSignUpDialog = EmailSignUpDialog.newInstance(bundle);
            emailSignUpDialog.setListener(this);
            emailSignUpDialog.show(getSupportFragmentManager(), "email_signup_dialog");
        }
    }

    @Override
    public void onOpenEmailForgotPassDialog(String role) {
        if (getSupportFragmentManager() != null) {
            Bundle bundle = new Bundle();
            bundle.putString("role", role);
            ForgotPasswordDialog forgotPasswordDialog = ForgotPasswordDialog.newInstance();
            forgotPasswordDialog.setListener(this);
            forgotPasswordDialog.show(getSupportFragmentManager(), "email_forgot_dialog");
        }
    }

    @Override
    public void onOpenForgotPassMessageDialog(String email) {
        if (getSupportFragmentManager() != null) {
            Bundle bundle = new Bundle();
            bundle.putString("email", email);
            ForgotPassMessageDialog forgotPasswordDialog = ForgotPassMessageDialog.newInstance(bundle);
            forgotPasswordDialog.show(getSupportFragmentManager(), "email_forgotpass_dialog");
        }
    }

    public void refresh() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            Fragment mainFragment = getSupportFragmentManager().findFragmentByTag(MAIN_FRAGMENT_TAG);
            if (mainFragment != null) {
//            mManager.beginTransaction()
//                    .detach(mainFragment)
//                    .attach(mainFragment)
//                    .commit();
                FragmentTransaction fragTransaction = getSupportFragmentManager().beginTransaction();
                fragTransaction.detach(mainFragment);
                fragTransaction.attach(mainFragment);
                fragTransaction.commit();
            }
        }
        switchSignUpButton(RegisterApi.isLogin(this));
    }

    @Override
    public void onOpenProfileSetting() {
        cameraButtonHide();
        Bundle bundle = new Bundle();
        bundle.putBoolean(ENABLE_TOOLBAR_TITLE, true);
        ProfileSettingFragment fragment = ProfileSettingFragment.newInstance(bundle);
        fragment.setListener(this);
        mManager.beginTransaction()
                .replace(R.id.content, fragment, PROFILE_SETTING_FRAGMENT_TAG)
                .addToBackStack(MAIN_FRAGMENT_TAG)
                .commit();
    }

    public void onOpenAllTalent() {
        cameraButtonHide();
        Bundle bundle = new Bundle();
        bundle.putInt(FeedFragment.TYPE, FeedFragment.TYPE_TALENT);
        FeedFragment fragment = FeedFragment.newInstance(bundle);
        mManager.beginTransaction()
                .replace(R.id.content, fragment, TALENT_FRAGMENT_TAG)
                .addToBackStack(MAIN_FRAGMENT_TAG)
                .commit();
    }

    @Override
    public void onOpenVerification() {
        mManager.beginTransaction()
                .replace(R.id.content, VerificationFragment.newInstance(), VERIFICATION_PAGE_FRAGMENT_TAG)
                .addToBackStack(MAIN_FRAGMENT_TAG)
                .commit();
    }

    public void onOpenCommentsPage(UserResponse response, String type, int position) {
        Bundle bundle = new Bundle();
        bundle.putString(CommentFragment.COMMENTS_TYPE, type);
        bundle.putString(CommentFragment.USER_RESPONSE_TYPE, App.gson().toJson(response));
        bundle.putInt(CommentFragment.COMMENT_FILE_POSITION, position);
        CommentFragment commentFragment = CommentFragment.newInstance(bundle);
        mManager.beginTransaction()
                .replace(R.id.content, commentFragment, COMMENT_FRAGMENT_TAG)
                .addToBackStack(MAIN_FRAGMENT_TAG)
                .commit();
    }

    @Override
    public void onOpenPreviewProfilePage(int type, UserResponse response) {
        cameraButtonHide();
        Bundle bundle = new Bundle();
        bundle.putInt(PortfolioFragment.ARGS_PORTFOLIO_TYPE, type);
        if (response != null)
            bundle.putString(PortfolioFragment.ARGS_PORTFOLIO_DATA, App.gson().toJson(response));

        PortfolioFragment profileFragment = (PortfolioFragment) getSupportFragmentManager().findFragmentByTag(PREVIEW_PROFILE_USER_FRAGMENT_TAG);
        if (profileFragment == null || !profileFragment.isVisible()) {
            mManager.beginTransaction()
                    .replace(R.id.content, PortfolioFragment.newInstance(bundle), PREVIEW_PROFILE_USER_FRAGMENT_TAG)
                    .addToBackStack(PROFILE_SETTING_FRAGMENT_TAG)
                    .commit();
        } else {
            mManager.beginTransaction()
                    .detach(profileFragment)
                    .attach(profileFragment)
                    .commit();
        }
    }

    public void onOpenPreviewProfilePage(String url) {
        cameraButtonHide();
        Bundle bundle = new Bundle();
        bundle.putInt(PortfolioFragment.ARGS_PORTFOLIO_TYPE, PortfolioAdapter.DEFAULT_TYPE);
        bundle.putString(PortfolioFragment.ARGS_PORTFOLIO_URL, url);
        PortfolioFragment portfolioFragment = PortfolioFragment.newInstance(bundle);
        mManager.beginTransaction()
                .replace(R.id.content, portfolioFragment, PREVIEW_PROFILE_FRAGMENT_TAG)
                .addToBackStack(MAIN_FRAGMENT_TAG)
                .commit();
        cameraButtonHide();
    }

    public void onOpenPreviewPage(int type, UserResponse response) {
        cameraButtonHide();
        Intent intent = new Intent(this, PreviewActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("preview_response", App.gson().toJson(response));
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void openFAQPage() {
        cameraButtonHide();
        FAQFragment fragment = FAQFragment.newInstance();
        mManager.beginTransaction()
                .replace(R.id.content, fragment, PROFILE_SETTING_FRAGMENT_TAG)
                .addToBackStack(MAIN_FRAGMENT_TAG)
                .commit();
    }

    public void openPrivacyPage() {
        cameraButtonHide();
        PrivacyFragment fragment = new PrivacyFragment();
        mManager.beginTransaction()
                .replace(R.id.content, fragment, PROFILE_SETTING_FRAGMENT_TAG)
                .addToBackStack(MAIN_FRAGMENT_TAG)
                .commit();
    }

    public void openTermOfUsePage() {
        TermOfUseFragment fragment = TermOfUseFragment.newInstance();
        mManager.beginTransaction()
                .replace(R.id.content, fragment, PROFILE_SETTING_FRAGMENT_TAG)
                .addToBackStack(MAIN_FRAGMENT_TAG)
                .commit();
    }

    public void openQuickPost(Bundle bundle) {
        cameraButtonHide();
        QuickPostingFragment fragment = QuickPostingFragment.newInstance(bundle);
        mManager.beginTransaction()
                .replace(R.id.content, fragment, PROFILE_QUICK_POST_TAG)
                .addToBackStack(MAIN_FRAGMENT_TAG)
                .commit();
    }

    @Override
    public Result executeTaskInBackground(String id, Bundle args) {
        switch (id) {
            case LoginApi.LOGIN_TAG:
//                return mLoginApi.request(LoginApi.getEmail(this), LoginApi.getPassword(this));
        }
        return null;
    }


    @Override
    public void onBackgroundTaskCompleted(String id, Result result) {
        switch (id) {
            case LoginApi.LOGIN_TAG:
                if (result.isSuccess()) {
                    switchSignUpButton(true);
                }
                break;
        }
    }

    public void restart() {
        finish();
        Intent i = getBaseContext().getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName());
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    public static void printHashKey(Context context) {
        try {
            final PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES);
            for (android.content.pm.Signature signature : info.signatures) {
                final MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                final String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i("AppLog", "key:" + hashKey + "=");
            }
        } catch (Exception e) {
            Log.e("AppLog", "error:", e);
        }
    }

    public void switchSignUpButton(boolean isLogin) {
        if (isLogin) {
//            FirebaseMessaging.getInstance().subscribeToTopic("test")
//                    .addOnCompleteListener(new OnCompleteListener<Void>() {
//                        @Override
//                        public void onComplete(@NonNull Task<Void> task) {
//                            String msg = "Subscribed";
//                            if (!task.isSuccessful()) {
//                                msg = "Subscribe fail";
//                            }
//                            Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
//                        }
//                    });
            if (mLoginButton != null) mLoginButton.setVisibility(View.GONE);
            setUpToolbarProfile();
        } else {
            if (mLoginButton != null) mLoginButton.setVisibility(View.VISIBLE);
            if (mToolbarProfile != null) mToolbarProfile.setVisibility(View.GONE);
        }

        updateRecruiterStatus();
    }

    private void setUpToolbarProfile() {
        ImageView profileImage = mToolbarProfile.findViewById(R.id.toolbar_profile_image);
        TextView profileName = mToolbarProfile.findViewById(R.id.toolbar_profile_name);
        ProfileResponse response = SetupApi.getUserData(getApplicationContext());
        if (mToolbarProfile != null)
            mToolbarProfile.setVisibility((response != null) ? View.VISIBLE : View.GONE);
        if (response != null) {
            if (response.profile_picture != null) {
                GlideApp.with(this)
                        .load(response.profile_picture)
                        .placeholder(R.drawable.profile_header_x3)
                        .apply(RequestOptions.fitCenterTransform())
                        .skipMemoryCache(false)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(profileImage);
            }
            if (response.name != null && !response.name.isEmpty()) {
                profileName.setText(response.name);
            }
        }
    }


    public void isSettingPage(boolean isSetting) {
        if (isSetting) {
            if (rightToolbar != null) rightToolbar.setVisibility(View.GONE);
        } else {
            if (rightToolbar != null) rightToolbar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onMultiImageSelected(List<Uri> uriList, String tag) {
        if (tag.equals("test")) {
            if (uriList.size() > 0) {
                for (int i = 0; i < uriList.size(); i++) {
                    Uri uri = uriList.get(i);
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (MxVideoPlayerWidget.backPress()) {
            return;
        } else if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            List<Fragment> fragmentList = getSupportFragmentManager().getFragments();
            getSupportFragmentManager().popBackStack();
        } else {
            AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog);
            } else {
                builder = new AlertDialog.Builder(this);
            }
            builder.setMessage("Are you sure you want to close application?")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            MainActivity.super.onBackPressed();
                        }
                    })
                    .setNegativeButton(android.R.string.no, null);
            Dialog dialog = builder.create();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.show();
        }
    }

    @Override
    public void onBSDestroy() {

    }

    @Override
    public void onOpenFavouriteFragment() {
        FavouriteFragment fragment = new FavouriteFragment();
        fragment.setListener(this);
        mManager.beginTransaction()
                .replace(R.id.content, fragment, FAVOURITE_FRAGMENT_TAG)
                .addToBackStack(MAIN_FRAGMENT_TAG)
                .commit();
    }

    @Override
    public void onSingleImageSelected(Uri uri, String tag) {
        Log.d("ProfileSetting", "onSingleImageSelected" + uri != null ? uri.toString() : "");
        switch (tag) {
            case "video":
                com.dojob.util.Utils.compressVideo(this, uri, new CompressCallbacbkListener() {
                    @Override
                    public void onStart() {

                    }

                    @Override
                    public void onSuccess(Uri uri) {
                        Bundle bundle = new Bundle();
                        bundle.putString(QuickPostingFragment.ARGS_FILE_URL, uri.getPath());
                        bundle.putString(QuickPostingFragment.ARGS_FILE_URI, uri.toString());
                        bundle.putInt(QuickPostingFragment.ARGS_FILE_TYPE, 1);
                        bundle.putBoolean(QuickPostingFragment.NO_TOOLBAR, true);
                        openQuickPost(bundle);
                    }

                    @Override
                    public void onError() {
                    }

                    @Override
                    public void onProgress(float percent) {
                    }

                });
                videoSelectionPicker.dismiss();
                break;
            case "cover":
                Bundle bundle = new Bundle();
                bundle.putString(QuickPostingFragment.ARGS_FILE_URL, uri.getPath());
                bundle.putString(QuickPostingFragment.ARGS_FILE_URI, uri.toString());
                bundle.putInt(QuickPostingFragment.ARGS_FILE_TYPE, 0);
                bundle.putBoolean(QuickPostingFragment.NO_TOOLBAR, true);
                openQuickPost(bundle);

                singleSelectionPicker.dismiss();
                break;
            default:
//                App.picasso().load(uri).fit().centerCrop().into(profilePhotoImage);
//                    profilePhotoImage.setImageDrawable(Drawable.createFromStream(getActivity().getContentResolver().openInputStream(uri),
//                            null));
//                profileUri = uri;
//                setChangeProfileButton();
                singleSelectionPicker.dismiss();
                break;
        }
    }

    public interface ToolBarClickedCallback {
        void onRefreshClicked();

        void onChatButtonClicked();
    }


    private void selectImage() {
        //   Constants.iscamera = true;
        final CharSequence[] items = {"Photo", "Video", "Video Capture",
                "Cancel"};

        TextView title = new TextView(this);
        title.setText("Add Photo/Video");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            title.setBackgroundColor(getColor(R.color.yellow));
        }
        title.setPadding(10, 15, 15, 10);
        title.setGravity(Gravity.CENTER);
        title.setTextColor(Color.WHITE);
        title.setTextSize(22);


        AlertDialog.Builder builder = new AlertDialog.Builder(
                MainActivity.this);


        builder.setCustomTitle(title);

        // builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Photo")) {

                    singleSelectionPicker = new BSImagePicker.Builder(BuildConfig.APPLICATION_ID + ".provider")
                            .setMaximumDisplayingImages(Integer.MAX_VALUE) //Default: Integer.MAX_VALUE. Don't worry about performance :)
                            .setTag("cover")
                            .setSpanCount(3) //Default: 3. This is the number of columns
                            .setGridSpacing(com.asksira.bsimagepicker.Utils.dp2px(2)) //Default: 2dp. Remember to pass in a value in pixel.
                            .setPeekHeight(com.asksira.bsimagepicker.Utils.dp2px(360)) //Default: 360dp. This is the initial height of the dialog.
                            .hideGalleryTile() //Default: show. Set this if you don't want to further let user select from a gallery app. In such case, I suggest you to set maximum     displaying    images to Integer.MAX_VALUE.
                            .build();

                    singleSelectionPicker.show(getSupportFragmentManager(), "picker");

                } else if (items[item].equals("Video")) {

                    videoSelectionPicker = new BSImagePicker.Builder("com.dojob.fileprovider")
                            .setMaximumDisplayingImages(120)
                            .setTag("video")
                            .hideCameraTile()
                            .hideGalleryTile()
                            .isLoadVideo(true)
                            .setMultiSelectBarBgColor(android.R.color.white) //Default: #FFFFFF. You can also set it to a translucent color.
                            .setMultiSelectTextColor(R.color.primary_text) //Default: #212121(Dark grey). This is the message in the multi-select bottom bar.
                            .setMultiSelectDoneTextColor(R.color.green) //Default: #388e3c(Green). This is the color of the "Done" TextView.
                            .setOverSelectTextColor(R.color.error_text) //Default: #b71c1c. This is the color of the message shown when user tries to select more than maximum select count.
                            .disableOverSelectionMessage() //You can also decide not to show this over select message.
                            .build();
                    videoSelectionPicker.show(getSupportFragmentManager(), "picker");
                } else if (items[item].equals("Video Capture")) {
                    askForPermissions();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void dispatchTakeVideoIntent() {
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        // takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, getVideoFile());

        String systemCamera;
        if (isPackageExists("com.google.android.camera")) {
            systemCamera = "com.google.android.camera";
        } else {
            systemCamera = findSystemCamera();
        }

        if (systemCamera != null) {
            takeVideoIntent.setPackage(systemCamera);
            if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(takeVideoIntent, VIDEO_CAPTURE_SELECT);
            }
        }
    }

    public boolean isPackageExists(String targetPackage) {
        List<ApplicationInfo> packages;
        PackageManager pm;
        pm = getPackageManager();
        packages = pm.getInstalledApplications(0);
        for (ApplicationInfo packageInfo : packages) {
            if (packageInfo.packageName.equals(targetPackage)) return true;
        }
        return false;
    }

    private String findSystemCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        PackageManager packageManager = this.getPackageManager();
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(intent, 0);
        for (ResolveInfo info :
                listCam) {
            if (isSystemApp(info.activityInfo.packageName)) {
                return info.activityInfo.packageName;
            }
        }
        return null;
    }


    boolean isSystemApp(String packageName) {
        ApplicationInfo ai;
        try {
            ai = this.getPackageManager().getApplicationInfo(packageName, 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        int mask = ApplicationInfo.FLAG_SYSTEM | ApplicationInfo.FLAG_UPDATED_SYSTEM_APP;
        return (ai.flags & mask) != 0;
    }

    public String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }

    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    private void askForPermissions() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_CONTACTS)) {
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_RERORD_VIDEO);
            }
        } else {
            dispatchTakeVideoIntent();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_RERORD_VIDEO) {
            if (grantResults.length > 3
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[2] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[3] == PackageManager.PERMISSION_GRANTED) {
                dispatchTakeVideoIntent();
            } else {
                askForPermissions();
            }
        } else {

            super.onRequestPermissionsResult(requestCode,permissions,grantResults);

//            if (grantResults.length > 3
//                    && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
//                    grantResults[1] == PackageManager.PERMISSION_GRANTED &&
//                    grantResults[2] == PackageManager.PERMISSION_GRANTED &&
//                    grantResults[3] == PackageManager.PERMISSION_GRANTED) {
//                dispatchTakeVideoIntent();
//            } else {
//                askForPermissions();
//            }
        }
    }
}
