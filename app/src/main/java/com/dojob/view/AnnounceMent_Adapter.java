package com.dojob.view;

import android.content.Context;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.dojob.Additions.local.AnnouncementItem;
import com.dojob.GlideApp;
import com.dojob.R;

import java.util.ArrayList;
import java.util.List;


public class AnnounceMent_Adapter extends PagerAdapter {


    private List<AnnouncementItem> mData;
    private LayoutInflater inflater;
    private Context context;


    public AnnounceMent_Adapter(Context context, List<AnnouncementItem> IMAGES) {
        this.context = context;
        this.mData = IMAGES;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.layout_announcement_item, view, false);

        ImageView imageView = (ImageView) imageLayout
                .findViewById(R.id.imageViewAnnounce);


        GlideApp.with(context)
                .load(mData.get(position).getUrl())
                .placeholder(R.drawable.placeholder_1)
                .skipMemoryCache(false)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);

        view.addView(imageLayout, 0);

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


}