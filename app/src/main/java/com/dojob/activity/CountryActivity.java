package com.dojob.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.dojob.Additions.CountryFragment;
import com.dojob.R;
import com.dojob.util.ResourcesUtil;

import static com.dojob.fragment.ProfileSettingFragment.PROFILE_SETTING_COUNTRY_CHOOSED;

public class CountryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolBarSetting();
        addFragment();

    }

    //Setting Tool Bar
    private void toolBarSetting() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setHomeAsUpIndicator(ResourcesUtil.getDrawable(R.drawable.baseline_navigate_before_black_24));
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setDisplayShowHomeEnabled(true);
        }
        TextView title = findViewById(R.id.toolbar_title);
        title.setVisibility(View.VISIBLE);
        title.setText(getString(R.string.selectcountry));
    }

    //add country fragment
    private void addFragment() {
        CountryFragment categoryFragment = new CountryFragment();
        categoryFragment.setArguments(getIntent().getExtras());
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content, categoryFragment)
                .commit();
    }

    //When click on the country item
    public void onCountryClick(Bundle bundle) {
        Intent localIntent = new Intent(PROFILE_SETTING_COUNTRY_CHOOSED);
        localIntent.putExtras(bundle);
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
        finish();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
