package com.dojob.Additions;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.dojob.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SkillAddingActivity extends Activity {

    public static String SKILL_NAME = "skill";

    @BindView(R.id.skill_bar)
    EditText skill_bar_edit_text;

    @BindView(R.id.cancel_text_view)
    TextView cancelTextView;

    @BindView(R.id.save_text_view)
    TextView saveTextView;

    private Unbinder unbinder;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_add_skill);
        unbinder = ButterKnife.bind(this);



        saveTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (skill_bar_edit_text.getText()!=null&&!skill_bar_edit_text.getText().toString().trim().equals("")){
                    Intent intent = new Intent();
                    intent.putExtra(SKILL_NAME,skill_bar_edit_text.getText().toString());
                    setResult(Activity.RESULT_OK,intent);
                    finish();
                }

            }
        });
        cancelTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              finish();
            }
        });
    }
}
