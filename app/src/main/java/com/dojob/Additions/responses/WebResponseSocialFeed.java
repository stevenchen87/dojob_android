package com.dojob.Additions.responses;

import com.dojob.Additions.local.SocialFeed;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WebResponseSocialFeed {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("socialFeedImage")
    @Expose
    private List<SocialFeed> socialFeedImage = null;
    @SerializedName("socialFeedVideo")
    @Expose
    private List<SocialFeed> socialFeedVideo = null;
    @SerializedName("current_page")
    @Expose
    private Integer currentPage;
    @SerializedName("next_page_url")
    @Expose
    private String nextPageUrl;
    @SerializedName("last_page")
    @Expose
    private Integer lastPage;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<SocialFeed> getSocialFeedImage() {
        return socialFeedImage;
    }

    public void setSocialFeedImage(List<SocialFeed> socialFeedImage) {
        this.socialFeedImage = socialFeedImage;
    }

    public List<SocialFeed> getSocialFeedVideo() {
        return socialFeedVideo;
    }

    public void setSocialFeedVideo(List<SocialFeed> socialFeedVideo) {
        this.socialFeedVideo = socialFeedVideo;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public String getNextPageUrl() {
        return nextPageUrl;
    }

    public void setNextPageUrl(String nextPageUrl) {
        this.nextPageUrl = nextPageUrl;
    }

    public Integer getLastPage() {
        return lastPage;
    }

    public void setLastPage(Integer lastPage) {
        this.lastPage = lastPage;
    }

}