package com.dojob.adapter.model;

import android.support.v7.widget.AppCompatButton;
import android.view.View;

import com.dojob.R;

public class PortfolioPreview {
    public static final int LAYOUT_ID = R.layout.layout_portfolio_preview;

    public PortfolioPreview() {

    }

    public void bindViewHolder(ViewHolder holder, int position){
        if(holder.confirm != null){
            holder.confirm.setTag(position);
        }
        if(holder.back != null){
            holder.back.setTag(position);
        }
    }

    public static class ViewHolder{

        public AppCompatButton confirm;
        public AppCompatButton back;
        public ViewHolder(View view){
//            confirm = view.findViewById(R.id.portfolio_preview_confirm);
            back= view.findViewById(R.id.portfolio_preview_back);
        }
    }
}
