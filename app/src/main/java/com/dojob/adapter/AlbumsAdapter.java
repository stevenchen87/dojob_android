package com.dojob.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dojob.R;
import com.dojob.adapter.holder.GalleryViewHolder;
import com.dojob.adapter.model.AlbumsCover;
import com.dojob.adapter.model.AlbumsItem;
import com.dojob.adapter.model.AlbumsTitle;
import com.dojob.adapter.model.PhotosAddBar;
import com.dojob.adapter.model.PhotosItem;
import com.dojob.adapter.model.PortfolioEmpty;
import com.dojob.backend.LoginApi;
import com.dojob.backend.model.FileResponse;
import com.dojob.backend.model.GalleryAlbumsItemResponse;
import com.dojob.core.BackgroundWorker;
import com.dojob.fragment.GalleryTabFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AlbumsAdapter extends RecyclerView.Adapter<GalleryViewHolder> implements View.OnClickListener, View.OnLongClickListener {

    public static final int ALBUMS_LIST_TYPE = 0;
    public static final int ALBUM_ITEM_LIST_TYPE = 1;
    private static final int TYPE_ALBUMS_ITEM = 1;
    private static final int TYPE_ALBUMS_COVER = 2;
    private static final int TYPE_ALBUMS_TITLE = 3;
    private static final int TYPE_PHOTOS_ITEM = 4;
    private static final int TYPE_ADD_BAR = 5;
    private static final int TYPE_PORTFOLIO_EMPTY = 6;
    private final int mUserId;
    private final Context mContext;
    private List<Object> mItems = new ArrayList<>();
    private int mType;
    private GalleryAlbumsItemResponse[] mResponse;
    private GalleryAlbumsItemResponse mAlbumResponse;
    private Listener mListener;
    private BackgroundWorker mWorker;
    private BackgroundWorker.Callbacks callbacks;

    public AlbumsAdapter(Context context, int userId, int type, BackgroundWorker mWorker, BackgroundWorker.Callbacks callback) {
        mContext = context;
        mUserId = userId;
        mType = type;
        this.mWorker = mWorker;
        this.callbacks = callback;
    }

    public void updateItem(GalleryAlbumsItemResponse[] response) {
        mResponse = response;
        updateItem();
    }

    public void updateItem(GalleryAlbumsItemResponse albumResponse) {
        mAlbumResponse = albumResponse;
        updateItem();
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }

    private void updateItem() {
        mItems.clear();
        switch (mType) {
            case ALBUMS_LIST_TYPE:
                if (LoginApi.isUser(mContext, mUserId))
                    mItems.add(new AlbumsItem(AlbumsItem.ALBUMS_DEFAULT, null, mContext));
                if (mResponse.length > 0) {
                    for (GalleryAlbumsItemResponse item : mResponse) {
                        mItems.add(new AlbumsItem(AlbumsItem.ALBUMS_IMAGES, item, mContext));
                    }
                }
                break;
            case ALBUM_ITEM_LIST_TYPE:
                if (mAlbumResponse.files.length > 0 && mAlbumResponse.album_type.equals("photo")) {
                    mItems.add(new AlbumsCover(mAlbumResponse.files[0].url));
                }
                long time = mAlbumResponse.created_at * 1000;
                mItems.add(new AlbumsTitle(mAlbumResponse.name, new SimpleDateFormat("dd-MM-yyyy").format(new Date(time))));
                if (LoginApi.isUser(mContext, mUserId)) mItems.add(new PhotosAddBar());
                if (mAlbumResponse.files.length > 0) {
                    for (FileResponse file : mAlbumResponse.files) {
                        mItems.add(new PhotosItem(file, file.id, mContext));
                    }
                }
                break;
        }

        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public GalleryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        int layout = 0;
        switch (viewType) {
            case TYPE_ALBUMS_ITEM:
                layout = AlbumsItem.LAYOUT_ID;
                break;
            case TYPE_ALBUMS_COVER:
                layout = AlbumsCover.LAYOUT_ID;
                break;
            case TYPE_ALBUMS_TITLE:
                layout = AlbumsTitle.LAYOUT_ID;
                break;
            case TYPE_PHOTOS_ITEM:
                layout = PhotosItem.LAYOUT_ID;
                break;
            case TYPE_ADD_BAR:
                layout = PhotosAddBar.LAYOUT_ID;
                break;
            case TYPE_PORTFOLIO_EMPTY:
                layout = PortfolioEmpty.LAYOUT_ID;
                break;
        }

        View view = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        GalleryViewHolder holder = new GalleryViewHolder(view);
        if (holder.albumsItem.container != null) {
            holder.albumsItem.container.setOnClickListener(this);
            holder.albumsItem.container.setOnLongClickListener(this);
        }
        if (holder.photosItem.container != null) {
            holder.photosItem.container.setOnClickListener(this);
            holder.photosItem.container.setOnLongClickListener(this);
        }
        if (holder.photosAddBar.container != null) {
            holder.photosAddBar.container.setOnClickListener(this);
            holder.photosAddBar.container.setOnLongClickListener(this);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull GalleryViewHolder holder, int position) {
        Object item = getItem(position);
        int type = getItemViewType(position);
        switch (type) {
            case TYPE_ALBUMS_ITEM:
                ((AlbumsItem) item).bindViewHolder(holder.albumsItem, position);
                break;
            case TYPE_ALBUMS_COVER:
                ((AlbumsCover) item).bindViewHolder(holder.albumsCover, position);
                break;
            case TYPE_ALBUMS_TITLE:
                ((AlbumsTitle) item).bindViewHolder(holder.albumsTitle, position);
                break;
            case TYPE_PHOTOS_ITEM:
                ((PhotosItem) item).bindViewHolder(holder.photosItem, position);
                break;
            case TYPE_ADD_BAR:
                ((PhotosAddBar) item).bindViewHolder(holder.photosAddBar, position);
                break;
            case TYPE_PORTFOLIO_EMPTY:
                ((PortfolioEmpty) item).bindViewHolder(holder.empty, position);
                break;

        }
    }

    @Override
    public int getItemViewType(int position) {
        Object item = getItem(position);
        if (item instanceof AlbumsItem) {
            return TYPE_ALBUMS_ITEM;
        } else if (item instanceof AlbumsCover) {
            return TYPE_ALBUMS_COVER;
        } else if (item instanceof AlbumsTitle) {
            return TYPE_ALBUMS_TITLE;
        } else if (item instanceof PhotosItem) {
            return TYPE_PHOTOS_ITEM;
        } else if (item instanceof PhotosAddBar) {
            return TYPE_ADD_BAR;
        } else if (item instanceof PortfolioEmpty) {
            return TYPE_PORTFOLIO_EMPTY;
        }
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        if (mItems != null) {
            return mItems.size();
        }
        return 0;
    }

    public Object getItem(int position) {
        int size = mItems.size();
        if (size > 0 && position < size) {
            return mItems.get(position);
        }
        return null;
    }

    @Override
    public void onClick(View view) {
        Object tag = view.getTag();
        if (tag == null) {
            return;
        }
        final Object item = getItem((int) view.getTag());
        switch (view.getId()) {
            case R.id.album_container:
                if (item instanceof AlbumsItem) {
                    AlbumsItem album = (AlbumsItem) item;
                    if (mListener != null) {
                        mListener.onOpenAlbumClicked(album.getItem());
                    }
                }
                break;
            case R.id.photo_container:
                if (item instanceof PhotosItem) {
                    if (mListener != null) {
                        FileResponse file = ((PhotosItem) item).getFile();
                        if (file != null) {
//                            if(file.fileType.equals("images")){
//                                mListener.onOpenImageViewer(((PhotosItem) item).getUrl());
//                            }
//                            else{
//                                mListener.onOpenVideoPlayer(((PhotosItem) item).getUrl());
//                            }
                            mListener.onOpenCommentFragment(file.fileType, (int) view.getTag(), (int) ((PhotosItem) item).getId());
                            return;
                        }
                        mListener.onOpenImageViewer(((PhotosItem) item).getUrl());
                    }
                }
                break;
            case R.id.photos_add_bar:
                if (mListener != null) {
                    mListener.onOpenMediaPicker();
                }
                break;
        }
    }

    private void deleteAlbum(final long fileId) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        alertDialogBuilder.setTitle("Confirm to Delete?");
        alertDialogBuilder
                .setMessage("Are you sure you would like to delete this album?")
                .setCancelable(false)
                .setPositiveButton("Delete Now", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Bundle bundle = new Bundle();
                        bundle.putLong("id", fileId);
                        if (mWorker != null)
                            mWorker.executeNewTask(GalleryTabFragment.TASK_DELETE_ALBUM, bundle, callbacks);
//                        galleryApi.deleteFiles(deletedFileId);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public boolean onLongClick(View view) {
        if (!LoginApi.isUser(mContext, mUserId)) return true;
        Object tag = view.getTag();
        if (tag == null) {
            return true;
        }
        final Object item = getItem((int) view.getTag());
        switch (view.getId()) {
            case R.id.album_container:
                if (item instanceof AlbumsItem) {
                    AlbumsItem album = (AlbumsItem) item;
                    deleteAlbum(album.getId());
                }
                break;
            case R.id.photo_container:
                if (item instanceof PhotosItem) {
                    if (mListener != null) {
                        FileResponse file = ((PhotosItem) item).getFile();
                    }
                }
                break;
        }
        return true;
    }

    public interface Listener {
        void onOpenAlbumClicked(GalleryAlbumsItemResponse item);

        void onOpenImageViewer(String url);

        void onOpenVideoPlayer(String url);

        void onOpenMediaPicker();

        void onOpenCommentFragment(String type, int position, int fileId);
    }
}
