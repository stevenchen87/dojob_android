package com.dojob.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dojob.R;
import com.dojob.activity.MainActivity;
import com.dojob.adapter.InboxAdapter;
import com.dojob.backend.InboxApi;
import com.dojob.backend.LoginApi;
import com.dojob.backend.ProfileApi;
import com.dojob.backend.RegisterApi;
import com.dojob.backend.model.InboxResponse;
import com.dojob.backend.model.InboxMessageResponse;
import com.dojob.backend.model.UserResponse;
import com.dojob.core.BackgroundWorker;
import com.dojob.core.Logger;
import com.dojob.core.Result;
import com.dojob.util.AppSP;
import com.dojob.util.Utils;
import com.github.ybq.android.spinkit.SpinKitView;
import com.malinskiy.superrecyclerview.SuperRecyclerView;

public class InboxFragment extends Fragment implements BackgroundWorker.Callbacks,
        InboxAdapter.Listener, MainActivity.ToolBarClickedCallback {
    private final String OBJ_INBOX_API = "obj_inbox_api";
    private final String OBJ_PROFILE_API = "obj_profile_api";
    private final String TASK_REQUEST_INBOX = "task_request_inbox";
//    private final String TASK_REQUEST_USER_PROFILE = "task_request_user_profile";
    private final String TASK_REQUEST_PROFILE = "task_request_profile";
    private BackgroundWorker mWorker;
    private InboxApi mInboxApi;
    private SpinKitView mProgress;
    private SuperRecyclerView mRecyclerView;
    private InboxAdapter mAdapter;
    private TextView empty_content;
    private ProfileApi mProfileApi;
    String url = "";
    int userID = 0;

    public static InboxFragment newInstance() {
        Bundle args = new Bundle();
        InboxFragment fragment = new InboxFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null) {
            mWorker = new BackgroundWorker(getActivity());
            mInboxApi = (InboxApi) mWorker.get(OBJ_INBOX_API);
            if (mInboxApi == null) {
                mInboxApi = new InboxApi(getContext());
                mWorker.put(OBJ_INBOX_API, mInboxApi);
            }

            mProfileApi = (ProfileApi) mWorker.get(OBJ_PROFILE_API);
            if (mProfileApi == null) {
                Log.i("PROFILE_API", "NULL");
                mProfileApi = new ProfileApi(getContext());
                mWorker.put(OBJ_PROFILE_API, mProfileApi);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_inbox, container, false);
        Utils.hideKeyBoard(getActivity());
        mProgress = view.findViewById(R.id.progress);
        mProgress.setVisibility(View.VISIBLE);
        empty_content = view.findViewById(R.id.portfolio_empty_content);
        mRecyclerView = (SuperRecyclerView)view.findViewById(R.id.recycle_view);

//        mRecyclerView.setItemViewCacheSize(20);
        mRecyclerView.setDrawingCacheEnabled(true);
        mRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        mRecyclerView.setVisibility(View.GONE);
        mAdapter = new InboxAdapter(getContext());
        mAdapter.setListener(this);
        mAdapter.setHasStableIds(true);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        if (RegisterApi.isLogin(getActivity())) {
            AppSP sp = AppSP.getInstance(getActivity());
            String userRole = sp.readString(Utils.USER_ROLE);
            if (userRole.equalsIgnoreCase(Utils.RECRUITEE_ROLE)) {
                empty_content.setText("You don’t have any new message\n\n" +
                        "Browse and message our\n cool talents to get in touch!");
            } else {
                empty_content.setText("You don’t have any new message");
            }
        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() != null) {
            if (mInboxApi != null) {
                InboxResponse[] response = mInboxApi.getChatRoomMessages();
                updateInbox(response);
                notifyInboxBadge(response);
            }
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (getActivity() != null) {
                ((MainActivity) getActivity()).setInboxToolbarCallback(this);
                if (mInboxApi != null) {
                    updateInbox(mInboxApi.getChatRoomMessages());

                }
                if (mWorker != null) {
                    mProgress.setVisibility(View.VISIBLE);
                    mWorker.executeNewTask(TASK_REQUEST_INBOX, null, this);
                }
            }
        }
    }

    @Override
    public Result executeTaskInBackground(String id, Bundle args) {
        switch (id) {
            case TASK_REQUEST_INBOX:
                try {
                    return mInboxApi.getMessage();
                } catch (Exception e) {
                    e.printStackTrace();
                } break;
            case TASK_REQUEST_PROFILE:
                return mProfileApi.request(url);

        }
        return null;
    }

    @Override
    public void onBackgroundTaskCompleted(String id, Result result) {
        switch (id) {
            case TASK_REQUEST_INBOX:
                if (result != null)
                    if (result.isSuccess() && mAdapter != null) {
                        InboxResponse[] response = (InboxResponse[]) result.getResult();
                        updateInbox(response);
                        notifyInboxBadge(response);
                    }
                mProgress.setVisibility(View.GONE);
                break;
            case TASK_REQUEST_PROFILE:
                mProgress.setVisibility(View.GONE);
                if (result.isSuccess() && mAdapter != null) {
                    UserResponse mResponse = (UserResponse) result.getResult();
                    if (mResponse != null) {
                        String newUrl = "";
                        if (mResponse.profile.position.equalsIgnoreCase("recruiter")){
                            newUrl = "http://step4work.com/api/recruiter/profile/get?uid="+userID;
                        }else{
                            newUrl = url;
                        }
                        AppCompatActivity activity = ((AppCompatActivity) getActivity());
                        if (activity != null) {
                            if (activity instanceof MainActivity) {
                                ((MainActivity) activity).onOpenPreviewProfilePage(newUrl);
                            }
                        }
                    }
                }
                break;
        }
    }

    @Override
    public void onOpenChatRoom(int id, String name, InboxMessageResponse[] response) {
        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        if (activity != null) {
            if (activity instanceof MainActivity) {
                ((MainActivity) activity).openChatRoom(id, name, response);
                for (InboxMessageResponse inboxMessageResponse : response) {
                    mInboxApi.updateReadStatus(inboxMessageResponse);
                }
            }
        }
    }

    @Override
    public void onOpenUserProfile(String url, int userId) {
//        String url1 = "http://step4work.com/api/profile/get?uid=3546";
//        this.url = url;
//        this.userID = userId;
//        checkUser();
        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        if (activity != null) {
            if (activity instanceof MainActivity) {
                ((MainActivity) activity).onOpenPreviewProfilePage(url);
            }
        }
    }

    private void checkUser(){
        mProgress.setVisibility(View.VISIBLE);
        mWorker.executeNewTask(TASK_REQUEST_PROFILE, null, this);
    }

    private void notifyInboxBadge(InboxResponse[] response) {
        if (response.length > 0) {
            boolean unread = false;
            for (InboxResponse inbox : response) {
                if (inbox.chatIsRead > 0) {
                    unread = true;
                }
            }
            if (unread && getActivity() != null) {
                ((MainActivity) getActivity()).setNewUnreadBadge();
            }
        }
    }

    public void updateInbox(InboxResponse[] response) {
        mAdapter.updateItem(response);
        mRecyclerView.setVisibility(View.VISIBLE);
        mProgress.setVisibility(View.GONE);
    }

    @Override
    public void onRefreshClicked() {
        mProgress.setVisibility(View.VISIBLE);
        if (mWorker != null) {
            mWorker.executeNewTask(TASK_REQUEST_INBOX, null, this);
        }
    }

    @Override
    public void onChatButtonClicked() {

    }
}
