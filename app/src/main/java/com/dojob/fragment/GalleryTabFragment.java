package com.dojob.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.asksira.bsimagepicker.BSImagePicker;
import com.dojob.BuildConfig;
import com.dojob.R;
import com.dojob.activity.MainActivity;
import com.dojob.adapter.GalleryTabAdapter;
import com.dojob.adapter.layout.PhotosSpanSizeLookup;
import com.dojob.backend.GalleryApi;
import com.dojob.backend.model.FileResponse;
import com.dojob.backend.model.GalleryAlbumsItemResponse;
import com.dojob.backend.model.MediaResponse;
import com.dojob.backend.model.UserResponse;
import com.dojob.core.App;
import com.dojob.core.BackgroundWorker;
import com.dojob.core.Logger;
import com.dojob.core.Result;
import com.dojob.listener.CompressCallbacbkListener;
import com.dojob.util.Utils;
import com.github.ybq.android.spinkit.SpinKitView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GalleryTabFragment extends Fragment implements GalleryTabAdapter.Listener,
        BSImagePicker.OnMultiImageSelectedListener,
        BackgroundWorker.Callbacks,
        BSImagePicker.OnSingleImageSelectedListener{

    public final static String TASK_DELETE_ALBUM = "task_delete_album";
    public final static String TASK_DELETE_FILE = "task_delete_file";
    public final static String TYPE_GALLERY = "task_gallery";
    public final static String TASK_GET_PHOTO_GALLERY = "gallery_photo_data";
    public final static String TASK_GET_VIDEO_GALLERY = "gallery_video_data";
    private static final String OBJ_GALLERY_API = "obj_gallery_api";
    private final String TASK_UPLOAD_PHOTO = "task_upload_photo";
    private final String TASK_UPLOAD_VIDEO = "task_upload_video";
    private RecyclerView mRecyclerView;
    private SpinKitView mProgress;
    private GalleryTabAdapter mAdapter;
    private BSImagePicker gallerySelectionPicker;
    private List<Uri> uriList;
    private BackgroundWorker mWorker;
    private GalleryApi mGalleryApi;

    private int mUserId;
    private int mType;

    public static GalleryTabFragment newInstance(Bundle args) {
        GalleryTabFragment fragment = new GalleryTabFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            if(getArguments().containsKey("user_id"))
                mUserId = getArguments().getInt("user_id");
            if(getArguments() != null && getArguments().containsKey(TYPE_GALLERY)){
                mType = getArguments().getInt(TYPE_GALLERY, 0);
            }
        }

        if(mType == GalleryApi.TYPE_GALLERY_PHOTO){
            gallerySelectionPicker = new BSImagePicker.Builder(BuildConfig.APPLICATION_ID + ".provider")
                    .setTag("add_photo")
                    .isLoadVideo(false)
                    .isMultiSelect() //Set this if you want to use multi selection mode.
                    .setMinimumMultiSelectCount(1) //Default: 1.
                    .setMaximumMultiSelectCount(5) //Default: Integer.MAX_VALUE (i.e. User can select as many images as he/she wants)
                    .setMultiSelectBarBgColor(android.R.color.white) //Default: #FFFFFF. You can also set it to a translucent color.
                    .setMultiSelectTextColor(R.color.primary_text) //Default: #212121(Dark grey). This is the message in the multi-select bottom bar.
                    .setMultiSelectDoneTextColor(R.color.green) //Default: #388e3c(Green). This is the color of the "Done" TextView.
                    .setOverSelectTextColor(R.color.error_text) //Default: #b71c1c. This is the color of the message shown when user tries to select more than maximum select count.
                    .disableOverSelectionMessage() //You can also decide not to show this over select message.
                    .build();
        }
        else{
            gallerySelectionPicker = new BSImagePicker.Builder(BuildConfig.APPLICATION_ID + ".provider")
                    .setTag("add_video")
                    .hideCameraTile()
                    .hideGalleryTile()
                    .isLoadVideo(true)
                    .setMultiSelectBarBgColor(android.R.color.white) //Default: #FFFFFF. You can also set it to a translucent color.
                    .setMultiSelectTextColor(R.color.primary_text) //Default: #212121(Dark grey). This is the message in the multi-select bottom bar.
                    .setMultiSelectDoneTextColor(R.color.green) //Default: #388e3c(Green). This is the color of the "Done" TextView.
                    .setOverSelectTextColor(R.color.error_text) //Default: #b71c1c. This is the color of the message shown when user tries to select more than maximum select count.
                    .disableOverSelectionMessage() //You can also decide not to show this over select message.
                    .build();
        }


        if(getActivity() != null){
            mWorker = new BackgroundWorker(getActivity());
            mGalleryApi = (GalleryApi) mWorker.get(OBJ_GALLERY_API);
            if (mGalleryApi == null) {
                mGalleryApi = new GalleryApi(getContext());
                mWorker.put(OBJ_GALLERY_API, mGalleryApi);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_photos, container, false);
        Utils.hideKeyBoard(getActivity());
        mRecyclerView = view.findViewById(R.id.recycle_view);
        mRecyclerView.setItemViewCacheSize(20);
        mRecyclerView.setDrawingCacheEnabled(true);
        mRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        mAdapter = new GalleryTabAdapter(getContext(), mUserId, mWorker, this);
        mAdapter.setHasStableIds(true);
        mAdapter.setListener(this);
        mRecyclerView.setAdapter(mAdapter);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3);
        gridLayoutManager.setSpanSizeLookup(new PhotosSpanSizeLookup(mAdapter));
        mRecyclerView.setLayoutManager(gridLayoutManager);
        mProgress = view.findViewById(R.id.progress);
        mRecyclerView.setVisibility(View.GONE);
        mProgress.setVisibility(View.VISIBLE);

        reloadData();
        return view;
    }
    public void reloadData(){
        if(mWorker != null) {
            mWorker.executeNewTask((mType == GalleryApi.TYPE_GALLERY_PHOTO)?TASK_GET_PHOTO_GALLERY:TASK_GET_VIDEO_GALLERY, null, this);
        }
    }
    @Override
    public void onOpenImageViewer(String url) {
        AppCompatActivity activity = ((AppCompatActivity)getActivity());
        if(activity != null){
            if(activity instanceof MainActivity){
                ((MainActivity) activity).openImageViewer(url);
            }
        }
    }

    @Override
    public void onOpenVideoPlayer(String url) {
        Utils.openVideoPlayer(getContext(), url);
    }

    @Override
    public void onOpenCommentFragment(String type, int position, int fileId) {
        AppCompatActivity activity = ((AppCompatActivity)getActivity());
        if(activity != null){
            if(activity instanceof MainActivity){
                ((MainActivity) activity).openCommentFragment(type,position,fileId);
            }
        }
    }


    @Override
    public void onOpenMediaPicker() {
        if(gallerySelectionPicker != null && (gallerySelectionPicker.getDialog() == null || !gallerySelectionPicker.getDialog().isShowing()))
            gallerySelectionPicker.show(getChildFragmentManager(), "add_photo");
    }

    @Override
    public void onSingleImageSelected(Uri uri, final String tag) {
        uriList = new ArrayList<>();
        uriList.add(uri);
        mProgress.setVisibility(View.VISIBLE);
        Utils.compressVideo(getActivity(), uri, new CompressCallbacbkListener() {
                    @Override
                    public void onStart() {

                    }

                    @Override
                    public void onSuccess(Uri uri) {
                        if(tag.equals("add_video")){
                            if(mWorker != null) mWorker.executeNewTask(TASK_UPLOAD_VIDEO, null, GalleryTabFragment.this);
                        }
                    }

                    @Override
                    public void onError() {
                        mProgress.setVisibility(View.GONE);
                    }

                    @Override
                    public void onProgress(float percent) {
                    }
                });


        if(gallerySelectionPicker != null && (gallerySelectionPicker.getDialog()!= null || gallerySelectionPicker.getDialog().isShowing())){
            gallerySelectionPicker.dismiss();
        }
    }

    @Override
    public void onMultiImageSelected(List<Uri> uriList, String tag) {
        this.uriList = uriList;
        mProgress.setVisibility(View.VISIBLE);
        if(tag.equals("add_photo")){
            if(mWorker != null) mWorker.executeNewTask(TASK_UPLOAD_PHOTO, null, this);
        }

        gallerySelectionPicker.dismiss();
    }

    @Override
    public void onBSDestroy() {

    }

    @Override
    public Result executeTaskInBackground(String id, Bundle args) {
        final HashMap<String, String> deletedFileId = new HashMap<>();
        switch (id){
            case TASK_DELETE_ALBUM:
                deletedFileId.put("0", args.get("id").toString());
                return mGalleryApi.deleteAlbum(deletedFileId);
            case TASK_DELETE_FILE:
                deletedFileId.put("0", args.get("id").toString());
                return mGalleryApi.deleteFiles(deletedFileId);
            case TASK_UPLOAD_PHOTO:
                return mGalleryApi.post("photo", uriList);
            case TASK_UPLOAD_VIDEO:
                return mGalleryApi.post("video", uriList);
            case TASK_GET_PHOTO_GALLERY:
            case TASK_GET_VIDEO_GALLERY:
                return mGalleryApi.getGallery(mType, String.valueOf(mUserId));
        }
        return null;
    }

    @Override
    public void onBackgroundTaskCompleted(String id, Result result) {
        switch (id){
            case TASK_DELETE_FILE:
                reloadData();
                break;
            case TASK_UPLOAD_PHOTO:
                if(result.isSuccess()){
                    UserResponse response = (UserResponse) result.getResult();
                    ArrayList<String> list = new ArrayList<>();
                    for(MediaResponse photoResponse: response.talentPhoto ){
                        list.add(photoResponse.url);
                    }
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mProgress.setVisibility(View.GONE);
                    mAdapter.updateItem(GalleryApi.TYPE_GALLERY_PHOTO, response.talentPhoto);
                    return;
                }
                if(mProgress != null) mProgress.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "Error uploading file", Toast.LENGTH_LONG).show();
                break;
            case TASK_UPLOAD_VIDEO:
                if(result.isSuccess()){
                    UserResponse response = (UserResponse) result.getResult();
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mProgress.setVisibility(View.GONE);
                    mAdapter.updateItem(GalleryApi.TYPE_GALLERY_VIDEO, response.talentVideo);
                    return;
                }
                if(mProgress != null) mProgress.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "Error uploading file", Toast.LENGTH_LONG).show();
                break;
            case TASK_GET_PHOTO_GALLERY:
            case TASK_GET_VIDEO_GALLERY:
                if(result.isSuccess()){
                    UserResponse response = (UserResponse) result.getResult();
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mProgress.setVisibility(View.GONE);
                    if(mType == GalleryApi.TYPE_GALLERY_PHOTO){
                        mAdapter.updateItem(GalleryApi.TYPE_GALLERY_PHOTO, response.talentPhoto);
                    }
                    else{
                        mAdapter.updateItem(GalleryApi.TYPE_GALLERY_VIDEO, response.talentVideo);
                    }
                    return;
                }
                break;
        }
    }
}
