package com.dojob.core;

import android.util.Log;


public class Logger {

    private static String TAG = "dojob";

    public static void d(String s) {
        Log.d(TAG, s);
    }

    public static void e(String s) {
        Log.e(TAG, s);
    }
}
