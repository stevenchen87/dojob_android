package com.dojob.view;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dojob.Additions.WrapContentHeightViewPager;
import com.dojob.Additions.WrapContentViewPager;
import com.dojob.Additions.local.AnnouncementItem;
import com.dojob.R;

import java.util.List;

public class AnnouncementDialog extends AppCompatDialogFragment implements View.OnClickListener {
    private final String TAG = "AnnouncementDialog";
    private WrapContentHeightViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private TextView tvpagenumber, tvslash, tvpageTotal, tvGotIt;
    private LinearLayout pageslayout;

    private List<AnnouncementItem> mData;

    public void setAnnounceMentData(List<AnnouncementItem> mData) {
        this.mData = mData;
    }

    public static AnnouncementDialog newInstance() {
        Bundle args = new Bundle();

        AnnouncementDialog fragment = new AnnouncementDialog();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            fragment.setStyle(AppCompatDialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Material_Light_Dialog_NoActionBar);
        } else {
            fragment.setStyle(AppCompatDialogFragment.STYLE_NO_TITLE, android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar);
        }
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_annoucements, container);
        Window window = getDialog().getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        return view;
    }
    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(this, tag);
            ft.commit();
        } catch (IllegalStateException e) {
            Log.d(TAG, "Exception", e);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mPager = (WrapContentHeightViewPager) view.findViewById(R.id.pager);
        pageslayout = (LinearLayout) view.findViewById(R.id.pageslayout);
        tvpagenumber = (TextView) view.findViewById(R.id.tvpagenumber);
        tvpageTotal = (TextView) view.findViewById(R.id.tvpageTotal);
        tvGotIt = (TextView) view.findViewById(R.id.tvGotIt);
        if (mData != null) {
            if (mData.size() > 1) {
                pageslayout.setVisibility(View.VISIBLE);
                tvpagenumber.setText("1");
                tvpageTotal.setText(mData.size() + "");
                tvGotIt.setText("Next");
            } else {
                pageslayout.setVisibility(View.GONE);
                tvGotIt.setText("Got it!");
            }
        }
        tvGotIt.setOnClickListener(this);

        AnnounceMent_Adapter ment_adapter = new AnnounceMent_Adapter(getContext(), mData);
        mPager.setAdapter(ment_adapter);
        currentPage = 0;
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                updatePage(position);
                currentPage = position;
            }

            @Override
            public void onPageSelected(int position) {
                //mPager.reMeasureCurrentPage(mPager.getCurrentItem());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void updatePage(int pageNo) {
        if (mData != null) {
            if (pageNo + 1 == mData.size()) {
                tvGotIt.setText("Got it!");
            } else {
                tvGotIt.setText("Next");
            }
            pageslayout.setVisibility(View.VISIBLE);
            tvpagenumber.setText(pageNo + 1 + "");
            tvpageTotal.setText(mData.size() + "");
            if (mData.size() == 1){
                pageslayout.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onStart()
    {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null)
        {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.tvGotIt:
                if (tvGotIt.getText().toString().equalsIgnoreCase("Got it!")) {
                    dismiss();
                } else {
                    mPager.setCurrentItem(currentPage + 1);
                }
                break;
            default:
                break;
        }
    }
}

