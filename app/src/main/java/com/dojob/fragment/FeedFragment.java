package com.dojob.fragment;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dojob.R;
import com.dojob.activity.MainActivity;
import com.dojob.adapter.FeedAdapter;
import com.dojob.adapter.PortfolioAdapter;
import com.dojob.adapter.layout.FeedSpanSizeLookup;
import com.dojob.backend.FeedsApi;
import com.dojob.backend.LoginApi;
import com.dojob.backend.model.FeedItemResponse;
import com.dojob.backend.model.FeedsResponse;
import com.dojob.core.App;
import com.dojob.core.BackgroundWorker;
import com.dojob.core.Result;
import com.dojob.listener.EndlessRecyclerOnScrollListener;
import com.dojob.util.AppSP;
import com.dojob.util.Utils;
import com.github.ybq.android.spinkit.SpinKitView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class FeedFragment extends Fragment implements BackgroundWorker.Callbacks {
    public final static String TYPE = "feed_type";
    public final static int TYPE_FEED = 0;
    public final static int TYPE_TALENT = 1;

    private final String OBJ_FEEDS_API = "obj_feed_api";
    private final String TASK_REQUEST_FEEDS = "task_request_feeds";
    private final String TASK_REQUEST_TALENT = "task_request_talent";
    private BackgroundWorker mWorker;
    private FeedAdapter mAdapter;
    private FeedsApi mFeedsApi;

    private SpinKitView mProgress;
    private RecyclerView mRecyclerView;
    private int mType;
    private boolean mNextPage;
    private String mNextUrl;
    private int scrollPos = 0;
    private GridLayoutManager gridLayoutManager;
    private FeedsResponse localFeedbackResponse;
    private ArrayList<FeedItemResponse> hotList = null;
    private ArrayList<FeedItemResponse> topList = null;
    private ArrayList<FeedItemResponse> talentList = null;

    public static FeedFragment newInstance(Bundle args) {
        FeedFragment fragment = new FeedFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null) {
            mWorker = new BackgroundWorker(getActivity());
            mFeedsApi = (FeedsApi) mWorker.get(OBJ_FEEDS_API);
            if (mFeedsApi == null) {
                mFeedsApi = new FeedsApi(getContext());
                mWorker.put(OBJ_FEEDS_API, mFeedsApi);
            }

            mType = (getArguments() != null && getArguments().containsKey(TYPE)) ? getArguments().getInt(TYPE) : 0;

            hotList = new ArrayList<FeedItemResponse>();
            topList = new ArrayList<FeedItemResponse>();
            talentList = new ArrayList<FeedItemResponse>();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        if (activity != null) {
            if (activity instanceof MainActivity && mType == TYPE_TALENT) {
                ActionBar actionbar = activity.getSupportActionBar();
                // will be used in fragment with back button
                if (actionbar != null) {
                    actionbar.setDisplayHomeAsUpEnabled(true);
                    actionbar.setDisplayShowHomeEnabled(true);
                }
                ((MainActivity) activity).setTitle("Talent");
//                ((MainActivity) getActivity()).setBasicToolbar();
            }
//            else{
//                if (activity instanceof MainActivity)
//                    ((MainActivity) activity).cameraButtonActive();
//            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (gridLayoutManager != null)
            scrollPos = gridLayoutManager.findLastVisibleItemPosition();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feed, container, false);
        Utils.hideKeyBoard(getActivity());
        mProgress = view.findViewById(R.id.progress);
        mProgress.setVisibility(View.VISIBLE);
        mRecyclerView = view.findViewById(R.id.recycle_view);
        mRecyclerView.setItemViewCacheSize(20);
        mRecyclerView.setDrawingCacheEnabled(true);
        mRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        mRecyclerView.setVisibility(View.GONE);
        mAdapter = new FeedAdapter(getActivity(), getActivity().getSupportFragmentManager(), 0, null);
        mAdapter.setListener(this.getActivity());
        mAdapter.setHasStableIds(true);
        mRecyclerView.setAdapter(mAdapter);
        gridLayoutManager = new GridLayoutManager(getContext(), 4);
        gridLayoutManager.setSpanSizeLookup(new FeedSpanSizeLookup(mAdapter));
        mRecyclerView.setLayoutManager(gridLayoutManager);
        if (localFeedbackResponse != null) {
            updatUI();
            if (mType == TYPE_TALENT) {
                mRecyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
                    @Override
                    public void onLoadMore() {
                        if (mNextPage && mNextUrl != null) {
                            Bundle bundle = new Bundle();
                            bundle.putString("talent_url", mNextUrl);
                            if (mWorker != null)
                                mWorker.executeNewTask(TASK_REQUEST_TALENT, bundle, FeedFragment.this);
                        }
                    }
                });
            }
        } else if (mType == TYPE_FEED) {
            if (mWorker != null) mWorker.executeNewTask(TASK_REQUEST_FEEDS, null, this);
        } else if (mType == TYPE_TALENT) {
            if (mWorker != null) mWorker.executeNewTask(TASK_REQUEST_TALENT, new Bundle(), this);
            mRecyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
                @Override
                public void onLoadMore() {
                    if (mNextPage && mNextUrl != null) {
                        Bundle bundle = new Bundle();
                        bundle.putString("talent_url", mNextUrl);
                        if (mWorker != null)
                            mWorker.executeNewTask(TASK_REQUEST_TALENT, bundle, FeedFragment.this);
                    }
                }
            });
        }

        return view;
    }


    @Override
    public Result executeTaskInBackground(String id, Bundle args) {
        switch (id) {
            case TASK_REQUEST_FEEDS:
                return mFeedsApi.request();
            case TASK_REQUEST_TALENT:
                return mFeedsApi.requestTalent(args.getString("talent_url", null));
        }
        return null;
    }

    @Override
    public void onBackgroundTaskCompleted(String id, Result result) {
        switch (id) {
            case TASK_REQUEST_FEEDS:
                if (result.isSuccess() && mAdapter != null) {
                    FeedsResponse resposne = (FeedsResponse) result.getResult();
                    mAdapter.updateItem(resposne);
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mProgress.setVisibility(View.GONE);
                    if (resposne.newMsg > 0 && getActivity() != null) {
                        ((MainActivity) getActivity()).setNewUnreadBadge();
                    }
                }
                break;
            case TASK_REQUEST_TALENT:
                if (result.isSuccess() && mAdapter != null) {
                    FeedsResponse response = (FeedsResponse) result.getResult();
                    updateLocalArray(response);

                    mNextPage = response.currentPage < response.lastPage;
                    if (mNextPage) {
                        mNextUrl = response.nextPageUrl;
                    } else {
                        mNextUrl = null;
                    }
                    mAdapter.updateTalent(response);
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mProgress.setVisibility(View.GONE);
//                    if (scrollPos != 0){
//                        if (scrollPos> mAdapter.getItemCount()){
//                            mRecyclerView.smoothScrollToPosition(mAdapter.getItemCount());
//                        }else{
//                            mRecyclerView.smoothScrollToPosition(scrollPos);
//                        }
//                    }
                }
                break;

        }
    }

    private void updatUI() {
        updatLocalItem();
        mAdapter.updateTalent(localFeedbackResponse);
        mRecyclerView.setVisibility(View.VISIBLE);
        mProgress.setVisibility(View.GONE);
    }

    private void updatLocalItem() {
        FeedItemResponse[] hotArray = new FeedItemResponse[hotList.size()];
        localFeedbackResponse.hot = hotList.toArray(hotArray);

        FeedItemResponse[] topArray = new FeedItemResponse[topList.size()];
        localFeedbackResponse.top = topList.toArray(topArray);

        FeedItemResponse[] talentArray = new FeedItemResponse[talentList.size()];
        localFeedbackResponse.talent = talentList.toArray(talentArray);
    }

    private void updateLocalArray(FeedsResponse response) {
        localFeedbackResponse = response;
        if (response.hot != null && response.hot.length > 0) {
            FeedItemResponse[] hotitem = response.hot;
            for (FeedItemResponse itemResponse : hotitem) {
                hotList.add(itemResponse);
            }
        }

        if (response.top != null && response.top.length > 0) {
            FeedItemResponse[] topitem = response.top;
            for (FeedItemResponse itemResponse : topitem) {
                topList.add(itemResponse);
            }
        }

        if (response.talent != null && response.talent.length > 0) {
            FeedItemResponse[] talentitem = response.talent;
            for (FeedItemResponse itemResponse : talentitem) {
                talentList.add(itemResponse);
            }
        }
    }

    public void reload() {
        if (mWorker != null) mWorker.executeNewTask(TASK_REQUEST_FEEDS, null, this);
    }

    public static class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    public static int dpToPx(int dp, Context context) {
        Resources r = context.getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
