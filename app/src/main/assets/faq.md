# General

## What is Step4work?
Step4work is a social app for people from around the world to connect and share their passionate talents. It is a trusted community platform for people to list and search for talent services.

Whether you are artists, promoters, actors, musicians, bakers or even more, Step4work is the easiest way for people to monetise their extra time, pursue their passion, and showcase their work. Hot talents are connected to big brands.

## How does Step4work work?
It is free to post a talent service, whether it is illustrating art commissions, emceeing events, using your voice or acting talent, composing or performing music, or even baking and catering. Just describe your talent and start receiving offers from interested potential Recruiters within the community!

Simply create an account and post your service – it’s free. Connect with fellow Step4work users and showcase your talent with updates on your achievements.

Potential Recruiters will be able to start a conversation with a Talent via service enquiry messaging and arrange further details about completing the service.

Currently, there will be no form of money transaction done over the Step4work app. Talents and Recruiters are responsible for completing any monetary transactions directly. 


## What are the terms of use?
Please refer to our Terms of Use [here](http://www.step4work.com/terms).

## How can I contact Step4work?
Should you have questions or need help with your step4work account, just go to [Contact Us](mailto:info@step4work.com) and drop us a message.

## How do I create a Step4work account?
To create a Step4work account:
 -	Go to the Google Play store or the Apple Appstore.
 -	Select and download the Step4work app. 
 -	Open the Step4work app.
 -	Choose to register as a Talent or a Recruiter. 
 -	Enter your name, email and password.
 -	Click Register.
 -	To finish creating your account, you need to confirm your email.
 -	Alternatively, you may register with your Facebook or Google accounts by tapping on the respective buttons. 
 -	Additionally, to complete your registration as a Talent, you must include a profile photo and a Talent photo (to showcase your Talent service).
Anyone eligible according to our Terms of Use can sign up on Step4work. Registration is free and easy.


## Why do I need a profile or profile photo on Step4work?
Your profile is a great way for others to understand how reliable, authentic and committed you are towards your passion. Let the community learn more about you with photos of your talent in action. The more information is shared, the more likely it is that people will want to engage your services.

Recruiters do not need to upload Talent photos themselves, but they are free to browse Talent photos and connect with our passionate Talents. 

## How do I add people to my favourites?
If you like a particular Talent on the app, you may add them to your Favourites list by clicking on “View Profile” and select “Send Connection Request” to include them in your Favourites. You will automatically receive updates from your Favourites and see their posts on your Favourites feed.

## Can I post a service and enquire about services at the same time?
Talent accounts may post Talent photos to publicise their services and may also enquire about services from other Talents. Recruiter accounts may enquire about services from Talents without having to post Talent photos themselves. 

## I would like to post a service. What type of information should I put in the description?
It is completely free to post a service on Step4work and get enquiries on your service. Simply create a Talent service in the “My Talent” section of your profile along with a photo of your talent.

Include the description, scope, and duration of your service. This helps potential Recruiters to determine whether you’re the right person for the job.

## How do I receive payment after completing my services?
Currently, there will be no form of monetary transaction done within the Step4work app. Payment details are handled between Talents and Recruiters.

## How do I send a message on Step4work?
To send a private message to your network:
1.	Tap the Message icon at the bottom of the screen.
2.	Search for person you want to message.
3.	Type your message, then tap “Send.”

## How much do I get paid per service completed?
That depends on the terms agreed upon between the Talent and Recruiter. Both parties are entirely responsible for determining whether the payment amounts to fair pay. Both parties may also negotiate on the amount offered.

## How do I receive payments after completing the job?
Recruiters shall pay Talents directly in cash or bank transfer upon completion of services. Currently, there will be no form of monetary transaction done over the Step4work app.

## How can I delete my account?
Should you wish to delete your account from Step4work, just go to [Contact Us](mailto:info@step4work.com) and drop us a message.













