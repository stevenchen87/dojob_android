package com.dojob.Additions;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import com.dojob.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class dialogEmail extends Activity {
    private Unbinder unbinder;

    @BindView(R.id.button_term_of_use)
    View buttonContactUs;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_dialog_email);
        unbinder = ButterKnife.bind(this);
        buttonContactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mailto = "mailto:info@step4work.com" +
                        "?cc=" + "" +
                        "&subject=" + Uri.encode(getString(R.string.app_name))+
                        "&body=" + Uri.encode("");

                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse(mailto));
                try {
                    startActivity(emailIntent);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(dialogEmail.this,getResources().getString(R.string.email),Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    }
