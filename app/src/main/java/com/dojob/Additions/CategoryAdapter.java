package com.dojob.Additions;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dojob.R;
import com.dojob.fragment.SearchFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryAdapter extends  RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {

    private Context mContext;

    private List<CategoryWIthID> catList;

    FragmentActivity fragmentActivity;

    public CategoryAdapter(Context mContext, List<CategoryWIthID> catList,FragmentActivity fragmentActivity) {
        this.mContext = mContext;
        this.catList = catList;
        this.fragmentActivity = fragmentActivity;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category,parent,false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
       CategoryWIthID map = catList.get(position);


       if (map.getTitle()!=null)
       holder.categoryItemTextView.setText(map.getTitle());
    }

    @Override
    public int getItemCount() {
        return catList.size();
    }


    public class MyViewHolder extends  RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R.id.category_item_text_view)
        TextView categoryItemTextView;

        @BindView(R.id.category_view)
        View categoryView;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            categoryView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            CategoryWIthID categoryWIthID = catList.get(position);
            SearchFragment searchFragment = new SearchFragment();
            Bundle bundle = new Bundle();
            bundle.putString(SearchFragment.SEARCH_FRAGMENT_TYPE,SearchFragment.SEARCH_FRAGMENT_CATEGORY_TYPE);
            bundle.putInt(SearchFragment.CATE_ID, categoryWIthID.getID());
            bundle.putString(SearchFragment.CATE_NAME,categoryWIthID.getTitle());
            searchFragment.setArguments(bundle);
            fragmentActivity.getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content,searchFragment)
                    .addToBackStack(null)
                    .commit();

        }
    }

}
