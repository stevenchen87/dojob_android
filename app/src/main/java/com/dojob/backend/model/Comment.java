package com.dojob.backend.model;

public class Comment{
    public int id;
    public int user_id;
    public int post_id;
    public String msg_body;
    public long updated_at;
    public String name;
    public String profile_image;

    public String parent_path;
    public String post_type;
    public String created_at;
    public boolean vip;

}
