package com.dojob.Additions.posts;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RatingPost {

    @SerializedName("target_id")
    @Expose
    private Integer targetId;
    @SerializedName("review")
    @Expose
    private String review;
    @SerializedName("rating")
    @Expose
    private Integer rating;

    public Integer getTargetId() {
        return targetId;
    }

    public void setTargetId(Integer targetId) {
        this.targetId = targetId;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

}