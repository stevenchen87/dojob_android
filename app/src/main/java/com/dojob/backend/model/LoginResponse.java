package com.dojob.backend.model;

public class LoginResponse {
    public String status;
    public String token;
    public String error;
//    public boolean tutorialFlag;
    public ProfileResponse data;
}
