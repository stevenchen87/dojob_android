package com.dojob.core;

import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.internal.Util;
import okio.Buffer;
import okio.BufferedSink;
import okio.Okio;
import okio.Source;

import com.dojob.R;
import com.dojob.backend.RegisterApi;
import com.dojob.core.Error;
import com.dojob.util.Utils;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

public class Http {
    private static final int SEGMENT_SIZE = 2048;
    private static OkHttpClient sClient;
    private static String sUserAgent;
    private static final String COOKIES = "set-cookie";
    private static TextView uploadTotalFile;
    private static AlertDialog progressDialog;
    private static int mTotalUploadSize;
    private static int uploadedFile;
    private static TextView uploadPercent;

    public static void init(Context context){
    }

    public static Request request(Context context) {
        if (sClient == null) {
            OkHttpClient.Builder builder = new OkHttpClient.Builder()
                    .connectTimeout(20, TimeUnit.SECONDS)
                    .readTimeout(20, TimeUnit.SECONDS)
                    .writeTimeout(20, TimeUnit.SECONDS);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;//"step4work.com".equalsIgnoreCase(hostname) || "www.dropbox.com".equalsIgnoreCase(hostname);
                }
            });
            sClient = builder.build();
        }

        return new Request(context);
    }

    public static void destroy() {
        sClient = null;
    }

    public static class Request {
        private okhttp3.Request.Builder mBuilder;
        private List<String> mLog = new ArrayList<>();
        private Context mContext;
        private String mUrl;

        Request(Context context) {
            mBuilder = new okhttp3.Request.Builder();
            mContext = context;
        }

        public Request testUrl(String url) {
            mUrl = url;
            mBuilder.url(url);

            mLog.add(0, "URL: " + url);
            return this;
        }

        public Request url(String url) {
            String token = RegisterApi.getToken(mContext);
            mUrl = url;
            mBuilder.url(url);
            String authorization = "Bearer ";
            if(token!=null && !token.isEmpty()){
                Log.d("Token", token);
                authorization = authorization + token;
            }
            mBuilder.addHeader("Authorization", authorization);

            mLog.add(0, "URL: " + url);
            return this;
        }

        public Request tag(String tag) {
            mBuilder.tag(tag);
            return this;
        }

        public Request post(Map<String, String> params) {
            if (params != null) {
                FormBody.Builder builder = new FormBody.Builder();

                for (Map.Entry<String, String> entry : params.entrySet()) {
                    String value = entry.getValue();
                    builder.add(entry.getKey(), value);
                }

                mLog.add("PARAM: " + params.toString());
                mBuilder.post(builder.build());
            }
            return this;
        }

        public Request postMultiPartToServer(final Context context, Map<String, String> formField, Map<String, Uri> filePart) {
            if (formField != null || filePart != null) {
                okhttp3.MultipartBody.Builder multipartBodyBuilder = new okhttp3.MultipartBody.Builder();
                multipartBodyBuilder.setType(okhttp3.MultipartBody.FORM);
                if (formField != null) {
                    for (Map.Entry<String, String> entry : formField.entrySet()) {
                        multipartBodyBuilder.addFormDataPart(entry.getKey(), entry.getValue());
                    }
                }
                if (filePart != null) {
                    Handler mainHandler = new Handler(Looper.getMainLooper());

                    mTotalUploadSize = filePart.size();
                    uploadedFile = 0;
                    if(mTotalUploadSize > 0){
                        mainHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                showProgressDialog(context);
                            }
                        });
                    }
                    for (Map.Entry<String, Uri> entry : filePart.entrySet()) {
                        Uri uri = entry.getValue();
                        File file= new File(uri.getPath());
                        file.getName();
                        multipartBodyBuilder.addFormDataPart(entry.getKey(), file.getName(), createCustomRequestBody(getMediaType(context, uri), file));
                    }
                }
                mBuilder.post(multipartBodyBuilder.build());
            }
            return this;
        }

        public RequestBody createCustomRequestBody(final MediaType contentType, final File file) {
            return new RequestBody() {
                long tempPercent;
                int percentage;

                @Override public MediaType contentType() {
                    return contentType;
                }
                @Override public long contentLength() {
                    return file.length();
                }
                @Override public void writeTo(BufferedSink sink) throws IOException {
                    Source source = null;
                    try {
                        source = Okio.source(file);
                        long total = 0;
                        percentage = 0;
                        tempPercent = 0;
                        long read;
                        while ((read = source.read(sink.buffer(), SEGMENT_SIZE)) != -1) {
                            total += read;
                            sink.flush();
                            percentage = (int)((total / (float) file.length()) * 100);

                            if(percentage > tempPercent){
                                if(uploadPercent != null){
                                    uploadPercent.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            uploadPercent.setText(String.format(Locale.US, "%d%%", percentage));
                                        }
                                    });
                                }
                                tempPercent = percentage;
                            }
                        }
                    } finally {

                        if(uploadTotalFile != null){
                            uploadTotalFile.post(new Runnable() {
                                @Override
                                public void run() {
                                    uploadedFile++;
                                    uploadTotalFile.setText(String.format(Locale.US, "Uploading (%d/%d)...", uploadedFile, mTotalUploadSize));
                                    if(uploadedFile >= mTotalUploadSize && progressDialog != null){
                                        progressDialog.dismiss();
                                    }
                                }
                            });
                        }
                        Util.closeQuietly(source);
                    }
                }
            };
        }

        public Result send() {
            Result result;
            if (!Utils.isOnline(mContext) || sClient == null) {
                result = new Result(Error.OFFLINE);
            } else {
                String output = null;
                try {
                    okhttp3.Request request = mBuilder.build();
                    mLog.add("Header: " + request.headers().toString());
                    Response response = sClient.newCall(request).execute();
                    mLog.add("Code: " + response.code() + " " + response.message());

                    if (response.isSuccessful()) {
                        // reading whole response as byte array (response.body().string()) may consume high memory and cause OutOfMemoryError,
                        // therefore, read the response as byte stream and convert to string using string builder
                        output = response.body().string();//sb.toString();
                    }
                    else{
                        mLog.add("Error Body: " + response.body().string());

                    }
                } catch (IOException e) {
                    mLog.add("Error: " + e.getMessage());
                }
                if (output != null) {
                    mLog.add("Response: " + output);
                    result = new Result(null, output);
                } else {
                    result = new Result(Error.NETWORK);
                }
            }
            for (String log : mLog) {
                Logger.d("HTTP[" + Integer.toHexString(hashCode()) + "] " + log);
            }
            return result;
        }

        public Call call() {
            return sClient.newCall(mBuilder.build());
        }

    }

    private static void showProgressDialog(Context context){
        if(context != null){
            final AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.AlertDialog));
            View view = LayoutInflater.from(context).inflate(R.layout.dialog_upload_progress, null);
            uploadPercent = view.findViewById(R.id.upload_item_percent);
            uploadTotalFile = view.findViewById(R.id.upload_total);
            uploadTotalFile.setText(String.format(Locale.US, "Uploading (%d/%d)...", uploadedFile, mTotalUploadSize));
            builder.setTitle("Upload file");
            builder.setView(view);
            builder.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    sClient.dispatcher().cancelAll();
                    dismissProgressDialog();
                }
            });
            progressDialog = builder.create();
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
    }

    private static void dismissProgressDialog(){
        if(progressDialog != null && progressDialog.isShowing()){
            progressDialog.dismiss();
        }
    }


    public static okhttp3.MediaType getMediaType(Context context, Uri uri) {
        String mimeType;
        String  schemeForUrl = uri.getScheme();
        if (schemeForUrl != null && schemeForUrl.equalsIgnoreCase(ContentResolver.SCHEME_CONTENT)) {
            ContentResolver cr = context.getContentResolver();
            mimeType = cr.getType(uri);
        } else {
            String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uri
                    .toString());
            mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    fileExtension.toLowerCase());
        }
        return okhttp3.MediaType.parse(mimeType);
    }
}
