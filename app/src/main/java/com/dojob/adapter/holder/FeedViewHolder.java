package com.dojob.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.dojob.adapter.model.FeedItem;
import com.dojob.adapter.model.FeedTitle;
import com.dojob.adapter.model.LoadingFooter;
import com.dojob.adapter.model.MainSearch;
import com.dojob.adapter.model.PortfolioOverflowScroll;
import com.dojob.adapter.model.SearchItem;

public class FeedViewHolder extends RecyclerView.ViewHolder{
    public SearchItem.ViewHolder searchItem;
    public PortfolioOverflowScroll.ViewHolder overFlowScroll;
    public LoadingFooter.ViewHolder footer;
    public MainSearch.ViewHolder search;
    public FeedTitle.ViewHolder title;
    public FeedItem.ViewHolder item;

    public FeedViewHolder(View itemView) {
        super(itemView);
        search = new MainSearch.ViewHolder(itemView);
        title = new FeedTitle.ViewHolder(itemView);
        item = new FeedItem.ViewHolder(itemView);
        footer = new LoadingFooter.ViewHolder(itemView);
        overFlowScroll = new PortfolioOverflowScroll.ViewHolder(itemView);
        searchItem = new SearchItem.ViewHolder(itemView);
    }
}
