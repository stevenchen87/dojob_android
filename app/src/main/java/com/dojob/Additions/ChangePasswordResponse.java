package com.dojob.Additions;

import com.google.gson.annotations.SerializedName;

public class ChangePasswordResponse {

    @SerializedName("status")
    private String Status;

    @SerializedName("msg")
    private String Message;

    public String getStatus() {
        return Status;
    }

    public String getMessage() {
        return Message;
    }
}
