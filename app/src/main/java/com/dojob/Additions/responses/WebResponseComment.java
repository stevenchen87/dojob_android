package com.dojob.Additions.responses;

import com.dojob.Additions.local.CommentDatum;
import com.dojob.Additions.local.FileData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WebResponseComment {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("fileData")
    @Expose
    private FileData fileData;
    @SerializedName("commentData")
    @Expose
    private List<CommentDatum> commentData = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public FileData getFileData() {
        return fileData;
    }

    public void setFileData(FileData fileData) {
        this.fileData = fileData;
    }

    public List<CommentDatum> getCommentData() {
        return commentData;
    }

    public void setCommentData(List<CommentDatum> commentData) {
        this.commentData = commentData;
    }

}