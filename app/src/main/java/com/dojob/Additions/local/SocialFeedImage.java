package com.dojob.Additions.local;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SocialFeedImage extends SocialFeedParent{

@SerializedName("id")
@Expose
private Integer id;
@SerializedName("linkID")
@Expose
private Integer linkID;
@SerializedName("linkType")
@Expose
private String linkType;
@SerializedName("url")
@Expose
private String url;
@SerializedName("fileType")
@Expose
private String fileType;
@SerializedName("caption")
@Expose
private String caption;
@SerializedName("album")
@Expose
private Integer album;
@SerializedName("data")
@Expose
private String data;
@SerializedName("updated_at")
@Expose
private Integer updatedAt;
@SerializedName("created_at")
@Expose
private Integer createdAt;
@SerializedName("thumbnail")
@Expose
private String thumbnail;
@SerializedName("like_count")
@Expose
private Integer likeCount;
@SerializedName("like_status")
@Expose
private Integer likeStatus;
@SerializedName("share_url")
@Expose
private String shareUrl;

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public Integer getLinkID() {
return linkID;
}

public void setLinkID(Integer linkID) {
this.linkID = linkID;
}

public String getLinkType() {
return linkType;
}

public void setLinkType(String linkType) {
this.linkType = linkType;
}

public String getUrl() {
return url;
}

public void setUrl(String url) {
this.url = url;
}

public String getFileType() {
return fileType;
}

public void setFileType(String fileType) {
this.fileType = fileType;
}

public String getCaption() {
return caption;
}

public void setCaption(String caption) {
this.caption = caption;
}

public Integer getAlbum() {
return album;
}

public void setAlbum(Integer album) {
this.album = album;
}

public String getData() {
return data;
}

public void setData(String data) {
this.data = data;
}

public Integer getUpdatedAt() {
return updatedAt;
}

public void setUpdatedAt(Integer updatedAt) {
this.updatedAt = updatedAt;
}

public Integer getCreatedAt() {
return createdAt;
}

public void setCreatedAt(Integer createdAt) {
this.createdAt = createdAt;
}

public String getThumbnail() {
return thumbnail;
}

public void setThumbnail(String thumbnail) {
this.thumbnail = thumbnail;
}

public Integer getLikeCount() {
return likeCount;
}

public void setLikeCount(Integer likeCount) {
this.likeCount = likeCount;
}

public Integer getLikeStatus() {
return likeStatus;
}

public void setLikeStatus(Integer likeStatus) {
this.likeStatus = likeStatus;
}

public String getShareUrl() {
return shareUrl;
}

public void setShareUrl(String shareUrl) {
this.shareUrl = shareUrl;
}

}