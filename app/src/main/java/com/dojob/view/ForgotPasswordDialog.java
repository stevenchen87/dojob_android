package com.dojob.view;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dojob.Additions.ApiInterface;
import com.dojob.Additions.PostForgotPass;
import com.dojob.Additions.ServiceGenerator;
import com.dojob.Additions.local.ForgotPassResult;
import com.dojob.R;
import com.dojob.activity.MainActivity;
import com.dojob.backend.LoginApi;
import com.dojob.backend.ProfileApi;
import com.dojob.backend.RegisterApi;
import com.dojob.backend.model.LoginResponse;
import com.dojob.core.Authentication;
import com.dojob.core.BackgroundWorker;
import com.dojob.core.Logger;
import com.dojob.core.Result;
import com.dojob.util.Utils;
import com.google.firebase.auth.FirebaseUser;

import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordDialog extends DialogFragment implements View.OnClickListener,
        Authentication.Listener,
        BackgroundWorker.Callbacks {
    private final String TAG = "LoginDialog";
    private Button confirm_button;
    private EditText login_email;
    private TextView text_email;
    private View mClose;
    private Listener mListener;
    private BackgroundWorker mWorker;
    private LoginApi mLoginApi;
    private String mRole;
    private String idToken;
    private AlertDialog progressDialog;

    public static ForgotPasswordDialog newInstance() {
        Bundle args = new Bundle();

        ForgotPasswordDialog fragment = new ForgotPasswordDialog();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            fragment.setStyle(AppCompatDialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Material_Light_Dialog_NoActionBar);
        } else {
            fragment.setStyle(AppCompatDialogFragment.STYLE_NO_TITLE, android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar);
        }
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null) {
            mWorker = new BackgroundWorker(getActivity());
            mLoginApi = (LoginApi) mWorker.get(LoginApi.LOGIN_TAG);
            if (mLoginApi == null) {
                mLoginApi = new LoginApi(getContext());
                mWorker.put(LoginApi.LOGIN_TAG, mLoginApi);
            }
            if (getArguments() != null)
                mRole = getArguments().getString("role", "talent");
            else
                mRole = "talent";

            if (getContext() != null) ProfileApi.setRole(getContext(), mRole);
        }
        Authentication.init(getActivity(), this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getActivity() != null) {
            mWorker = new BackgroundWorker(getActivity());
            mLoginApi = (LoginApi) mWorker.get(LoginApi.LOGIN_TAG);
            if (mLoginApi == null) {
                mLoginApi = new LoginApi(getContext());
                mWorker.put(LoginApi.LOGIN_TAG, mLoginApi);
            }
            if (getArguments() != null) mRole = getArguments().getString("role", "talent");
        }

        View view = inflater.inflate(R.layout.dialog_forgot_password, container);
        Window window = getDialog().getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }

        login_email = (EditText) view.findViewById(R.id.login_email);
        text_email = (TextView) view.findViewById(R.id.text_email);
        confirm_button = (Button) view.findViewById(R.id.confirm_button);
        mClose = view.findViewById(R.id.close);
        //login_email.setText("abc8@gmail.com");
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Authentication.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (confirm_button != null) {
            confirm_button.setOnClickListener(this);
        }
        if (mClose != null) {
            mClose.setOnClickListener(this);
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if (confirm_button != null) {
            confirm_button.setOnClickListener(null);
        }
        if (mClose != null) {
            mClose.setOnClickListener(null);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.confirm_button:
                if (login_email.getText().toString().equalsIgnoreCase("")) {
                    text_email.setText("The email field is required");
                    text_email.setVisibility(View.VISIBLE);
                } else if (!Utils.isValidEmailId(login_email.getText().toString())) {
                    text_email.setText("The email must be a valid email address");
                    text_email.setVisibility(View.VISIBLE);
                } else {
                    text_email.setVisibility(View.GONE);
                    showLoadingProgress();
                    callApi();
                }
                break;
            case R.id.close:
                dismiss();
                break;
            default:
                break;
        }
    }


    private void callApi() {
        PostForgotPass postForgotPass = new PostForgotPass();
        postForgotPass.setEmail(login_email.getText().toString());
        ServiceGenerator serviceGenerator = new ServiceGenerator();
        ApiInterface apiInterface = serviceGenerator.createService(ApiInterface.class);
        retrofit2.Call<ForgotPassResult> call = apiInterface.forgotPassword(postForgotPass.getEmail());
        call.enqueue(new Callback<ForgotPassResult>() {
            @Override
            public void onResponse(retrofit2.Call<ForgotPassResult> call, Response<ForgotPassResult> response) {
                dismissLoadingProgress();
                if (response.isSuccessful()) {
                    Log.i("RESPONSE", "OKAY");
                    if (response.body() != null) {
                        dismiss();
                        if (mListener != null) {
                            mListener.onOpenForgotPassMessageDialog(login_email.getText().toString());
                        }
                    }

                } else {
                    Log.i("RESPONSE", response.message());
                    Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<ForgotPassResult> call, Throwable t) {
                Log.i("RESPONSE", t.getMessage());
                dismissLoadingProgress();
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void setListener(Listener listener) {
        mListener = listener;
    }

    ///Authentication.Listener

    @Override
    public void onComplete(FirebaseUser user, String accessToken) {
        Log.d(TAG, "\nname:" + user.getDisplayName() + "\nemail:" + user.getEmail() + "\nphote:" + user.getPhotoUrl());
        Log.d(TAG, "accessToken:" + accessToken);
        this.idToken = accessToken;
        if (!accessToken.isEmpty()) {
            showLoadingProgress();
            if (mWorker != null) mWorker.executeNewTask(LoginApi.LOGIN_TAG, null, this);
        }
    }

    @Override
    public void onError(Exception e) {
        dismissLoadingProgress();
        e.printStackTrace();
    }



    @Override
    public Result executeTaskInBackground(String id, Bundle args) {
        switch (id) {
            case LoginApi.LOGIN_TAG:
                return mLoginApi.request(this.idToken, mRole,Utils.getDeviceToken(getActivity()));
        }
        return null;
    }

    @Override
    public void onBackgroundTaskCompleted(String id, Result result) {
        switch (id) {
            case LoginApi.LOGIN_TAG:
                if (result.isSuccess()) {
                    LoginResponse response = (LoginResponse) result.getResult();
                    Log.d("EmailLogin.status", "" + response.status + "," + response.status.equals("ok"));
                    Log.d("EmailLogin.token", "" + response.token);
                    Log.d("EmailLogin.mListener", "" + mListener);

                    if (response != null && response.status.equals("ok")) {
                        if (response.token != null && response.token.length() > 0 && response.data.verified == 1) {
                            RegisterApi.saveToken(this.getActivity(), response.token);
                        }
                        ((MainActivity) this.getActivity()).switchSignUpButton(true);
                        if (mListener != null)
                            ((MainActivity) this.getActivity()).onOpenProfileSetting();
                        dismiss();
                    }
                }
                dismissLoadingProgress();
                break;
        }
    }

    public interface Listener {
        //        void onOpenEmailLoginDialog(String role);
//        void onOpenEmailSignUpDialog(String role);
        void onOpenForgotPassMessageDialog(String email);
    }

    private void showLoadingProgress() {
        if (getActivity() != null) {
        AlertDialog.Builder progressDialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_loading, null);
        progressDialogBuilder.setView(dialogView);
        progressDialog = progressDialogBuilder.create();
        progressDialog.show();
        }
    }

    private void dismissLoadingProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }
}

