package com.dojob.adapter.model;

import android.content.Context;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatRatingBar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.dojob.Additions.SubCategory;
import com.dojob.GlideApp;
import com.dojob.R;
import com.dojob.activity.MainActivity;
import com.dojob.backend.LoginApi;
import com.dojob.backend.SetupApi;
import com.dojob.backend.model.UserResponse;
import com.dojob.core.App;
import com.dojob.core.Logger;
import com.dojob.fragment.ProfileSettingFragment;
import com.dojob.util.ResourcesUtil;
import com.dojob.util.Utils;
import com.squareup.picasso.Picasso;

import static com.dojob.activity.MainActivity.ENABLE_TOOLBAR_TITLE;

public class PortfolioSelf {
    public static final int LAYOUT_ID = R.layout.layout_portfolio_self;
    private final Context mContext;
    private UserResponse mResponse;
    private FragmentActivity fragmentActivity;

    public PortfolioSelf(Context context, UserResponse response, FragmentActivity fragmentActivity) {
        mContext = context;
        mResponse = response;
        this.fragmentActivity = fragmentActivity;
    }

    public void bindViewHolder(ViewHolder holder, int position) {
        if (holder.userImage != null) {
            try {
                GlideApp.with(mContext)
                        .load(mResponse.profile.profile_picture)
                        .placeholder(R.drawable.placeholder_1)
                        .apply(RequestOptions.fitCenterTransform())
                        .skipMemoryCache(false)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.userImage);
                /*App.picasso().load(mResponse.profile.profile_picture).fit().centerCrop().into(holder.userImage);*/
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }

        }
        if (holder.rateNumber != null) {
            holder.rateNumber.setText(String.valueOf(mResponse.profile.rating));
        }
        if (holder.ratingStar != null) {
            holder.ratingStar.setRating(mResponse.profile.rating);
        }
        if (holder.enquiry != null) {
            holder.enquiry.setTag(position);
        }
        if (holder.rate != null) {
            holder.rate.setTag(position);
        }
        if (holder.editProfile != null) {
            holder.editProfile.setTag(position);
        }
        if (holder.share != null) {
            holder.share.setTag(position);
        }
        if (holder.bookmark != null) {
            holder.bookmark.setTag(position);
        }
        if (holder.bullet != null) {
            holder.bullet.setTag(position);
        }
        if (holder.leftContainer != null) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.gravity = Gravity.START | Gravity.CENTER;
            holder.leftContainer.setLayoutParams(params);
        }
        if (LoginApi.isLoggedIn(mContext) && LoginApi.isUser(mContext, mResponse.profile.id)) {
            if (holder.editProfile != null) {
                holder.editProfile.setVisibility(View.VISIBLE);
                holder.bullet.setVisibility(View.VISIBLE);
            }
            if (holder.enquiry != null) {
                holder.enquiry.setVisibility(View.GONE);
            }

//            if(holder.rate != null){
//                if(mResponse.profile.vip){
//                    holder.rate.setBackground(ResourcesUtil.getDrawable(R.drawable.vip_x3));
//                    holder.rate.setEnabled(false);
//                }
//                else{
//                    holder.rate.setVisibility(View.VISIBLE);
//                }
//            }

            if (holder.bookmark != null) {
                holder.bookmark.setVisibility(View.GONE);
                holder.bookmark.setTag(position);
            }

            if (holder.bullet != null) {
                holder.bullet.setTag(position);
            }
        } else if (!LoginApi.isLoggedIn(mContext)) {
            if (holder.enquiry != null) {
                holder.enquiry.getBackground().setColorFilter(ResourcesUtil.getColor(R.color.grey), PorterDuff.Mode.SRC_ATOP);
//                Utils.greyScale( holder.enquiry);
                holder.enquiry.setEnabled(true);
            }

//            if(holder.rate != null){
//                holder.rate.getBackground().setColorFilter(ResourcesUtil.getColor(R.color.grey), PorterDuff.Mode.SRC_ATOP);
//                holder.rate.setEnabled(false);
//            }

            if (holder.bookmark != null) {
                holder.bookmark.setColorFilter(ResourcesUtil.getColor(R.color.grey), PorterDuff.Mode.SRC_ATOP);
                holder.bookmark.setEnabled(false);
            }

            if (holder.bullet != null) {
                holder.bullet.setColorFilter(ResourcesUtil.getColor(R.color.grey), PorterDuff.Mode.SRC_ATOP);
                holder.bullet.setEnabled(false);
            }
        } else {
            if (holder.enquiry != null)
                holder.enquiry.setVisibility(View.VISIBLE);
            //holder.enquiry.setVisibility(SetupApi.isUserRecruiter(mContext) ? View.VISIBLE : View.GONE);
        }

        if (holder.tv_country != null){
            if (mResponse.profile!= null && mResponse.profile.country != null)
            holder.tv_country.setText(mResponse.profile.country);
        }

//        if(holder.rate!= null) holder.rate.setVisibility(SetupApi.isUserRecruiter(mContext)?View.VISIBLE:View.GONE);
        if (holder.positionSeperator != null) {
            holder.positionSeperator.setVisibility(View.GONE);
        }
        if (holder.mainPosition != null) {
            holder.mainPosition.setVisibility(View.GONE);
            if (mResponse.profile != null)
                if (mResponse.profile.category_info != null)
                    if (mResponse.profile.category_info.cat != null)
                        if (mResponse.profile.category_info.cat.category_type != null) {
                            holder.mainPosition.setText(mResponse.profile.category_info.cat.category_type);
                            holder.mainPosition.setVisibility(View.VISIBLE);
                            holder.positionSeperator.setVisibility(View.VISIBLE);
                        }

        }

        if (holder.position != null) {
            holder.position.setVisibility(View.GONE);
            if (mResponse.profile != null)
                if (mResponse.profile.category_info != null)
                    if (mResponse.profile.category_info.sub_cat != null)
                        if (mResponse.profile.category_info.sub_cat.name != null) {
                            holder.position.setText(mResponse.profile.category_info.sub_cat.name);
                            holder.position.setVisibility(View.VISIBLE);
                        }
        }


        if (holder.position != null) {
            if (mResponse.profile.favourite_status == 0)
                holder.bookmark.setImageResource(R.drawable.drawable_bookmark);
            else if (mResponse.profile.favourite_status == 1)
                holder.bookmark.setImageResource(R.drawable.drawable_bookmark_selected);
        }
        holder.name.setText(mResponse.profile.name);
        holder.userImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mResponse.profile.profile_picture != null) {
                    String url = mResponse.profile.profile_picture;
                    if (mContext instanceof MainActivity)
                    ((MainActivity) mContext).openImageViewer(url);
                }
            }
        });
    }

    public static class ViewHolder {

        public ImageView userImage;
        public TextView name;
        public TextView rateNumber;
        public AppCompatRatingBar ratingStar;
        public AppCompatButton enquiry;
        public Button rate;
        public ImageView bookmark;
        public ImageView bullet;
        public ImageView share;
        public TextView mainPosition;
        public TextView position;
        public TextView tv_country;
        public View positionSeperator;
        public View leftContainer;
        public View rightContainer;
        public AppCompatImageView editProfile;
        private FragmentActivity fragmentActivity;
        private String PROFILE_SETTING_FRAGMENT_TAG = "profile_setting_fragment";
        private String MAIN_FRAGMENT_TAG = "main_fragment";


        public ViewHolder(View view, final FragmentActivity fragmentActivity) {
            this.fragmentActivity = fragmentActivity;
            leftContainer = view.findViewById(R.id.self_left_container);
            rightContainer = view.findViewById(R.id.self_right_container);
            userImage = view.findViewById(R.id.portfolio_self_image);
            name = view.findViewById(R.id.portfolio_self_name);
            rateNumber = view.findViewById(R.id.portfolio_self_rate_number);
            ratingStar = view.findViewById(R.id.portfolio_self_rating);
            enquiry = view.findViewById(R.id.portfolio_self_enquiry);
            rate = view.findViewById(R.id.portfolio_self_rate);
            bookmark = view.findViewById(R.id.portfolio_self_bookmark);
            bullet = view.findViewById(R.id.portfolio_self_list_bullet);
            share = view.findViewById(R.id.portfolio_self_share);
            mainPosition = view.findViewById(R.id.portfolio_main_position);
            positionSeperator = view.findViewById(R.id.portfolio_position_seperator);
            position = view.findViewById(R.id.portfolio_self_position);
            tv_country = view.findViewById(R.id.tv_country);
            editProfile = view.findViewById(R.id.portfolio_self_edit_profile);
            if (editProfile != null)
                editProfile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.i("MISSION", "SUCCESSFUL");
                        Bundle bundle = new Bundle();
                        bundle.putBoolean(ENABLE_TOOLBAR_TITLE, true);
                        ProfileSettingFragment fragment = ProfileSettingFragment.newInstance(bundle);
                        fragmentActivity.getSupportFragmentManager().beginTransaction()
                                .replace(R.id.content, fragment, PROFILE_SETTING_FRAGMENT_TAG)
                                .addToBackStack(MAIN_FRAGMENT_TAG)
                                .commit();
                    }
                });
        }
    }
}
