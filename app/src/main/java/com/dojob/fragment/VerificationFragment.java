package com.dojob.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.dojob.R;
import com.dojob.activity.MainActivity;
import com.dojob.backend.RegisterApi;
import com.dojob.backend.SetupApi;
import com.dojob.backend.VerificationApi;
import com.dojob.backend.model.Response;
import com.dojob.core.BackgroundWorker;
import com.dojob.core.Result;
import com.dojob.util.Utils;

public class VerificationFragment extends Fragment implements View.OnClickListener,
        BackgroundWorker.Callbacks{
    private final String OBJ_VERIFICATION_API = "obj_verification_api";
    private final String TASK_CONFIRM_CODE = "task_confirm_code";
    private final String TASK_RESEND_CODE = "task_resend_code";

    private EditText verifiedCode;
    private AppCompatButton verifiedConfirmButton;
    private AppCompatButton verifiedResendButton;
    private BackgroundWorker mWorker;
    private VerificationApi mVerificationApi;
    private AlertDialog progressDialog;
    private boolean mVerified;

    public static VerificationFragment newInstance() {
        Bundle args = new Bundle();
        VerificationFragment fragment = new VerificationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mWorker = new BackgroundWorker(getActivity());
        mVerificationApi = (VerificationApi) mWorker.get(OBJ_VERIFICATION_API);
        if (mVerificationApi == null) {
            mVerificationApi = new VerificationApi(getContext());
            mWorker.put(OBJ_VERIFICATION_API, mVerificationApi);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        AppCompatActivity activity = ((AppCompatActivity)getActivity());
        if(activity != null) {
            // will be used in fragment with back button
            ActionBar actionbar = activity.getSupportActionBar();
            if (actionbar != null) {
                actionbar.setDisplayHomeAsUpEnabled(true);
                actionbar.setDisplayShowHomeEnabled(true);
            }
            if(activity instanceof MainActivity){
                ((MainActivity) activity).setTitle("Verification");
            }
        }
        Utils.hideKeyBoard(getActivity());
        View view = inflater.inflate(R.layout.fragment_verification, container, false);
        verifiedCode = view.findViewById(R.id.verified_code);
        verifiedConfirmButton = view.findViewById(R.id.verification_confirm);
        verifiedResendButton = view.findViewById(R.id.verification_resend);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        verifiedConfirmButton.setOnClickListener(this);
        verifiedResendButton.setOnClickListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        verifiedConfirmButton.setOnClickListener(null);
        verifiedResendButton.setOnClickListener(null);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(!mVerified){
            RegisterApi.deleteToken(getContext());
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.verification_confirm:
                showLoadingProgress(0);
                if(mWorker != null) mWorker.executeNewTask(TASK_CONFIRM_CODE, null, this);
                break;
            case R.id.verification_resend:
                showLoadingProgress(1);
                if(mWorker != null) mWorker.executeNewTask(TASK_RESEND_CODE, null, this);
                break;
        }
    }

    @Override
    public Result executeTaskInBackground(String id, Bundle args) {
        switch (id){
            case TASK_CONFIRM_CODE:
                return mVerificationApi.confirm(verifiedCode.getText().toString());
            case TASK_RESEND_CODE:
                return mVerificationApi.resend();
        }
        return null;
    }

    @Override
    public void onBackgroundTaskCompleted(String id, Result result) {
        switch (id){
            case TASK_CONFIRM_CODE:
                if(result.isSuccess()){
                    Response response = (Response) result.getResult();

                    if(response.status.equals("ok") && getActivity() != null){
                        ((MainActivity)getActivity()).onOpenProfileSetting();
                        Utils.isRefreshRequired = 1;
                        mVerified = true;
                    }
                    else{
                        Snackbar.make(getActivity().findViewById(android.R.id.content), response.msg,
                                Snackbar.LENGTH_LONG).show();
                        verifiedCode.setText("");
                    }

                    ((MainActivity)getActivity()).onOpenProfileSetting();
                    mVerified = true;
                }
                break;
            case TASK_RESEND_CODE:
                if(result.isSuccess()){
                    Response response = (Response) result.getResult();
                    if(response.status.equals("ok")){
                        Snackbar.make(getActivity().findViewById(android.R.id.content), "Code is sent to your email",
                                Snackbar.LENGTH_LONG).show();
                    }
                }
                break;
        }
        dismissLoadingProgress();
    }

    private void showLoadingProgress(int type){
        AlertDialog.Builder progressDialogBuilder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_loading, null);
        TextView msg = dialogView.findViewById(R.id.loading_msg);
        if(type == 0) msg.setText("Verifying...");
        else msg.setText("Sending...");
        progressDialogBuilder.setView(dialogView);
        progressDialog = progressDialogBuilder.create();
        progressDialog.show();
    }

    private void dismissLoadingProgress(){
        if(progressDialog != null){
            progressDialog.dismiss();
        }
    }
}
