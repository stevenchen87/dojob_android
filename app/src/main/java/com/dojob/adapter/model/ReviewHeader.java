package com.dojob.adapter.model;

import android.support.v7.widget.AppCompatRatingBar;
import android.view.View;
import android.widget.TextView;

import com.dojob.R;
import com.dojob.backend.model.UserResponse;

public class ReviewHeader {
    public static final int LAYOUT_ID = R.layout.layout_review_header;

    private UserResponse mResponse;

    public ReviewHeader(UserResponse response) {
        mResponse = response;
    }

    public void bindViewHolder(ViewHolder holder, int position){
        if(holder.title != null){
            holder.title.setText(mResponse.profile.name);
        }

        if(holder.rateNumber != null){
            holder.rateNumber.setText(String.valueOf(mResponse.profile.rating));
        }

        if(holder.rating != null){
            holder.rating.setRating(mResponse.profile.rating);
        }

        if(holder.totalRating != null){
            if(mResponse.profile.review.length > 0) {
                holder.totalRating.setText(String.format("%s Ratings", mResponse.profile.review.length));
                holder.totalRating.setVisibility(View.VISIBLE);
            }
            else holder.totalRating.setVisibility(View.GONE);
        }
    }

    public static class ViewHolder{

        public TextView title;
        public TextView rateNumber;
        public AppCompatRatingBar rating;
        public TextView totalRating;
        public ViewHolder(View view){
            title = view.findViewById(R.id.portfolio_self_name);
            rateNumber = view.findViewById(R.id.portfolio_self_rate_number);
            rating = view.findViewById(R.id.portfolio_self_rating);
            totalRating = view.findViewById(R.id.portfolio_self_total_rating);
        }
    }
}
