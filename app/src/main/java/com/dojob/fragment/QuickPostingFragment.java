package com.dojob.fragment;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dojob.Additions.local.SocialFeed;
import com.dojob.Additions.local.SocialFeedParent;
import com.dojob.R;
import com.dojob.activity.MainActivity;
import com.dojob.adapter.GalleryTabAdapter;
import com.dojob.backend.GalleryApi;
import com.dojob.backend.SetupApi;
import com.dojob.backend.model.ProfileResponse;
import com.dojob.backend.model.UserResponse;
import com.dojob.core.BackgroundWorker;
import com.dojob.core.Result;
import com.dojob.util.Utils;
import com.github.ybq.android.spinkit.SpinKitView;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class QuickPostingFragment extends Fragment implements View.OnClickListener, GalleryTabAdapter.Listener, BackgroundWorker.Callbacks {


    public final static String ARGS_FILE_TYPE = "args_file_type";
    public final static String ARGS_FILE_URL = "args_file_url";
    public final static String ARGS_FILE_URI = "args_file_uri";
    public final static String ARGS_PORTFOLIO_DATA = "args_portfolio_data";
    public final static String NO_TOOLBAR = "args_no_back_toolbar";

    public final static String TYPE_GALLERY = "task_gallery";
    public final static String TASK_GET_PHOTO_GALLERY = "gallery_photo_data";
    public final static String TASK_GET_VIDEO_GALLERY = "gallery_video_data";
    private static final String OBJ_GALLERY_API = "obj_gallery_api";
    private final String TASK_UPLOAD_PHOTO = "task_upload_photo";
    private final String TASK_UPLOAD_VIDEO = "task_upload_video";

    String file_url = "";
    int file_type = 0;
    Uri file_URI = null;
    boolean mNoToolbar;
    private SpinKitView mProgress;

    private BackgroundWorker mWorker;
    private GalleryApi mGalleryApi;
    List<Uri> uriList = null;


    private ImageView thubImage;
    private Button btnCancel, btnUpload;

    private ArrayList<SocialFeed> socialFeedArray;
    private ArrayList<SocialFeedParent> socialFeedParentArray;

    public static QuickPostingFragment newInstance(Bundle bundle) {
        QuickPostingFragment fragment = new QuickPostingFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        uriList = new ArrayList<>();
        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        if (getArguments() != null) {
            if (getArguments().containsKey(NO_TOOLBAR)) {
                mNoToolbar = getArguments().getBoolean(NO_TOOLBAR);
            }
            if (getArguments().containsKey(ARGS_FILE_TYPE)) {
                file_type = getArguments().getInt(ARGS_FILE_TYPE);
            }
            if (getArguments().containsKey(ARGS_FILE_URL)) {
                file_url = getArguments().getString(ARGS_FILE_URL);
                String uri = getArguments().getString(ARGS_FILE_URI);
                file_URI = Uri.parse(uri);
                uriList.add(file_URI);
            }
        }

        if (activity != null) {
            if (activity instanceof MainActivity && !mNoToolbar) {
                ActionBar actionbar = activity.getSupportActionBar();
                if (actionbar != null) {
                    actionbar.setDisplayHomeAsUpEnabled(true);
                    actionbar.setDisplayShowHomeEnabled(true);
                }
//                ((MainActivity) activity).isSettingPage(false);
//                if (mType == PortfolioAdapter.PREVIEW_TYPE) {
//                    // will be used in fragment with back button
//                    ((MainActivity) activity).setTitle("Profile Preview");
//                } else {
//                    ((MainActivity) activity).setTitle("Profile");
//                }
            }

            if (getActivity() != null) {
                mWorker = new BackgroundWorker(getActivity());
                mGalleryApi = (GalleryApi) mWorker.get(OBJ_GALLERY_API);
                if (mGalleryApi == null) {
                    mGalleryApi = new GalleryApi(getContext());
                    mWorker.put(OBJ_GALLERY_API, mGalleryApi);
                }
            }
//            mWorker = new BackgroundWorker(getActivity());
//            mGalleryApi = (GalleryApi) mWorker.get(OBJ_GALLERY_API);
//            if (mGalleryApi == null) {
//                mGalleryApi = new GalleryApi(getContext());
//                mWorker.put(OBJ_GALLERY_API, mGalleryApi);
//            }
//            mProfileApi = (ProfileApi) mWorker.get(OBJ_PROFILE_API);
//            if (mProfileApi == null) {
//                Log.i("PROFILE_API", "NULL");
//                mProfileApi = new ProfileApi(getContext());
//                mWorker.put(OBJ_PROFILE_API, mProfileApi);
//            } else {
//                Log.i("PROFILE_API", "NOT_NULL");
//            }
//            mFavouriteApi = (FavouriteApi) mWorker.get(OBJ_FAVOURITE_API);
//            if (mFavouriteApi == null) {
//                mFavouriteApi = new FavouriteApi(getContext());
//                mWorker.put(OBJ_FAVOURITE_API, mFavouriteApi);
//            }
        }

    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_quick_posting, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Utils.hideKeyBoard(getActivity());
        mProgress = view.findViewById(R.id.progress);
        mProgress.setVisibility(View.GONE);
        thubImage = view.findViewById(R.id.thubImage);
        btnCancel = view.findViewById(R.id.btnCancel);
        btnUpload = view.findViewById(R.id.btnUpload);

        btnUpload.setOnClickListener(this);
        btnCancel.setOnClickListener(this);

        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        if (activity != null) {
            if (activity instanceof MainActivity) {
                ((MainActivity) activity).getSupportActionBar().hide();
            }
        }

        if (file_url != null &&
                !file_url.equalsIgnoreCase("")) {

//            String url = getRealPathFromUri(getActivity(),Uri.parse(file_url));

            if (file_type == 0) {
                File f = new File(file_url);
                Uri selectedImageUri = Uri.fromFile(f);
                thubImage.setImageURI(selectedImageUri);
            } else {
                videoThubnil();
            }
//            try {
//                final InputStream imageStream = getActivity().getContentResolver().openInputStream(Uri.parse(file_url));
//                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
//                thubImage.setImageBitmap(selectedImage);
//            } catch (FileNotFoundException e) {
//                e.printStackTrace();
//                Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG).show();
//            }
        }
    }

    private void videoThubnil() {
//        String[] filePathColumn = {MediaStore.Images.Media.DATA};
//        Cursor cursor = getActivity().getContentResolver().query(file_URI, filePathColumn, null, null, null);
//        cursor.moveToFirst();
//        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
//        String picturePath = cursor.getString(columnIndex);
//        cursor.close();

        Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(file_URI.getPath(), MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);
        if (bitmap != null) {
            thubImage.setImageBitmap(bitmap);
        }else{

            Glide
                    .with(getActivity())
                    .load(file_URI)
                    .into( thubImage );
//            Bitmap bitmap1 = ThumbnailUtils.createVideoThumbnail(getRealPathFromURI(getActivity(),file_URI), MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);
//            if (bitmap1 != null) {
//                thubImage.setImageBitmap(bitmap1);
//            }
//            InputStream inputStream = null;
//            try {
//                inputStream = getActivity().getContentResolver().openInputStream(file_URI);
//                Bitmap bmp = BitmapFactory.decodeStream(inputStream);
//                if( inputStream != null ) inputStream.close();
//                if (bmp != null){
//                    thubImage.setImageBitmap(bitmap);
//                }
//            } catch (FileNotFoundException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }

        }
    }

    public static String getRealPathFromUri(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Video.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            if (cursor != null){
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);}
            else return "";
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnCancel:
                AppCompatActivity activity = ((AppCompatActivity) getActivity());
                if (activity != null) {
                    if (activity instanceof MainActivity) {
                        ((MainActivity) activity).getSupportActionBar().show();
                    }
                }
                getFragmentManager().popBackStack();
                break;
            case R.id.btnUpload:
                //mProgress.setVisibility(View.VISIBLE);
                if (file_type == 0) {
                    if (mWorker != null) mWorker.executeNewTask(TASK_UPLOAD_PHOTO, null, this);
                } else {
                    if (mWorker != null) mWorker.executeNewTask(TASK_UPLOAD_VIDEO, null, this);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        if (activity != null) {
            if (activity instanceof MainActivity) {
                ((MainActivity) activity).getSupportActionBar().show();
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        if (activity != null) {
            if (activity instanceof MainActivity) {
                ((MainActivity) activity).getSupportActionBar().show();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        if (activity != null) {
            if (activity instanceof MainActivity) {
                ((MainActivity) activity).getSupportActionBar().show();
            }
        }
    }

    @Override
    public void onOpenImageViewer(String url) {

    }

    @Override
    public void onOpenMediaPicker() {

    }

    @Override
    public void onOpenVideoPlayer(String url) {

    }

    @Override
    public void onOpenCommentFragment(String type, int position, int fileId) {

    }

    @Override
    public Result executeTaskInBackground(String id, Bundle args) {
        final HashMap<String, String> deletedFileId = new HashMap<>();
        switch (id) {
            case TASK_UPLOAD_PHOTO:
                return mGalleryApi.post("photo", uriList);
            case TASK_UPLOAD_VIDEO:
                return mGalleryApi.post("video", uriList);
//            case TASK_GET_PHOTO_GALLERY:
//            case TASK_GET_VIDEO_GALLERY:
//                return mGalleryApi.getGallery(mType, String.valueOf(mUserId));
        }
        return null;
    }

    @Override
    public void onBackgroundTaskCompleted(String id, Result result) {
        switch (id) {
            case TASK_UPLOAD_PHOTO:
                if (mProgress != null)
                    mProgress.setVisibility(View.GONE);
                if (result.isSuccess()) {
                    if (mProgress != null) mProgress.setVisibility(View.GONE);
                    UserResponse response = (UserResponse) result.getResult();
                    updateProfile();
//                    ProfileResponse response1 = SetupApi.getUserData(getActivity());
//                    if (response1 != null) {
//                        String newUrl = "";
//                        if (response1.position.equalsIgnoreCase("recruiter")) {
//                            newUrl = "http://step4work.com/api/recruiter/profile/get?uid=" + response1.id;
//                        } else {
//                            newUrl = "http://step4work.com/api/profile/get?uid=" + response1.id;
//                        }
//
//                        AppCompatActivity activity = ((AppCompatActivity) getActivity());
//                        if (activity != null) {
//                            if (activity instanceof MainActivity) {
//                                getFragmentManager().popBackStack();
//                                ((MainActivity) activity).onOpenPreviewProfilePage(newUrl);
//                            }
//                        }
//                    }

//                    ArrayList<String> list = new ArrayList<>();
//                    for(MediaResponse photoResponse: response.talentPhoto ){
//                        list.add(photoResponse.url);
//                    }
//                    mRecyclerView.setVisibility(View.VISIBLE);
//                    mProgress.setVisibility(View.GONE);
//                    mAdapter.updateItem(GalleryApi.TYPE_GALLERY_PHOTO, response.talentPhoto);
                    return;
                }

                Toast.makeText(getActivity(), "Error uploading file", Toast.LENGTH_LONG).show();
                break;
            case TASK_UPLOAD_VIDEO:
                if (mProgress != null) mProgress.setVisibility(View.GONE);
                if (result.isSuccess()) {
                    if (mProgress != null) mProgress.setVisibility(View.GONE);
                    UserResponse response = (UserResponse) result.getResult();
                    updateProfile();
//                    mRecyclerView.setVisibility(View.VISIBLE);
//                    mProgress.setVisibility(View.GONE);
//                    mAdapter.updateItem(GalleryApi.TYPE_GALLERY_VIDEO, response.talentVideo);
                    return;
                }

                Toast.makeText(getActivity(), "Error uploading file", Toast.LENGTH_LONG).show();
                break;
//            case TASK_GET_PHOTO_GALLERY:
//            case TASK_GET_VIDEO_GALLERY:
//                if(result.isSuccess()){
//                    UserResponse response = (UserResponse) result.getResult();
//                    mRecyclerView.setVisibility(View.VISIBLE);
//                    mProgress.setVisibility(View.GONE);
//                    if(mType == GalleryApi.TYPE_GALLERY_PHOTO){
//                        mAdapter.updateItem(GalleryApi.TYPE_GALLERY_PHOTO, response.talentPhoto);
//                    }
//                    else{
//                        mAdapter.updateItem(GalleryApi.TYPE_GALLERY_VIDEO, response.talentVideo);
//                    }
//                    return;
//                }
//                break;
        }
    }

    private void updateProfile() {
        ProfileResponse response1 = SetupApi.getUserData(getActivity());
        if (response1 != null) {
            String newUrl = "";
            if (response1.position.equalsIgnoreCase("recruiter")) {
                newUrl = "http://step4work.com/api/recruiter/profile/get?uid=" + response1.id;
            } else {
                newUrl = "http://step4work.com/api/profile/get?uid=" + response1.id;
            }

            AppCompatActivity activity = ((AppCompatActivity) getActivity());
            if (activity != null) {
                if (activity instanceof MainActivity) {
                    ((MainActivity) activity).getSupportActionBar().show();
                    getFragmentManager().popBackStack();
                    ((MainActivity) activity).onOpenPreviewProfilePage(newUrl);
                }
            }
        }
    }
}
