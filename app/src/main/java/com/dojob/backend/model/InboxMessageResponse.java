package com.dojob.backend.model;

public class InboxMessageResponse {
//    public boolean owner;
//    public String content;
//    public String date;
    public String status;
    public int id;
    public int user_id;
    public int sender_id;
    public int chatroom_id;
    public String msg_body;
    public long created_at;
    public ChatRoomResponse chatroomData;

}
