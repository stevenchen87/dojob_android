package com.dojob.adapter.model;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.dojob.GlideApp;
import com.dojob.R;
import com.dojob.backend.model.FileResponse;
import com.dojob.core.App;
import com.dojob.core.Logger;
import com.squareup.picasso.Picasso;

import java.io.File;

public class PhotosItem {
    public static final int LAYOUT_ID = R.layout.layout_photos_item;
    private FileResponse mFile;


    private long id;
    private String mUrl;

    private Context mContext;

    public PhotosItem(FileResponse file, long id, Context context) {
        mFile = file;
        this.id = id;
        this.mContext = context;
    }

    public PhotosItem(String url, long id, Context context) {
        mUrl = url;
        this.id = id;
        this.mContext = context;
    }

    public void bindViewHolder(ViewHolder holder, int position) {
        if (holder.image != null) {
            if (mUrl != null) {
                Log.e("PATH", 1 + "");
                GlideApp.with(mContext)
                        .load(mUrl)
                        .placeholder(R.drawable.placeholder_1)
                        .apply(RequestOptions.fitCenterTransform())
                        .skipMemoryCache(false)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .dontAnimate()
                        .into(holder.image);
                /*Picasso.get().load(mUrl).fit().centerCrop().into(holder.image);*/
            } else {
                Log.e("PATH", 2 + "");
                if (mFile != null && mFile.url != null) {
                    if (mFile.fileType.equals("images")) {
                        Log.e("PATH", 3 + "");
                        GlideApp.with(mContext)
                                .load(mFile.url)
                                .placeholder(R.drawable.placeholder_1)
                                .apply(RequestOptions.fitCenterTransform())
                                .skipMemoryCache(false)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .dontAnimate()
                                .into(holder.image);
//                        Picasso.get().load(mFile.url).into(holder.image);
                        /*Picasso.get().load(mFile.url).fit().centerCrop().into(holder.image);*/
                    } else {
                        Log.e("PATH", 4 + "");
                        if (mFile.url != null && !mFile.url.equalsIgnoreCase("")) {
                            GlideApp.with(mContext)
                                    .load(mFile.url)
                                    .placeholder(R.drawable.placeholder_1)
                                    .apply(RequestOptions.fitCenterTransform())
                                    .skipMemoryCache(false)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .dontAnimate()
                                    .override(250,250)
                                    .into(holder.image);
//                            Picasso.get().load(mFile.url).into(holder.image);
                        } else if (mFile.thumbnail != null && !mFile.thumbnail.equalsIgnoreCase("")) {
                            GlideApp.with(mContext)
                                    .load(mFile.thumbnail)
                                    .placeholder(R.drawable.placeholder_1)
                                    .apply(RequestOptions.fitCenterTransform())
                                    .skipMemoryCache(false)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .dontAnimate()
                                    .override(250,250)
                                    .into(holder.image);
//                        Picasso.get().load(mFile.thumbnail).into(holder.image);
//                        }
                            /*Picasso.get().load(mFile.thumbnail).fit().centerCrop().into(holder.image);*/
                        }
                    }

                }
            }
            if (holder.container != null) {
                Log.e("PATH", 5 + "");
                holder.container.setTag(position);
            }
        }
    }

    public FileResponse getFile() {
        return mFile;
    }

    public long getId() {
        return this.id;
    }

    public String getUrl() {
        if (mFile != null) {
            return mFile.url;
        }

        return mUrl;
    }

    public FileResponse getFileData() {
        return mFile;
    }

    public static class ViewHolder {

        public CardView container;
        public ImageView image;

        public ViewHolder(View view) {
            container = view.findViewById(R.id.photo_container);
            image = view.findViewById(R.id.photos_image);
        }
    }
}
