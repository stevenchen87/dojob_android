package com.dojob.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dojob.R;
import com.dojob.activity.MainActivity;
import com.dojob.adapter.FavouriteAdapter;
import com.dojob.backend.FavouriteApi;
import com.dojob.backend.model.FavouriteItemResponse;
import com.dojob.backend.model.FavouriteListResponse;
import com.dojob.backend.model.Response;
import com.dojob.core.BackgroundWorker;
import com.dojob.core.Result;
import com.dojob.util.Utils;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Admin on 29-Nov-18.
 */
public class FavouriteFragment extends Fragment implements BackgroundWorker.Callbacks {

    private final static String TAG = "FavouriteFragment";
    private BackgroundWorker mWorker;
    private SpinKitView mProgress;
    private FavouriteAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private TextView tvEdit;
    private Listner mListener;
    private ArrayList<FavouriteItemResponse> favourite_data;

    // for favourite api
    private FavouriteApi mFavouriteApi;
    private final String OBJ_FAVOURITE_API = "obj_favourite_api";
    private final String TASK_REQUEST_FAVOURITE = "task_request_favourite";
    private final String TASK_REQUEST_USER_REMOVE_FAVOURITE = "task_request_user_remove_favourite";

    // for pagging api
    private int page = 1, last_page;
    GridLayoutManager mLayoutManager;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private boolean loading = true;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null) {
            favourite_data = new ArrayList<>();
            mWorker = new BackgroundWorker(getActivity());
            mFavouriteApi = (FavouriteApi) mWorker.get(OBJ_FAVOURITE_API);
            if (mFavouriteApi == null) {
                mFavouriteApi = new FavouriteApi(getContext());
                mWorker.put(OBJ_FAVOURITE_API, mFavouriteApi);
            }
        }
    }
    private void settingTitle() {
        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        if (activity != null) {
            if (activity instanceof MainActivity) {
                ActionBar actionbar = activity.getSupportActionBar();
                if (actionbar != null) {
                    actionbar.setDisplayHomeAsUpEnabled(true);
                    actionbar.setDisplayShowHomeEnabled(true);
                }
                ((MainActivity) activity).setTitle("Favourite");
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favourite, container, false);
        Utils.hideKeyBoard(getActivity());
        mProgress = view.findViewById(R.id.progress);

        mRecyclerView = view.findViewById(R.id.recycle_view);
        mRecyclerView.setItemViewCacheSize(20);
        mRecyclerView.setDrawingCacheEnabled(true);
        mRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        mAdapter = new FavouriteAdapter(getActivity(), favourite_data);
        mAdapter.setHasStableIds(true);
        mRecyclerView.setAdapter(mAdapter);
//                    gridLayoutManager.setSpanSizeLookup(new FeedSpanSizeLookup(mAdapter));
        mLayoutManager = new GridLayoutManager(getContext(), 2);
        mRecyclerView.setLayoutManager(mLayoutManager);
        if (favourite_data.size() == 0)
            mRecyclerView.setVisibility(View.GONE);
        else {
            mProgress.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        }

        tvEdit = view.findViewById(R.id.tvEdit);
        tvEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tvEdit.getText().toString().equals(getString(R.string.edit))) {
                    tvEdit.setText(getString(R.string.remove_talent));
                    mAdapter.showCheckBox(true);
                } else if (tvEdit.getText().toString().equals(getString(R.string.remove_talent))) {
                    tvEdit.setText(getString(R.string.edit));
                    mAdapter.showCheckBox(false);
                    if (!TextUtils.isEmpty(getJsonArray()))
                        showDeleteDialog();
                }
            }
        });

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            if (page < last_page) {
                                page++;
                                callFavouriteListApi();
                            }
                        }
                    }
                }
            }
        });

        return view;
    }

    private void showDeleteDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setTitle(getString(R.string.app_name));
        alertDialogBuilder
                .setMessage("Do you want to delete?")
                .setCancelable(false)
                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        callRemoveFavouriteApi();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void setListener(Listner listener) {
        mListener = listener;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (favourite_data.size() == 0) {
            favourite_data.clear();
            callFavouriteListApi();
        } else {
            mAdapter.notifyDataSetChanged();
        }
        settingTitle();
    }

    @Override
    public Result executeTaskInBackground(String id, Bundle args) {
        switch (id) {
            case TASK_REQUEST_FAVOURITE:
                return mFavouriteApi.getFavouriteList(page);
            case TASK_REQUEST_USER_REMOVE_FAVOURITE:
                if (!TextUtils.isEmpty(getJsonArray()))
                    return mFavouriteApi.requestRemoveFavourite(getJsonArray());
        }
        return null;
    }

    private String getJsonArray() {

        HashMap<String, String> hashMap = new HashMap<>();
        for (int i = 1; i <= favourite_data.size(); i++) {
            if (favourite_data.get(i - 1).isSelected)
                hashMap.put(String.valueOf(i), String.valueOf(favourite_data.get(i - 1).favourite_id));
        }

        Gson gson = new Gson();
        Type type = new TypeToken<HashMap<String, String>>() {
        }.getType();
        if (hashMap.size() > 0)
            return gson.toJson(hashMap, type);
        else
            return "";
    }

    private ArrayList<FavouriteItemResponse> getRemoveItem() {
        ArrayList<FavouriteItemResponse> removeArray = new ArrayList<>();
        for (int i = 1; i <= favourite_data.size(); i++) {
            if (favourite_data.get(i - 1).isSelected)
                removeArray.add(favourite_data.get(i - 1));
        }
        return removeArray;
    }

    private void showUpdateData() {
        ArrayList<FavouriteItemResponse> removeItems = getRemoveItem();
        favourite_data.removeAll(removeItems);
        mAdapter.updateNewData(favourite_data);
    }

    @Override
    public void onBackgroundTaskCompleted(String id, Result result) {
        switch (id) {
            case TASK_REQUEST_FAVOURITE:
                if (result.isSuccess()) {
                    loading = true;
                    FavouriteListResponse resposne = (FavouriteListResponse) result.getResult();
                    favourite_data.addAll(resposne.favourite_data);
                    last_page = resposne.last_page;
                    mAdapter.notifyDataSetChanged();
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mProgress.setVisibility(View.GONE);
                } else {
                    loading = false;
                    mProgress.setVisibility(View.GONE);
                }
                break;
            case TASK_REQUEST_USER_REMOVE_FAVOURITE:
                if (result.isSuccess()) {
                    Response response = (Response) result.getResult();
                    if (getActivity() != null) {
                        if (response.status.equals("ok")) {
//                            favourite_data.clear();
//                            callFavouriteListApi();
                            showUpdateData();
                        } else {
                            if (response.msg != null)
                                Utils.showSnackbar(getActivity(), response.msg);
                        }
                    }
                }
                break;
        }
    }

    public void callRemoveFavouriteApi() {
        if (mWorker != null) mWorker.executeNewTask(TASK_REQUEST_USER_REMOVE_FAVOURITE, null, this);
    }

    public void callFavouriteListApi() {
        loading = false;

        mProgress.setVisibility(View.VISIBLE);
        if (mWorker != null) mWorker.executeNewTask(TASK_REQUEST_FAVOURITE, null, this);
    }

    public interface Listner {
        void onOpenFavouriteFragment();
    }
}
