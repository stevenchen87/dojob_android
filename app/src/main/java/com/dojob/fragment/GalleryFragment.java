package com.dojob.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dojob.R;
import com.dojob.activity.MainActivity;
import com.dojob.adapter.GalleryAdapter;
import com.dojob.backend.model.UserResponse;
import com.dojob.core.App;
import com.dojob.util.Utils;

public class GalleryFragment extends Fragment {
    public final static int TYPE_PHOTOS = 0;
    public final static int TYPE_ALBUMS = 1;
    public final static int TYPE_VIDEOS = 2;
    public final static String GALLERY_PAGE = "gallery_page";
    public final static String GALLERY_USER_RESPONSE = "gallery_user_response";
    private GalleryAdapter mAdapter;
    private int mCurrentPage;
    private UserResponse mUserResponse;

    public static GalleryFragment newInstance(Bundle args) {
        GalleryFragment fragment = new GalleryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getActivity() != null){
            Bundle args = getArguments();
            if(args != null){
                mCurrentPage = args.getInt(GALLERY_PAGE, 0);
                mUserResponse = App.gson().fromJson(getArguments().getString(GALLERY_USER_RESPONSE), UserResponse.class);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gallery, container, false);
        Utils.hideKeyBoard(getActivity());
        AppCompatActivity activity = ((AppCompatActivity)getActivity());
        if(activity != null){
            ActionBar actionbar = activity.getSupportActionBar();
            if (actionbar != null) {
                actionbar.setDisplayHomeAsUpEnabled(true);
                actionbar.setDisplayShowHomeEnabled(true);
            }
            if(activity instanceof MainActivity){
                ((MainActivity) activity).setTitle("Gallery");
            }
            ViewPager vp = view.findViewById(R.id.view_pager);
            vp.setOffscreenPageLimit(3);
            mAdapter = new GalleryAdapter(getChildFragmentManager(), mUserResponse);
            vp.setAdapter(mAdapter);
            vp.setCurrentItem(mCurrentPage);
            TabLayout tabLayout = view.findViewById(R.id.tabs);
            tabLayout.setupWithViewPager(vp);
        }
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(mUserResponse != null){
//            mAdapter.updatePage(mUserResponse);
        }
    }
}
