package com.dojob.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.dojob.R;
import com.dojob.fragment.ImageViewerFragment;

public class ImageViewerActivty extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_viewer);
        Bundle bundle = new Bundle();
        bundle.putAll(getIntent().getExtras());
        FragmentManager mManager = getSupportFragmentManager();
        mManager.beginTransaction()
                .replace(R.id.content, ImageViewerFragment.newInstance(getIntent().getExtras()))
                .commit();
    }
}
