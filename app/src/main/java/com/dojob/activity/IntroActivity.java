package com.dojob.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Toast;

import com.dojob.Additions.ApiInterface;
import com.dojob.Additions.ServiceGenerator;
import com.dojob.Additions.local.CommentDatum;
import com.dojob.Additions.responses.WebResponseComment;
import com.dojob.Additions.responses.WebResponseTutorial;
import com.dojob.R;
import com.dojob.backend.RegisterApi;
import com.dojob.fragment.SampleSlide;
import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;
import com.github.paolorotolo.appintro.model.SliderPage;

import java.util.ArrayList;

import retrofit2.Callback;
import retrofit2.Response;

public class IntroActivity extends AppIntro {

    public  static  int isSetting = 0;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Note here that we DO NOT use setContentView();

        // Add your slide fragments here.
        // AppIntro will automatically generate the dots indicator and buttons.
//        addSlide(firstFragment);
//        addSlide(secondFragment);
//        addSlide(thirdFragment);
//        addSlide(fourthFragment);

        // Instead of fragments, you can also use our default slide.
        // Just create a `SliderPage` and provide title, description, background and image.
        // AppIntro will do the rest.
//        SliderPage sliderPage = new SliderPage();
//        sliderPage.setTitle("");
//        sliderPage.setDescription("");
//        sliderPage.setImageDrawable(R.drawable.tut_page_1);
//        addSlide(AppIntroFragment.newInstance(sliderPage));
//
//        SliderPage sliderPage2 = new SliderPage();
//        sliderPage2.setTitle("");
//        sliderPage2.setDescription("");
//        sliderPage2.setImageDrawable(R.drawable.tut_page_2);
//        addSlide(AppIntroFragment.newInstance(sliderPage2));
        addSlide(SampleSlide.newInstance(R.layout.slide_layout_1));
        addSlide(SampleSlide.newInstance(R.layout.slide_layout_2));
        addSlide(SampleSlide.newInstance(R.layout.slide_layout_3));
        addSlide(SampleSlide.newInstance(R.layout.slide_layout_4));
        addSlide(SampleSlide.newInstance(R.layout.slide_layout_5));

        // OPTIONAL METHODS
        // Override bar/separator color.
        setBarColor(Color.parseColor("#f89e26"));
        setSeparatorColor(Color.parseColor("#f89e26"));


        // Hide Skip/Done button.
        showSkipButton(true);
        setProgressButtonEnabled(true);

        // Turn vibration on and set intensity.
        // NOTE: you will probably need to ask VIBRATE permission in Manifest.
        setVibrate(false);
        setVibrateIntensity(30);
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        // Do something when users tap on Skip button.
        finish();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        // Do something when users tap on Done button.
        if (isSetting !=1){
            callApiDone();
        }else{
            finish();
        }
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
        // Do something when the slide changes.
    }

    private void callApiDone() {
        ServiceGenerator serviceGenerator = new ServiceGenerator();
        ApiInterface apiInterface = serviceGenerator.createService(ApiInterface.class);
        String token = "Bearer " + RegisterApi.getToken(this);
        retrofit2.Call<WebResponseTutorial> call = apiInterface.doneTutorial(token);
        call.enqueue(new Callback<WebResponseTutorial>() {
            @Override
            public void onResponse(retrofit2.Call<WebResponseTutorial> call, Response<WebResponseTutorial> response) {
                //  mProgress.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    Log.i("RESPONSE", "OKAY");
                    if (response.body() != null) {
                        String status = response.body().getStatus();
                        if (status.equalsIgnoreCase("ok")) {
                            finish();
                        } else {
                            Toast.makeText(IntroActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(IntroActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Log.i("RESPONSE", response.message());
                    Toast.makeText(IntroActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<WebResponseTutorial> call, Throwable t) {
                Log.i("RESPONSE", t.getMessage());
                //  mProgress.setVisibility(View.GONE);
                Toast.makeText(IntroActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}