package com.dojob.Additions;

import android.os.Parcel;
import android.os.Parcelable;

import com.dojob.backend.model.SetupResponse;

public class SubCategory implements Parcelable {
    public int id;
    public String name;
    public int category_id;
    public String translation_key;


    protected SubCategory(Parcel in) {
        id = in.readInt();
        name = in.readString();
        category_id = in.readInt();
        translation_key = in.readString();
    }

    public static final Creator<SubCategory> CREATOR = new Creator<SubCategory>() {
        @Override
        public SubCategory createFromParcel(Parcel in) {
            return new SubCategory(in);
        }

        @Override
        public SubCategory[] newArray(int size) {
            return new SubCategory[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
        parcel.writeInt(category_id);
        parcel.writeString(translation_key);
    }
}
