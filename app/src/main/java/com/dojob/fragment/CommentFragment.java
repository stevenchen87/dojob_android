package com.dojob.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.dojob.R;
import com.dojob.activity.MainActivity;
import com.dojob.adapter.PortfolioAdapter;
import com.dojob.adapter.model.PortfolioCover;
import com.dojob.backend.CommentApi;
import com.dojob.backend.ProfileApi;
import com.dojob.backend.RegisterApi;
import com.dojob.backend.model.Comment;
import com.dojob.backend.model.CommentsInboxResponse;
import com.dojob.backend.model.CommentsResponse;
import com.dojob.backend.model.Response;
import com.dojob.backend.model.UserResponse;
import com.dojob.core.App;
import com.dojob.core.BackgroundWorker;
import com.dojob.core.Result;
import com.dojob.util.Utils;
import com.github.ybq.android.spinkit.SpinKitView;

public class CommentFragment extends Fragment implements
        BackgroundWorker.Callbacks,
        PortfolioAdapter.Listener {

    public static final String COMMENTS_TYPE = "comments_type";
    public static final String TYPE_IMAGE = "image";
    public static final String TYPE_VIDEO = "video";
    public static final String TYPE_ALBUM = "album";


    public static final String COMMENT_FILE_POSITION = "comment_file_position";

    public static final String USER_RESPONSE_TYPE = "user_response_type";

    private final String OBJ_COMMENT_API = "obj_comment_api";
    private final String OBJ_PROFILE_API = "obj_profile_api";
    private final String TASK_REQUEST_COMMENT = "task_request_comment";
    private final String TASK_SEND_COMMENT = "task_send_profile";
    private final String TASK_REQUEST_PROFILE = "task_request_profile";
    private final String TASK_LIKE = "task_like";

    private BackgroundWorker mWorker;
    private CommentApi mCommetApi;
    private PortfolioAdapter mAdapter;
    private SpinKitView mProgress;
    private RecyclerView mRecyclerView;
    private String mType;
    private UserResponse mUserResponse;
    private int mFilePosition;
    private long mFileId;
    private ProfileApi mProfileApi;
    private Comment[] mCommentResponse;

    public static CommentFragment newInstance(Bundle args) {
        CommentFragment fragment = new CommentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatActivity activity = ((AppCompatActivity) getActivity());

        if (getArguments() != null) {
            if (getArguments().containsKey(COMMENTS_TYPE))
                mType = getArguments().getString(COMMENTS_TYPE);
            if (getArguments().containsKey(USER_RESPONSE_TYPE))
                mUserResponse = App.gson().fromJson(getArguments().getString(USER_RESPONSE_TYPE), UserResponse.class);
            if (getArguments().containsKey(COMMENT_FILE_POSITION))
                mFilePosition = getArguments().getInt(COMMENT_FILE_POSITION);
        }

        if (activity != null) {
            if (activity instanceof MainActivity) {
                ActionBar actionbar = activity.getSupportActionBar();
                if (actionbar != null) {
                    actionbar.setDisplayHomeAsUpEnabled(true);
                    actionbar.setDisplayShowHomeEnabled(true);
                }
                ((MainActivity) activity).isSettingPage(false);
                ((MainActivity) activity).setTitle("Profile");
            }
            mWorker = new BackgroundWorker(getActivity());
            mCommetApi = (CommentApi) mWorker.get(OBJ_COMMENT_API);
            if (mCommetApi == null) {
                mCommetApi = new CommentApi(getContext());
                mWorker.put(OBJ_COMMENT_API, mCommetApi);
            }
            mProfileApi = (ProfileApi) mWorker.get(OBJ_PROFILE_API);
            if (mProfileApi == null) {
                mProfileApi = new ProfileApi(getContext());
                mWorker.put(OBJ_PROFILE_API, mProfileApi);
            }
        }

        if (mUserResponse != null && mType.equals(TYPE_IMAGE) && mUserResponse.talentPhoto.length > 0)
            mFileId = mUserResponse.talentPhoto[mFilePosition].id;
        else if (mUserResponse != null && mType.equals(TYPE_VIDEO) && mUserResponse.profile.featuredVideo.length > 0) {
            mFileId = mUserResponse.profile.featuredVideo[mFilePosition].id;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_portfolio, container, false);
        Utils.hideKeyBoard(getActivity());
        mProgress = view.findViewById(R.id.progress);
        mRecyclerView = view.findViewById(R.id.recycle_view);
        mRecyclerView.setItemViewCacheSize(20);
        mRecyclerView.setDrawingCacheEnabled(true);
        mRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        mRecyclerView.setVisibility(View.VISIBLE);
        mAdapter = new PortfolioAdapter(getContext(), 1);
        mAdapter.setListener(getActivity(), this);
        mRecyclerView.setAdapter(mAdapter);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(llm);
        mProgress.setVisibility(View.VISIBLE); //TODO
        if (mWorker != null) mWorker.executeNewTask(TASK_REQUEST_COMMENT, null, this);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        if (activity != null) {
            if (activity instanceof MainActivity) {
                ActionBar actionbar = activity.getSupportActionBar();
                // will be used in fragment with back button
                if (actionbar != null) {
                    actionbar.setDisplayHomeAsUpEnabled(true);
                    actionbar.setDisplayShowHomeEnabled(true);
                }
                ((MainActivity) activity).setTitle("Profile");
            }
        }
    }

    @Override
    public Result executeTaskInBackground(String id, Bundle args) {
        switch (id) {
            case TASK_REQUEST_COMMENT:
                return mCommetApi.request((int) mFileId, mType);
            case TASK_SEND_COMMENT:
                return mCommetApi.sendMessage((int) mFileId, mType, args.getString("query"));
            case TASK_REQUEST_PROFILE:
                return mProfileApi.request(mUserResponse.profile.url);
            case TASK_LIKE:
                return mProfileApi.like(args.getString("file_id"), args.getString("file_type"));
        }
        return null;
    }

    @Override
    public void onBackgroundTaskCompleted(String id, Result result) {
        int type = (mType.equals(TYPE_IMAGE)) ? PortfolioCover.TYPE_COVER : PortfolioCover.TYPE_VIDOES;
        switch (id) {
            case TASK_REQUEST_COMMENT:
            case TASK_SEND_COMMENT:
                if (result.isSuccess() && mAdapter != null) {
                    mCommentResponse = (Comment[]) result.getResult();
                    if (mCommentResponse != null) {
                        mAdapter.updateItem(mUserResponse, type, mCommentResponse, mFilePosition);
                        mRecyclerView.setVisibility(View.VISIBLE);
                        mProgress.setVisibility(View.GONE);
                    }
                }
                break;
            case TASK_LIKE:
                if (result.isSuccess()) {
                    Response response = (Response) result.getResult();
                    if (getActivity() != null) {
                        if (response.status.equals("ok")) {
                            if (mWorker != null)
                                mWorker.executeNewTask(TASK_REQUEST_PROFILE, null, this);
                        }
                        if (response.msg != null) {
                            Utils.showSnackbar(getActivity(), response.msg);
                        }
                    }
                }
                break;
            case TASK_REQUEST_PROFILE:
                if (result.isSuccess()) {
                    mUserResponse = (UserResponse) result.getResult();
                    if (mUserResponse != null) {
                        mAdapter.updateItem(mUserResponse, type, mCommentResponse, mFilePosition);
                        mRecyclerView.setVisibility(View.VISIBLE);
                        mProgress.setVisibility(View.GONE);
                    }
                }
                break;
        }
    }

    @Override
    public void onOpenAllGallery(int page, UserResponse response) {
        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        if (activity != null) {
            if (activity instanceof MainActivity) {
                ((MainActivity) activity).openGalleryPage(page, response);
            }
        }
    }

    @Override
    public void onEnqueryClick(UserResponse response) {

    }

    @Override
    public void onEditProfileClick() {

    }

    @Override
    public void onRateClick() {

    }

    @Override
    public void onBookmarkedClick(boolean result, int id) {
        Log.d("CommentFragment", "clicked");
    }

    @Override
    public void onListBulletClick() {

    }

    @Override
    public void onLoginDialogOpen() {
        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        if (activity != null) {
            if (activity instanceof MainActivity) {
                ((MainActivity) activity).openLoginDialog();
            }
        }
    }

    @Override
    public void onOpenCommentPage(UserResponse response, String type, int position) {

        if (type.equals(CommentFragment.TYPE_IMAGE)) {
            AppCompatActivity activity = ((AppCompatActivity) getActivity());
            if (activity != null) {
                if (activity instanceof MainActivity) {
                    String url = mUserResponse.talentPhoto[position].url;
                    ((MainActivity) activity).openImageViewer(url);
                }
            }
        }

    }

    @Override
    public void onSaveComment(String query) {
        if (RegisterApi.isLogin(getActivity())) {
            mProgress.setVisibility(View.VISIBLE);
            Bundle bundle = new Bundle();
            bundle.putString("query", query);
            if (mWorker != null) mWorker.executeNewTask(TASK_SEND_COMMENT, bundle, this);
        } else {
            final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
            onLoginDialogOpen();
        }
    }

    @Override
    public void onOpenViewReply(Comment[] comments) {
//        Bundle bundle = new Bundle();
//        bundle.putString(COMMENTS_TYPE, mType);
//        bundle.putString(ReplyFragment.COMMENTS_RESPONSE, App.gson().toJson(comments));
//        bundle.putInt(ReplyFragment.COMMENTS_FILE_ID, mFileId);
//        ReplyFragment replyFragment = ReplyFragment.newInstance(bundle);
//        replyFragment.show(getActivity().getSupportFragmentManager(), "view_replies");
    }

    @Override
    public void onBackClick() {

    }

    @Override
    public void onOpenReviewsPage(UserResponse response) {

    }

    @Override
    public void onCoverImageSwiped(int position) {
       /* mRecyclerView.setVisibility(View.GONE);
        mProgress.setVisibility(View.VISIBLE);
        mFilePosition = position;
        if(mUserResponse != null && mUserResponse.talentPhoto.length>0)
            mFileId = mUserResponse.talentPhoto[position].id;
        if(mWorker != null) mWorker.executeNewTask(TASK_REQUEST_COMMENT, null, this);*/

        mFilePosition = position;
        if (mType.equals(TYPE_IMAGE) && mUserResponse != null && mUserResponse.talentPhoto.length > 0)
            mFileId = mUserResponse.talentPhoto[position].id;
        else if (mUserResponse != null && mType.equals(TYPE_VIDEO) && mUserResponse.profile.featuredVideo.length > 0) {
            mFileId = mUserResponse.profile.featuredVideo[mFilePosition].id;
        }
    }

    @Override
    public void onCoverLike(String type, int position) {
        if (RegisterApi.isLogin(getActivity())) {
            Bundle bundle = new Bundle();
            bundle.putString("file_type", type);
            if (type.equals(CommentFragment.TYPE_IMAGE)) {
                bundle.putString("file_id", String.valueOf(mUserResponse.talentPhoto[position].id));
            } else {
                bundle.putString("file_id", String.valueOf(mUserResponse.talentVideo[position].id));
            }

            if (mWorker != null) mWorker.executeNewTask(TASK_LIKE, bundle, this);
        } else {
            onLoginDialogOpen();
        }
    }

    @Override
    public void onCoverClick(String type, int position) {
        if (type.equals(CommentFragment.TYPE_IMAGE)) {
//            AppCompatActivity activity = ((AppCompatActivity)getActivity());
//            if(activity != null){
//                if(activity instanceof MainActivity){
//                   String url =  mUserResponse.profile.url;
//                    ((MainActivity) activity).openImageViewer(url);
//                }
//            }
        } else {
            Utils.openVideoPlayer(getContext(), mUserResponse.profile.featuredVideo[position].url);
        }
    }

    @Override
    public void onOpenCommentFragment(String type, int position, int fileId) {
//        AppCompatActivity activity = ((AppCompatActivity)getActivity());
//        if(activity != null){
//            if(activity instanceof MainActivity){
//                ((MainActivity) activity).openCommentFragment(type,position,fileId);
//            }
//        }
    }

}