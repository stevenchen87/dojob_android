package com.dojob.adapter.model;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.dojob.GlideApp;
import com.dojob.R;
import com.dojob.backend.model.ProfileResponse;
import com.dojob.core.App;
import com.dojob.util.ResourcesUtil;

public class PortfolioRecruiterItem {
    public static final int LAYOUT_ID = R.layout.layout_recruiter_item;
    public static final int TYPE_RECOMMENDED = 0;
    public static final int TYPE_VIEWED = 1;
    private Context mContext;
    private int mType;
    private ProfileResponse mResponse;

    public PortfolioRecruiterItem(Context context, int type, ProfileResponse response) {
        mResponse = response;
        mType = type;
        mContext = context;
    }

    public ProfileResponse getData() {
        return mResponse;
    }

    public void bindViewHolder(ViewHolder holder, int position) {
        if (holder.clickable != null) {
            holder.clickable.setTag(position);
        }
        if (holder.image != null) {
//            App.picasso().load(mResponse.profile_picture)
//                    .fit()
//                    .centerCrop()
//                    .into(holder.image);

            GlideApp.with(mContext)
                    .load(mResponse.profile_picture)
                    .placeholder(R.drawable.placeholder_1)
                    .apply(RequestOptions.fitCenterTransform())
                    .skipMemoryCache(false)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.image);
        }
        if (holder.name != null) {
            holder.name.setText(mResponse.name);
        }
        if (holder.position != null) {
            if (mResponse != null)
                if (mResponse.category_info != null)
                    if (mResponse.category_info.sub_cat != null)
                        if (mResponse.category_info.sub_cat.name != null)
                            holder.position.setText(mResponse.category_info.sub_cat.name);
        }

        if (holder.content != null) {
            if (mResponse.hot) {
                holder.content.setBackgroundColor(ResourcesUtil.getColor(R.color.yellow));
            } else {
                holder.content.setBackgroundColor(ResourcesUtil.getColor(R.color.green));
            }
        }

        if (holder.hot != null) {
            holder.hot.setVisibility(mResponse.hot ? View.VISIBLE : View.GONE);
        }
    }

    public static class ViewHolder {
        public View hot;
        public CardView container;
        public ImageView image;
        public TextView name;
        public TextView position;
        public View content;
        public View clickable;

        public ViewHolder(View view) {
            container = view.findViewById(R.id.card_view);
            image = view.findViewById(R.id.feed_image);
            name = view.findViewById(R.id.feed_name);
            position = view.findViewById(R.id.feed_position);
            content = view.findViewById(R.id.content);
            clickable = view.findViewById(R.id.clickable);
            hot = view.findViewById(R.id.hot);
        }
    }
}
