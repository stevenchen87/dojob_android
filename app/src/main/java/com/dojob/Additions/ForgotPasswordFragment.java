package com.dojob.Additions;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.dojob.R;
import com.dojob.activity.MainActivity;
import com.dojob.backend.RegisterApi;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordFragment extends Fragment {

    private Unbinder unbinder;

    @BindView(R.id.old_password_edit_text)
    EditText oldPasswordEditText;

    @BindView(R.id.new_password_edit_text)
    EditText newPasswordEditText;

    @BindView(R.id.confirm_pass_edit_text)
    EditText confirmPasswordEditText;

    @BindView(R.id.button_confirm)
    AppCompatButton confirmButton;

    @BindView(R.id.message_text_view)
    TextView messageTextView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_forgot_password, container, false);
        unbinder = ButterKnife.bind(this, rootView);



        if (getActivity()!=null){
            ((MainActivity) getActivity()).setBasicToolbar();
            ((MainActivity) getActivity()).setTitle("  "+getResources().getString(R.string.change_pass_title));

            if (((MainActivity) getActivity()).getSupportActionBar()!=null)
            ((MainActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (newPasswordEditText.getText()!=null&&!newPasswordEditText.getText().toString().trim().equals("")
                        &&confirmPasswordEditText.getText()!=null&&!confirmPasswordEditText.getText().toString().trim().equals("")
                        &&oldPasswordEditText.getText()!=null&&!oldPasswordEditText.getText().toString().trim().equals("")){
                    if (confirmButton!=null){
                        confirmButton.setVisibility(View.GONE);
                    }
                    ServiceGenerator serviceGenerator = new ServiceGenerator();
                    ApiInterface apiInterface = serviceGenerator.createService(ApiInterface.class);
                    Call<ChangePasswordResponse> call = apiInterface.changePassword("Bearer "+RegisterApi.getToken(getContext())
                            ,new ChangePassword(newPasswordEditText.getText().toString(),confirmPasswordEditText.getText().toString()
                                    ,oldPasswordEditText.getText().toString()));
                    call.enqueue(new Callback<ChangePasswordResponse>() {
                        @Override
                        public void onResponse(Call<ChangePasswordResponse> call, Response<ChangePasswordResponse> response) {
                            if (response.isSuccessful()){
                                   if (response.body()!=null)
                                        if (response.body().getStatus().equals("ok")){
                                            if (messageTextView!=null) {
                                                messageTextView.setText(getResources().getString(R.string.password_successfully_changed));
                                                messageTextView.setTextColor(getResources().getColor(R.color.green));
                                                final Handler handler = new Handler();
                                                handler.postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        if (getActivity()!=null)
                                                            ((MainActivity)getActivity()).openMainFragment();
                                                    }
                                                }, 1000);
                                            }
                                        }else {
                                            if (messageTextView!=null){
                                                messageTextView.setText(getResources().getString(R.string.password_not_successfully_changed));
                                                messageTextView.setTextColor(getResources().getColor(R.color.red));

                                                if (confirmButton!=null){
                                                    confirmButton.setVisibility(View.VISIBLE);
                                                }
                                            }
                                        }



                                if (response.body()!=null)
                                Log.i("RESPONSE_SUCCESS",response.body().toString());


                            }else {
                                if (messageTextView!=null){
                                    messageTextView.setText(getResources().getString(R.string.password_not_successfully_changed));
                                    messageTextView.setTextColor(getResources().getColor(R.color.red));
                                    if (response.body()!=null)
                                    Log.i("RESPONSE_NO_SUCCESS",response.body().toString());

                                    if (confirmButton!=null){
                                        confirmButton.setVisibility(View.VISIBLE);
                                    }
                                }

                            }
                        }

                        @Override
                        public void onFailure(Call<ChangePasswordResponse> call, Throwable t) {
                            Log.i("RESPONSE_FAILURE",t.getMessage());
                            if (messageTextView!=null){
                                messageTextView.setText(getResources().getString(R.string.password_not_successfully_changed));
                                messageTextView.setTextColor(getResources().getColor(R.color.red));
                                if (confirmButton!=null){
                                    confirmButton.setVisibility(View.VISIBLE);
                                }
                            }

                        }
                    });
                }else {
                    messageTextView.setText(getResources().getString(R.string.fill_detail_title));
                    messageTextView.setTextColor(getResources().getColor(R.color.red));
                }

            }
        });


        return rootView;
    }
}
