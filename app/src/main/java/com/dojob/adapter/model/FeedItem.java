package com.dojob.adapter.model;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.dojob.GlideApp;
import com.dojob.R;
import com.dojob.backend.model.FeedItemResponse;
import com.dojob.util.ResourcesUtil;
import com.dojob.util.Utils;

public class FeedItem {
    public static final int LAYOUT_ID = R.layout.layout_feed_item;
    public static final int HOTTEST_TALENT = 0;
    public static final int TOP_PICK = 1;
    public static final int SEARCH_RESULT = 2;
    private Context mContext;
    private int mType;
    private FeedItemResponse mFeedResponse;

    public FeedItem(Context context, int type, FeedItemResponse response) {
        mFeedResponse = response;
        mType = type;
        mContext = context;
    }

    public FeedItemResponse getData() {
        return mFeedResponse;
    }

    public void bindViewHolder(ViewHolder holder, int position) {
        if (holder.clickable != null) {
            holder.clickable.setTag(position);
        }
        if (holder.image != null && mFeedResponse.cover_photo != null) {
           /* App.picasso().load(mFeedResponse.image)
                    .fit().
                    .centerInside()
                    .into(holder.image);*/
            Uri uri = Uri.parse(mFeedResponse.cover_photo);


            Log.i("URI", mFeedResponse.cover_photo);
           /* Glide.with(context)

                    .load(uri)


                    .into(holder.feedImage);*/


            GlideApp.with(mContext)
                    .load(mFeedResponse.cover_photo)
                    .placeholder(R.drawable.placeholder_1)
                    .apply(RequestOptions.fitCenterTransform())
                    .skipMemoryCache(false)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.image);
            /*Glide.with(mContext).load(mFeedResponse.image).*/
        }
        if (holder.tvcountryCode != null) {
            if (mFeedResponse.countrycode != null) {
                holder.tvcountryCode.setText(mFeedResponse.countrycode);
                holder.tvcountryCode.setVisibility(View.VISIBLE);
            }else
                holder.tvcountryCode.setVisibility(View.GONE);

        }
        if (holder.name != null) {
            holder.name.setText(mFeedResponse.name);
        }
        if (holder.position != null) {
            if (mFeedResponse.category_info != null) {
                holder.position.setText(mFeedResponse.category_info.sub_cat.name);
            }
        }

        if (holder.content != null) {
            switch (mType) {
                case HOTTEST_TALENT:
                    holder.content.setBackgroundColor(ResourcesUtil.getColor(R.color.yellow));
                    int recyclerViewPadding = Utils.dpToPx(8);
                    int itemMargin = Utils.dpToPx(2);
                    int widthPerCard = Utils.getScreenWidth() / 2 - recyclerViewPadding - itemMargin;

                    CardView.LayoutParams hotParams = new CardView.LayoutParams(widthPerCard, Utils.dpToPx(180));
                    hotParams.setMargins(Utils.dpToPx(2), Utils.dpToPx(2), Utils.dpToPx(2), Utils.dpToPx(2));
                    holder.container.setPadding(Utils.dpToPx(2), Utils.dpToPx(2), Utils.dpToPx(2), Utils.dpToPx(2));
                    holder.container.setLayoutParams(hotParams);
                    break;
                case SEARCH_RESULT:
                    holder.content.setBackgroundColor(ResourcesUtil.getColor(R.color.yellow));
                    break;
                case TOP_PICK:
//                    holder.content.setBackgroundColor(ResourcesUtil.getColor(R.color.yellow));
                    holder.content.setBackgroundColor(ResourcesUtil.getColor(R.color.green));
                    CardView.LayoutParams topParams = new CardView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Utils.dpToPx(180));
                    topParams.setMargins(Utils.dpToPx(2), Utils.dpToPx(2), Utils.dpToPx(2), Utils.dpToPx(2));
                    holder.container.setLayoutParams(topParams);
                    break;
            }
        }
    }



    public static class ViewHolder {
        public CardView container;
        public ImageView image;
        public TextView name;
        public TextView position;
        public TextView tvcountryCode;
        public View content;
        public View clickable;

        public ViewHolder(View view) {
            container = view.findViewById(R.id.card_view);
            image = view.findViewById(R.id.feed_image);
            name = view.findViewById(R.id.feed_name);
            position = view.findViewById(R.id.feed_position);
            content = view.findViewById(R.id.content);
            clickable = view.findViewById(R.id.clickable);
            tvcountryCode = view.findViewById(R.id.tvcountryCode);
        }
    }
}
