package com.dojob.backend.model;

import com.google.gson.annotations.SerializedName;

public class FeedsResponse extends Response{
    @SerializedName("top_10")
    public FeedItemResponse[] hot;
    @SerializedName("top_pick")
    public FeedItemResponse[] top;

    public FeedItemResponse[] talent;

    public int newMsg;
}
