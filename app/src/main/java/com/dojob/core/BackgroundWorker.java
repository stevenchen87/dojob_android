package com.dojob.core;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class BackgroundWorker {

    private static final int CPU_COUNT = 8;
    private static final int CORE_POOL_SIZE = CPU_COUNT + 1;
    private static final int MAXIMUM_POOL_SIZE = CPU_COUNT * 2 + 1;
    private static final int KEEP_ALIVE = 1;

    private static final ThreadFactory sThreadFactory = new ThreadFactory() {
        private final AtomicInteger mCount = new AtomicInteger(1);

        public Thread newThread(Runnable r) {
            return new Thread(r, "BackgroundWorker #" + mCount.getAndIncrement());
        }
    };

    private static final BlockingQueue<Runnable> sPoolWorkQueue = new LinkedBlockingQueue<>(128);
    private static final Executor THREAD_POOL_EXECUTOR = new ThreadPoolExecutor(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE, KEEP_ALIVE, TimeUnit.SECONDS, sPoolWorkQueue, sThreadFactory);

    private HeadlessFragment mFragment;

    public BackgroundWorker(FragmentActivity activity) {
        String tag = activity.getClass().getName() + "_dojob";
        FragmentManager fm = activity.getSupportFragmentManager();
        HeadlessFragment fragment = (HeadlessFragment) fm.findFragmentByTag(tag);
        if (fragment == null && !activity.isFinishing()) {
            fragment = new HeadlessFragment();
            try {
                fm.beginTransaction().add(fragment, tag).commit();
            } catch (IllegalStateException ignored) {
            }
        }
        mFragment = fragment;
    }


    public void executeTask(String id, Bundle args, Callbacks callbacks) {
        mFragment.executeTask(id, args, callbacks);
    }

    public void executeNewTask(String id, Bundle args, Callbacks callbacks) {
        mFragment.executeNewTask(id, args, callbacks);
    }

    public void put(String id, Object object) {
        mFragment.put(id, object);
    }

    public void remove(String id) {
        mFragment.remove(id);
    }

    public Object get(String id) {
        if (mFragment != null) {
            return mFragment.get(id);
        }
        return null;
    }

    public interface Callbacks {
        Result executeTaskInBackground(String id, Bundle args);

        void onBackgroundTaskCompleted(String id, Result result);
    }

    public static class BackgroundTask extends AsyncTask<Void, Void, Result> {

        private HeadlessFragment mFragment;
        private String mId;
        private Bundle mArgs;
        private Callbacks mCallbacks;

        public BackgroundTask(HeadlessFragment fragment, String id, Bundle args, Callbacks callbacks) {
            mFragment = fragment;
            mId = id;
            mArgs = args;
            mCallbacks = callbacks;
        }

        @Override
        protected Result doInBackground(Void... voids) {
            return mCallbacks.executeTaskInBackground(mId, mArgs);
        }

        @Override
        protected void onPostExecute(Result result) {
            if (mCallbacks != null) {
                mCallbacks.onBackgroundTaskCompleted(mId, result);
            }
            release();
        }

        @Override
        protected void onCancelled() {
            release();
        }

        public void update(Bundle args, Callbacks callbacks) {
            mArgs = args;
            mCallbacks = callbacks;
        }

        private void release() {
            mFragment.taskCompleted(mId);
            mFragment = null;
            mCallbacks = null;
        }
    }

    public static class HeadlessFragment extends Fragment {

        private Map<String, Object> mObjects = new HashMap<>();
        private Map<String, BackgroundTask> mTasks = new HashMap<>();

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setRetainInstance(true);
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            for (Map.Entry<String, BackgroundTask> entry : mTasks.entrySet()) {
                entry.getValue().cancel(true);
            }
            mTasks.clear();
            mObjects.clear();
        }

        public void put(String id, Object object) {
            mObjects.put(id, object);
        }

        public void remove(String id) {
            mObjects.remove(id);
        }

        public Object get(String id) {
            return mObjects.get(id);
        }

        public void executeTask(String id, Bundle args, Callbacks callbacks) {
            BackgroundTask task = mTasks.get(id);
            if (task != null) {
                task.update(args, callbacks);
                return;
            }
            task = new BackgroundTask(this, id, args, callbacks);
            mTasks.put(id, task);
            if (Build.VERSION.SDK_INT > 10) {
                task.executeOnExecutor(THREAD_POOL_EXECUTOR);
            } else {
                task.execute();
            }
        }

        public void executeNewTask(String id, Bundle args, Callbacks callbacks) {
            BackgroundTask task = mTasks.remove(id);
            if (task != null) {
                task.cancel(true);
            }
            executeTask(id, args, callbacks);
        }

        public void taskCompleted(String id) {
            mTasks.remove(id);
        }
    }
}
