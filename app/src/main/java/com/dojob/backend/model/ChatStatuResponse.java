package com.dojob.backend.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChatStatuResponse {

    @SerializedName("chatroomData")
    @Expose
    private ChatRoomResponse chatroomData;
    @SerializedName("status")
    @Expose
    private String status;

    public ChatRoomResponse getChatroomData() {
        return chatroomData;
    }

    public void setChatroomData(ChatRoomResponse chatroomData) {
        this.chatroomData = chatroomData;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}