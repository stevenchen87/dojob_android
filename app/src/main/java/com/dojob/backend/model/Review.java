package com.dojob.backend.model;

public class Review {
    public String title;
    public String user_id;
    public int review_by;
    public float rating;
    public String review;
    public int created_at;
    public ProfileResponse review_by_user;
}
