package com.dojob.adapter.model;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.dojob.GlideApp;
import com.dojob.R;
import com.dojob.backend.model.MediaResponse;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;

public class NewVideosItem {
    public static final int LAYOUT_ID = R.layout.layout_videos_item_new;
    private final Context mContext;

    private MediaResponse mResponse;

    private int mType;

    public NewVideosItem(Context context, MediaResponse response,int type) {
        mContext = context;
        mResponse = response;
        this.mType = type;
    }

    public void bindViewHolder(ViewHolder holder, int position){
        if(holder.image != null){
            if(mResponse.thumbnail != null && !mResponse.thumbnail.isEmpty()){
                if (mType==0){
                    Picasso.get().load(mResponse.thumbnail).into(holder.image);
                }else {

                    Log.i("TYPE","1");
                    GlideApp.with(mContext).asBitmap()
                            .load(mResponse.thumbnail)
                            .placeholder(R.drawable.placeholder_1)
                            .apply(new RequestOptions().override(800,300))
                            .skipMemoryCache(false)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(holder.image);
                }

            }
            else if(mResponse.url != null && !URLUtil.isFileUrl(mResponse.url)){
                File file = new File(mResponse.url);
                if (mType==0){
                    if(file.exists()){
                        try {

                            holder.image.setImageDrawable(Drawable.createFromStream(mContext.getContentResolver().openInputStream(Uri.fromFile(file)),
                                    null));
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                }else {
                    Log.i("TYPE","1");

                    RequestOptions options = new RequestOptions().frame(3000000);
                    GlideApp.with(mContext).asBitmap()
                            .load(Uri.fromFile(file))
                            .placeholder(R.drawable.placeholder_1)
                            .apply(options.override(600,200))
                            .skipMemoryCache(false)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(holder.image);
                }

            }
        }

        if(holder.container != null){
            holder.container.setTag(position);
        }
    }

    public String getUrl(){
        return mResponse.url;
    }

    public static class ViewHolder{

        public View container;
        public ImageView image;

        public ViewHolder(View view){
            container = view.findViewById(R.id.video_container);
            image = view.findViewById(R.id.videos_photo);
        }
    }
}
