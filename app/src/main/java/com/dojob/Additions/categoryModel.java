package com.dojob.Additions;

import com.dojob.backend.model.Categories;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

public class categoryModel {

    @SerializedName("recommendCategory")
    HashMap<String,String> keyValueArray;

    public HashMap<String, String> getKeyValueArray() {
        return keyValueArray;
    }

    public void setKeyValueArray(HashMap<String, String> keyValueArray) {
        this.keyValueArray = keyValueArray;
    }
}
