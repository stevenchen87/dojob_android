package com.dojob.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteException;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dojob.Additions.dialogEmail;
import com.dojob.BuildConfig;
import com.dojob.R;
import com.dojob.activity.IntroActivity;
import com.dojob.activity.MainActivity;
import com.dojob.backend.LoginApi;
import com.dojob.backend.RegisterApi;
import com.dojob.backend.SetupApi;
import com.dojob.backend.model.Response;
import com.dojob.core.Authentication;
import com.dojob.core.BackgroundWorker;
import com.dojob.core.Result;
import com.dojob.database.CommentsTable;
import com.dojob.database.MessagesTable;
import com.dojob.databinding.FragmentSettingsBinding;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;

public class SettingFragment extends Fragment implements AppCompatButton.OnClickListener, BackgroundWorker.Callbacks {
    private BackgroundWorker mWorker;
    private LoginApi mLoginApi;
    private final String OBJ_LOGIN_API = "obj_login_api";
    private AlertDialog progressDialog;
    FragmentSettingsBinding mBinding;

    public static SettingFragment newInstance() {
        Bundle args = new Bundle();
        SettingFragment fragment = new SettingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mWorker = new BackgroundWorker(getActivity());
        mLoginApi = (LoginApi) mWorker.get(OBJ_LOGIN_API);
        if (mLoginApi == null) {
            mLoginApi = new LoginApi(getContext());
            mWorker.put(OBJ_LOGIN_API, mLoginApi);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_settings, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding.settingVersion.setText(String.valueOf("App version " + BuildConfig.VERSION_NAME + " " + BuildConfig.VERSION_CODE + ""));
    }

    @Override
    public void onResume() {
        super.onResume();
        mBinding.buttonChnagePassword.setOnClickListener(this);
        mBinding.buttonPrivacy.setOnClickListener(this);
        mBinding.buttonContact.setOnClickListener(this);
        mBinding.buttonFaq.setOnClickListener(this);
        mBinding.buttonTermOfUse.setOnClickListener(this);
        mBinding.buttonAccountSetting.setOnClickListener(this);
        mBinding.buttonLogOut.setOnClickListener(this);
        mBinding.buttonTutorial.setOnClickListener(this);


        if (RegisterApi.isLogin(this.getActivity())) {
            mBinding.buttonLogOut.setVisibility(View.VISIBLE);
            if (FirebaseAuth.getInstance().getCurrentUser() != null)
                for (UserInfo user : FirebaseAuth.getInstance().getCurrentUser().getProviderData()) {
                    if (!user.getProviderId().equals("facebook.com") && !user.getProviderId().equals("google.com")) {
                        if (mBinding.buttonChnagePassword != null)
                            mBinding.buttonChnagePassword.setVisibility(View.VISIBLE);
                    } else {
                        if (mBinding.buttonChnagePassword != null)
                            mBinding.buttonChnagePassword.setVisibility(View.GONE);

                    }
                }
            /*if (PreferenceManager.getDefaultSharedPreferences(getContext()).getInt(LoginApi.LOG_IN,0)==1){

                if (RegisterApi.logInType == 1){
                    if (changePass!=null)
                        changePass.setVisibility(View.VISIBLE);
                }else {
                    if (changePass!=null)
                        changePass.setVisibility(View.GONE);
                }
            }*/

        } else {
            mBinding.buttonLogOut.setVisibility(View.GONE);
            if (mBinding.buttonChnagePassword != null)
                mBinding.buttonChnagePassword.setVisibility(View.GONE);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mBinding.buttonChnagePassword.setOnClickListener(null);
        mBinding.buttonPrivacy.setOnClickListener(null);
        mBinding.buttonContact.setOnClickListener(null);
        mBinding.buttonFaq.setOnClickListener(null);
        mBinding.buttonTermOfUse.setOnClickListener(null);
        mBinding.buttonAccountSetting.setOnClickListener(null);
        mBinding.buttonLogOut.setOnClickListener(null);
        mBinding.buttonTutorial.setOnClickListener(null);
    }

    @Override
    public void onClick(View v) {
        if (getActivity() != null) {
            switch (v.getId()) {
                case R.id.button_contact:
                    Intent intent = new Intent(getActivity(), dialogEmail.class);
                    startActivity(intent);
                    break;
                case R.id.button_privacy:
                    ((MainActivity) getActivity()).openPrivacyPage();
                    break;
                case R.id.button_faq:
                    ((MainActivity) getActivity()).openFAQPage();
                    break;
                case R.id.button_term_of_use:
                    ((MainActivity) getActivity()).openTermOfUsePage();
                    break;
                case R.id.button_account_setting:
                    ((MainActivity) getActivity()).onOpenProfileSetting();
                    break;
                case R.id.button_log_out:
                    if (getContext() != null) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
                        alertDialogBuilder.setTitle("Log Out");
                        alertDialogBuilder
                                .setMessage("Are you sure you would like to log out?")
                                .setCancelable(false)
                                .setPositiveButton("Log out", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        showLoadingProgress();
                                        mWorker.executeNewTask(LoginApi.LOGOUT_TAG, null, SettingFragment.this);
                                        PreferenceManager.getDefaultSharedPreferences(getContext()).edit().putInt(LoginApi.LOG_IN, 0).commit();
                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.dismiss();
                                    }
                                });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }
                    break;
                case R.id.button_chnage_password:
                    if (getActivity() != null)
                        ((MainActivity) getActivity()).openPasswordChnageFragment();
                    break;
                case R.id.button_tutorial:
                    if (getActivity() != null) {
                        Intent intent1 = new Intent(getActivity(), IntroActivity.class);
                        IntroActivity.isSetting = 1;
                        getActivity().startActivity(intent1);
                    }
                    break;
                default:
                    break;

            }
        }
    }

    private void reset() {
        if (getActivity() != null) {
            RegisterApi.deleteToken(getActivity());
            SetupApi.deleteId(getActivity());
            try {
                MessagesTable.deleteAll(MessagesTable.class);
            } catch (SQLiteException e) {
                e.printStackTrace();
            }

            try {
                CommentsTable.deleteAll(CommentsTable.class);
            } catch (SQLiteException e) {
                e.printStackTrace();
            }

            SharedPreferences settings = getActivity().getSharedPreferences("dojob", Context.MODE_PRIVATE);
            settings.edit().clear().apply();
            Authentication.signOut();

            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    try {
                        FirebaseInstanceId.getInstance().deleteInstanceId();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void result) {
                    ((MainActivity) getActivity()).switchSignUpButton(false);
                    ((MainActivity) getActivity()).restart();
                    dismissLoadingProgress();
                }
            }.execute();
        }
    }


    @Override
    public Result executeTaskInBackground(String id, Bundle args) {
        switch (id) {
            case LoginApi.LOGOUT_TAG:
                return mLoginApi.logout();
        }
        return null;
    }

    @Override
    public void onBackgroundTaskCompleted(String id, Result result) {
        switch (id) {
            case LoginApi.LOGOUT_TAG:
                if (result.isSuccess()) {
                    Response response = (Response) result.getResult();
                    if (response.status.equals("ok")) {
                        reset();
                    }
                }
                break;
        }
    }


    private void showLoadingProgress() {
        if (getActivity() != null) {
            AlertDialog.Builder progressDialogBuilder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.layout_loading, null);
            TextView msg = dialogView.findViewById(R.id.loading_msg);
            msg.setText("Logout...");
            progressDialogBuilder.setView(dialogView);
            progressDialog = progressDialogBuilder.create();
            progressDialog.show();
        }
    }

    private void dismissLoadingProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }
}
