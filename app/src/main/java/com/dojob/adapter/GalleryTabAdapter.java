package com.dojob.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Gallery;

import com.dojob.R;
import com.dojob.adapter.holder.GalleryViewHolder;
import com.dojob.adapter.model.PhotosAddBar;
import com.dojob.adapter.model.PhotosItem;
import com.dojob.adapter.model.PortfolioEmpty;
import com.dojob.adapter.model.VideosItem;
import com.dojob.backend.GalleryApi;
import com.dojob.backend.LoginApi;
import com.dojob.backend.model.FileResponse;
import com.dojob.backend.model.MediaResponse;
import com.dojob.core.BackgroundWorker;
import com.dojob.core.Logger;
import com.dojob.fragment.CommentFragmentNew;
import com.dojob.fragment.GalleryTabFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GalleryTabAdapter extends RecyclerView.Adapter<GalleryViewHolder> implements View.OnClickListener, View.OnLongClickListener{

    private static final int TYPE_ADD_BAR = 1;
    private static final int TYPE_ITEM = 2;
    private static final int TYPE_VIDEO_ITEM = 3;
    private static final int TYPE_PORTFOLIO_EMPTY = 4;
    private final Context mContext;
    private final int mUserId;
    private List<Object> mItems = new ArrayList<>();
    private Listener mListener;
    private BackgroundWorker mWorker;
    private BackgroundWorker.Callbacks callbacks;
    private int mTotalPhots = 0;
    public GalleryTabAdapter(Context context, int userId, BackgroundWorker mWorker, BackgroundWorker.Callbacks callback){
        mContext = context;
        mUserId = userId;
        this.mWorker = mWorker;
        this.callbacks = callback;
    }

    public void updateItem(int type, MediaResponse[] files) {
        mItems.clear();
        if(LoginApi.isUser(mContext, mUserId))
            mItems.add(new PhotosAddBar());
        if(files != null && files.length > 0){
            if(type == GalleryApi.TYPE_GALLERY_PHOTO){
                mTotalPhots = files.length;
                for(MediaResponse photoResponse: files ){
                    mItems.add(new PhotosItem(photoResponse.url, photoResponse.id, mContext));
                }

            }
            else{
                for(MediaResponse file: files){
                    mItems.add(new VideosItem(mContext, file,0, file.id));
                }
            }
        }
        else{
            if(type == GalleryApi.TYPE_GALLERY_PHOTO){
                mItems.add(new PortfolioEmpty("No Photo"));
            }
            else mItems.add(new PortfolioEmpty("No Video"));
        }

        notifyDataSetChanged();
    }

    public void setListener(Listener listener){
        mListener = listener;
    }

    @NonNull
    @Override
    public GalleryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        int layout = 0;
        switch (viewType){
            case TYPE_ADD_BAR:
                layout = PhotosAddBar.LAYOUT_ID;
                break;
            case TYPE_ITEM:
                layout = PhotosItem.LAYOUT_ID;
                break;
            case TYPE_VIDEO_ITEM:
                layout = VideosItem.LAYOUT_ID;
                break;
            case TYPE_PORTFOLIO_EMPTY:
                layout = PortfolioEmpty.LAYOUT_ID;
                break;
        }

        View view = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        GalleryViewHolder holder = new GalleryViewHolder(view);
        if(holder.photosItem.container != null){
            holder.photosItem.container.setOnClickListener(this);
            holder.photosItem.container.setOnLongClickListener(this);
        }
        if(holder.photosAddBar.container != null){
            holder.photosAddBar.container.setOnClickListener(this);
            holder.photosAddBar.container.setOnLongClickListener(this);
        }
        if(holder.videosItem.container != null){
            holder.videosItem.container.setOnClickListener(this);
            holder.videosItem.container.setOnLongClickListener(this);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull GalleryViewHolder holder, int position) {
        Object item = getItem(position);
        int type = getItemViewType(position);
        switch (type){
            case TYPE_ADD_BAR:
                ((PhotosAddBar)item).bindViewHolder(holder.photosAddBar, position);
                break;
            case TYPE_ITEM:
                ((PhotosItem)item).bindViewHolder(holder.photosItem, position);
                break;
            case TYPE_VIDEO_ITEM:
                ((VideosItem)item).bindViewHolder(holder.videosItem, position);
                break;
            case TYPE_PORTFOLIO_EMPTY:
                ((PortfolioEmpty)item).bindViewHolder(holder.empty, position);
                break;

        }
    }

    @Override
    public int getItemViewType(int position) {
        Object item = getItem(position);
        if (item instanceof PhotosAddBar) {
            return TYPE_ADD_BAR;
        } else if (item instanceof PhotosItem) {
            return TYPE_ITEM;
        } else if(item instanceof VideosItem){
            return TYPE_VIDEO_ITEM;
        } else if(item instanceof PortfolioEmpty){
            return TYPE_PORTFOLIO_EMPTY;
        }
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        if (mItems != null) {
            return mItems.size();
        }
        return 0;
    }

    public Object getItem(int position) {
        int size = mItems.size();
        if (size > 0 && position < size) {
            return mItems.get(position);
        }
        return null;
    }

    @Override
    public void onClick(View view) {
        Object tag = view.getTag();
        if (tag == null) {
            return;
        }
        final Object item = getItem((int) view.getTag());
        switch (view.getId()) {
            case R.id.photo_container:
                if(item instanceof PhotosItem){
                    if(mListener != null) {
                        //mListener.onOpenImageViewer(((PhotosItem) item).getUrl());
                        mListener.onOpenCommentFragment(CommentFragmentNew.TYPE_IMAGE,0, (int) ((PhotosItem) item).getId());
                    }
                }
                break;
            case R.id.photos_add_bar:
                if(mListener != null) {
                    mListener.onOpenMediaPicker();
                }
                break;
            case R.id.video_container:
                if(item instanceof VideosItem){
                    if(mListener != null) {
                        //mListener.onOpenVideoPlayer(((VideosItem) item).getUrl());
                        mListener.onOpenCommentFragment(CommentFragmentNew.TYPE_VIDEO,0, (int) ((VideosItem) item).getId());
                    }
                }
                break;
        }
    }

    private void deleteFile(final long fileId){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        alertDialogBuilder.setTitle("Confirm to Delete?");
        alertDialogBuilder
                .setMessage("Are you sure you would like to delete this picture?")
                .setCancelable(false)
                .setPositiveButton("Delete Now",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        Bundle bundle = new Bundle();
                        bundle.putLong("id", fileId);
                        if(mWorker != null) mWorker.executeNewTask(GalleryTabFragment.TASK_DELETE_FILE, bundle, callbacks);
//                        galleryApi.deleteFiles(deletedFileId);
                    }
                })
                .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
    @Override
    public boolean onLongClick(View view) {
        if(!LoginApi.isUser(mContext, mUserId)) return true;
        Object tag = view.getTag();
        if (tag == null) {
            return false;
        }
        final Object item = getItem((int) view.getTag());
        switch (view.getId()) {
            case R.id.photo_container:
                if(item instanceof PhotosItem){
                    if(mListener != null) {
//                        mListener.onOpenImageViewer(((PhotosItem) item).getUrl());
//                        Log.d("GTAdapter", "LongPress"+((PhotosItem)item).getId());
                        if(mTotalPhots == 1){
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
                            alertDialogBuilder
                                    .setMessage(" Your profile must contain at least one Talent photo.")
                                    .setCancelable(false)
                                    .setPositiveButton("Ok", null);
                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                        }
                        else{
                            deleteFile(((PhotosItem)item).getId());
                        }
                    }
                }
                break;
            case R.id.photos_add_bar:
                if(mListener != null) {
//                    mListener.onOpenMediaPicker();
                    Log.d("GTAdapter", "LongPress"+(item).toString());
                }
                break;
            case R.id.video_container:
                if(item instanceof VideosItem){
                    if(mListener != null) {
//                        mListener.onOpenVideoPlayer(((VideosItem) item).getUrl());
                        Log.d("GTAdapter", "LongPress"+((VideosItem)item).toString());
                        deleteFile(((VideosItem)item).getId());
                    }
                }
                break;
        }
        return true;
    }

    public interface Listener{
        void onOpenImageViewer(String url);
        void onOpenMediaPicker();
        void onOpenVideoPlayer(String url);
        void onOpenCommentFragment(String type, int position, int fileId);
    }
}
