package com.dojob.Additions.local;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SocialFeedParent {
    int rowType;
    ArrayList<SocialFeed> arrayList;

    public int getRowType() {
        return rowType;
    }

    public void setRowType(int rowType) {
        this.rowType = rowType;
    }

    public ArrayList<SocialFeed> getArrayList() {
        return arrayList;
    }

    public void setArrayList(ArrayList<SocialFeed> arrayList) {
        this.arrayList = arrayList;
    }
}
