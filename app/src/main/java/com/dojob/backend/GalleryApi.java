package com.dojob.backend;

import android.content.Context;
import android.net.Uri;

import com.dojob.backend.model.GalleryResponse;
import com.dojob.backend.model.UserResponse;
import com.dojob.core.App;
import com.dojob.core.Error;
import com.dojob.core.Http;
import com.dojob.core.Result;
import com.dojob.util.Utils;
import com.google.gson.JsonSyntaxException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GalleryApi {
    public final static int TYPE_GALLERY_ALBUM = 0;
    public final static int TYPE_GALLERY_PHOTO = 1;
    public final static int TYPE_GALLERY_VIDEO = 2;

    public final static String GALLERY_TAG = "gallery_tag";

    private final static String GALLERY_DELETE_FILE_TAG  = "gallery_delete_file_tag";
    private final static String GALLERY_ALBUM_TAG = "gallery_album_tag";
    private final static String GALLERY_VIDEO_TAG = "gallery_video_tag";
    private final static String GALLERY_PHOTO_TAG = "gallery_photo_tag";
    private final static String GALLERY_UPLOAD_VIDEO_IMAGES_TAG = "gallery_upload_video_image_tag";
    private final static String GALLERY_ADD_ALBUM_TAG = "gallery_add_album_tag";
    private final static String GALLERY_DELETE_ALBUM_TAG = "gallery_delete_album_tag";
    private final static String GALLERY_ADD_FILE_ALBUM_TAG = "gallery_add_file_album_tag";
    private final static String GALLERY_DELETE_FILE_ALBUM_TAG = "gallery_delete_file_album_tag";
    private final Context mContext;
    private final String mAddPhotoUrl = "http://step4work.com/api/talent/addTalentPhoto";
    private final String mAddVideoUrl = "http://step4work.com/api/talent/addTalentVideo";

    public GalleryApi(Context context) {
        mContext = context;
    }

    public Result post(String type, List<Uri> fileDataUriList) {
        UserResponse response = null;
        if (!Utils.isOnline(mContext)) {
            return new Result(Error.OFFLINE);
        }
        boolean isPhoto = type.equals("photo");
        String url = isPhoto?mAddPhotoUrl:mAddVideoUrl;
        Http.Request request = Http.request(mContext)
                .url(url)
                .tag(GALLERY_UPLOAD_VIDEO_IMAGES_TAG);
        Map<String, Uri> fileData = new HashMap<>();
        if(isPhoto && fileDataUriList != null){
            for(int i = 0; i< fileDataUriList.size(); i++){
                Uri uri = fileDataUriList.get(i);
                fileData.put("talentImage["+i+"]",uri);
            }
        }
        else{
            for(int i = 0; i< fileDataUriList.size(); i++){
                Uri uri = fileDataUriList.get(i);
                fileData.put("talentVideo["+i+"]",uri);
            }
        }

        request.postMultiPartToServer(mContext, null, fileData);

        Result result = request.send();
        if (result.isSuccess()) {
            String responseText = (String) result.getResult();

            try {
                response = App.gson().fromJson(responseText, UserResponse.class);
            } catch (JsonSyntaxException ex) {
                ex.printStackTrace();
            }
            if (response != null) {
                return new Result(null, response);
            }
            return new Result(Error.DATA);
        }

        return new Result(Error.NETWORK);
    }

    public Result getGallery(int type, String userId){
        UserResponse response = null;
        if (!Utils.isOnline(mContext)) {
            return new Result(Error.OFFLINE);
        }
        HashMap<String, String> param = new HashMap<>();
        param.put("user_id", userId);
        Http.Request request = Http.request(mContext);

        switch (type){
            case TYPE_GALLERY_ALBUM:
                request.url("http://step4work.com/api/profile/album/all?"+Utils.hashMapToString(param));
                request.tag(GALLERY_ALBUM_TAG);
                break;
            case TYPE_GALLERY_PHOTO:
                request.url("http://step4work.com/api/profile/talentPhoto/all?"+Utils.hashMapToString(param));
                request.tag(GALLERY_PHOTO_TAG);
                break;
            case TYPE_GALLERY_VIDEO:
                request.url("http://step4work.com/api/profile/talentVideo/all?"+Utils.hashMapToString(param));
                request.tag(GALLERY_VIDEO_TAG);
                break;
        }

        Result result = request.send();
        if (result.isSuccess()) {
            String responseText = (String) result.getResult();
            try {
                response = App.gson().fromJson(responseText, UserResponse.class);
            } catch (JsonSyntaxException ex) {
                ex.printStackTrace();
            }
            if (response != null) {
                return new Result(null, response);
            }
            return new Result(Error.DATA);
        }

        return new Result(Error.NETWORK);
    }

    public Result setAlbum(Map<String, String> formData, Map<String, Uri> fileData) {
        GalleryResponse response = null;
        if (!Utils.isOnline(mContext)) {
            return new Result(Error.OFFLINE);
        }
        Http.Request request = Http.request(mContext)
                .url("http://step4work.com/api/talent/album/set")
                .tag(GALLERY_ADD_ALBUM_TAG);
        request.postMultiPartToServer(mContext, formData, fileData);

        Result result = request.send();
        if (result.isSuccess()) {
            String responseText = (String) result.getResult();

            try {
                response = App.gson().fromJson(responseText, GalleryResponse.class);
            } catch (JsonSyntaxException ex) {
                ex.printStackTrace();
            }
            if (response != null) {
                return new Result(null, response);
            }
            return new Result(Error.DATA);
        }

        return new Result(Error.NETWORK);
    }

    public Result addFile(Map<String, String> formData, Map<String, Uri> fileData) {
        UserResponse response = null;
        if (!Utils.isOnline(mContext)) {
            return new Result(Error.OFFLINE);
        }
        Http.Request request = Http.request(mContext)
                .url("http://step4work.com/api/talent/album/add")
                .tag(GALLERY_ADD_FILE_ALBUM_TAG);
        request.postMultiPartToServer(mContext, formData, fileData);
        Result result = request.send();
        if (result.isSuccess()) {
            String responseText = (String) result.getResult();
            try {
                response = App.gson().fromJson(responseText, UserResponse.class);
            } catch (JsonSyntaxException ex) {
                ex.printStackTrace();
            }
            if (response != null) {
                return new Result(null, response);
            }
            return new Result(Error.DATA);
        }

        return new Result(Error.NETWORK);
    }

    public Result deleteAlbumFiles(String albumId, String filesId) {
        UserResponse response = null;
        if (!Utils.isOnline(mContext)) {
            return new Result(Error.OFFLINE);
        }
        Map<String, String> params = new HashMap<>();
        params.put("albumID", albumId);
        params.put("fileID", filesId);
        Http.Request request = Http.request(mContext)
                .url("http://step4work.com/api/talent/album/deleteFiles")
                .post(params)
                .tag(GALLERY_DELETE_FILE_ALBUM_TAG);
        Result result = request.send();
        if (result.isSuccess()) {
            String responseText = (String) result.getResult();
            try {
                response = App.gson().fromJson(responseText, UserResponse.class);
            } catch (JsonSyntaxException ex) {
                ex.printStackTrace();
            }
            if (response != null) {
                return new Result(null, response);
            }
            return new Result(Error.DATA);
        }

        return new Result(Error.NETWORK);
    }

    public Result deleteAlbum(HashMap<String, String> filesId) {
        UserResponse response = null;
        if (!Utils.isOnline(mContext)) {
            return new Result(Error.OFFLINE);
        }
        Map<String, String> params = new HashMap<>();
        params.put("album_id", App.gson().toJson(filesId));
        Http.Request request = Http.request(mContext)
                .url("http://step4work.com/api/talent/album/deleteAlbum")
                .post(params)
                .tag(GALLERY_DELETE_ALBUM_TAG);
        Result result = request.send();
        if (result.isSuccess()) {
            String responseText = (String) result.getResult();
            try {
                response = App.gson().fromJson(responseText, UserResponse.class);
            } catch (JsonSyntaxException ex) {
                ex.printStackTrace();
            }
            if (response != null) {
                return new Result(null, response);
            }
            return new Result(Error.DATA);
        }

        return new Result(Error.NETWORK);
    }
    public Result deleteFiles(HashMap<String, String> filesId) {
        UserResponse response = null;
        if (!Utils.isOnline(mContext)) {
            return new Result(Error.OFFLINE);
        }
        Map<String, String> params = new HashMap<>();
        params.put("fileID", App.gson().toJson(filesId));
        Http.Request request = Http.request(mContext)
                .url("http://step4work.com/api/talent/deleteTalentFiles")
                .post(params)
                .tag(GALLERY_DELETE_FILE_TAG);
        Result result = request.send();
        if (result.isSuccess()) {
            String responseText = (String) result.getResult();
            try {
                response = App.gson().fromJson(responseText, UserResponse.class);
            } catch (JsonSyntaxException ex) {
                ex.printStackTrace();
            }
            if (response != null) {
                return new Result(null, response);
            }
            return new Result(Error.DATA);
        }

        return new Result(Error.NETWORK);
    }
}
