package com.dojob.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dojob.Additions.CountryData;
import com.dojob.R;

import java.util.ArrayList;

public class CountrysAdapter extends RecyclerView.Adapter<CountrysAdapter.CommentHolder> {

    private Context mContext;
    private ArrayList<CountryData> dataArrayList;

    public CountrysAdapter(Context context, ArrayList<CountryData> dataArrayList) {
        mContext = context;
        this.dataArrayList = dataArrayList;
    }

    @NonNull
    @Override
    public CountrysAdapter.CommentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.row_item_country, parent, false);
//        if (inflater != null) {
//            RowItemCountryBinding binding = DataBindingUtil.inflate(
//                    inflater, R.layout.row_item_country, parent, false);
//            return new CommentHolder(binding);
//        }
        return new CommentHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentHolder holder, int position) {
        CountryData countryData = dataArrayList.get(position);
        if (countryData != null) {
//            holder.bind(countryData);
            holder.tvname.setText(countryData.name);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        if (dataArrayList != null) {
            return dataArrayList.size();
        }
        return 0;
    }


    class CommentHolder extends RecyclerView.ViewHolder {
//        RowItemCountryBinding binding1;

        TextView tvname;

        CommentHolder(View item) {
            super(item);
//            binding1 = binding;
            tvname = item.findViewById(R.id.tvname);
        }

//        private void bind(CountryData countryData) {
//            binding1.setCountrydata(countryData);
//            binding1.executePendingBindings();
//
//        }

//        CommentHolder(RowItemCountryBinding binding) {
//            super(binding.getRoot());
//            binding1 = binding;
//        }
//
//        private void bind(CountryData countryData) {
//            binding1.setCountrydata(countryData);
//            binding1.executePendingBindings();
//
//        }
    }
}
