package com.dojob.view;

import android.content.Intent;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import com.dojob.R;
import com.dojob.activity.IntroActivity;
import com.dojob.activity.MainActivity;
import com.dojob.backend.LoginApi;
import com.dojob.backend.RegisterApi;
import com.dojob.backend.model.LoginResponse;
import com.dojob.core.Authentication;
import com.dojob.core.BackgroundWorker;
import com.dojob.core.Logger;
import com.dojob.core.Result;
import com.dojob.util.AppSP;
import com.dojob.util.ResourcesUtil;
import com.dojob.util.Utils;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.firebase.auth.FirebaseUser;

public class EmailLoginDialog extends AppCompatDialogFragment implements View.OnClickListener, Authentication.Listener, BackgroundWorker.Callbacks {

    private final String TAG = "EmailLoginDialog";
    private View mClose;
    private AppCompatButton mCreateProfileButton;
    private Listener mListener;
    private BackgroundWorker mWorker;
    private LoginApi mLoginApi;
    private TextView tvforgotPass;
    private TextView textEmail, textPassword;
    private EditText email, password;
    private String mRole;
    private AppCompatButton mGoogleButton;
    private AppCompatButton mFacebookButton;
    private String idToken;
    private AlertDialog progressDialog;
    private int normalLogin = 1;
    private SpinKitView mProgress;

    public static EmailLoginDialog newInstance(Bundle args) {
        EmailLoginDialog fragment = new EmailLoginDialog();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            fragment.setStyle(AppCompatDialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Material_Light_Dialog_NoActionBar);
        } else {
            fragment.setStyle(AppCompatDialogFragment.STYLE_NO_TITLE, android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar);
        }
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getActivity() != null) {
            mWorker = new BackgroundWorker(getActivity());
            mLoginApi = (LoginApi) mWorker.get(LoginApi.LOGIN_TAG);
            if (mLoginApi == null) {
                mLoginApi = new LoginApi(getContext());
                mWorker.put(LoginApi.LOGIN_TAG, mLoginApi);
            }
            if (getArguments() != null) mRole = getArguments().getString("role", "talent");


        }
        Authentication.init(getActivity(), this);
        View view = inflater.inflate(R.layout.dialog_login_email, container);
        Window window = getDialog().getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }
        mProgress = view.findViewById(R.id.progress);
        mProgress.setVisibility(View.VISIBLE);
        mFacebookButton = view.findViewById(R.id.login_facebook_button);
        Drawable fbBackgroundDrawable = DrawableCompat.wrap(mFacebookButton.getBackground()).mutate();
        DrawableCompat.setTint(fbBackgroundDrawable, ResourcesUtil.getColor(R.color.fb_blue));
        mGoogleButton = view.findViewById(R.id.login_google_button);
        Drawable googleBackgroundDrawable = DrawableCompat.wrap(mGoogleButton.getBackground()).mutate();
        DrawableCompat.setTint(googleBackgroundDrawable, ResourcesUtil.getColor(R.color.google_red));
        mCreateProfileButton = view.findViewById(R.id.login_create_profile_button);
        mClose = view.findViewById(R.id.close);
        email = view.findViewById(R.id.login_email);
        textEmail = view.findViewById(R.id.text_email);
        textPassword = view.findViewById(R.id.text_password);
        password = view.findViewById(R.id.login_password);

        tvforgotPass = (TextView) view.findViewById(R.id.tvforgotPass);
        tvforgotPass.setPaintFlags(tvforgotPass.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mCreateProfileButton != null) {
            mCreateProfileButton.setOnClickListener(this);
        }
        if (mClose != null) {
            mClose.setOnClickListener(this);
        }
        if (tvforgotPass != null) {
            tvforgotPass.setOnClickListener(this);
        }
        if (mFacebookButton != null) {
            mFacebookButton.setOnClickListener(this);
        }
        if (mGoogleButton != null) {
            mGoogleButton.setOnClickListener(this);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mCreateProfileButton != null) {
            mCreateProfileButton.setOnClickListener(null);
        }
        if (mClose != null) {
            mClose.setOnClickListener(null);
        }
        if (tvforgotPass != null) {
            tvforgotPass.setOnClickListener(null);
        }
        if (mFacebookButton != null) {
            mFacebookButton.setOnClickListener(null);
        }
        if (mGoogleButton != null) {
            mGoogleButton.setOnClickListener(null);
        }
        Utils.hideKeyBoard(getActivity(),getView());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Utils.hideKeyBoard(getActivity(),getView());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_create_profile_button:
                if (mWorker != null) {
                    mWorker.executeNewTask(LoginApi.LOGIN_TAG, null, this);
                    normalLogin = 1;
                }
                break;
            case R.id.close:
                dismiss();
                break;
            case R.id.tvforgotPass:
                dismiss();
                if (mListener != null) {
                    mListener.onOpenEmailForgotPassDialog(mRole);
                }
                break;
            case R.id.login_google_button:
                if (getActivity() != null && !getActivity().isFinishing()) {
                    normalLogin = 0;
                    Utils.isFromLogin = 1;
                    Authentication.google(getActivity());
                }

                break;
            case R.id.login_facebook_button:
                if (getActivity() != null && !getActivity().isFinishing()) {
                    normalLogin = 0;
                    Utils.isFromLogin = 1;
                    Authentication.facebook(getActivity());
                }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Authentication.onActivityResult(requestCode, resultCode, data);
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }

    @Override
    public void onComplete(FirebaseUser user, String accessToken) {
        Log.d(TAG, "\nname:" + user.getDisplayName() + "\nemail:" + user.getEmail() + "\nphote:" + user.getPhotoUrl());
        Log.d(TAG, "accessToken:" + accessToken);
        this.idToken = accessToken;
        if (!accessToken.isEmpty()) {
            showLoadingProgress();
            if (mWorker != null) mWorker.executeNewTask(LoginApi.LOGIN_TAG, null, this);
        }

    }

    @Override
    public void onError(Exception e) {
        dismissLoadingProgress();
        e.printStackTrace();
    }

    @Override
    public Result executeTaskInBackground(String id, Bundle args) {
        switch (id) {
            case LoginApi.LOGIN_TAG:
                if (normalLogin == 1) {
                    showProgressView();
                    showHideErrorView();
                    return mLoginApi.request(email.getText().toString(), password.getText().toString(), mRole, Utils.getDeviceToken(getActivity()));
                } else {
                    showProgressView();
                    showHideErrorView();
                    return mLoginApi.request(this.idToken, mRole, Utils.getDeviceToken(getActivity()));
                }
        }
        return null;
    }

    @Override
    public void onBackgroundTaskCompleted(String id, Result result) {

        switch (id) {
            case LoginApi.LOGIN_TAG:
                mProgress.setVisibility(View.GONE);
                if (result.isSuccess()) {
                    LoginResponse response = (LoginResponse) result.getResult();
                    Log.d("EmailLogin.status", "" + response.status + "," + response.status.equals("ok"));
                    Log.d("EmailLogin.token", "" + response.token);
                    Log.d("EmailLogin.mListener", "" + mListener);

                    textEmail.setVisibility(View.GONE);
                    textPassword.setVisibility(View.GONE);
                    if (response.status.equals("ok")) {
                        Utils.isRefreshRequired = 1;
                        if (normalLogin == 1) {
                            ((MainActivity) this.getActivity()).switchSignUpButton((response.data.verified == 1));
                            AppSP sp = AppSP.getInstance(this.getActivity());
                            sp.savePreferences(Utils.USER_ROLE,response.data.position);
                            sp.savePreferences(Utils.ANNOUNCEMENT_BIGGEST_ID,response.data.announeMentID);
//                        Log.d("EmailLogin.open", "open");
                            if (response.data.verified == 1) {
                                ((MainActivity) this.getActivity()).refresh();
                            } else if (mListener != null) mListener.onOpenVerification();
                        } else {
                            if (response.token != null && response.token.length() > 0 && response.data.verified == 1) {
                                RegisterApi.saveToken(this.getActivity(), response.token);
                                AppSP sp = AppSP.getInstance(this.getActivity());
                                sp.savePreferences(Utils.USER_ROLE,response.data.position);
                                sp.savePreferences(Utils.ANNOUNCEMENT_BIGGEST_ID,response.data.announeMentID);
                            }

                            ((MainActivity) this.getActivity()).switchSignUpButton(true);
                            if (mListener != null)
                                ((MainActivity) this.getActivity()).refresh();

                        }
                        if (response.data.setup_profile != 0) {
                            if (!response.data.isTutorialShown.equalsIgnoreCase("1")) {
                                Intent intent1 = new Intent(getActivity(), IntroActivity.class);
                                IntroActivity.isSetting = 0;
                                getActivity().startActivity(intent1);
                            }
                        }
                        Utils.hideKeyBoard(getActivity(), getView());
                        dismiss();
                    } else {
                        try {
                            String error = response.error;
                            textPassword.setText(error);
                            textPassword.setVisibility(View.VISIBLE);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                Utils.isFromLogin = 0;
                dismissLoadingProgress();
                break;
        }
    }

    private void showProgressView(){
        if (getActivity()!= null){
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mProgress.setVisibility(View.VISIBLE);
                }
            });
        }

    }

    private void showHideErrorView(){
        if (getActivity()!= null){
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    textPassword.setVisibility(View.GONE);
                }
            });
        }
    }


//    @Override
//    public void onComplete(FirebaseUser user, String accessToken) {
//        Log.d(TAG, "\nname:"+user.getDisplayName()+"\nemail:"+user.getEmail()+"\nphote:"+user.getPhotoUrl());
//        Log.d(TAG, "accessToken:"+accessToken);
//        this.idToken = accessToken;
//        if(!accessToken.isEmpty()){
//            showLoadingProgress();
//            if(mWorker != null) mWorker.executeNewTask(LoginApi.LOGIN_TAG, null, this);
//        }
//    }
//
//    @Override
//    public void onError(Exception e) {
//        dismissLoadingProgress();
//        e.printStackTrace();
//    }
//    @Override
//    public Result executeTaskInBackground(String id, Bundle args) {
//        switch (id){
//            case LoginApi.LOGIN_TAG:
//                if (normalLogin ==1){
//                    return mLoginApi.request(email.getText().toString(), password.getText().toString(), mRole);
//                }else{
//                    return mLoginApi.request(this.idToken, mRole);
//                }
//        }
//        return null;
//    }
//
//    @Override
//    public void onBackgroundTaskCompleted(String id, Result result) {
//        switch (id){
//            case LoginApi.LOGIN_TAG:
//                if(result.isSuccess() ){
//                    LoginResponse response = (LoginResponse) result.getResult();
//                    Log.d("EmailLogin.status", ""+response.status+","+response.status.equals("ok"));
//                    Log.d("EmailLogin.token", ""+response.token );
//                    Log.d("EmailLogin.mListener", ""+mListener);
//
//                    textEmail.setVisibility(View.GONE);
//                    textPassword.setVisibility(View.GONE);
//                    if (response.status.equals("ok")){
//                        ((MainActivity)this.getActivity()).switchSignUpButton((response.data.verified == 1));
////                        Log.d("EmailLogin.open", "open");
//                        if(response.data.verified == 1) {
//                            if(mListener != null) mListener.onOpenProfileSetting();
//                        } else if(mListener != null) mListener.onOpenVerification();
//                        dismiss();
//                    }
//                    else{
//                        try {
//                            String error = response.error;
//                            textPassword.setText(error);
//                            textPassword.setVisibility(View.VISIBLE);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//                dismissLoadingProgress();
//                break;
//        }
//    }

    public interface Listener {
        void onOpenProfileSetting();

        void onOpenVerification();

        void onOpenEmailForgotPassDialog(String role);
    }

    private void showLoadingProgress() {
        if (getActivity() != null) {
            AlertDialog.Builder progressDialogBuilder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.layout_loading, null);
            progressDialogBuilder.setView(dialogView);
            progressDialog = progressDialogBuilder.create();
            progressDialog.show();
        }
    }

    private void dismissLoadingProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }
}
