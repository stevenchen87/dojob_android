package com.dojob.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.dojob.adapter.model.Devider;
import com.dojob.adapter.model.InboxChatOther;
import com.dojob.adapter.model.InboxChatOwner;
import com.dojob.adapter.model.InboxItem;

public class InboxVIewHolder extends RecyclerView.ViewHolder {

    public InboxItem.ViewHolder inboxItem;
    public InboxChatOwner.ViewHolder owner;
    public InboxChatOther.ViewHolder other;
    public Devider.ViewHolder devider;

    public InboxVIewHolder(View itemView) {
        super(itemView);
        inboxItem = new InboxItem.ViewHolder(itemView);
        owner = new InboxChatOwner.ViewHolder(itemView);
        other = new InboxChatOther.ViewHolder(itemView);
        devider = new Devider.ViewHolder(itemView);
    }
}
