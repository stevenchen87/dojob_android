package com.dojob.Additions;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dojob.R;
import com.dojob.backend.model.ProfileResponse;
import com.dojob.core.App;
import com.dojob.core.Logger;
import com.dojob.fragment.FeedFragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.dojob.util.Utils.dpToPx;

public class CategoryFragment  extends Fragment {

    public static String CATEGORY_KEY = "cate";
    public static String UB_CAT_KEY = "sub_cat";

    @BindView(R.id.category_recycle_view)
    RecyclerView recyclerView;


    private Unbinder unbinder;

    private List<CategoryWIthID> catList;

    private List<SubCategory> subCategoryList;

    private List<Object> allCatList;

    private Handler handler;
    private HashMap<String, String> jobCategory;
    private ProfileCategoryAdapter categoryAdapter;
    private SubCategory[] subCategoryArrayList;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_category, container, false);
        unbinder = ButterKnife.bind(this, rootView);

        catList = new ArrayList<>();

        subCategoryList = new ArrayList<>();

        allCatList = new ArrayList<>();

        handler = new Handler();

        categoryAdapter = new ProfileCategoryAdapter(getActivity(), allCatList,getActivity());

        if (getArguments() != null) {
            jobCategory = (HashMap<String, String>) getArguments().getSerializable(CATEGORY_KEY);
            subCategoryArrayList = App.gson().fromJson(getArguments().getString(UB_CAT_KEY), SubCategory[].class);

            LinearLayoutManager verticalLayoutManager = new LinearLayoutManager(getContext());

            recyclerView.setLayoutManager(verticalLayoutManager);

            recyclerView.setItemAnimator(new DefaultItemAnimator());

            recyclerView.addItemDecoration(new FeedFragment.GridSpacingItemDecoration(1, 0, true));

            recyclerView.setAdapter(categoryAdapter);

            handler.post(new Task1());
        }
        return  rootView;


    }
    class Task1 implements Runnable{
        public void run(){
            //perform task 1
            if (jobCategory!=null){
//                for (int i=1;i<=jobCategory.size();i++){
//                    String value = jobCategory.get(String.valueOf(i));
//                    catList.add(new CategoryWIthID(i,value));
//                }
                Iterator it = jobCategory.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry)it.next();
                    System.out.println(pair.getKey() + " = " + pair.getValue());
                    String value  = (String) pair.getValue();
                    int key = Integer.parseInt(pair.getKey().toString());
                    catList.add(new CategoryWIthID(key,value));
                }
            }

            handler.postDelayed(new Task2(),10);
        }
    }

    class Task2 implements Runnable{
        public void run(){
            //perform task 2
            if (subCategoryArrayList != null)
                subCategoryList = Arrays.asList(subCategoryArrayList);

            Collections.sort(subCategoryList, new Comparator<SubCategory>()
            {
                @Override
                public int compare(SubCategory sub1, SubCategory sub2) {
                    return sub1.name.compareToIgnoreCase(sub2.name);
                }
            });

            Collections.sort(catList, new Comparator<CategoryWIthID>()
            {
                @Override
                public int compare(CategoryWIthID cat1, CategoryWIthID cat2) {
                    if (cat1 == null ||cat2 == null || cat1.getTitle() == null || cat2.getTitle() == null){
                        Log.d("","Object Type "+cat1);
                        return 1;
                    }

                    return cat1.getTitle().compareToIgnoreCase(cat2.getTitle());
                }
            });

            for(CategoryWIthID cat: catList){
                allCatList.add(cat);
                for(SubCategory sub: subCategoryList){
                    if (sub.category_id == cat.getID()) {
                        allCatList.add(sub);
                    }
                }
            }
            categoryAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
