package com.dojob.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.dojob.adapter.model.AlbumsItem;
import com.dojob.adapter.model.FeedItem;
import com.dojob.adapter.model.NewVideosItem;
import com.dojob.adapter.model.PortfolioEmpty;
import com.dojob.adapter.model.PortfolioRecruiterItem;
import com.dojob.adapter.model.PortfolioReviewItem;
import com.dojob.adapter.model.SearchItem;
import com.dojob.adapter.model.VideosItem;

public class PortfolioOverflowViewHolder extends RecyclerView.ViewHolder {

    public FeedItem.ViewHolder feeds;
    public PortfolioReviewItem.ViewHolder review;
    public AlbumsItem.ViewHolder albums;
    public SearchItem.ViewHolder search;
    public PortfolioRecruiterItem.ViewHolder recruiter;
    public NewVideosItem.ViewHolder videos;


    public PortfolioOverflowViewHolder(View itemView) {
        super(itemView);
        review = new PortfolioReviewItem.ViewHolder(itemView);
        albums = new AlbumsItem.ViewHolder(itemView);
        feeds = new FeedItem.ViewHolder(itemView);
        search = new SearchItem.ViewHolder(itemView);
        recruiter = new PortfolioRecruiterItem.ViewHolder(itemView);
        videos = new NewVideosItem.ViewHolder(itemView);

    }
}