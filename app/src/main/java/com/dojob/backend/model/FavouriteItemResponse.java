package com.dojob.backend.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 30-Nov-18.
 */
public class FavouriteItemResponse {

    public int id;
    public int user_id;
    public int favourite_id;
    public FavouriteUserResponse fav_user_data;
    public boolean isSelected;

}
