package com.dojob.adapter;

import android.app.Activity;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dojob.R;
import com.dojob.activity.MainActivity;
import com.dojob.adapter.holder.FeedViewHolder;
import com.dojob.adapter.model.FeedItem;
import com.dojob.adapter.model.FeedTitle;
import com.dojob.adapter.model.LoadingFooter;
import com.dojob.adapter.model.MainSearch;
import com.dojob.adapter.model.PortfolioOverflowScroll;
import com.dojob.adapter.model.SearchItem;
import com.dojob.backend.model.CategoryResponse;
import com.dojob.backend.model.FeedItemResponse;
import com.dojob.backend.model.FeedsResponse;
import com.dojob.backend.model.GalleryAlbumsItemResponse;
import com.dojob.backend.model.Review;
import com.dojob.backend.model.SearchResponse;
import com.dojob.core.Logger;

import java.util.ArrayList;
import java.util.List;

public class FeedAdapter extends RecyclerView.Adapter<FeedViewHolder> implements View.OnClickListener, TextView.OnEditorActionListener, PortfolioOverflowScroll.Listener{
    private static final int TYPE_TITLE = 1;
    private static final int TYPE_ITEM = 2;
    private static final int TYPE_SEARCH_BAR = 3;
    private static final int TYPE_FOOTER = 4;
    private static final int TYPE_OVERFLOW_SCROLL = 5;
    private static final int TYPE_SEARCH_ITEM = 6;
    private String SEARCH_PAGE_FRAGMENT_TAG = "search_page_fragment";
    private Activity mActivity;
    private List<Object> mItems = new ArrayList<>();


    private FeedsResponse mFeedsResponse;
    private Activity mListener;
    private LoadingFooter mLoadingFooter;
    private SearchResponse mSearchResponse;
    private CategoryResponse mCategoryResponse;
    private FragmentManager mFragmentManager;
    private int mType;
    private String feedTitle;



    public FeedAdapter(Activity activity, FragmentManager fragmentManager, int mType, @Nullable String feedTitle){
        mActivity = activity;
        this.mFragmentManager = fragmentManager;
        this.mType = mType;
        this.feedTitle = feedTitle;

    }

    public void updateItem(FeedsResponse response){
        mFeedsResponse = response;
        updateItem();
    }

    public void updateTalent(FeedsResponse respnose) {
        if(respnose != null && respnose.talent != null && respnose.talent.length > 0){
            if(mItems.size() > 0){
                mItems.remove(mLoadingFooter);
            }

            for(FeedItemResponse item :respnose.talent){
                mItems.add(new FeedItem(mActivity, FeedItem.TOP_PICK, item));
            }

            if(respnose.currentPage < respnose.lastPage){
                mLoadingFooter = new LoadingFooter(LoadingFooter.TYPE_LOADING);
                mItems.add(mLoadingFooter);
            }
        }
        notifyDataSetChanged();
    }

    public void updateItem(SearchResponse response){
        mItems.clear();
        mSearchResponse = response;
        updateItem();
//        if(searchTalentsResponse != null && searchTalentsResponse.length > 0){
////            if(mItems.size() > 0 && mItems.contains(mLoadingFooter)){
////                mItems.remove(mLoadingFooter);
////            }
//            PortfolioOverflowScroll feedAdapter = new PortfolioOverflowScroll(mActivity, mFeedsResponse.hot);
//            feedAdapter.setListener(this);
//            mItems.add(feedAdapter);
//            for(FeedItemResponse item : response){
//                mItems.add(new SearchItem(mActivity.getApplicationContext(), SearchItem.SEARCH_RESULT, item));
//            }
////            if(nextPage){
////                mLoadingFooter = new LoadingFooter();
////                mItems.add(mLoadingFooter);
////            }
//
//        }
//        notifyDataSetChanged();
    }



    public void updateCategory(CategoryResponse response){
        mCategoryResponse = response;
    }

    private void updateItem(){
        mItems.clear();
        if (mType==0) mItems.add(new MainSearch());
        if(mFeedsResponse != null && mFeedsResponse.hot.length>0){
            mItems.add(new FeedTitle(FeedTitle.HOTTEST_TALENT));
            PortfolioOverflowScroll feedAdapter = new PortfolioOverflowScroll(mActivity, PortfolioOverflowScroll.FEED_TYPE, mFeedsResponse.hot);
            feedAdapter.setListener(this);
            mItems.add(feedAdapter);
//            for(FeedItemResponse item :mFeedsResponse.hot){
//                mItems.add(new FeedItem(FeedItem.HOTTEST_TALENT, item));
//            }
        }

        if(mFeedsResponse != null && mFeedsResponse.top.length>0){
            mItems.add(new FeedTitle(FeedTitle.TOP_PICK));
            for(FeedItemResponse item :mFeedsResponse.top){
                mItems.add(new FeedItem(mActivity, FeedItem.TOP_PICK, item));
            }

            mItems.add(new LoadingFooter(LoadingFooter.TYPE_MORE));
        }

        if(mSearchResponse != null){
            FeedItemResponse[] searchTalentsResponse = mSearchResponse.nameSearch;
            if(searchTalentsResponse != null && searchTalentsResponse.length > 0){
                mItems.add(new FeedTitle(FeedTitle.SEARCH_TALENT));
                for(FeedItemResponse item  : searchTalentsResponse){
                    mItems.add(new SearchItem(mActivity, SearchItem.SEARCH_RESULT, item));
                }
            }
            if(mCategoryResponse != null && mCategoryResponse.catData.length>0){
                /*mItems.add(new FeedTitle(FeedTitle.SEARCH_CATEGORY));*/
                PortfolioOverflowScroll feedAdapter = new PortfolioOverflowScroll(mActivity, PortfolioOverflowScroll.FEED_TYPE, mFeedsResponse.hot);
                feedAdapter.setListener(this);
                mItems.add(feedAdapter);
                /*for(FeedItemResponse item :mFeedsResponse.hot){
                    mItems.add(new FeedItem(mActivity, FeedItem.TOP_PICK, item));
                }*/
//            for(FeedItemResponse item :mFeedsResponse.hot){
//                mItems.add(new FeedItem(FeedItem.HOTTEST_TALENT, item));
//            }
            }

            FeedItemResponse[] searchCategoriesResponse = mSearchResponse.categorySearch;
            if(searchCategoriesResponse != null && searchCategoriesResponse.length > 0){
                if(feedTitle == null || feedTitle.isEmpty()){
                    feedTitle = "Category";
                }
                mItems.add(new FeedTitle(FeedTitle.SEARCH_CATEGORY, feedTitle));
                PortfolioOverflowScroll feedAdapter = new PortfolioOverflowScroll(mActivity,  PortfolioOverflowScroll.SEARCH_TYPE, searchCategoriesResponse);
                feedAdapter.setListener(this);
                for(FeedItemResponse item :mSearchResponse.categorySearch){
                    mItems.add(new FeedItem(mActivity, FeedItem.TOP_PICK, item));
                }
                /*mItems.addAll(feedAdapter);*/
            }
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public FeedViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        int layout = 0;
        switch (viewType){
            case TYPE_TITLE:
                layout = FeedTitle.LAYOUT_ID;
                break;
            case TYPE_ITEM:
                layout = FeedItem.LAYOUT_ID;
                break;
            case TYPE_SEARCH_BAR:
                layout = MainSearch.LAYOUT_ID;
                break;
            case TYPE_FOOTER:
                layout = LoadingFooter.LAYOUT_ID;
                break;
            case TYPE_OVERFLOW_SCROLL:
                layout = PortfolioOverflowScroll.LAYOUT_ID;
                break;
            case TYPE_SEARCH_ITEM:
                layout = SearchItem.LAYOUT_ID;
                break;
        }
        View view = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        final FeedViewHolder holder = new FeedViewHolder(view);

        if(holder.item.clickable != null){
            holder.item.clickable.setOnClickListener(this);
        }
        if(holder.footer.viewMore != null){
            holder.footer.viewMore.setOnClickListener(this);
        }
        if(holder.searchItem.clickable != null){
            holder.searchItem.clickable.setOnClickListener(this);
        }
        if(holder.search.container != null){
            holder.search.container.setOnEditorActionListener(this);
        }
        if (holder.search.container!=null){
            Fragment searchFragment = mFragmentManager.findFragmentByTag(MainActivity.PRE_SEARCH_PAGE_FRAGMENT_TAG);
            if(searchFragment == null) {
                holder.search.container.setFocusableInTouchMode(false);
                holder.search.container.setCursorVisible(false);
                holder.search.container.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //Changing things here
                        /*EditText v = holder.search.container;
                        String query = v.getText().toString();
                        v.setText(null);
                        ((MainActivity)mListener).openSearchPage(query);*/
                        ((MainActivity)mListener).openNewFragment();
                    }
                });
            }
        }
        return holder;
    }
    public static void changeMainFragmentWithBack(FragmentActivity fragmentActivity, Fragment fragment) {

        fragmentActivity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content, fragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onBindViewHolder(@NonNull FeedViewHolder holder, int position) {
        holder.setIsRecyclable(false);
        Object item = getItem(position);
        int type = getItemViewType(position);
        switch (type){
            case TYPE_TITLE:
                ((FeedTitle)item).bindViewHolder(holder.title, position);
                break;
            case TYPE_ITEM:
                ((FeedItem)item).bindViewHolder(holder.item, position);
                break;
            case TYPE_SEARCH_ITEM:
                ((SearchItem)item).bindViewHolder(holder.searchItem, position);
                break;
            case TYPE_SEARCH_BAR:
                ((MainSearch)item).bindViewHolder(holder.search, position);
                break;
            case TYPE_FOOTER:
                ((LoadingFooter)item).bindViewHolder(holder.footer, position);
                break;
            case TYPE_OVERFLOW_SCROLL:
                ((PortfolioOverflowScroll)item).bindViewHolder(holder.overFlowScroll, position);
                break;}

    }

    @Override
    public int getItemViewType(int position) {
        Object item = getItem(position);
        if (item instanceof FeedTitle) {
            return TYPE_TITLE;
        } else if (item instanceof FeedItem) {
            return TYPE_ITEM;
        } else if (item instanceof SearchItem) {
            return TYPE_SEARCH_ITEM;
        } else if (item instanceof MainSearch) {
            return TYPE_SEARCH_BAR;
        } else if(item instanceof LoadingFooter){
            return TYPE_FOOTER;
        } else if (item instanceof PortfolioOverflowScroll) {
            return TYPE_OVERFLOW_SCROLL;
        }
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        if (mItems != null) {
            return mItems.size();

        }
        return 0;
    }

    @Override
    public void onClick(View view) {
        Log.d("Feed Adapter", "clicked"+ view);
        Object tag = view.getTag();
        if (tag == null) {
            return;
        }
        final Object item = getItem((int) view.getTag());
        if(item instanceof FeedItem) {
            FeedItem feedItem = (FeedItem) getItem((int) view.getTag());
            FeedItemResponse feedItemResponse = feedItem.getData();
            Log.d("Feed Adapter", "feedItemResponse:" + feedItemResponse.toString());
            ((MainActivity) mListener).onOpenPreviewProfilePage(feedItemResponse.url);
        }
        else if(item instanceof SearchItem){
            SearchItem searchItem = (SearchItem) getItem((int) view.getTag());
            FeedItemResponse feedItemResponse = searchItem.getData();
            Log.d("Feed Adapter", "feedItemResponse:"+ feedItemResponse.toString());
            ((MainActivity)mListener).onOpenPreviewProfilePage(feedItemResponse.url);
        }
        else if(item instanceof LoadingFooter){
            ((MainActivity)mListener).onOpenAllTalent();
        }
    }

    public Object getItem(int position) {
        int size = mItems.size();
        if (size > 0 && position >= 0 && position < size) {
            return mItems.get(position);
        }

        return null;
    }
    public void setListener(Activity activity){
        mListener = activity;
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        String query = v.getText().toString();
        v.clearFocus();
        v.setText(null);
        ((MainActivity)mListener).searchOnlySecond(query);
        return false;
    }

    @Override
    public void onAlbumClick(GalleryAlbumsItemResponse album) {

    }

    @Override
    public void onReviewClick(Review review) {

    }

    @Override
    public void onFeedItemClick(String url) {
        ((MainActivity)mListener).onOpenPreviewProfilePage(url);
    }
}
