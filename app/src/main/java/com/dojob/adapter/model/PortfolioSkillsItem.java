package com.dojob.adapter.model;

import android.view.View;
import android.widget.TextView;

import com.dojob.R;
import com.dojob.backend.model.SkillResponse;

public class PortfolioSkillsItem {

    public static final int LAYOUT_ID = R.layout.layout_portfolio_skills_item;
    private SkillResponse response;

    public PortfolioSkillsItem(SkillResponse skill) {
        response = skill;
    }

    public void bindViewHolder(ViewHolder holder, int position){
        if(holder.skill != null){
            holder.skill.setText(response.skill);
        }
    }

    public static class ViewHolder{
        public TextView skill;

        public ViewHolder(View view){
            skill = view.findViewById(R.id.portfolio_skills_item);
        }
    }
}
