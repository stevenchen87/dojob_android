package com.dojob.backend.model;

public class CommentsInboxResponse {

    public int id;
    public int user_id;
    public int post_id;
    public String msg_body;
    public long updated_at;
    public String name;
    public boolean vip;
    public String profile_image;
    public Comment[] comments;
}
