package com.dojob.core;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.danikula.videocache.HttpProxyCacheServer;
import com.dojob.util.AppSP;
import com.dojob.util.ResourcesUtil;
import com.dojob.util.Utils;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.FirebaseApp;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.orm.SugarApp;
import com.squareup.picasso.LruCache;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.util.concurrent.TimeUnit;

import me.leolin.shortcutbadger.ShortcutBadger;
import okhttp3.OkHttpClient;

public class App extends SugarApp {
    private static Gson sGson;
    private static Picasso sPicasso;
    private static Context mContext;


    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this.getApplicationContext();
        ResourcesUtil.init(getApplicationContext());
        FirebaseApp.initializeApp(getApplicationContext());
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        MultiDex.install(this);

        AppSP.getInstance(this).savePreferences(Utils.NOFICATION_NUMBER, 0);
        ShortcutBadger.removeCount(this);
    }

    private HttpProxyCacheServer proxy;

    public static HttpProxyCacheServer getProxy(Context context) {
        App app = (App) context.getApplicationContext();
        return app.proxy == null ? (app.proxy = app.newProxy()) : app.proxy;
    }

    private HttpProxyCacheServer newProxy() {
        return new HttpProxyCacheServer(this);
    }
    public static Gson gson() {
        if (sGson == null) {
            sGson = new GsonBuilder().create();
        }
        return sGson;
    }

    public static Picasso picasso() {
        if (sPicasso == null) {
            OkHttpClient.Builder okhttpBuilder = new OkHttpClient.Builder()
                    .connectTimeout(20, TimeUnit.SECONDS)
                    .readTimeout(20, TimeUnit.SECONDS)
                    .writeTimeout(20, TimeUnit.SECONDS);
            Picasso.Builder builder = new Picasso.Builder(mContext)
                    .downloader(new OkHttp3Downloader(okhttpBuilder.build()))
                    .memoryCache(new LruCache(mContext));
            sPicasso = builder.build();
        }
        return sPicasso;
    }
}
