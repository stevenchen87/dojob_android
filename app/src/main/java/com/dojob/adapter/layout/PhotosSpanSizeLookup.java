package com.dojob.adapter.layout;

import android.support.v7.widget.GridLayoutManager;

import com.dojob.adapter.GalleryTabAdapter;
import com.dojob.adapter.model.PhotosAddBar;
import com.dojob.adapter.model.PhotosItem;
import com.dojob.adapter.model.VideosItem;

public class PhotosSpanSizeLookup extends GridLayoutManager.SpanSizeLookup {
    private GalleryTabAdapter mAdapter;

    public PhotosSpanSizeLookup(GalleryTabAdapter adapter) {
        mAdapter = adapter;
    }

    @Override
    public int getSpanSize(int position) {
        Object item = mAdapter.getItem(position);
        if(item instanceof PhotosAddBar){
            return 3;
        }
        else if(item instanceof PhotosItem || item instanceof VideosItem){
            return 1;
        }
        return 3;
    }
}
