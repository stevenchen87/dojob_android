package com.dojob.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.dojob.Additions.ApiInterface;
import com.dojob.Additions.ServiceGenerator;
import com.dojob.Additions.SpacesItemDecoration;
import com.dojob.Additions.local.SocialFeed;
import com.dojob.Additions.local.SocialFeedParent;
import com.dojob.Additions.responses.WebResponseSocialFeed;
import com.dojob.R;
import com.dojob.activity.MainActivity;
import com.dojob.adapter.SocialFeedAdapter2;
import com.dojob.listener.SocialItemClickListener;
import com.dojob.util.Utils;
import com.github.ybq.android.spinkit.SpinKitView;
import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;

import java.util.ArrayList;

import retrofit2.Callback;
import retrofit2.Response;

public class SocialFeedFragment extends Fragment implements MainActivity.ToolBarClickedCallback, SocialItemClickListener {

    private static String ALL_FILTER = "all";
    private static String IMAGE_FILTER = "image";
    private static String VIDEO_FILTER = "video";
    private static String SELECTED_FILTER = ALL_FILTER;

    private static final int TYPE_NORMAL = 0;
    private static final int TYPE_LEFT = 1;
    private static final int TYPE_RIGHT = 2;
    private static final int TURN_RIGHT = 0;
    private static final int TURN_LEFT = 1;

    private SpinKitView mProgress;

    WebResponseSocialFeed mWebResponseSocialFeed;

    private SocialFeedAdapter2 mAdapter2;
    private SuperRecyclerView mRecyclerView;
    private RadioGroup radioGroup;
    private RadioButton radioAll, radioImage, radioVideo;
    private int CurrentPAGE = 1;
    private TextView empty_content;
    private static int scrollPos = 0;
    private View rootView;

    private ArrayList<SocialFeed> socialFeedArray;
    private ArrayList<SocialFeedParent> socialFeedParentArray;

    public static SocialFeedFragment newInstance() {
        Bundle args = new Bundle();
        SocialFeedFragment fragment = new SocialFeedFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        socialFeedArray = new ArrayList<>();
        socialFeedParentArray = new ArrayList<>();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_social_feed, container, false);


//        if (rootView == null)
//        {
//            //first time creating this fragment view
//            rootView = inflater.inflate(R.layout.fragment_social_feed, container, false);
//
//            //Initialization
//            //TODO:
//        }
//        else
//        {
//            //not first time creating this fragment view
//            ViewGroup parent = (ViewGroup)rootView.getParent();
//            if (parent != null)
//            {
//                parent.removeView(rootView);
//            }
//        }

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Utils.hideKeyBoard(getActivity());
        mProgress = view.findViewById(R.id.progress);
        mProgress.setVisibility(View.GONE);
        empty_content = view.findViewById(R.id.portfolio_empty_content);
        radioGroup = view.findViewById(R.id.radioGroup);
        radioAll = view.findViewById(R.id.radioAll);
        radioImage = view.findViewById(R.id.radioImage);
        radioVideo = view.findViewById(R.id.radioVideo);
        mRecyclerView = view.findViewById(R.id.recycle_view);
        mRecyclerView.addItemDecoration(new SpacesItemDecoration(3));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter2 = new SocialFeedAdapter2(getActivity(), socialFeedParentArray, SELECTED_FILTER);
        mAdapter2.setSocialItemListner(this);
        mAdapter2.setHasStableIds(true);
        mRecyclerView.setAdapter(mAdapter2);
        empty_content.setText("Loading Data");
        mProgress.setVisibility(View.GONE);
        setPreSelectedFilter();
        radioButtonListner();
        loadingMoreAndRefresh();
        mRecyclerView.setLoadingMore(false);
        if (socialFeedArray != null && socialFeedArray.size() == 0) {
            mProgress.setVisibility(View.VISIBLE);
            callApiSocialFeed();
        } else {
            if (socialFeedParentArray.size() > scrollPos)
                mRecyclerView.smoothScrollBy(0, scrollPos);
        }
    }

    /* show the button clicked already and show the data accoriding to it*/
    private void setPreSelectedFilter() {
        if (SELECTED_FILTER.equalsIgnoreCase(ALL_FILTER)) {
            radioAll.setChecked(true);
        } else if (SELECTED_FILTER.equalsIgnoreCase(IMAGE_FILTER)) {
            radioImage.setChecked(true);
        } else if (SELECTED_FILTER.equalsIgnoreCase(VIDEO_FILTER)) {
            radioVideo.setChecked(true);
        }
    }

    private void loadingMoreAndRefresh() {
        mRecyclerView.setRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (mRecyclerView.isLoadingMore())
                    mRecyclerView.setLoadingMore(false);
                socialFeedArray.clear();
                socialFeedParentArray.clear();
                mAdapter2.resetAdapterData(socialFeedParentArray, SELECTED_FILTER);
                mAdapter2.notifyDataSetChanged();
                mRecyclerView.setRefreshing(true);
                empty_content.setText("Loading Data");
                CurrentPAGE = 1;
                callApiSocialFeed();
            }
        });

        // when there is only 10 items to see in the recycler, this is triggered
        mRecyclerView.setupMoreListener(new OnMoreListener() {
            @Override
            public void onMoreAsked(int numberOfItems, int numberBeforeMore, int currentItemPos) {

                if (mWebResponseSocialFeed != null) {
                    if (mWebResponseSocialFeed.getCurrentPage() < mWebResponseSocialFeed.getLastPage() &&
                            mWebResponseSocialFeed.getCurrentPage() == CurrentPAGE) {
                        mRecyclerView.setLoadingMore(false);
                        CurrentPAGE++;
                        callApiSocialFeedLoadMore(CurrentPAGE);
                    }
                }
            }
        }, 3);
    }

    private void radioButtonListner() {
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                changeCheck(checkedId);
                mProgress.setVisibility(View.VISIBLE);
                socialFeedArray.clear();
                socialFeedParentArray.clear();
                CurrentPAGE = 1;
                mAdapter2.resetAdapterData(socialFeedParentArray, SELECTED_FILTER);
                mAdapter2.notifyDataSetChanged();
                empty_content.setText("Loading Data");
                callApiSocialFeed();
            }
        });
    }

    private void changeCheck(int checkedId) {
        switch (checkedId) {
            case R.id.radioAll:
                SELECTED_FILTER = ALL_FILTER;
                break;
            case R.id.radioImage:
                SELECTED_FILTER = IMAGE_FILTER;
                break;
            case R.id.radioVideo:
                SELECTED_FILTER = VIDEO_FILTER;
                break;
            default:
                break;
        }
    }

    private void callApiSocialFeed() {
        ServiceGenerator serviceGenerator = new ServiceGenerator();
        ApiInterface apiInterface = serviceGenerator.createService(ApiInterface.class);
        retrofit2.Call<WebResponseSocialFeed> call = apiInterface.getSocialFeedData(SELECTED_FILTER, 1);
        call.enqueue(new Callback<WebResponseSocialFeed>() {
            @Override
            public void onResponse(retrofit2.Call<WebResponseSocialFeed> call, Response<WebResponseSocialFeed> response) {
                mProgress.setVisibility(View.GONE);
                mRecyclerView.setRefreshing(false);
                if (response.isSuccessful()) {
                    Log.i("RESPONSE", "OKAY");
                    if (response.body() != null) {
                        String status = response.body().getStatus();
                        if (status.equalsIgnoreCase("ok")) {
                            mWebResponseSocialFeed = response.body();
                            updateLocalDataArray(response.body());
                        } else {
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Log.i("RESPONSE", response.message());
                    Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<WebResponseSocialFeed> call, Throwable t) {
                Log.i("RESPONSE", t.getMessage());
                mProgress.setVisibility(View.GONE);
                mRecyclerView.setRefreshing(false);
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void callApiSocialFeedLoadMore(int pageNo) {
        ServiceGenerator serviceGenerator = new ServiceGenerator();
        ApiInterface apiInterface = serviceGenerator.createService(ApiInterface.class);
        retrofit2.Call<WebResponseSocialFeed> call = apiInterface.getSocialFeedData(SELECTED_FILTER, pageNo);
        call.enqueue(new Callback<WebResponseSocialFeed>() {
            @Override
            public void onResponse(retrofit2.Call<WebResponseSocialFeed> call, Response<WebResponseSocialFeed> response) {

                mProgress.setVisibility(View.GONE);
                mRecyclerView.setRefreshing(false);
                //  mRecyclerView.setLoadingMore(true);
                if (response.isSuccessful()) {
                    Log.i("RESPONSE", "OKAY");
                    if (response.body() != null) {
                        String status = response.body().getStatus();
                        if (status.equalsIgnoreCase("ok")) {
                            mWebResponseSocialFeed = response.body();
                            updateLocalDataArrayMore(mWebResponseSocialFeed);
                        } else {
                            Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Log.i("RESPONSE", response.message());
                    Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<WebResponseSocialFeed> call, Throwable t) {
                Log.i("RESPONSE", t.getMessage());
                mProgress.setVisibility(View.GONE);
                mRecyclerView.setRefreshing(false);
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateLocalDataArray(WebResponseSocialFeed webResponseSocialFeed) {
        socialFeedParentArray.clear();
        socialFeedArray.clear();
        if (mWebResponseSocialFeed != null) {
            ArrayList<SocialFeedParent> socialFeedParents2 = new ArrayList<>();
            int previousLength = socialFeedArray.size();
            if (mWebResponseSocialFeed.getSocialFeedImage() != null)
                socialFeedArray.addAll(mWebResponseSocialFeed.getSocialFeedImage());
            if (mWebResponseSocialFeed.getSocialFeedVideo() != null &&
                    mWebResponseSocialFeed.getSocialFeedVideo().size() > 0) {
                if (SELECTED_FILTER.equalsIgnoreCase(IMAGE_FILTER) ||
                        SELECTED_FILTER.equalsIgnoreCase(VIDEO_FILTER)) {
                    socialFeedArray.addAll(mWebResponseSocialFeed.getSocialFeedVideo());
                } else {
                    if (getTurnType(previousLength) == TURN_RIGHT) {
                        if (socialFeedArray.size() > 2)
                            socialFeedArray.add(2, mWebResponseSocialFeed.getSocialFeedVideo().get(0));
                    } else {
                        socialFeedArray.add(0, mWebResponseSocialFeed.getSocialFeedVideo().get(0));
                    }
                }
            }
            ArrayList<SocialFeed> socialFeedsArray1 = new ArrayList<>();
            for (int i = 0; i < socialFeedArray.size(); i++) {
                socialFeedsArray1.add(socialFeedArray.get(i));
                if (socialFeedsArray1.size() == 3) {
                    SocialFeedParent socialFeedParent = new SocialFeedParent();
                    socialFeedParent.setArrayList(socialFeedsArray1);
//                    socialFeedParentArray.add(socialFeedParent);
                    socialFeedParents2.add(socialFeedParent);
                    socialFeedsArray1 = new ArrayList<>();
                }
            }
            socialFeedParentArray.addAll(socialFeedParents2);
            mAdapter2.resetAdapterData(socialFeedParents2, SELECTED_FILTER);
        }

    }

    private void updateLocalDataArrayMore(WebResponseSocialFeed webResponseSocialFeed) {
        if (mWebResponseSocialFeed != null) {
            ArrayList<SocialFeedParent> socialFeedParents2 = new ArrayList<>();
            int previousLength = socialFeedArray.size();
            if (mWebResponseSocialFeed.getSocialFeedImage() != null)
                socialFeedArray.addAll(mWebResponseSocialFeed.getSocialFeedImage());

            if (mWebResponseSocialFeed.getSocialFeedVideo() != null
                    && mWebResponseSocialFeed.getSocialFeedVideo().size() > 0) {
                if (SELECTED_FILTER.equalsIgnoreCase(IMAGE_FILTER) ||
                        SELECTED_FILTER.equalsIgnoreCase(VIDEO_FILTER)) {
                    socialFeedArray.addAll(mWebResponseSocialFeed.getSocialFeedVideo());
                } else {
                    if (getTurnType(previousLength) == TURN_RIGHT) {
                        if (socialFeedArray.size() > 0)
                            socialFeedArray.add(previousLength + 2, mWebResponseSocialFeed.getSocialFeedVideo().get(0));
                    } else {
                        if (socialFeedArray.size() > 0)
                            socialFeedArray.add(previousLength, mWebResponseSocialFeed.getSocialFeedVideo().get(0));
                    }
                }
            }
            ArrayList<SocialFeed> socialFeedsArray1 = new ArrayList<>();
            for (int i = previousLength; i < socialFeedArray.size(); i++) {
                socialFeedsArray1.add(socialFeedArray.get(i));
                if (socialFeedsArray1.size() == 3) {
                    SocialFeedParent socialFeedParent = new SocialFeedParent();
                    socialFeedParent.setArrayList(socialFeedsArray1);
//                    socialFeedParentArray.add(socialFeedParent);
                    socialFeedParents2.add(socialFeedParent);
                    socialFeedsArray1 = new ArrayList<>();
                }
            }
            socialFeedParentArray.addAll(socialFeedParents2);
            mAdapter2.updateAdapterData(socialFeedParents2, SELECTED_FILTER);
        }
    }

    private int getTurnType(int position) {
        if (position == 0 || (position / 9) % 2 == 0) {
            return TURN_RIGHT;
        }
        return TURN_LEFT;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }


    @Override
    public void onRefreshClicked() {
    }

    @Override
    public void onChatButtonClicked() {

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("socailFeed", "onResume() called");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d("socailFeed", "onAttach() called");
    }

    @Override
    public void onPause() {
        super.onPause();
        //onSaveInstanceState(new Bundle());
        Log.d("socailFeed", "onPause() called");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //onSaveInstanceState(new Bundle());
        Log.d("socailFeed", "onDetach() called");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("socailFeed", "onStop() called");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("socailFeed", "onDestroyView() called");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("socailFeed", "onDestroy() called");
    }

    @Override
    public void onSocialItemClick(String fileType, int fileId) {
        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).openCommentFragment
                    (fileType, 0, fileId);
//            Bundle outState = new Bundle();
//            Gson gson = new Gson();
//            String json = gson.toJson(socialFeedParentArray);
//            outState.putString("listparent", json);
//
//            Gson gson1 = new Gson();
//            String json1 = gson1.toJson(socialFeedParentArray);
//            outState.putSerializable("listSocial", json1);
//            onSaveInstanceState(outState);
//
//            LinearLayoutManager myLayoutManager = (LinearLayoutManager) mRecyclerView.getRecyclerView().getLayoutManager();
//             scrollPos = myLayoutManager.findFirstVisibleItemPosition();
        }
    }
}
