package com.dojob.Additions;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RecommendedTalentSuperClass {

    @SerializedName("recommendTalent")
    List<RecommendedTalent> recommendedTalentList;
}
