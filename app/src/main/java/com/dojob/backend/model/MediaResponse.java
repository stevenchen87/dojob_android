package com.dojob.backend.model;

public class MediaResponse {
    public String thumbnail;
    public String url;
    public long id;
    public int like_status;
    public int like_count;

    public boolean isLocal; // used for profile setting page
}
