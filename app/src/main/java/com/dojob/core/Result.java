package com.dojob.core;

import android.os.Bundle;

public class Result {

    private Error mError;
    private Object mResult;
    private Bundle mArgs;
    private String mErrorMessage;
    private int mErrorId;

    public Result(Error error, Object result, Bundle args) {
        mError = error;
        mResult = result;
        if (args == null) {
            args = new Bundle();
        }
        mArgs = args;
    }

    public Result(Error error, Object object) {
        this(error, null, null);
        if (object instanceof Bundle) {
            mArgs = (Bundle) object;
        } else {
            if (error != null && object instanceof String) {
                mErrorMessage = (String) object;
            } else {
                mResult = object;
            }
        }
    }

    public Result(Error error) {
        this(error, null, null);
    }

    public static String getErrorMessage(Error error) {
        String message = "";
        switch (error) {
            case DATA:
                message = "Data problem";
                break;
            case NETWORK:
                message = "Connection problem";
                break;
            case OFFLINE:
                message = "No internet connection";
                break;
            case EMPTY:
                message = "App Key or User Id cannot be empty";
                break;
        }
        return message;
    }

    public boolean isSuccess() {
        return mError == null;
    }

    public Error getError() {
        return mError;
    }

    public String getErrorMessage() {
        if (mErrorMessage != null) {
            return mErrorMessage;
        }
        return getErrorMessage(mError);
    }

    public void setErrorMessage(String message) {
        mErrorMessage = message;
    }

    public Object getResult() {
        return mResult;
    }

    public Bundle getArgs() {
        return mArgs;
    }
}
