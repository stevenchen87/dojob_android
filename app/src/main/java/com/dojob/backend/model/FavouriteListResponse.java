package com.dojob.backend.model;

import java.util.ArrayList;

/**
 * Created by Admin on 30-Nov-18.
 */
public class FavouriteListResponse {

    public ArrayList<FavouriteItemResponse> favourite_data = new ArrayList<>();
    public int current_page;
    public int last_page;

}
