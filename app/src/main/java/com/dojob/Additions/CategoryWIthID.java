package com.dojob.Additions;

public class CategoryWIthID {

    private int ID;

    private String Title;

    public CategoryWIthID(int ID, String title) {
        this.ID = ID;
        Title = title;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }
}
