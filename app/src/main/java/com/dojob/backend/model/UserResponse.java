package com.dojob.backend.model;

import com.google.gson.annotations.SerializedName;

public class UserResponse {
    public String status;
    public String error;

    @SerializedName("userData")
    public ProfileResponse profile;
    @SerializedName("userSkill")
    public SkillResponse[] skill;
    public GalleryAlbumsItemResponse[] albumData;
    @SerializedName("photoAlbum")
    public GalleryAlbumsItemResponse[] albums;
    @SerializedName("videoAlbum")
    public GalleryAlbumsItemResponse[] videoAlbums;
    @SerializedName("talentPhoto")
    public MediaResponse[] talentPhoto;

    public MediaResponse[] talentVideo;

    // Recruiter
    @SerializedName("recommendTalent")
    public ProfileResponse[] recommended;

    /*@SerializedName("recommendCategory")
    public CategoryApi[] categoryRecommended;*/

    @SerializedName("recentTalent")
    public ProfileResponse[] viewed;


}
