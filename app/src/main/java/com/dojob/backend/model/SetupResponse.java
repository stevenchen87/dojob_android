package com.dojob.backend.model;

import com.dojob.Additions.CountryData;
import com.dojob.Additions.SubCategory;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SetupResponse extends UserResponse{
    @SerializedName("category")
    public HashMap<String,String> category;
    @SerializedName("jobSubCategory")
    public SubCategory[] subCategories;

    @SerializedName("countryData")
    public ArrayList<CountryData> countryDataArrayList;

    public Map<String, String> getCategory() {
        return category;
    }

    public void setCategory(HashMap<String, String> category) {
        this.category = category;
    }

    public SubCategory[] getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(SubCategory[] subCategories) {
        this.subCategories = subCategories;
    }

    public ArrayList<CountryData> getCountryDataArrayList() {
        return countryDataArrayList;
    }

    public void setCountryDataArrayList(ArrayList<CountryData> countryDataArrayList) {
        this.countryDataArrayList = countryDataArrayList;
    }
}
