package com.dojob.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.asksira.bsimagepicker.BSImagePicker;
import com.asksira.bsimagepicker.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.dojob.Additions.CategoryFragment;
import com.dojob.Additions.CountryFragment;
import com.dojob.Additions.PreSearchFragment;
import com.dojob.Additions.ProfileCategoryAdapter;
import com.dojob.Additions.Skill;
import com.dojob.Additions.SkillAddingActivity;
import com.dojob.Additions.SkillsAdapter;
import com.dojob.Additions.SubCategory;
import com.dojob.BuildConfig;
import com.dojob.R;
import com.dojob.activity.CategoryActivity;
import com.dojob.activity.CountryActivity;
import com.dojob.activity.IntroActivity;
import com.dojob.activity.MainActivity;
import com.dojob.adapter.PortfolioAdapter;
import com.dojob.backend.GalleryApi;
import com.dojob.backend.ProfileApi;
import com.dojob.backend.RegisterApi;
import com.dojob.backend.SetupApi;
import com.dojob.backend.model.MediaResponse;
import com.dojob.backend.model.ProfileResponse;
import com.dojob.backend.model.Review;
import com.dojob.backend.model.SetupResponse;
import com.dojob.backend.model.SkillResponse;
import com.dojob.backend.model.UserResponse;
import com.dojob.core.App;
import com.dojob.core.BackgroundWorker;
import com.dojob.core.Result;
import com.dojob.listener.CompressCallbacbkListener;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.dojob.Additions.CountryFragment.COUNTRY_NAME;
import static com.dojob.util.Utils.dpToPx;

public class ProfileSettingFragment extends Fragment implements View.OnClickListener, BackgroundWorker.Callbacks,
        BSImagePicker.OnSingleImageSelectedListener {
    public final static String PROFILE_SETTING_CATEGORY_CHOOSED = "profile_setting_category_choosed";
    public final static String PROFILE_SETTING_COUNTRY_CHOOSED = "profile_setting_country_choosed";
    private ImageView mSkillsAdd;
    private LinearLayout skills_add_layout;
    private AppCompatImageView mPreviewButton;
    private Listener mListener;
    private BackgroundWorker mWorker;
    private SetupApi mSetupApi;
    private CheckBox disclaimerCheckBox;
    private View saveButton, addVideo;
    private ImageView profilePhotoImage;
    private TextView textMessage;
    private AppCompatTextView addPhotoButton, addCoverButton;
    private EditText textName, textEmail, textSkill, textTalent, textVideoCaption;
    EditText category, countyEdt;
    BSImagePicker singleSelectionPicker, multiSelectionPicker, videoSelectionPicker;
    List<ImageView> servicePhotoView;
    List<Uri> serviceUri = new ArrayList<>();
    ;
    private List<Uri> videoUri = new ArrayList<>();
    List<String> categoryKeySet;
    List<EditText> skills;
    Uri profileUri;
    SetupResponse setupReponse;
    private AppCompatButton videoUpdateButton;
    private ImageView videoUploadedImageView;
    SetupResponse response;
    private ImageView coverPhoto;
    private LinearLayout coverPhotoChildContainer, videoChildContainer;
    private RecyclerView recyclerView;
    private SubCategory subCategory;
    private SkillsAdapter skillsAdapter;
    private List<Skill> skillsList;
    private List<Skill> skillsToRemove;
    private View skillContainer, talentContainer, videoContainer;

    private int SKILLS_ACTIVITY_CODE = 205;
    private int maxSoFar;
    private GalleryApi mGalleryApi;
    private HashMap<String, String> deletedFileId = new HashMap<>();

    private BroadcastReceiver categoryChoosedListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            getArguments().putAll(intent.getExtras());
            subCategory = getArguments().getParcelable(ProfileCategoryAdapter.SUB_CAT_KEY);
            category.setText(subCategory.name);
        }
    };

    private BroadcastReceiver countryChoosedListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (getArguments() != null) {
                getArguments().putAll(intent.getExtras());
                String countyname = getArguments().getString(COUNTRY_NAME);
                countyEdt.setText(countyname);
            }
        }
    };

    public static ProfileSettingFragment newInstance(Bundle args) {
        ProfileSettingFragment fragment = new ProfileSettingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (getActivity() != null) {
            mWorker = new BackgroundWorker(getActivity());
            mSetupApi = (SetupApi) mWorker.get(SetupApi.SETUP_TAG);
            if (mSetupApi == null) {
                mSetupApi = new SetupApi(getContext());
                mWorker.put(SetupApi.SETUP_TAG, mSetupApi);
            }
            mGalleryApi = (GalleryApi) mWorker.get(GalleryApi.GALLERY_TAG);
            if (mGalleryApi == null) {
                mGalleryApi = new GalleryApi(getContext());
                mWorker.put(GalleryApi.GALLERY_TAG, mGalleryApi);
            }
            ((MainActivity)getActivity()).isSettingPage(true);
            ((MainActivity)getActivity()).cameraButtonHide();
        }


        singleSelectionPicker = new BSImagePicker.Builder(BuildConfig.APPLICATION_ID + ".provider")
                .setMaximumDisplayingImages(Integer.MAX_VALUE) //Default: Integer.MAX_VALUE. Don't worry about performance :)
                .setTag("profile")
                .setSpanCount(3) //Default: 3. This is the number of columns
                .setGridSpacing(Utils.dp2px(2)) //Default: 2dp. Remember to pass in a value in pixel.
                .setPeekHeight(Utils.dp2px(360)) //Default: 360dp. This is the initial height of the dialog.
                .hideGalleryTile() //Default: show. Set this if you don't want to further let user select from a gallery app. In such case, I suggest you to set maximum     displaying    images to Integer.MAX_VALUE.
                .build();

        multiSelectionPicker = new BSImagePicker.Builder(BuildConfig.APPLICATION_ID + ".provider")
                .setMaximumDisplayingImages(Integer.MAX_VALUE) //Default: Integer.MAX_VALUE. Don't worry about performance :)
                .setTag("cover")
                .setSpanCount(3) //Default: 3. This is the number of columns
                .setGridSpacing(Utils.dp2px(2)) //Default: 2dp. Remember to pass in a value in pixel.
                .setPeekHeight(Utils.dp2px(360)) //Default: 360dp. This is the initial height of the dialog.
                .hideGalleryTile() //Default: show. Set this if you don't want to further let user select from a gallery app. In such case, I suggest you to set maximum     displaying    images to Integer.MAX_VALUE.
                .build();

        videoSelectionPicker = new BSImagePicker.Builder("com.dojob.fileprovider")
                .setMaximumDisplayingImages(120)
                .setTag("video")
                .hideCameraTile()
                .hideGalleryTile()
                .isLoadVideo(true)
                .setMultiSelectBarBgColor(android.R.color.white) //Default: #FFFFFF. You can also set it to a translucent color.
                .setMultiSelectTextColor(R.color.primary_text) //Default: #212121(Dark grey). This is the message in the multi-select bottom bar.
                .setMultiSelectDoneTextColor(R.color.green) //Default: #388e3c(Green). This is the color of the "Done" TextView.
                .setOverSelectTextColor(R.color.error_text) //Default: #b71c1c. This is the color of the message shown when user tries to select more than maximum select count.
                .disableOverSelectionMessage() //You can also decide not to show this over select message.
                .build();

        skillsList = new ArrayList<>();
        View view = inflater.inflate(R.layout.fragment_profile_setting, container, false);

        if (getArguments() != null && getArguments().containsKey(MainActivity.ENABLE_TOOLBAR_TITLE)) {
            AppCompatActivity activity = ((AppCompatActivity) getActivity());
            if (activity != null) {
//             will be used in fragment with back button
                ActionBar actionbar = activity.getSupportActionBar();
                if (actionbar != null) {
                    actionbar.setDisplayHomeAsUpEnabled(true);
                    actionbar.setDisplayShowHomeEnabled(true);
                }
                if (activity instanceof MainActivity) {
                    ((MainActivity) activity).setTitle("Profile Setting");
                }
            }
        }

        com.dojob.util.Utils.hideKeyBoard(getActivity());
        skillsToRemove = new ArrayList<>();
        skillsAdapter = new SkillsAdapter(getContext(), skillsList, this);
        recyclerView = view.findViewById(R.id.skills_recycler_view);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new PreSearchFragment.GridSpacingItemDecoration(1, dpToPx(5), true));
        recyclerView.setAdapter(skillsAdapter);
        skillContainer = view.findViewById(R.id.profile_setting_skills);
        talentContainer = view.findViewById(R.id.profile_setting_talent);
        videoContainer = view.findViewById(R.id.profile_setting_video);
        category = view.findViewById(R.id.profile_setting_category_spinner);
        category.setFocusableInTouchMode(false);
        if (getContext() != null && !ProfileApi.getRole(getContext()).equals("talent")) {
            skillContainer.setVisibility(View.GONE);
            talentContainer.setVisibility(View.GONE);
            videoContainer.setVisibility(View.GONE);
            category.setHint("Select "+getString(R.string.industry));
        } else {
            skillContainer.setVisibility(View.VISIBLE);
            talentContainer.setVisibility(View.VISIBLE);
            videoContainer.setVisibility(View.VISIBLE);

        }
        //adding on click listener to category




        countyEdt = view.findViewById(R.id.profile_setting_country_spinner);
        countyEdt.setFocusableInTouchMode(false);

        if (getArguments() != null && getArguments().containsKey(ProfileCategoryAdapter.SUB_CAT_KEY)) {
            subCategory = getArguments().getParcelable(ProfileCategoryAdapter.SUB_CAT_KEY);
            Log.i("SUB_CAT", subCategory.name + " " + subCategory.id);
            category.setText(subCategory.name);
        }

        if (getContext() != null) {
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), R.array.category, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            /* category.setAdapter(new HintSpinnerAdapter(adapter, R.layout.layout_hint_row_item ,getContext()));*/
        }
        /*  TableLayout skillsLayout = view.findViewById(R.id.layout_skill);*/
        /*skills = new ArrayList<EditText>();
        for( int i = 0; i < skillsLayout.getChildCount(); i++ )
            if( skillsLayout.getChildAt( i ) instanceof TableRow){
//                Log.d("ProfileSetting", "TableRow "+i);
                TableRow row = (TableRow) skillsLayout.getChildAt( i );
                for( int j = 0; j < row.getChildCount(); j++ ){
                    if( row.getChildAt( j ) instanceof LinearLayout){
                        LinearLayout ll = (LinearLayout) row.getChildAt( j );
//                        Log.d("ProfileSetting", "LinearLayout "+ll.getChildCount());
                        if (ll.findViewById(R.id.skills_name) instanceof EditText){
//                            Log.d("ProfileSetting", "EditText found");
                            skills.add((EditText)ll.findViewById(R.id.skills_name));
                        }
//                        skills.add();
                    }
                }
            }*/
//                myEditTextList.add( (EditText) myLayout.getChildAt( i ) );

        mSkillsAdd = view.findViewById(R.id.skills_add);
        skills_add_layout = view.findViewById(R.id.skills_add_layout);
        mPreviewButton = view.findViewById(R.id.disclaimer_preview_button);
        saveButton = view.findViewById(R.id.disclaimer_save_button);
        disclaimerCheckBox = view.findViewById(R.id.disclaimer_checkbox);
        textName = view.findViewById(R.id.basic_name);
        textEmail = view.findViewById(R.id.basic_email);
//        textSkill = view.findViewById(R.id.disclaimer_checkbox);
        textTalent = view.findViewById(R.id.services_caption);
        textVideoCaption = view.findViewById(R.id.video_caption);
        textMessage = view.findViewById(R.id.text_message);
        addPhotoButton = view.findViewById(R.id.photo_uploaded_button);
        addCoverButton = view.findViewById(R.id.cover_uploaded_button);
        profilePhotoImage = view.findViewById(R.id.profile_photo);
        servicePhotoView = new ArrayList<ImageView>();
        coverPhoto = view.findViewById(R.id.image_uploaded);
        coverPhotoChildContainer = view.findViewById(R.id.services_photo_container);
//        servicePhotoView.add(coverPhoto);
        videoUpdateButton = view.findViewById(R.id.video_uploaded_button);
        videoUploadedImageView = view.findViewById(R.id.video_uploaded);
        videoChildContainer = view.findViewById(R.id.video_child_container);

//        for(int i =0; i<5; i++){
//            ImageView img = (ImageView)view.findViewById(R.id.services_photo_1+i);
//            img.setTag(i);
//            servicePhotoView.add(img);
//        }
//        mSkillsAdd.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Log.i("SKILLS_ADD", "CLICKED");
//                Intent intent = new Intent(getActivity(), SkillAddingActivity.class);
//                startActivityForResult(intent, 205);
//            }
//        });
        skills_add_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("SKILLS_ADD", "CLICKED");
                Intent intent = new Intent(getActivity(), SkillAddingActivity.class);
                startActivityForResult(intent, 205);
            }
        });
        category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(CategoryFragment.CATEGORY_KEY, response.category);
                    bundle.putString(CategoryFragment.UB_CAT_KEY, App.gson().toJson(response.subCategories));
                    Intent intent = new Intent(getContext(), CategoryActivity.class);
                    intent.putExtras(bundle);
                    if (getContext() != null)
                        getContext().startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        countyEdt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(CountryFragment.COUNTRY_KEY, response.countryDataArrayList);
//                    bundle.putString(CategoryFragment.UB_CAT_KEY, App.gson().toJson(response.subCategories));
                    Intent intent = new Intent(getContext(), CountryActivity.class);
                    intent.putExtras(bundle);
                    if (getContext() != null)
                        getContext().startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        if (mWorker != null && getContext() != null)
            mWorker.executeNewTask(SetupApi.SETUP_GET_TAG, null, this);

        if (getContext() != null) {
            LocalBroadcastManager.getInstance(getContext()).registerReceiver(categoryChoosedListener, new IntentFilter(PROFILE_SETTING_CATEGORY_CHOOSED));
            LocalBroadcastManager.getInstance(getContext()).registerReceiver(countryChoosedListener, new IntentFilter(PROFILE_SETTING_COUNTRY_CHOOSED));
        }
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        mPreviewButton.setOnClickListener(this);
        saveButton.setOnClickListener(this);
        addPhotoButton.setOnClickListener(this);
        profilePhotoImage.setOnClickListener(this);
        addCoverButton.setOnClickListener(this);
        videoUpdateButton.setOnClickListener(this);
        coverPhoto.setOnClickListener(this);
        for (ImageView img : servicePhotoView) {
            img.setOnClickListener(this);
        }

        if (getActivity() != null && getActivity() instanceof MainActivity){
        ((MainActivity)getActivity()).cameraButtonHide();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        mPreviewButton.setOnClickListener(null);
        saveButton.setOnClickListener(null);
        addPhotoButton.setOnClickListener(null);
        addCoverButton.setOnClickListener(null);
        videoUpdateButton.setOnClickListener(null);
        profilePhotoImage.setOnClickListener(null);
        coverPhoto.setOnClickListener(null);
        for (ImageView img : servicePhotoView) {
            img.setOnClickListener(null);
        }

//        ((MainActivity)this.getActivity()).isSettingPage(false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getContext() != null) {
            LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(categoryChoosedListener);
            LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(countryChoosedListener);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 205) {
            Log.i("REQUEST", "OKAY");
            if (data != null) {
                Log.i("ARGUMENTS", "OKAY");
                String skill = data.getStringExtra(SkillAddingActivity.SKILL_NAME);
                if (skill != null) {
                    Log.i("SKILL", skill);
                    Log.i("SKILL", "OKAY");
                    skillsList.add(new Skill(0, skill));
                    skillsAdapter.notifyDataSetChanged();
                }
            }
        }
    }

    public void removeItem(Skill item, int type) {
        if (skillsList.contains(item))
            skillsList.remove(item);

        if (type == 1) {
            if (item != null)
                skillsToRemove.add(item);
        }

        /*skillsAdapter.notifyDataSetChanged();*/
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.photo_uploaded_button:
            case R.id.profile_photo:
                Log.d("ProfileSetting", "Single Photo Upload");
                singleSelectionPicker.show(getChildFragmentManager(), "picker");
                break;
            case R.id.cover_uploaded_button:
            case R.id.image_uploaded:
                Log.d("ProfileSetting", "Multi Photo Upload");
                multiSelectionPicker.show(getChildFragmentManager(), "picker");
                break;
            case R.id.skills_add:
                break;
            case R.id.disclaimer_preview_button:
                if (getActivity() != null) {
                    SetupResponse response = setupProfile();
                    ((MainActivity) getActivity()).onOpenPreviewPage(PortfolioAdapter.PREVIEW_TYPE, response);
                }
                break;
            case R.id.disclaimer_save_button:
                Log.d("Setting.save", "Checked:" + disclaimerCheckBox.isChecked());
                // if (disclaimerCheckBox.isChecked()) {
                textMessage.setVisibility(View.GONE);
                textMessage.setText("");
                if (response != null) {
                    if (mWorker != null) mWorker.executeNewTask(SetupApi.SETUP_TAG, null, this);

                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.wait_title), Toast.LENGTH_SHORT).show();
                }
//                } else {
//                    textMessage.setText("");
//                    showErrorMessage("Please read Privacy Policy");
//                }
                break;
            case R.id.video_uploaded_button:
                videoSelectionPicker.show(getChildFragmentManager(), "picker");
                break;
        }
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }

    public Result showErrorMessage(final String errorMessage) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textMessage.setText(textMessage.getText().toString() + errorMessage);
                textMessage.setVisibility(View.VISIBLE);
            }
        });

        return null;
    }

    @Override
    public Result executeTaskInBackground(String id, Bundle args) {
        switch (id) {
            case SetupApi.SETUP_GET_TAG:
                return mSetupApi.get();
            case GalleryApi.GALLERY_TAG:
                return mGalleryApi.deleteFiles(deletedFileId);
            case SetupApi.SETUP_TAG:
                // if (disclaimerCheckBox.isChecked()) {
                int error = 0;
                Map<String, String> data = new HashMap<String, String>();
                Map<String, Uri> fileData = new HashMap<String, Uri>();
                data.put("name", textName.getText().toString());
                data.put("country", countyEdt.getText().toString());

                if (subCategory != null) {
                    Log.i("SUB_CAT_PUT", subCategory.id + "");
                    try {
                        data.put("category", "" + subCategory.id);
                    } catch (RuntimeException e) {
                        e.printStackTrace();
                    }
                }
                if (category.getText().toString().isEmpty()) {
                    if (getContext() != null && ProfileApi.getRole(getContext()).equals("talent")) {
                        showErrorMessage("Please select category\n");
                    } else {
                        showErrorMessage("Please select industry\n");
                    }
                    error++;
                }

                if (textName.getText().toString().isEmpty()) {
                    showErrorMessage("Please fill up your name\n");
                    error++;
                }
                if (countyEdt.getText().toString().isEmpty()) {
                    showErrorMessage("Please select country\n");
                    error++;
                }

                if (profileUri != null) {
                    fileData.put("profileImage", profileUri);
                } else if (setupReponse.profile.profile_picture == null || setupReponse.profile.profile_picture.isEmpty()) {
                    StringBuilder temp = new StringBuilder();
                    temp.append("Profile Setting Fragment No Profile Image");
                    Log.d(temp.toString(), "" + setupReponse.profile.profile_picture);
                    showErrorMessage("Please upload you Profile Image \n");
                    error++;
                }
                    /*if(category.getSelectedItemPosition()>0){
                        data.put("category",""+categoryKeySet.get(category.getSelectedItemPosition()-1));
                    }
                    else{
                        showErrorMessage("Please choose your Talent Category\n");
                        error++;
                    }

                    Log.d("Setting.category", categoryKeySet.toString()+" , selected:"+category.getSelectedItemPosition());*/
                String skillSet = "";
                String deleteSet = "";
                if (getContext() != null && ProfileApi.getRole(getContext()).equals("talent")) {
                    for (int i = 0; i < skillsList.size(); i++) {
                        if (skillsList.get(i) != null) {
                            if (!skillSet.isEmpty()) {
                                skillSet += ",";
                            }
                            if (skillsList.get(i).getValue() != null) {
                                skillSet += "\"" + (i) + "\":\"" + skillsList.get(i).getValue() + "\"";
                            }
                        }
                    }

                    for (int i = 0; i < skillsToRemove.size(); i++) {
                        if (skillsToRemove.get(i) != null) {
                            if (!deleteSet.isEmpty()) {
                                deleteSet += ",";
                            }
                            if (skillsToRemove.get(i) != null) {
                                deleteSet += "\"" + (i + 1) + "\":\"" + skillsToRemove.get(i).getKey() + "\"";
                            }
                        }
                    }

                    if (skillSet.isEmpty()) {
                        showErrorMessage("Please add at least one skill\n");
                        error++;
                    }
                    data.put("skills", "{" + skillSet + "}");
                    Log.d("Setting.skills", "{" + skillSet + "}");
//
                    data.put("talentDescription", textTalent.getText().toString());
                    data.put("delete_skill_id", "{" + deleteSet + "}");


                    if (textTalent.getText().toString().isEmpty()) {
                        showErrorMessage("Please fill up your Talent/Services Caption\n");
                        error++;
                    }

//                    data.put("coverPhoto","");
//                    if(textVideoCaption.getText().toString().isEmpty()){
//                        showErrorMessage("Please fill up your Video Caption\n");
//                        error++;
//                    }
                    Log.d("Setting.data", data.toString());

                    okhttp3.MultipartBody.Builder multipartBodyBuilder = new okhttp3.MultipartBody.Builder();
                    multipartBodyBuilder.setType(okhttp3.MultipartBody.FORM);

                    for (Map.Entry<String, String> entry : data.entrySet()) {
                        if (!entry.getValue().isEmpty()) {
                            multipartBodyBuilder.addFormDataPart(entry.getKey(), entry.getValue());
                        }
                    }

                    if (coverPhotoChildContainer.getChildCount() == 0) {
                        showErrorMessage("Your profile must contain at least one Talent photo.\n");
                        error++;
                    }

                    if (serviceUri != null) {
                        //for(Uri talentUri: serviceUri){
                        for (int i = 0; i < serviceUri.size(); i++) {
                            Uri uri = serviceUri.get(i);
                            fileData.put("talentImage[" + i + "]", uri);
                            if (i == 0) {
                                data.put("coverPhoto", uri.getLastPathSegment());
                            }
                        }
                    }
                    if (videoUri != null) {
                        for (int i = 0; i < videoUri.size(); i++) {
                            Uri uri = videoUri.get(i);
                            fileData.put("talentVideo[" + i + "]", uri);
                        }
                        data.put("talentVideoCaption", textVideoCaption.getText().toString());
                    }
                }

                if (!disclaimerCheckBox.isChecked()) {
                    showErrorMessage("Please read Privacy Policy");
                    error++;
                }


                if (error > 0) {
                    return null;
                }
                return mSetupApi.post(data, fileData);
        }
//        }
        return null;
    }

    @Override
    public void onBackgroundTaskCompleted(String id, Result result) {
        switch (id) {
            case SetupApi.SETUP_GET_TAG:
                response = (SetupResponse) result.getResult();
                updateUI();
                break;
            case GalleryApi.GALLERY_TAG:
                if (result != null && result.isSuccess()) {
                    UserResponse userResponse = (UserResponse) result.getResult();
                    if (userResponse.status.equals("ok")) {
                        response.talentPhoto = userResponse.talentPhoto;
                        response.profile.featuredVideo = userResponse.talentVideo;
                        updateUI();
                    }
                }
                break;
            case SetupApi.SETUP_TAG:
                if (getActivity() != null && result != null && result.isSuccess()) {
                    response = (SetupResponse) result.getResult();
                    Log.d("SETUP POST.status", "" + response.status + "," + response.status.equals("ok"));
                    textMessage.setText("Profile Updated");
                    textMessage.setVisibility(View.VISIBLE);
                    SetupApi.saveUserData(getActivity(), response.profile);
                    PortfolioFragment.NeedNewData = 1;
                    com.dojob.util.Utils.isRefreshRequired = 1;
                    if (getActivity() != null) {
                        if (getActivity() instanceof MainActivity) {
                            ((MainActivity) getActivity()).refresh();
                            ((MainActivity) getActivity()).cameraButtonHide();
                        }
                        getActivity().getSupportFragmentManager().popBackStackImmediate();

                    }
                }
                break;
        }
    }

    private void updateUI() {
        if (response != null && getContext() != null) {
            if (getActivity() != null && !getActivity().isFinishing() && response.profile.name != null && !response.profile.name.isEmpty() && getUserVisibleHint()) {
                ((MainActivity) getActivity()).switchSignUpButton(RegisterApi.isLogin(getContext()));
            }
            setupReponse = response;
            if (getArguments() != null && !getArguments().containsKey(ProfileCategoryAdapter.SUB_CAT_KEY)) {
                Log.i("CONTAINS", "KEY");
                Log.i("RESPONSE_CATEGORY", response.profile.category + "");

                if (response.profile.category > 0) {
                    Log.i("CATEGORY_ID", "YES");
                    for (SubCategory subCategory : response.subCategories) {
                        if (subCategory.id == response.profile.category) {
                            Log.i("CATEGORY_ID", subCategory.name);
                            subCategory = subCategory;
                            category.setText(subCategory.name);
                        }
                    }
                }
            } else if (getArguments() != null && getArguments().containsKey(ProfileCategoryAdapter.SUB_CAT_KEY)) {
                subCategory = getArguments().getParcelable(ProfileCategoryAdapter.SUB_CAT_KEY);
                if (subCategory != null) category.setText(subCategory.name);
            } else {
                Log.i("CONTAINS", "NO_KEY");

            }

            countyEdt.setText(response.profile.country != null ? response.profile.country : "");
            textName.setText(response.profile.name);
            textEmail.setText(response.profile.email);
            textTalent.setText(response.profile.talent);
            if (response.profile.profile_picture != null)
                if (URLUtil.isNetworkUrl(response.profile.profile_picture)) {
                    Glide.with(getContext())
                            .load(response.profile.profile_picture)
                            .apply(RequestOptions.fitCenterTransform())
                            .into(profilePhotoImage);
                    setChangeProfileButton();

                } else {
                    Glide.with(getContext())
                            .load(Uri.fromFile(new File(response.profile.profile_picture)))
                            .apply(RequestOptions.fitCenterTransform())
                            .into(profilePhotoImage);
                    setChangeProfileButton();
                }
            /*Picasso.get().load(response.profile.profile_picture).fit().centerCrop().into(profilePhotoImage);*/
            if (response.profile.cover_photo != null && !response.profile.cover_photo.isEmpty() && servicePhotoView.size() > 0) {
                Picasso.get().load(response.profile.cover_photo).into(servicePhotoView.get(0));
            }

            maxSoFar = Integer.MIN_VALUE;
            if (response.skill != null) {
                skillsList.clear();
                Log.d("ProfileSetting", "response.skill " + (response.skill.length));
                if (response.skill != null)
                for (int i = 0; i < response.skill.length; i++) {
                    Log.i(response.skill[i].id + "", response.skill[i].skill);
                    skillsList.add(new Skill(response.skill[i].id, response.skill[i].skill));
                    if (response.skill[i].id > maxSoFar) {
                        maxSoFar = response.skill[i].id;
                    }
                    /*skills.get(i).setText(response.skill[i].skill);
                    skills.get(i).setTag(response.skill[i].id);*/
                    skillsAdapter.notifyDataSetChanged();
                }
            }
        }

        if (response != null && response.talentPhoto != null && response.talentPhoto.length > 0) {
            LinkedHashMap<MediaResponse, Uri> file = new LinkedHashMap<>();
            List<Uri> uriList = new ArrayList<>();
            for (MediaResponse photo : response.talentPhoto) {
                file.put(photo, Uri.parse(photo.url));
                uriList.add(Uri.parse(photo.url));
            }
            updateTalentPhoto(file);
        } else if (response != null) {
            response.talentPhoto = new MediaResponse[0];
            updateTalentPhoto(null);
        }

        if (response != null && response.profile.featuredVideo != null && response.profile.featuredVideo.length > 0) {
            LinkedHashMap<MediaResponse, Uri> file = new LinkedHashMap<>();
            for (MediaResponse video : response.profile.featuredVideo) {
                file.put(video, Uri.parse(video.thumbnail));
            }
            updateVideoUI(file);
        } else {
            response.profile.featuredVideo = new MediaResponse[0];
            updateVideoUI(null);
        }

        if (response.profile.isTutorialShown != null && !response.profile.isTutorialShown.equalsIgnoreCase("1")) {
            Intent intent1 = new Intent(getActivity(), IntroActivity.class);
            IntroActivity.isSetting = 0;
            getActivity().startActivity(intent1);
        }

        if (!response.profile.position.equals("talent")) {
            skillContainer.setVisibility(View.GONE);
            talentContainer.setVisibility(View.GONE);
            videoContainer.setVisibility(View.GONE);
            videoContainer.setVisibility(View.GONE);
            category.setHint("Select "+getString(R.string.industry));
        } else {
            skillContainer.setVisibility(View.VISIBLE);
            talentContainer.setVisibility(View.VISIBLE);
            videoContainer.setVisibility(View.VISIBLE);
        }
    }

    private Bitmap getBitmap(Uri uri) {
        return ThumbnailUtils.createVideoThumbnail(uri.getPath(), MediaStore.Video.Thumbnails.MINI_KIND);
    }

    private void updateVideoUI(HashMap<MediaResponse, Uri> uriList) {
        videoChildContainer.removeAllViews();
        if (uriList != null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            Iterator it = uriList.entrySet().iterator();
            int i = 0;
            while (it.hasNext()) {
                final Map.Entry pair = (Map.Entry) it.next();
                final Uri uri = (Uri) pair.getValue();
                final MediaResponse response = (MediaResponse) pair.getKey();
                View container = inflater.inflate(R.layout.layout_photo_default, null, false);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(Utils.dp2px(80), Utils.dp2px(80));
                params.setMargins(Utils.dp2px(4), Utils.dp2px(4), Utils.dp2px(4), Utils.dp2px(4));
                container.setLayoutParams(params);
                final ImageView image = container.findViewById(R.id.profile_setting_image_child);
                image.setTag(R.id.TAG_VIDEO_PREVIEW, response);
                View remove = container.findViewById(R.id.profile_setting_image_child_close);
                remove.setTag(i);
                remove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (!response.isLocal) {
                            deletedFileId.clear();
                            deletedFileId.put("0", String.valueOf(response.id));
                            if (mWorker != null && getContext() != null)
                                mWorker.executeNewTask(GalleryApi.GALLERY_TAG, null, ProfileSettingFragment.this);
                        } else {
                            videoChildContainer.removeViewAt((int) view.getTag());
                            videoUri.remove(uri);
                        }

                        if (videoChildContainer.getChildCount() == 0) {
                            videoUploadedImageView.setImageDrawable(null);
                            setAddVideoButton();
                        }
                    }
                });

                if (response.isLocal) {
                    Glide.with(this)
                            .asBitmap()
                            .load(getBitmap(uri))
                            .apply(RequestOptions.fitCenterTransform())
                            .into(image);
                    setChangeVideoButton();
                } else {
                    App.picasso().load(uri).fit().centerCrop().into(image);
                    setChangeVideoButton();
                }

                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (response.isLocal) {
                            Glide.with(getContext())
                                    .asBitmap()
                                    .load(getBitmap(uri))
                                    .apply(RequestOptions.fitCenterTransform())
                                    .into(videoUploadedImageView);
                            setChangeVideoButton();
                        } else {
                            App.picasso().load(uri).fit().centerCrop().into(videoUploadedImageView);
                            setChangeVideoButton();
                        }
                    }
                });
                videoChildContainer.addView(container);
                setChangeVideoButton();
                i++;
            }
            Uri firstUri = (Uri) uriList.values().toArray()[0];
            if (URLUtil.isNetworkUrl(firstUri.toString())) {
                App.picasso().load(firstUri).into(videoUploadedImageView);
                setChangeVideoButton();
            } else {
                Glide.with(this)
                        .asBitmap()
                        .load(getBitmap(firstUri))
                        .apply(RequestOptions.fitCenterTransform())
                        .into(videoUploadedImageView);
                setChangeVideoButton();
            }
        } else {
            videoUploadedImageView.setImageDrawable(null);
            setAddVideoButton();
        }
    }

    private void updateTalentPhoto(final HashMap<MediaResponse, Uri> uriList) {
        coverPhotoChildContainer.removeAllViews();
        if (uriList != null && getContext() != null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            Iterator it = uriList.entrySet().iterator();
            int i = 0;
            while (it.hasNext()) {
                final Map.Entry pair = (Map.Entry) it.next();
                final Uri uri = (Uri) pair.getValue();
                final MediaResponse response = (MediaResponse) pair.getKey();
                View container = inflater.inflate(R.layout.layout_photo_default, null, false);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(Utils.dp2px(80), Utils.dp2px(80));
                params.setMargins(Utils.dp2px(4), Utils.dp2px(4), Utils.dp2px(4), Utils.dp2px(4));
                container.setLayoutParams(params);
                ImageView image = container.findViewById(R.id.profile_setting_image_child);
                image.setTag(response);
                View remove = container.findViewById(R.id.profile_setting_image_child_close);
                remove.setTag(i);
                remove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (coverPhotoChildContainer.getChildCount() > 1) {
                            if (!response.isLocal) {
                                deletedFileId.clear();
                                deletedFileId.put("0", String.valueOf(response.id));
                                if (mWorker != null && getContext() != null)
                                    mWorker.executeNewTask(GalleryApi.GALLERY_TAG, null, ProfileSettingFragment.this);
                            } else {
                                uriList.remove(response);
                                serviceUri.remove(uri);
//                                coverPhotoChildContainer.removeViewAt((int)view.getTag());
                                updateTalentPhoto(uriList);

                            }
                        }

                        if (coverPhotoChildContainer.getChildCount() == 0) {
                            coverPhoto.setImageDrawable(null);
                            setAddCoverButton();
                        }
                    }
                });

                App.picasso().load(uri).fit().centerCrop().into(image);
                image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        App.picasso().load(uri).fit().centerCrop().into(coverPhoto);
                    }
                });
                coverPhotoChildContainer.addView(container);
                setChangeCoverButton();
                i++;
            }

            App.picasso().load((Uri) uriList.values().toArray()[0]).into(coverPhoto);
        } else {
            coverPhoto.setImageDrawable(null);
            setAddCoverButton();
        }
    }

    @Override
    public void onSingleImageSelected(Uri uri, String tag) {
        Log.d("ProfileSetting", "onSingleImageSelected" + uri != null ? uri.toString() : "");
        switch (tag) {
            case "video":
                com.dojob.util.Utils.compressVideo(getActivity(), uri, new CompressCallbacbkListener() {
                    @Override
                    public void onStart() {

                    }

                    @Override
                    public void onSuccess(Uri uri) {
                        videoUri.add(uri);
                        List<MediaResponse> list = new ArrayList<>(Arrays.asList(response.profile.featuredVideo));

                        for (Uri fileUri : videoUri) {
                            MediaResponse media = new MediaResponse();
                            media.id = new Date().getTime();
                            media.url = fileUri.getPath();
                            media.isLocal = true;
                            list.add(media);
                        }
//                            Bitmap bMap = ThumbnailUtils.createVideoThumbnail(uri.getPath() , MediaStore.Video.Thumbnails.MINI_KIND);
//                            videoUploadedImageView.setImageBitmap(bMap);

                        LinkedHashMap<MediaResponse, Uri> file = new LinkedHashMap<>();
                        for (MediaResponse photo : list) {
                            Uri path;
                            if (URLUtil.isNetworkUrl(photo.thumbnail)) {
                                path = (Uri.parse(photo.thumbnail));
                            } else {
                                path = (Uri.fromFile(new File(photo.url)));
                            }
                            file.put(photo, path);
                        }
                        updateVideoUI(file);
                    }

                    @Override
                    public void onError() {
                    }

                    @Override
                    public void onProgress(float percent) {
                    }

                });
                videoSelectionPicker.dismiss();
                break;
            case "cover":
                List<MediaResponse> list = new ArrayList<>(Arrays.asList(response.talentPhoto));
                serviceUri.add(uri);
                for (Uri fileUri : serviceUri) {
                    MediaResponse media = new MediaResponse();
                    media.id = new Date().getTime();
                    media.url = fileUri.getPath();
                    media.isLocal = true;
                    list.add(media);
                }

                LinkedHashMap<MediaResponse, Uri> file = new LinkedHashMap<>();
                for (MediaResponse photo : list) {
                    Uri path;
                    if (URLUtil.isNetworkUrl(photo.url)) {
                        path = (Uri.parse(photo.url));
                    } else {
                        path = (Uri.fromFile(new File(photo.url)));
                    }
                    file.put(photo, path);
                }
                updateTalentPhoto(file);

                multiSelectionPicker.dismiss();
                break;
            default:
                App.picasso().load(uri).fit().centerCrop().into(profilePhotoImage);
//                    profilePhotoImage.setImageDrawable(Drawable.createFromStream(getActivity().getContentResolver().openInputStream(uri),
//                            null));
                profileUri = uri;
                setChangeProfileButton();
                singleSelectionPicker.dismiss();
                break;
        }
    }

    public interface Listener {
        void onOpenPreviewProfilePage(int type, UserResponse response);
    }

    private SetupResponse setupProfile() {
        ProfileResponse profile = new ProfileResponse();
        profile.id = setupReponse.profile.id;
        profile.name = textName.getText().toString();
        profile.email = setupReponse.profile.email;
        if (setupReponse.profile.category > 0) {
            profile.category = setupReponse.profile.category;
            profile.category_info = setupReponse.profile.category_info;
        } else if (subCategory != null) {
            profile.category = subCategory.category_id;
            if (profile.category_info != null)
                if (profile.category_info.sub_cat != null)
                    if (profile.category_info.sub_cat.name != null)
                        profile.category_info.sub_cat.name = subCategory.name;

        }

        profile.review = new Review[0];
        profile.position = ProfileApi.getRole(getContext());
        profile.profile_picture = (setupReponse.profile.profile_picture != null) ? setupReponse.profile.profile_picture : (profileUri != null) ? profileUri.getPath() : "";
        profile.talent = textTalent.getText().toString();
        response.profile = profile;
        ArrayList<SkillResponse> skillsResponse = new ArrayList<>();
        if (getContext() != null && ProfileApi.getRole(getContext()).equals("talent")) {
            if (skillsList != null) {
                for (int i = 0; i < skillsList.size(); i++) {
                    if (!skillsList.get(i).getValue().isEmpty()) {
                        SkillResponse skill = new SkillResponse();
                        skill.id = i;
                        skill.skill = skillsList.get(i).getValue();
                        skillsResponse.add(skill);
                    }
                }
            }
        }
        response.skill = skillsResponse.toArray(new SkillResponse[skillsResponse.size()]);
//        ArrayList<GalleryAlbumsItemResponse> albums = new ArrayList<>();
//        ArrayList<GalleryAlbumsItemResponse> videoAlbums = new ArrayList<>();
        ArrayList<MediaResponse> talentPhoto = new ArrayList<>();
        for (int i = 0; i < coverPhotoChildContainer.getChildCount(); i++) {
            View view = coverPhotoChildContainer.getChildAt(i);
            ImageView image = view.findViewById(R.id.profile_setting_image_child);
            MediaResponse response = (MediaResponse) image.getTag();
            talentPhoto.add(response);
        }
        response.talentPhoto = talentPhoto.toArray(new MediaResponse[talentPhoto.size()]);

        ArrayList<MediaResponse> talentVideo = new ArrayList<>();
        for (int i = 0; i < videoChildContainer.getChildCount(); i++) {
            View view = videoChildContainer.getChildAt(i);
            ImageView image = view.findViewById(R.id.profile_setting_image_child);
            MediaResponse response = (MediaResponse) image.getTag(R.id.TAG_VIDEO_PREVIEW);
            talentVideo.add(response);
            setChangeVideoButton();
        }
//        if(videoUri != null){
//            for(int i = 0; i< videoUri.size(); i++){
//                MediaResponse videoLink = new MediaResponse();
//                Uri uri = videoUri.get(i);
//                videoLink.id = i;
//                videoLink.url = uri.getPath();
//                talentVideo.add(videoLink);
//            }
//        }

        response.profile.featuredVideo = talentVideo.toArray(new MediaResponse[talentVideo.size()]);
        // Recruiter
        ArrayList<ProfileResponse> recommended = new ArrayList<>();
        ArrayList<ProfileResponse> viewed = new ArrayList<>();


        return response;
    }

    private void setChangeProfileButton() {
        addPhotoButton.setText("Change Photo");
    }

    private void setChangeCoverButton() {
        addCoverButton.setText("Add Photo");
    }

    private void setAddCoverButton() {
        addCoverButton.setText("Add Photo");
    }

    private void setChangeVideoButton() {
        videoUpdateButton.setText("Add Video");
    }

    private void setAddVideoButton() {
        videoUpdateButton.setText("Add Video");
    }


}
