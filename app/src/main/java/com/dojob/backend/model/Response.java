package com.dojob.backend.model;

import com.google.gson.annotations.SerializedName;

public class Response {
    public String status;
    public String msg;

    //talent
    @SerializedName("next_page_url")
    public String nextPageUrl;
    @SerializedName("current_page")
    public int currentPage;
    @SerializedName("last_page")
    public int lastPage;
}
