package com.dojob.adapter.model;

import android.animation.ObjectAnimator;
import android.text.util.Linkify;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import com.dojob.R;

public class PortfolioDescription {
    public static final int LAYOUT_ID = R.layout.layout_portfolio_description;
    private final String mContent;
    private boolean expand = true;

    public PortfolioDescription(String content) {
        mContent = content;
    }

    public void bindViewHolder(final ViewHolder holder, int position) {
        if (holder.description != null) {
            holder.description.setText(mContent);
            Linkify.addLinks(holder.description, Linkify.ALL);

            holder.description.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    if(expand) {
                        expand = false;
                        if (holder.description.getLineCount() > 4) {
                            holder.seeMore.setVisibility(View.VISIBLE);
                            ObjectAnimator animation = ObjectAnimator.ofInt(holder.description, "maxLines", 5);
                            animation.setDuration(0).start();
                        }
                    }
                    holder.description.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });

            holder.seeMore.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    if (!expand) {
                        expand = true;
                        ObjectAnimator animation = ObjectAnimator.ofInt(holder.description, "maxLines", holder.description.getLineCount());
                        animation.setDuration(100).start();
                        holder.seeMore.setText("hide");
                    } else {
                        expand = false;
                        ObjectAnimator animation = ObjectAnimator.ofInt(holder.description, "maxLines", 5);
                        animation.setDuration(100).start();
                        holder.seeMore.setText("read more");
                    }
                }
            });

        }
    }

    public static class ViewHolder {
        public TextView description, seeMore;

        public ViewHolder(View view) {
            description = view.findViewById(R.id.portfolio_description);
            seeMore = view.findViewById(R.id.portfolio_description_more);
        }
    }
}
