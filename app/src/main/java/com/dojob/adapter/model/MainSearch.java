package com.dojob.adapter.model;

import android.view.View;
import android.widget.EditText;

import com.dojob.R;

public class MainSearch {
    public static final int LAYOUT_ID = R.layout.layout_search_bar;

    public MainSearch() {
    }

    public void bindViewHolder(ViewHolder holder, int position){

    }

    public static class ViewHolder{

        public EditText container;

        public ViewHolder(View view){
            container = view.findViewById(R.id.search_bar);
        }
    }
}
