package com.dojob.Additions;

import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.dojob.R;
import com.dojob.activity.MainActivity;
import com.dojob.fragment.FeedFragment;
import com.dojob.fragment.SearchFragment;
import com.github.ybq.android.spinkit.SpinKitView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Callback;
import retrofit2.Response;

public class PreSearchFragment extends Fragment {

    private Unbinder unbinder;

    private String SEARCH_PAGE_FRAGMENT_TAG = "search_page_fragment";

    @BindView(R.id.search_bar)
    EditText editText;

    @BindView(R.id.items_recycle_view)
    RecyclerView itemsRecycleView;

    @BindView(R.id.category_recycle_view)
    RecyclerView categoryRecycleView;

    @BindView(R.id.progress)
    SpinKitView spinKitView;

    private CategoryAdapter categoryAdapter;

    ItemsAdapter itemsAdapter;

    List<RecommendedTalent> recommendedTalentList;

    List<CategoryWIthID> categoryArrayList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_pre_search, container, false);

        unbinder = ButterKnife.bind(this,rootView);

        recommendedTalentList = new ArrayList<>();
        categoryArrayList =  new ArrayList<>();
        itemsAdapter = new ItemsAdapter(getContext(),recommendedTalentList,getActivity());
        categoryAdapter = new CategoryAdapter(getContext(),categoryArrayList,getActivity());

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);

        itemsRecycleView.setLayoutManager(layoutManager);

        itemsRecycleView.setItemAnimator(new DefaultItemAnimator());
        itemsRecycleView.setHasFixedSize(true);

        itemsRecycleView.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(5), true));

        itemsRecycleView.setAdapter(itemsAdapter);

        itemsRecycleView.setNestedScrollingEnabled(false);

        LinearLayoutManager verticalLayoutManager
                = new LinearLayoutManager(getContext());

        categoryRecycleView.setLayoutManager(verticalLayoutManager);

        categoryRecycleView.setItemAnimator(new DefaultItemAnimator());

        categoryRecycleView.addItemDecoration(new FeedFragment.GridSpacingItemDecoration(1, dpToPx(5), true));

        categoryRecycleView.setAdapter(categoryAdapter);

        categoryRecycleView.setNestedScrollingEnabled(false);

        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {

                if(getActivity() != null){
                    ((MainActivity)getActivity()).searchOnlySecond( editText.getText().toString());
                }
//                SearchFragment searchFragment = new SearchFragment();
//                    Bundle bundle = new Bundle();
//                    bundle.putString(SearchFragment.QUERY, editText.getText().toString());
//                    searchFragment.setArguments(bundle);
//                    getActivity().getSupportFragmentManager().beginTransaction()
//                            .replace(R.id.content,searchFragment)
//                            .addToBackStack(null)
//                            .commit();
                return false;
            }
        });

        getItems();

        getCategories();

        return rootView;
    }
    private void getItems(){
        ServiceGenerator serviceGenerator = new ServiceGenerator();
        ApiInterface apiInterface = serviceGenerator.createService(ApiInterface.class);
        retrofit2.Call<RecommendedTalentSuperClass> call = apiInterface.getRecommendedTalent();
        call.enqueue(new Callback<RecommendedTalentSuperClass>() {
            @Override
            public void onResponse(retrofit2.Call<RecommendedTalentSuperClass> call, Response<RecommendedTalentSuperClass> response) {
                if (response.isSuccessful()){
                    Log.i("RESPONSE","OKAY");
                    if (response.body()!=null)
                    recommendedTalentList.addAll(response.body().recommendedTalentList);
                    if (recommendedTalentList.size()>0){
                        if (spinKitView!=null)
                            spinKitView.setVisibility(View.GONE);
                    }
                    itemsAdapter.notifyDataSetChanged();
                }else {
                    Log.i("RESPONSE",response.message());
                }
            }

            @Override
            public void onFailure(retrofit2.Call<RecommendedTalentSuperClass> call, Throwable t) {
                Log.i("RESPONSE",t.getMessage());
            }
        });


    }



    private void getCategories(){
        ServiceGenerator serviceGenerator = new ServiceGenerator();
        ApiInterface apiInterface = serviceGenerator.createService(ApiInterface.class);
        retrofit2.Call<categoryModel> call = apiInterface.getcategories();
        call.enqueue(new Callback<categoryModel>() {
            @Override
            public void onResponse(retrofit2.Call<categoryModel> call, Response<categoryModel> response) {
                if (response.isSuccessful()){
                    Log.i("NRESPONSE","OKAY");
                    if(response.body()!=null)
                    Log.i("NRESPONSE",response.body().getKeyValueArray().get("1"));
                    Iterator it = response.body().getKeyValueArray().entrySet().iterator();
                    while(it.hasNext()){
                        Map.Entry pair = (Map.Entry) it.next();
                        categoryArrayList.add(new CategoryWIthID(Integer.valueOf(pair.getKey().toString()), (String)pair.getValue()));
                    }
                    Collections.sort(categoryArrayList, new Comparator<CategoryWIthID>() {
                        @Override
                        public int compare(CategoryWIthID t1, CategoryWIthID t2) {
                            String s1 = t1.getTitle();
                            String s2 = t2.getTitle();
                            return s1.compareToIgnoreCase(s2);
                        }

                    });
                    categoryAdapter.notifyDataSetChanged();
                }else {
                    Log.i("NRESPONSE",response.message());
                }
            }

            @Override
            public void onFailure(retrofit2.Call<categoryModel> call, Throwable t) {
                Log.i("NRESPONSE",t.getMessage());
            }
        });


    }

    public static class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }
        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)
                /*if (position < spanCount) { // top edge*/
                outRect.top = spacing;
                /*   }*/
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }
    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
