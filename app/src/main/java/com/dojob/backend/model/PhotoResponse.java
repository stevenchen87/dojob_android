package com.dojob.backend.model;

public class PhotoResponse {
    public int id;
    public String url;
    public String cover_flag;
}
