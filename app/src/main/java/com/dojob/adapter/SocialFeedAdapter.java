package com.dojob.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.dojob.Additions.local.SocialFeed;
import com.dojob.Additions.local.SocialFeedParent;
import com.dojob.GlideApp;
import com.dojob.R;

import java.util.ArrayList;
import java.util.Random;


public class SocialFeedAdapter extends RecyclerView.Adapter<SocialFeedAdapter.ViewHolder>{
   // private int[] mDataSet;
    private ArrayList<SocialFeed> mDataSet;
    private Context mContext;
    private Random mRandom = new Random();

    public SocialFeedAdapter(Context context, ArrayList<SocialFeed> mDataSet){
        this.mDataSet = mDataSet;
        mContext = context;
    }

    public void resetAdapterData(ArrayList<SocialFeed> mDataSet){
        this.mDataSet = mDataSet;
        notifyDataSetChanged();
    }

    public void updateAdapterData(ArrayList<SocialFeed> mDataSet){
        this.mDataSet.addAll(mDataSet);
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView mTextView;
        public ImageView image;
        public LinearLayout video_indicator;
        public ViewHolder(View v){
            super(v);
//            mTextView = (TextView)v.findViewById(R.id.tv);
            image = (ImageView) v.findViewById(R.id.image);
            video_indicator = (LinearLayout) v.findViewById(R.id.portfolio_cover_play);
        }
    }

    @Override
    public SocialFeedAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        // Create a new View
        View v = LayoutInflater.from(mContext).inflate(R.layout.row_item_social,parent,false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position){
//        StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) holder.itemView.getLayoutParams();
//        int spanIndex  = layoutParams.getSpanIndex();

        SocialFeed socialFeed = mDataSet.get(position);
        //holder.mTextView.setText(mDataSet[position]);
        // Set a random height for TextView
//        holder.mTextView.getLayoutParams().height = getRandomIntInRange(280,280);
       // holder.image.getLayoutParams().width = getRandomIntInRange(280,100);

//        if (position %2 == 0 && spanIndex == 0){
//            holder.image.getLayoutParams().height = 400;
//        }else{
//            holder.image.getLayoutParams().height = 200;
//        }
        // Set a random color for TextView background
       // holder.mTextView.setBackgroundColor(getRandomHSVColor());
       // holder.image.setImageResource(mDataSet[position]);
        if (socialFeed.getFileType().equalsIgnoreCase("image")){
            GlideApp.with(mContext)
                    .load(socialFeed.getUrl())
                    .placeholder(R.drawable.placeholder_1)
                    .apply(RequestOptions.fitCenterTransform())
                    .skipMemoryCache(false)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.image);
            holder.video_indicator.setVisibility(View.GONE);
        }else{
            GlideApp.with(mContext)
                    .load(socialFeed.getThumbnail())
                    .placeholder(R.drawable.placeholder_1)
                    .apply(RequestOptions.fitCenterTransform())
                    .skipMemoryCache(false)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.image);
            holder.video_indicator.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount(){
        return mDataSet.size();
    }

    // Custom method to get a random number between a range
    protected int getRandomIntInRange(int max, int min){
        return mRandom.nextInt((max-min)+min)+min;
    }

    // Custom method to generate random HSV color
    protected int getRandomHSVColor(){
        // Generate a random hue value between 0 to 360
        int hue = mRandom.nextInt(361);
        // We make the color depth full
        float saturation = 1.0f;
        // We make a full bright color
        float value = 1.0f;
        // We avoid color transparency
        int alpha = 255;
        // Finally, generate the color
        int color = Color.HSVToColor(alpha, new float[]{hue, saturation, value});
        // Return the color
        return color;
    }
}