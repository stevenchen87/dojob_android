package com.dojob.fragment;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.http.SslCertificate;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.dojob.R;
import com.dojob.util.Utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

public class ImageViewerFragment extends Fragment implements
        View.OnClickListener{

    public final static String IMAGE_URL = "image_url";
    private static final String TAG = "ImageViewerFragment";
    private WebView mWebView;
    private String mImageUrl;
    private ImageView mCloseButton;

    public static ImageViewerFragment newInstance(Bundle args) {
        ImageViewerFragment fragment = new ImageViewerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            mImageUrl = args.getString(IMAGE_URL);
            mImageUrl = mImageUrl.replace("https","http");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image_viewer, container, false);
        Utils.hideKeyBoard(getActivity());
        mWebView = view.findViewById(R.id.web);
        mCloseButton = view.findViewById(R.id.close);
        WebSettings settings = mWebView.getSettings();
        mWebView.setBackgroundColor(Color.TRANSPARENT);
        mWebView.setVerticalScrollBarEnabled(false);
        settings.setBuiltInZoomControls(true);
        settings.setDisplayZoomControls(false);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        initImageViwer();
        mWebView.setWebViewClient(new MyWebViewClient());
        mWebView.loadUrl(mImageUrl);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mCloseButton.setOnClickListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mCloseButton.setOnClickListener(null);
    }


    private void initImageViwer() {
        String html = "<html><body><img src=\"" + mImageUrl + "\" width=\"100%\" height=\"100%\"\"/><span class=\"aligner\"></span></body></html>";
        mWebView.loadDataWithBaseURL(null, "<style>img{display: inline-block;height: auto;max-width: 100%;vertical-align:middle;}.aligner{display:inline-block;vertical-align:middle;height:100%;}body, html{height:100%;background: transparent;}</style>" + html, "text/html", "UTF-8", null);
    }



    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.close) {
            getActivity().finish();
        }
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            mWebView.loadUrl(url);
            return true;
        }

//        @Override
//        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
//            super.onReceivedSslError(view, handler, error);
//            handler.proceed();
//        }
    }


//    public static Certificate convertSSLCertificateToCertificate(SslCertificate sslCertificate) {
//        CertificateFactory cf = null;
//        Certificate certificate = null;
//        Bundle bundle = sslCertificate.saveState(sslCertificate);
//        byte[] bytes = bundle.getByteArray("x509-certificate");
//
//        if (bytes != null) {
//            try {
//                CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
//                Certificate cert = certFactory.generateCertificate(new ByteArrayInputStream(bytes));
//                certificate = cert;
//            } catch (CertificateException e) {
//                Log.e(TAG, "exception", e);
//            }
//        }
//
//        return certificate;
//    }
//
//    public static Certificate getCertificateForRawResource(int resourceId, Context context) {
//        CertificateFactory cf = null;
//        Certificate ca = null;
//        Resources resources = context.getResources();
//        InputStream caInput = resources.openRawResource(resourceId);
//
//        try {
//            cf = CertificateFactory.getInstance("X.509");
//            ca = cf.generateCertificate(caInput);
//        } catch (CertificateException e) {
//            Log.e(TAG, "exception", e);
//        } finally {
//            try {
//                caInput.close();
//            } catch (IOException e) {
//                Log.e(TAG, "exception", e);
//            }
//        }
//
//        return ca;
//    }
}
