package com.dojob.adapter.model;

import android.content.Context;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.dojob.GlideApp;
import com.dojob.R;
import com.dojob.backend.SetupApi;
import com.dojob.backend.model.ProfileResponse;
import com.dojob.backend.model.UserResponse;
import com.dojob.core.App;

public class CommentUserItem {
    public static final int LAYOUT_ID = R.layout.layout_comment_user;
    private Context mContext;


    public CommentUserItem(Context context){
        mContext = context;
    }

    public void bindViewHolder(ViewHolder holder, int position){
        if(holder.image != null){
            ProfileResponse response = SetupApi.getUserData(mContext);
            if (response!=null)
            GlideApp.with(mContext)
                    .load(response.profile_picture)
                    .placeholder(R.drawable.profile_header_x3)
                    .apply(RequestOptions.fitCenterTransform())
                    .skipMemoryCache(false)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.image);
            /*if(response != null) App.picasso().load(response.profile.profile_picture).fit().centerCrop().into(holder.image);*/
        }
        if(holder.msg != null){
            holder.msg.setTag(position);
        }
    }

    public static class ViewHolder{
        public ImageView image;
        public EditText msg;

        public ViewHolder(View view){
            image = view.findViewById(R.id.comment_user_image);
            msg = view.findViewById(R.id.comment_user_message);
        }
    }
}
