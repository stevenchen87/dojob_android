package com.dojob.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.dojob.R;
import com.dojob.activity.MainActivity;
import com.dojob.adapter.MainAdapter;
import com.dojob.backend.LoginApi;
import com.dojob.backend.RegisterApi;
import com.dojob.backend.SetupApi;
import com.dojob.util.AppSP;
import com.dojob.util.ResourcesUtil;
import com.dojob.util.Utils;
import com.dojob.view.CustomViewPager;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import q.rorbin.badgeview.Badge;
import q.rorbin.badgeview.QBadgeView;

public class MainFragment extends Fragment implements BottomNavigationView.OnNavigationItemSelectedListener {

    private View rootView;
    private CustomViewPager mViewPager;
    BottomNavigationViewEx mButtonNavigationView;
    private MainAdapter viewPagerAdapter;
    private Listener mListener;
    private Badge mBadge, mBadeSocail;
    private boolean mIsBadgeHide;

    public static MainFragment newInstance() {
        Bundle args = new Bundle();
        MainFragment fragment = new MainFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        if (rootView == null) {
            //first time creating this fragment view
            rootView = inflater.inflate(R.layout.fragment_main, container, false);

            //Initialization
            //TODO:
        } else {
            //not first time creating this fragment view
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (parent != null) {
                parent.removeView(rootView);
            }
        }
        if (Utils.isRefreshRequired == 1) {
            rootView = inflater.inflate(R.layout.fragment_main, container, false);
            viewPagerAdapter = null;
            mViewPager = null;
        }
//        AppCompatActivity activity = ((AppCompatActivity) getActivity());
//        if (activity != null) {
//            // will be used in fragment with back button
//            ActionBar actionbar = activity.getSupportActionBar();
//            if (actionbar != null) {
//                actionbar.setHomeAsUpIndicator(ResourcesUtil.getDrawable(R.drawable.baseline_navigate_before_black_24));
//                actionbar.setDisplayHomeAsUpEnabled(false);
//                actionbar.setDisplayShowHomeEnabled(false);
//            }
//            rootView = view;
//            mButtonNavigationView = view.findViewById(R.id.bottom_view_public);
//            if (RegisterApi.isLogin(activity)) {
//                mButtonNavigationView = view.findViewById(R.id.bottom_view);
//            }
//            mButtonNavigationView.setVisibility(View.VISIBLE);
//            mButtonNavigationView.enableAnimation(false);
//            mButtonNavigationView.enableShiftingMode(false);
//            mButtonNavigationView.enableItemShiftingMode(false);
//            mButtonNavigationView.setTextVisibility(false);
//
//            mViewPager = view.findViewById(R.id.view_pager);
//            mViewPager.setPagingEnabled(false);
//            mViewPager.setOffscreenPageLimit(5);
//            viewPagerAdapter = new MainAdapter(activity, activity.getSupportFragmentManager());
//            mViewPager.setAdapter(viewPagerAdapter);
//            mButtonNavigationView.setupWithViewPager(mViewPager);
//
//        }

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Utils.hideKeyBoard(getActivity());
        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        if (activity != null) {
            // will be used in fragment with back button
            ActionBar actionbar = activity.getSupportActionBar();
            if (actionbar != null) {
                actionbar.setHomeAsUpIndicator(ResourcesUtil.getDrawable(R.drawable.baseline_navigate_before_black_24));
                actionbar.setDisplayHomeAsUpEnabled(false);
                actionbar.setDisplayShowHomeEnabled(false);
            }
            rootView = view;
            mButtonNavigationView = view.findViewById(R.id.bottom_view_public);
            if (RegisterApi.isLogin(activity)) {
                mButtonNavigationView = view.findViewById(R.id.bottom_view);
            }
            mButtonNavigationView.setVisibility(View.VISIBLE);
            mViewPager = view.findViewById(R.id.view_pager);
            mButtonNavigationView.enableAnimation(false);
            mButtonNavigationView.enableShiftingMode(false);
            mButtonNavigationView.enableItemShiftingMode(false);
            mButtonNavigationView.setTextVisibility(false);


            mViewPager.setPagingEnabled(false);

            if (viewPagerAdapter == null) {
                mViewPager.setOffscreenPageLimit(5);
                viewPagerAdapter = new MainAdapter(activity, activity.getSupportFragmentManager());
                mViewPager.setAdapter(viewPagerAdapter);
            } else {
                if (Utils.isRefreshRequired == 1) {
                    mViewPager.setOffscreenPageLimit(5);
                    viewPagerAdapter.updateData();
                    viewPagerAdapter.notifyDataSetChanged();
                }
            }

            Utils.isRefreshRequired = 0;

            mButtonNavigationView.setupWithViewPager(mViewPager);
            AppSP sp = AppSP.getInstance(this.getActivity());
            if (!sp.readBool(Utils.ISSOCIALFEED_OPENED,false)){
                setSocailFeedBackge();
            }

        }
    }

    private void updateAdapter() {
        int TIME = 1000; //5000 ms (5 Seconds)

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (viewPagerAdapter != null) {
                    viewPagerAdapter.notifyDataSetChanged();
                }

            }
        }, TIME);
    }

    public void setCurrentItem(final int position) {
        mViewPager.post(new Runnable() {
            @Override
            public void run() {
                mViewPager.setCurrentItem(position);
            }
        });
    }

    public void setMessageBadge() {
        if (mBadge == null) {
            mBadge = new QBadgeView(getContext())
                    .setBadgeNumber(-1)
                    .setBadgeGravity(Gravity.TOP | Gravity.END)
                    .setGravityOffset(30, 4, true)
                    .bindTarget(mButtonNavigationView.getBottomNavigationItemView(1))
                    .setBadgeBackgroundColor(ResourcesUtil.getColor(R.color.yellow))
                    .setOnDragStateChangedListener(new Badge.OnDragStateChangedListener() {
                        @Override
                        public void onDragStateChanged(int dragState, Badge badge, View targetView) {
                        }
                    });
        }
    }

    public void setSocailFeedBackge() {
            if (RegisterApi.isLogin(getActivity())) {
                mBadeSocail = new QBadgeView(getContext())
                        .setBadgeText("New")
                        .setBadgeTextSize(7,true)
                        .setBadgeGravity(Gravity.TOP | Gravity.END)
                        .setGravityOffset(7, 5, true)
                        .bindTarget(mButtonNavigationView.getBottomNavigationItemView(2))
                        .setBadgeBackgroundColor(ResourcesUtil.getColor(R.color.yellow))
                        .setOnDragStateChangedListener(null);
            } else {
                mBadeSocail = new QBadgeView(getContext())
                        .setBadgeText("New")
                        .setBadgeTextSize(7,true)
                        .setBadgeGravity(Gravity.TOP | Gravity.END)
                        .setGravityOffset(40
                                , 5, true)
                        .bindTarget(mButtonNavigationView.getBottomNavigationItemView(1))
                        .setBadgeBackgroundColor(ResourcesUtil.getColor(R.color.yellow))
                        .setOnDragStateChangedListener(null);
            }
    }

    @Override
    public void onResume() {
        super.onResume();
        mButtonNavigationView.setOnNavigationItemSelectedListener(this);
        setToolbarTitle(mViewPager.getCurrentItem());
        if (RegisterApi.isLogin(getActivity())) {
            if (getActivity() != null && SetupApi.getUserData(getActivity()).setup_profile == 0) {
                ((MainActivity) getActivity()).onOpenProfileSetting();
            }
        }
        if (mButtonNavigationView != null){
           int itemAccount =  mButtonNavigationView.getItemCount();
           int selectItem = mButtonNavigationView.getCurrentItem();
           if (itemAccount == 5){
               if (selectItem == 0 || selectItem == 2){
                   ((MainActivity)getActivity()).cameraButtonActive();
               }else{
                   ((MainActivity)getActivity()).cameraButtonHide();
               }
           }else{
               ((MainActivity)getActivity()).cameraButtonActive();
           }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mBadge = null;
        mButtonNavigationView.setOnNavigationItemSelectedListener(null);

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        setToolbarTitle(item.getOrder());
        return true;
    }

    private void setToolbarTitle(int position) {
        AppCompatActivity activity = ((AppCompatActivity) getActivity());
        if (activity != null) {
            ActionBar actionbar = activity.getSupportActionBar();
            if (actionbar != null) {
                actionbar.setDisplayHomeAsUpEnabled(false);
                actionbar.setDisplayShowHomeEnabled(false);
                actionbar.setDisplayShowTitleEnabled(false);
            }
            if (activity instanceof MainActivity) {
                ((MainActivity) activity).isSettingPage(false);
                switch (position) {
                    case MainAdapter.FEED_PAGE:
                        viewPagerAdapter.getItem(0);
                        ((MainActivity) activity).setIcon();
                        ((MainActivity) activity).setBasicToolbar();
                        ((MainActivity) activity).cameraButtonActive();
                        break;
                    case MainAdapter.CHAT_PAGE:

                        if (RegisterApi.isLogin(getContext())) {
                            ((MainActivity) activity).setTitle("Inbox", true);
                            ((MainActivity) activity).setInboxToolbar();
                            if (mBadge != null) {
                                mBadge.hide(false);
                            }
                        } else {
                            ((MainActivity) activity).setTitle("Social Feed", true);
                            ((MainActivity) activity).isSettingPage(true);
                        }
                        ((MainActivity) activity).cameraButtonHide();
                        break;
                    case MainAdapter.SOCIAL_PAGE:
                        ((MainActivity) activity).setTitle("Social Feed", true);
                        if(mBadeSocail != null){
                            mBadeSocail.hide(true);
                            AppSP sp = AppSP.getInstance(this.getActivity());
                            sp.savePreferences(Utils.ISSOCIALFEED_OPENED,true);
                        }

                        ((MainActivity) activity).cameraButtonActive();
                        break;
                    case MainAdapter.EDIT_PROFILE_PAGE:
                        ((MainActivity) activity).setTitle("Profile Settings", true);
                        ((MainActivity) activity).setBasicToolbar();
                        ((MainActivity) activity).cameraButtonHide();
                        break;
                    case MainAdapter.SETTINGS_PAGE:
                        ((MainActivity) activity).setTitle("Settings", true);
                        ((MainActivity) activity).isSettingPage(true);
                        ((MainActivity) activity).cameraButtonHide();
                        break;

                    default:
                        ((MainActivity) activity).setIcon();
                }
            }
        }
    }

    public int getSelectItem(){
        if (mButtonNavigationView != null){
            return mButtonNavigationView.getSelectedItemId();
        }
        return 0;
    }


    public interface Listener {
        void setCurrentItem(int position);
    }
}
