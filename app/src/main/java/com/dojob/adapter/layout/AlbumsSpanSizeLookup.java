package com.dojob.adapter.layout;

import android.support.v7.widget.GridLayoutManager;

import com.dojob.adapter.AlbumsAdapter;
import com.dojob.adapter.model.AlbumsCover;
import com.dojob.adapter.model.AlbumsItem;
import com.dojob.adapter.model.AlbumsTitle;
import com.dojob.adapter.model.PhotosItem;

public class AlbumsSpanSizeLookup extends GridLayoutManager.SpanSizeLookup {

    private AlbumsAdapter mAdapter;

    public AlbumsSpanSizeLookup(AlbumsAdapter adapter) {
        mAdapter = adapter;
    }

    @Override
    public int getSpanSize(int position) {
        Object item = mAdapter.getItem(position);
        if(item instanceof AlbumsItem){
            return 3;
        }
        else if(item instanceof AlbumsCover || item instanceof AlbumsTitle){
            return 6;
        }
        else if(item instanceof PhotosItem){
            return 2;
        }
        return 6;
    }
}
