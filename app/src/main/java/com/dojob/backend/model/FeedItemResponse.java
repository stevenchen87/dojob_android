package com.dojob.backend.model;

import com.google.gson.annotations.SerializedName;

public class FeedItemResponse {
    public String name;

    @SerializedName("role")
    public String position;
    @SerializedName("category")
    public String category_name;
    @SerializedName("profile_image")
    public String image;
    public String cover_photo;
    public CategoryInfo category_info;
    public String url;

    @SerializedName("country_alpha3")
    public String countrycode;

    public class CategoryInfo{
        public ProfileResponse.Category sub_cat;
        public ProfileResponse.Category cat;
    }
    public class Category{
        public int id;
        public String name;
        public int category_id;
        public String translation_key;
    }
}
