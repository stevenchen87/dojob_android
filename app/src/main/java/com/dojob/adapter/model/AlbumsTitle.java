package com.dojob.adapter.model;

import android.view.View;
import android.widget.TextView;

import com.dojob.R;

import java.util.Locale;

public class AlbumsTitle {
    public static final int LAYOUT_ID = R.layout.layout_albums_title;

    private String mTitle;
    private String mTimeCreated;

    public AlbumsTitle(String title, String timeCreated) {
        mTimeCreated = timeCreated;
        mTitle = title;
    }

    public void bindViewHolder(ViewHolder holder, int position){
        if(holder.timeCreated != null){
            holder.timeCreated.setText(String.format(Locale.US, "Created %s", mTimeCreated));
        }
        if(holder.title != null){
            holder.title.setText(mTitle);
        }
    }

    public static class ViewHolder{
        public TextView timeCreated;
        public TextView title;

        public ViewHolder(View view){
            timeCreated = view.findViewById(R.id.albums_date_created);
            title = view.findViewById(R.id.albums_title);
        }
    }
}
