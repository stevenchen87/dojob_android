package com.dojob.adapter.layout;

import android.support.v7.widget.GridLayoutManager;

import com.dojob.adapter.PortfolioAdapter;
import com.dojob.adapter.model.PortfolioSkillsItem;

public class PortfolioSpanSizeLookup extends GridLayoutManager.SpanSizeLookup {
    private PortfolioAdapter mAdapter;

    public PortfolioSpanSizeLookup(PortfolioAdapter adapter) {
        mAdapter = adapter;
    }

    @Override
    public int getSpanSize(int position) {
        Object item = mAdapter.getItem(position);
        if(item instanceof PortfolioSkillsItem){
            return 1;
        }
        return 2;
    }
}
